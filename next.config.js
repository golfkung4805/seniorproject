const { i18n } = require('./next-i18next.config');

module.exports = {
  // future: {
  //   webpack5: true,
  // },
  i18n,
  env: {
    BASE_URL: process.env.BASE_URL,
    API_URL: process.env.API_URL,
    FACEBOOK_APP_ID: process.env.FACEBOOK_APP_ID,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    OMISE_PUBLIC_KEY: process.env.OMISE_PUBLIC_KEY,
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
  }
};