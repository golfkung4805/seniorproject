-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2021 at 12:00 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `x63011270020`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `CID` varchar(7) NOT NULL,
  `CName` varchar(50) NOT NULL,
  `CIcon` varchar(100) DEFAULT 'fa-concierge-bell',
  `CStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`CID`, `CName`, `CIcon`, `CStatus`) VALUES
('C-00001', 'อาหารจานเดียว', 'fa-drumstick-bite', 1),
('C-00002', 'เครื่องดื่ม', 'fa-cocktail', 1),
('C-00003', 'อาหารทานเล่น', 'fa-bread-slice', 1),
('C-00004', 'เครื่องดื่มอื่นๆ', 'fa-glass-whiskey', 1),
('C-00005', 'ของหวาน', 'fa-candy-cane', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category_ingredients`
--

CREATE TABLE `tb_category_ingredients` (
  `CIID` varchar(8) NOT NULL,
  `CID` varchar(7) NOT NULL,
  `CIName` varchar(50) NOT NULL,
  `CIMultiple` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category_ingredients`
--

INSERT INTO `tb_category_ingredients` (`CIID`, `CID`, `CIName`, `CIMultiple`) VALUES
('CI-00001', 'C-00001', 'เนื้อสัตว์', 1),
('CI-00002', 'C-00001', 'ท็อปปิ้ง', NULL),
('CI-00003', 'C-00001', 'ขนาด', NULL),
('CI-00004', 'C-00002', 'ท็อปปิ้ง', 1),
('CI-00005', 'C-00002', 'ขนาด', NULL),
('CI-00006', 'C-00002', 'แบบ', NULL),
('CI-00007', 'C-00003', 'ขนาด', NULL),
('CI-00008', 'C-00004', 'อื่นๆ', NULL),
('CI-00009', 'C-00005', 'อื่นๆ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category_ingredients_option`
--

CREATE TABLE `tb_category_ingredients_option` (
  `CIOID` varchar(9) NOT NULL,
  `CIID` varchar(8) NOT NULL,
  `CIOName` varchar(50) NOT NULL,
  `CIOPrice` float(13,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category_ingredients_option`
--

INSERT INTO `tb_category_ingredients_option` (`CIOID`, `CIID`, `CIOName`, `CIOPrice`) VALUES
('CIO-00001', 'CI-00001', 'หมูชื้น', 5.00),
('CIO-00002', 'CI-00001', 'หมูสับ', 5.00),
('CIO-00003', 'CI-00001', 'ไก่', 5.00),
('CIO-00004', 'CI-00001', 'หมูกรอบ', 10.00),
('CIO-00005', 'CI-00001', 'กุ้ง', 10.00),
('CIO-00006', 'CI-00001', 'ทะเล', 15.00),
('CIO-00007', 'CI-00002', '-', 0.00),
('CIO-00008', 'CI-00002', 'ไข่ดาว', 7.00),
('CIO-00009', 'CI-00002', 'ไข่เจียว', 10.00),
('CIO-00010', 'CI-00003', 'ธรรมดา', 0.00),
('CIO-00011', 'CI-00003', 'พิเศษ', 10.00),
('CIO-00012', 'CI-00005', 'เล็ก', 5.00),
('CIO-00013', 'CI-00005', 'กลาง', 7.00),
('CIO-00014', 'CI-00005', 'ใหญ่', 10.00),
('CIO-00015', 'CI-00006', 'ร้อน', 0.00),
('CIO-00016', 'CI-00006', 'เย็น', 0.00),
('CIO-00017', 'CI-00006', 'ปั่น', 5.00),
('CIO-00018', 'CI-00004', '-', 0.00),
('CIO-00019', 'CI-00004', 'วิปปิ้งครีม', 10.00),
('CIO-00020', 'CI-00004', 'ไข่มุก', 5.00),
('CIO-00021', 'CI-00004', 'วุ้นฟรุตสลัด', 5.00),
('CIO-00022', 'CI-00007', 'ชุดเล็ก', 0.00),
('CIO-00023', 'CI-00007', 'ชุดกลาง', 5.00),
('CIO-00024', 'CI-00007', 'ชุดใหญ่', 9.00),
('CIO-00025', 'CI-00009', '-', 0.00),
('CIO-00026', 'CI-00008', '-', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `CusID` varchar(50) NOT NULL,
  `CusUsername` varchar(100) DEFAULT '-',
  `CusPasswd` varchar(100) DEFAULT '-',
  `CusGender` varchar(50) DEFAULT ' ',
  `CusFirstname` varchar(100) NOT NULL,
  `CusLastname` varchar(100) NOT NULL,
  `CusNickname` varchar(50) NOT NULL,
  `CusTel` varchar(20) DEFAULT '-',
  `CusEmail` varchar(100) NOT NULL,
  `CusPoint` float(13,2) NOT NULL DEFAULT 0.00,
  `CusStatus` tinyint(1) NOT NULL DEFAULT 1,
  `CusPicture` varchar(150) DEFAULT NULL,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer_voucher`
--

CREATE TABLE `tb_customer_voucher` (
  `_ID` int(11) NOT NULL,
  `CusID` varchar(50) NOT NULL,
  `VoucID` varchar(7) NOT NULL,
  `VoucHisUsePoint` float NOT NULL,
  `VoucHisStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_customer_voucher`
--

INSERT INTO `tb_customer_voucher` (`_ID`, `CusID`, `VoucID`, `VoucHisUsePoint`, `VoucHisStatus`, `createdAt`) VALUES
(1, '3701411473417614', 'V-00002', 1, 1, '2021-10-28 14:31:00'),
(2, '3701411473417614', 'V-00001', 1, 0, '2021-10-28 14:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employees`
--

CREATE TABLE `tb_employees` (
  `EmpID` varchar(9) NOT NULL,
  `EmpUsername` varchar(100) NOT NULL,
  `EmpPasswd` varchar(100) NOT NULL,
  `EmpGender` varchar(50) NOT NULL,
  `EmpFirstname` varchar(100) NOT NULL,
  `EmpLastname` varchar(100) NOT NULL,
  `EmpNickname` varchar(50) NOT NULL,
  `EmpTel` varchar(20) NOT NULL,
  `EmpEmail` varchar(100) NOT NULL,
  `EmpRole` enum('admin','counter','kitchen','') NOT NULL DEFAULT 'counter',
  `EmpStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_employees`
--

INSERT INTO `tb_employees` (`EmpID`, `EmpUsername`, `EmpPasswd`, `EmpGender`, `EmpFirstname`, `EmpLastname`, `EmpNickname`, `EmpTel`, `EmpEmail`, `EmpRole`, `EmpStatus`, `createdAt`, `updatedAt`) VALUES
('Emp-00000', 'admin', '$2a$10$2muqedpFzYKEv3oe5BQL8.qQKWqmrnGFJDF/f3hlQxKytVCdXHFjS', 'นาย', 'เจ้าของร้าน', '-', 'ผู้ดูแลระบบ', '0', 'admin@admin.com', 'admin', 1, '2021-09-20 17:13:08', '2021-10-27 16:51:06'),
('Emp-00001', 'counter', '$2a$10$PujIB3d8tuld5ZNGf1CQ1eNkX0Cd.ZcoAUxEpwYB3agA5k9SriU8q', 'นาย', 'พนักงาน', 'เคาน์เตอร์', 'ฝ่ายเคาน์เตอร์', '2', 'counter@counter.com', 'counter', 1, '2021-10-20 17:07:16', '2021-10-27 16:51:20'),
('Emp-00002', 'kitchen', '$2a$10$JcezS9iyTgsbV7gSbC1Dz.DRPMWQJXm6F2JJoD.aUJ/C.HYWkjHI2', 'นาย', 'พนักงาน', 'ครัว', 'ฝ่ายครัว', '3', 'kitchen@kitchen.com', 'kitchen', 1, '2021-10-27 12:54:40', '2021-10-27 16:51:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `ODID` varchar(100) NOT NULL,
  `TableID` varchar(12) NOT NULL,
  `CusID` varchar(255) NOT NULL,
  `EmpID` varchar(9) DEFAULT NULL,
  `VoucCode` varchar(255) DEFAULT NULL,
  `PayID` varchar(25) DEFAULT NULL,
  `ODPayDetail` text DEFAULT NULL COMMENT 'Object from Webhooks',
  `ODStatus` enum('openOrder','successful','failed','overdue','paid','unpaid') NOT NULL DEFAULT 'openOrder',
  `ODTotal` float NOT NULL DEFAULT 0,
  `ODTax` float NOT NULL DEFAULT 0,
  `VoucDiscount` float NOT NULL DEFAULT 0,
  `ODTotalPoint` float NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `_ID` int(11) NOT NULL,
  `ODID` varchar(100) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `PDName` varchar(50) NOT NULL,
  `ODSizeList` text NOT NULL COMMENT '[พิเศษ,หมู,ไข่ดาว]',
  `ODSizeListDisplay` text NOT NULL,
  `ODPriceList` varchar(255) NOT NULL COMMENT '[5,5,7]',
  `PDPrice` float NOT NULL,
  `ODQty` int(11) NOT NULL,
  `ODTotalPrice` float NOT NULL COMMENT '(PDPrice + ODPriceList)*ODQty',
  `ODTotalTax` float NOT NULL,
  `ODDesc` text NOT NULL,
  `ODStatus` enum('complete','cancel','waitQueue') NOT NULL DEFAULT 'waitQueue',
  `ODReview` decimal(3,1) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp(),
  `Ref` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_method`
--

CREATE TABLE `tb_payment_method` (
  `PayID` varchar(25) NOT NULL,
  `PayName` varchar(50) NOT NULL,
  `PayStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_payment_method`
--

INSERT INTO `tb_payment_method` (`PayID`, `PayName`, `PayStatus`) VALUES
('checkout_counter', 'Checkout Counter', 1),
('credit_card', 'Credit Card', 1),
('internet_banking', 'Internet Banking', 1),
('rabbit_linepay', 'Rabbit Linepay', 1),
('truemoney', 'Truemoney', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `PDID` varchar(8) NOT NULL,
  `PDName` varchar(50) NOT NULL,
  `PDDesc` text NOT NULL,
  `PDPrice` float(13,2) NOT NULL,
  `PDRating` decimal(3,2) NOT NULL DEFAULT 0.00,
  `PDStatus` tinyint(1) NOT NULL DEFAULT 1,
  `CID` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`PDID`, `PDName`, `PDDesc`, `PDPrice`, `PDRating`, `PDStatus`, `CID`) VALUES
('PD-00001', 'กะเพรา', '-', 30.00, '0.00', 1, 'C-00001'),
('PD-00002', 'ผัดมาม่า', '-', 30.00, '0.00', 1, 'C-00001'),
('PD-00003', 'ข้าวผัด', '-', 30.00, '0.00', 1, 'C-00001'),
('PD-00004', 'ข้าวไข่เจียว', '-', 30.00, '0.00', 1, 'C-00001'),
('PD-00005', 'โกโก้', '-', 30.00, '0.00', 1, 'C-00002'),
('PD-00006', 'ชาเขียว', '-', 35.00, '0.00', 1, 'C-00002'),
('PD-00007', 'มอคค่า', '-', 35.00, '0.00', 1, 'C-00002'),
('PD-00008', 'ปิงซูมัทฉะ', '-', 139.00, '0.00', 1, 'C-00005'),
('PD-00009', 'ไดฟูกุ', 'ไดฟูกุสตอเบอรรี่ 6 ชิ้น', 149.00, '0.00', 1, 'C-00005'),
('PD-00010', 'เค้กไอศกรีมโอรีโอ', '-', 69.00, '0.00', 1, 'C-00005'),
('PD-00011', 'ฮันนี่โทส', '-', 59.00, '0.00', 1, 'C-00005'),
('PD-00012', 'เกี๊ยวซ่า', '-', 40.00, '0.00', 1, 'C-00003'),
('PD-00013', 'นักเกต', 'นักเกตไก่', 40.00, '0.00', 1, 'C-00003'),
('PD-00014', 'เฟรนฟราย', '-', 40.00, '0.00', 1, 'C-00003'),
('PD-00015', 'โค้ก', 'โค้ก ขนาด 500ml', 12.00, '0.00', 0, 'C-00004'),
('PD-00016', 'น้ำเปล่า', 'น้ำเปล่า ขนาด 700 ml', 7.00, '0.00', 1, 'C-00004'),
('PD-00017', 'คะน้า', '-', 30.00, '0.00', 1, 'C-00001'),
('PD-00018', 'ผัดพริกแกง', '-', 30.00, '0.00', 1, 'C-00001');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_image`
--

CREATE TABLE `tb_product_image` (
  `_ID` int(7) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `PDImage` varchar(100) NOT NULL,
  `PDImageIndex` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sequence`
--

CREATE TABLE `tb_sequence` (
  `_ID` int(7) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `LastID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sequence`
--

INSERT INTO `tb_sequence` (`_ID`, `tableName`, `LastID`) VALUES
(1, 'TbProduct', 20),
(2, 'TbCategory', 9),
(3, 'TbCategoryIngredients', 15),
(4, 'TbCategoryIngredientsOption', 38),
(5, 'TbTable', 7),
(6, 'TbVoucher', 4),
(7, 'TbEmployees', 6),
(8, 'TbCustomer', 2),
(9, 'TbOrder', 27);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sysconfig`
--

CREATE TABLE `tb_sysconfig` (
  `_ID` int(11) NOT NULL,
  `SysID` varchar(9) NOT NULL,
  `SysName` varchar(100) NOT NULL,
  `SysLogo` varchar(100) NOT NULL,
  `SysOpen` time NOT NULL,
  `SysClose` time NOT NULL,
  `SysPointType` varchar(50) DEFAULT NULL,
  `SysPointCondition` varchar(100) NOT NULL COMMENT '100/1 ทุกๆ100บาทจะได้รับ1Point',
  `SysKitchenOpen` time NOT NULL,
  `SysKitchenClose` time NOT NULL,
  `SysStatus` decimal(1,0) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sysconfig`
--

INSERT INTO `tb_sysconfig` (`_ID`, `SysID`, `SysName`, `SysLogo`, `SysOpen`, `SysClose`, `SysPointType`, `SysPointCondition`, `SysKitchenOpen`, `SysKitchenClose`, `SysStatus`) VALUES
(1, 'SYS-0001', 'SR', '-', '08:00:00', '22:00:00', NULL, '100/1', '08:00:00', '22:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_table`
--

CREATE TABLE `tb_table` (
  `TableID` varchar(12) NOT NULL,
  `TableName` varchar(50) NOT NULL,
  `TableQR` varchar(100) NOT NULL,
  `TableStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_table`
--

INSERT INTO `tb_table` (`TableID`, `TableName`, `TableQR`, `TableStatus`) VALUES
('TableID-0001', 'โต๊ะ 1', 'TableID-0001.png', 1),
('TableID-0002', 'โต๊ะ 2', 'TableID-0002.png', 1),
('TableID-0003', 'โต๊ะ 3', 'TableID-0003.png', 1),
('TableID-0004', 'โต๊ะ 4', 'TableID-0004.png', 1),
('TableID-0005', 'โต๊ะ 5', 'TableID-0005.png', 1),
('TableID-0006', 'โต๊ะ 6', 'TableID-0006.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_voucher`
--

CREATE TABLE `tb_voucher` (
  `VoucID` varchar(7) NOT NULL,
  `VoucCode` varchar(255) NOT NULL,
  `VoucName` text NOT NULL,
  `VoucUsePoint` float(13,2) NOT NULL DEFAULT 0.00,
  `VoucDiscount` float(13,2) NOT NULL,
  `VoucType` varchar(11) NOT NULL,
  `VoucCondition` float(13,2) NOT NULL DEFAULT 0.00,
  `VoucStart` datetime NOT NULL,
  `VoucEnd` datetime NOT NULL,
  `VoucQuota` int(11) NOT NULL,
  `VoucUse` int(11) NOT NULL DEFAULT 0,
  `VoucStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_voucher`
--

INSERT INTO `tb_voucher` (`VoucID`, `VoucCode`, `VoucName`, `VoucUsePoint`, `VoucDiscount`, `VoucType`, `VoucCondition`, `VoucStart`, `VoucEnd`, `VoucQuota`, `VoucUse`, `VoucStatus`, `createdAt`, `updatedAt`) VALUES
('V-00001', '7v9FBgLLPZ', 'คูปองส่วนลด 50 บาท สั่งซื้อขั้นต่ำ 1 บาท', 1.00, 50.00, '฿', 1.00, '2021-10-01 00:33:37', '2021-10-31 00:33:37', 100, 1, 1, '2021-10-28 00:33:41', '2021-10-28 00:33:41'),
('V-00002', 'divVweNXLc', 'คูปองส่วนลด 10% สั่งซื้อขั้นต่ำ 100 บาท', 1.00, 10.00, '%', 100.00, '2021-10-01 00:34:08', '2021-10-31 00:34:08', 100, 1, 1, '2021-10-28 00:34:14', '2021-10-28 00:34:14'),
('V-00003', 'rIkUqH4nFU', 'คูปองส่วนลด 100 บาท สั่งซื้อขั้นต่ำ 200 บาท', 50.00, 100.00, '฿', 200.00, '2021-10-01 11:43:19', '2022-10-31 11:43:19', 100, 0, 1, '2021-10-29 11:43:25', '2021-10-29 11:43:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `tb_category_ingredients`
--
ALTER TABLE `tb_category_ingredients`
  ADD PRIMARY KEY (`CIID`),
  ADD KEY `TbCategoryIngredients_CID_FK` (`CID`);

--
-- Indexes for table `tb_category_ingredients_option`
--
ALTER TABLE `tb_category_ingredients_option`
  ADD PRIMARY KEY (`CIOID`),
  ADD KEY `TbCategoryIngredientsDetail_CIID_FK` (`CIID`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`CusID`),
  ADD UNIQUE KEY `EmpUsername` (`CusUsername`);

--
-- Indexes for table `tb_customer_voucher`
--
ALTER TABLE `tb_customer_voucher`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `CusVouc_CusID_FK` (`CusID`),
  ADD KEY `CusVouc_VoucID_FK` (`VoucID`);

--
-- Indexes for table `tb_employees`
--
ALTER TABLE `tb_employees`
  ADD PRIMARY KEY (`EmpID`),
  ADD UNIQUE KEY `EmpUsername` (`EmpUsername`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`ODID`),
  ADD KEY `TbOder_TableID_FK` (`TableID`),
  ADD KEY `TbOder_EmpID_FK` (`EmpID`),
  ADD KEY `TbOder_PayID_FK` (`PayID`),
  ADD KEY `TbOder_VoucCode_FK` (`VoucCode`);

--
-- Indexes for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbOderDetail_ODID_FK` (`ODID`),
  ADD KEY `TbOderDetail_PDID_FK` (`PDID`);

--
-- Indexes for table `tb_payment_method`
--
ALTER TABLE `tb_payment_method`
  ADD PRIMARY KEY (`PayID`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`PDID`),
  ADD KEY `TbProduct_CID_FK` (`CID`);

--
-- Indexes for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbProductImage_PDID_FK` (`PDID`);

--
-- Indexes for table `tb_sequence`
--
ALTER TABLE `tb_sequence`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `tb_sysconfig`
--
ALTER TABLE `tb_sysconfig`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `tb_table`
--
ALTER TABLE `tb_table`
  ADD PRIMARY KEY (`TableID`);

--
-- Indexes for table `tb_voucher`
--
ALTER TABLE `tb_voucher`
  ADD PRIMARY KEY (`VoucID`),
  ADD UNIQUE KEY `VoucCode` (`VoucCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_customer_voucher`
--
ALTER TABLE `tb_customer_voucher`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  MODIFY `_ID` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_sequence`
--
ALTER TABLE `tb_sequence`
  MODIFY `_ID` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_sysconfig`
--
ALTER TABLE `tb_sysconfig`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_category_ingredients`
--
ALTER TABLE `tb_category_ingredients`
  ADD CONSTRAINT `TbCategoryIngredients_CID_FK` FOREIGN KEY (`CID`) REFERENCES `tb_category` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_category_ingredients_option`
--
ALTER TABLE `tb_category_ingredients_option`
  ADD CONSTRAINT `TbCategoryIngredientsDetail_CIID_FK` FOREIGN KEY (`CIID`) REFERENCES `tb_category_ingredients` (`CIID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD CONSTRAINT `TbOder_EmpID_FK` FOREIGN KEY (`EmpID`) REFERENCES `tb_employees` (`EmpID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_PayID_FK` FOREIGN KEY (`PayID`) REFERENCES `tb_payment_method` (`PayID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_TableID_FK` FOREIGN KEY (`TableID`) REFERENCES `tb_table` (`TableID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_VoucCode_FK` FOREIGN KEY (`VoucCode`) REFERENCES `tb_voucher` (`VoucCode`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD CONSTRAINT `TbOderDetail_ODID_FK` FOREIGN KEY (`ODID`) REFERENCES `tb_order` (`ODID`),
  ADD CONSTRAINT `TbOderDetail_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD CONSTRAINT `TbProduct_CID_FK` FOREIGN KEY (`CID`) REFERENCES `tb_category` (`CID`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD CONSTRAINT `TbProductImage_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
