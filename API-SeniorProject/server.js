import _ from 'lodash'
import { createServer } from "http";
import express from 'express';
import cors from 'cors';
import logger from 'morgan';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import { Server } from 'socket.io';

import dashboardRouter from './routers/dashboardRouter.js';
import productRouter from './routers/productRouter.js';
import categoryRouter from './routers/categoryRouter.js';
import tableRouter from './routers/tableRouter.js';
import paymentMethodRouter from './routers/paymentMethodRouter.js';
import voucherRouter from './routers/voucherRouter.js';
import employeesRouter from './routers/employeesRouter.js';
import customerRouter from './routers/customerRouter.js';
import customerVoucherRouter from './routers/customerVoucherRouter.js';
import orderRouter from './routers/orderRouter.js';
import settingRouter from './routers/settingRouter.js';

dotenv.config();

const port = process.env.PORT || 4000;

const app = express();
app.use(cors({
  origin: '*',
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/v1/dashboard', dashboardRouter);
app.use('/v1/product', productRouter);
app.use('/v1/category', categoryRouter);
app.use('/v1/table', tableRouter);
app.use('/v1/paymentmethod', paymentMethodRouter);
app.use('/v1/voucher', voucherRouter);
app.use('/v1/employee', employeesRouter);
app.use('/v1/customer', customerRouter);
app.use('/v1/customer/voucher', customerVoucherRouter);
app.use('/v1/order', orderRouter);
app.use('/v1/setting', settingRouter);

// app.get('/', (req, res) => {
//   res.send('Hello World!');
// })

const httpServer = createServer(app);

let allUserList = []
app.get('/', (req, res) => {
  res.json({allUserList});
})
const io = new Server(httpServer, {
  cors: { origin: process.env.BASE_URL }
});

io.on("connection", (socket) => {
  console.log(`connection ${socket.id}`)

  // socket.on('fixUseEffect2ndOnline', (socketID) => {
  //   allUserList = allUserList.filter((x)=> x.socket !== socketID)
  //   console.table(allUserList)
  // })

  socket.on('online', (cusInfo) => {
    const updateUser = {
      ...cusInfo, 
      // online: true,
      socket: socket.id
    }
    if(updateUser.isLogin !== false){
      socket.join('counter');
      socket.join('kitchen');
    }
    const existUser = allUserList.find((x)=>x.CusID === updateUser.CusID)
    if(existUser){
      existUser.socket = socket.id
    }else{
      allUserList.push(updateUser)
    }
  })

  // customer click btnService call the staff sent alert to all page /manage/ for employee
  socket.on("newService", (service) => {
    // service => user->[name or id], TableID
    io.to('counter').emit('newService', service);
    console.table(service)
  });

  // kitchen change status orderDetail send new status to customer who ordered
  socket.on("kitchenChangeOrder", (order) => {
    const userOrder = allUserList.find((x)=> x.CusID === order.CusID)
    io.emit('tableStatusChangeOrder', order)
    if(userOrder){
      io.to(userOrder.socket).emit('kitchenChangeOrder', order)
    }
  })

  socket.on("kitchenNextQueueOrder", (nextOrder) => {
    const userOrder = allUserList.find((x)=> x.CusID === nextOrder.CusID)
    if(userOrder){
      io.to(userOrder.socket).emit('kitchenNextQueueOrder', nextOrder)
    }
  })

  socket.on("confirmPayment", (data) => {
    const userOrder = allUserList.find((x)=> x.CusID === data.Detail.checkTableUse.CusID)
    if(userOrder){
      io.to(userOrder.socket).emit('confirmPayment', data)
    }
    io.to('kitchen').emit('confirmPayment', data)
  })

  socket.on('disconnect', () => {
    allUserList = allUserList.filter((x)=> x.socket !== socket.id)
    console.log(`socket ${socket.id} disconnected`);
  })
});

httpServer.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

export { io };