import fs from 'fs';

const removeImage = (dataImages, pathRoot) => {
  if(dataImages.length > 0){
    dataImages.map((path) => {
      fs.access(`${pathRoot}/${path[Object.keys(path)]}`, fs.F_OK, (err) => {
        if(err){
          console.error(err)
          return
        }
        fs.unlinkSync(`${pathRoot}/${path[Object.keys(path)]}`);
      });
    });
  }
}

export {
  removeImage
}