import _ from 'lodash'
import { check, body,validationResult } from 'express-validator';

import { connectDB } from '../config/connectDB.js';

// ! ValidationResult
const validationDataResult = (req, res, next) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    return res.status(400).json({ errors: errors.array() });
  }
  next();
}

// ! Validator productRouter
const validationProduct = [
  body(['PDName'], 'กรุณากรอกชื่ออาหาร')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['PDPrice'], 'กรุณากรอกราคาให้ถูกต้อง')
    .trim()
    .not()
    .isEmpty()
    .isFloat({gt: 0}),
  body(['CID'], 'กรุณาเลือกประเภทอาหาร')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['PDDesc'], 'กรุณากรอกรายละเอียดอาหาร')
    .trim()
    .escape()
    .not()
    .isEmpty(),
];

// ! validator productReviewRouter
const validationProductReview = [
  body('ODReview', 'กรุณาให้คะแนนรีวิว 1- 5 ดาวเท่านั้น')
    .isFloat({min:1, max:5}),
  // check('RevComment', 'กรุณากรอกคำติชมอาหาร')
  //     .trim()
  //     .escape()
  //     .not()
  //     .isEmpty()
];

// ! validator tableRouter
const validationTable = [
  body(['*.TName'], 'กรุณากรอกโต๊ะอาหาร')
    .trim()
    .escape()
    .not()
    .isEmpty(),
];

// ! Validator categoryRouter
const validationCategory = [
  body('*.CName','กรุณากรอกประเภทอาหารให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body('*.Ingredient.*.CIName','กรุณากรอกส่วนประกอบให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty()
];

// ! Validator categoryRouter
const validationCategoryIngredientsOption = [
  body('*.Option.*.CIOName','กรุณากรอกตัวเลือกส่วนประกอบให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body('*.Option.*.CIOPrice')
    .isFloat({gt: -1})
    .withMessage('กรุณากรอกราคาให้ถูกต้อง')
    .trim()
    .not()
    .isEmpty()
    .withMessage('กรุณากรอกราคาให้ถูกต้อง')
];

// ! Validator paymentMethodRouter
const validationPaymentMethod = [

];

// ! Validator Voucher
const validationVoucher = [
  body(['*.VoucCode'],'กรุณากรอกรหัสคูปองให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .isLength({min:5, max:10}),
  body(['*.VoucName'],'กรุณากรอกชื่อคูปองให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.VoucUsePoint'])
    .isFloat({gt: -1})
    .withMessage('กรุณากรอกจำนวนพ้อยท์แลกคูปองให้ถูกต้อง')
    .not()
    .isEmpty()
    .trim()
    .withMessage('กรุณากรอกจำนวนพ้อยท์แลกคูปองให้ถูกต้อง'),
  body(['*.VoucDiscount'])
    .isFloat({gt: -1})
    .withMessage('กรุณากรอกส่วนลดคูปองให้ถูกต้อง')
    .trim()
    .not()
    .isEmpty()
    .withMessage('กรุณากรอกส่วนลดคูปองเป็น [% หรือ ฿] เท่านั้น'),
  body(['*.VoucType'],'กรุณากรอกประเภทคูปองให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .isIn(['%', '฿']),
  body(['*.VoucCondition'])
    .isFloat({gt: 0})
    .withMessage('กรุณากรอกขั้นต่ำการใช้คูปองตั้งแต่ 0 เป็นต้นไป')
    .trim()
    .not()
    .isEmpty()
    .withMessage('กรุณากรอกขั้นต่ำการใช้คูปองให้ถูกต้อง'),
  body(['*.VoucStart'])
    .custom((inputDate, { req }) => {
      const dateStart = new Date(inputDate);
      const dateEnd = new Date(req.body[0].VoucEnd);

      if(isNaN(dateStart)){
        throw new Error('กรุณากรอกวันเริ่มการใช้คูปองให้ถูกต้อง');
      }
      if(isNaN(dateEnd)){
        throw new Error('กรุณากรอกวันสิ้นสุดการใช้คูปองให้ถูกต้อง');
      }
      if(dateEnd < dateStart){
        throw new Error('วันเริ่มต้นการใช้คูปองต้องเริ่มก่อนวันสิ้นสุดการใช้คูปอง');
      }
      return true;
    }),
  body(['*.VoucQuota'])
    .isFloat({gt: 0})
    .withMessage('กรุณากรอกสิทธิ์การใช้ตั้งแต่ 0 ขึ้นไป')
    .trim()
    .not()
    .isEmpty()
    .withMessage('กรุณากรอกสิทธิ์การใช้คูปองให้ถูกต้อง')
];

// ! Validator Employees and Customer
const validationPerson = [
  body(['*.Username'], 'กรุณากรอกชื่อผู้ใช้ให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Passwd'],'กรุณากรอกรหัสผ่านให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Gender'],'กรุณากรอกคำนำหน้าเป็น [นาย นาง หรือ นางสาว] เท่านั้น')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .isIn(['นาย', 'นาง', 'นางสาว']),
  body(['*.Firstname'],'กรุณากรอกชื่อจริงให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Lastname'],'กรุณากรอกนามสกุลให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Nickname'],'กรุณากรอกชื่อเล่นให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Tel'],'กรุณากรอกเบอร์โทรให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty(),
  body(['*.Email'],'กรุณากรอกอีเมลให้ถูกต้อง')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .isEmail()
];

// ! Validator Reward
// const validationReward = [
    
// ];

// ! Validator RewardHistory
// const validationRewardHistory = [

// ];

// ! Validator Order
const validationOrder = [
  body(['*.TableID'], 'กรุณาสแกน QRCode โต๊ะใหม่')
    .trim()
    .escape()
    .not()
    .isEmpty(),
];

const validationOrderDetail = [
  body(['*.cartItems'], 'ตะกร้าสินค้าว่าง')
    .not()
    .isEmpty(),
  body(['*.cartItems.*.PDID'])
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('กรุณาเลือกอาหาร')
    .custom(async(value, { req })=>{
      const connection = await connectDB();
      const [ dataProduct ] = await connection.query('SELECT PDPrice FROM tb_product WHERE PDID = ?',[
        value
      ])

      if(_.isEmpty(dataProduct)){
        throw new Error('ไม่พบข้อมูลอาหาร');
      }

      req.body[0].cartItems.map((item)=>item.PDID === value?item.PDPrice = dataProduct[0].PDPrice : item)
      
      // TODO: validate objectOptions
      const thisProduct = req.body[0].cartItems.find((item)=>item.PDID === value)

      if(!_.isObject(thisProduct.objectOptions)){
        throw new Error('เกิดข้อผิดพลาดของตัวเลือก')
      }

      let keyOptions = _.keys(thisProduct.objectOptions)
      let getPrice = []
      if(!_.isEmpty(keyOptions)){
        const connection = await connectDB();
        const [ dataOptions ] = await connection.query('SELECT * FROM `tb_category_ingredients` LEFT JOIN tb_category_ingredients_option ON tb_category_ingredients.CIID = tb_category_ingredients_option.CIID WHERE tb_category_ingredients_option.CIID IN (?)', [
          keyOptions
        ])
        
        keyOptions.map((key)=>{
          if(_.isArray(thisProduct.objectOptions[key])){ // Checkbox
            thisProduct.objectOptions[key].map((option)=>{
              const getOption = dataOptions.find((x)=>x.CIOName === option)
              if(_.isEmpty(getOption)){
                throw new Error('ไม่พบตัวเลือกในระบบ')
              }
              // push price of respective option -> objectOptions 
              getPrice = [...getPrice, getOption.CIOPrice]
            })
          }else{ // Radio
            const getOption = dataOptions.find((x)=>x.CIOName === thisProduct.objectOptions[key])
            if(_.isEmpty(getOption)){
              throw new Error('ไม่พบตัวเลือกในระบบ')
            }
            // push price of respective option -> objectOptions 
            getPrice = [...getPrice, getOption.CIOPrice]
          }
        })
      }
      
      req.body[0].cartItems.map((item)=>item.PDID === value?item.optionPrice = getPrice : item)

      return true;
    }),
  body(['*.cartItems.*.orderQty'])
    .isInt({min: 1, max: 20})
    .withMessage('กรุณากรอกจำนวนระหว่าง 1 - 20 เท่านั้น')
    .trim()
    .not()
    .isEmpty()
    .withMessage('กรุณากรอกจำนวนอาหารให้ถูกต้อง')
];

export { 
    validationDataResult,
    validationProduct,
    validationProductReview,

    validationTable,
    validationCategory,
    validationCategoryIngredientsOption,
    validationPaymentMethod,
    validationVoucher,
    validationPerson,
    // validationReward,
    // validationRewardHistory,
    validationOrder,
    validationOrderDetail
};