import jwt from 'jsonwebtoken'

const generateTokenCus = (user) => {
  return jwt.sign({
    CusID: user.CusID,
    CusUsername : user.CusUsername,
    Name: user.CusName,
    CusGender: user.CusGender,
    CusFirstname: user.CusFirstname,
    CusLastname: user.CusLastname,
    CusNickname: user.CusNickname,
    CusTel: user.CusTel,
    CusEmail: user.CusEmail,
    CusPoint: user.CusPoint,
  },
  process.env.JWT_SECRET_KEY || '',
  {
    expiresIn: '12h'
  })
}

const generateTokenCusSocialLogin = (user) => {
  return jwt.sign({
    CusID: user.CusID,
    Name: user.CusNickname,
    CusEmail: user.CusEmail,
    CusPoint: user.CusPoint,
    CusPicture: user.CusPicture
  },
  process.env.JWT_SECRET_KEY || '',
  {
    expiresIn: '12h'
  })
}

const generateTokenEmp = (user) => {
  return jwt.sign({
    _id: user.EmpID,
    EmpID: user.EmpID,
    EmpUsername : user.EmpUsername,
    Name: user.EmpName,
    EmpGender: user.EmpGender,
    EmpFirstname: user.EmpFirstname,
    EmpLastname: user.EmpLastname,
    EmpNickname: user.EmpNickname,
    EmpTel: user.EmpTel,
    EmpEmail: user.EmpEmail,
    EmpRole: user.EmpRole,
  },
  process.env.JWT_SECRET_KEY || '',
  {
    expiresIn: '12h'
  })
}

export {
  generateTokenCus,
  generateTokenCusSocialLogin,
  generateTokenEmp
}