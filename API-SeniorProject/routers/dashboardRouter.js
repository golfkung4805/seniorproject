import express from 'express';
import moment from 'moment';

import { connectDB } from '../config/connectDB.js';

const router = express.Router();

const typeFormatSQL = {
  'date': "%H:00",
  'week': "%Y-%m-%d",
  'month': "%Y-%m-%d",
  'year': "%Y-%m",
}
const typeFormat = {
  'date_query': 'YYYY-MM-DD',
  'week_query': 'YYYY-MM-DD',
  'month_query': 'YYYY-MM',
  'year_query': 'YYYY',

  'hour': 'HH:mm',
  'week': 'YYYY-MM-DD',
  'month': 'YYYY-MM-DD',
  'year': 'YYYY-MM',
};

const formatDashboard = async(dataDashboard, datePicker, typePicker, add_) => {
  const connection = await connectDB();
  let RangeDateTime = [], datePickerStart = moment(datePicker), datePickerEnd = moment(datePicker);
  if(typePicker === 'date'){
    typePicker = add_
    const [ timeOpenClose ] = await connection.query(`SELECT SysOpen, SysClose FROM tb_sysconfig WHERE SysID = 'SYS-0001'`) 
    datePickerStart = moment().hours(timeOpenClose[0].SysOpen.split(':')[0])
    datePickerEnd = moment().hours(timeOpenClose[0].SysClose.split(':')[0])
  }

  for(let dayOf_ = datePickerStart.startOf(typePicker); dayOf_ <= datePickerEnd.endOf(typePicker); dayOf_.add(1, add_)){
    let findDate = dataDashboard.find((x)=>x.DateTime === dayOf_.format(typeFormat[`${typePicker}`]))
    
    RangeDateTime = [...RangeDateTime, { 
      DateTime: dayOf_.format(typeFormat[`${typePicker}`]), 
      subTotal: findDate? findDate.subTotal: 0,
      orderTotal: findDate? Number(findDate.orderTotal): 0,
    }]
  }
  await connection.end();
  return RangeDateTime;
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  const typePicker = req.query.typePicker, datePicker = req.query.datePicker;

  try {
    let sqlCommand = ``, sqlCommandCondition = ``, paramsQuery = [];
    
    if(typePicker === 'date' || typePicker === 'month' || typePicker === 'year'){
      sqlCommandCondition += ` AND tb_order.createdAt LIKE ?`;
      paramsQuery = [...paramsQuery,
        `%${moment(datePicker).format(typeFormat[`${typePicker}_query`])}%`
      ]
    }
    if(typePicker === 'week'){
      sqlCommandCondition += ` AND WEEK(tb_order.createdAt) = ?`;
      paramsQuery = [...paramsQuery,
        moment(req.query.datePicker).week()-1
      ]
    }

    sqlCommand = `SELECT 
                    DATE_FORMAT(tb_order.createdAt, "${typeFormatSQL[typePicker]}")as DateTime, 
                    SUM(tb_order.ODTotal)as subTotal,
                    /* SUM(tb_order_detail.ODQty)as orderTotal, */
                    (SELECT SUM(tb_order_detail.ODQty) FROM tb_order_detail WHERE DATE_FORMAT(tb_order_detail.createdAt, "${typeFormatSQL[typePicker]}") = DATE_FORMAT(tb_order.createdAt, "${typeFormatSQL[typePicker]}") AND tb_order_detail.ODStatus = "complete")as orderTotal
                  FROM tb_order 
                  /* LEFT JOIN tb_order_detail ON tb_order.ODID = tb_order_detail.ODID */
                  WHERE tb_order.ODStatus = 'successful' ${sqlCommandCondition} 
                  GROUP BY DATE_FORMAT(tb_order.createdAt, "${typeFormatSQL[typePicker]}")`;
    
    const [ dataDashboard ] = await connection.query(sqlCommand, paramsQuery);
    const typeAddMoment = (typePicker === 'date')? 'hour': (typePicker === 'week' || typePicker === 'month')? 'day' : (typePicker === 'year')? 'month': 'hour';
    let FormatDataDashboard = await formatDashboard(dataDashboard, datePicker, typePicker, typeAddMoment);

    if(typePicker === 'date' || typePicker === 'month' || typePicker === 'year'){
      sqlCommandCondition = ` AND tb_order_detail.createdAt LIKE ?`;
    }
    if(typePicker === 'week'){
      sqlCommandCondition = ` AND WEEK(tb_order_detail.createdAt) = ?`;
    }

    sqlCommand = `SELECT
                    tb_category.CName as name,
                    SUM(tb_order_detail.ODTotalPrice)as subTotal,
                    SUM(tb_order_detail.ODQty)as subQty
                  FROM tb_order_detail 
                  LEFT JOIN tb_product ON tb_order_detail.PDID = tb_product.PDID
                  LEFT JOIN tb_category ON tb_product.CID = tb_category.CID
                  WHERE tb_order_detail.ODStatus = 'complete' ${sqlCommandCondition} AND DATE_FORMAT(tb_order_detail.createdAt, "${typeFormatSQL[typePicker]}")
                  GROUP BY tb_category.CID
                  LIMIT 5`;
    const [ dataDashboardCategory ] = await connection.query(sqlCommand, paramsQuery);

    sqlCommand = `SELECT 
                    PDName,
                    SUM(tb_order_detail.ODTotalPrice)as subTotal,
                    SUM(tb_order_detail.ODQty)as subQty
                  FROM tb_order_detail 
                  WHERE tb_order_detail.ODStatus = 'complete' ${sqlCommandCondition} AND DATE_FORMAT(tb_order_detail.createdAt, "${typeFormatSQL[typePicker]}")
                  GROUP BY PDID
                  LIMIT 5`;
    const [ dataDashboardProduct ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({ overview: FormatDataDashboard, popularProduct: dataDashboardProduct, popularCategory: dataDashboardCategory });
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
})

router.get('/export', async (req, res) => {
  
})

export default router;