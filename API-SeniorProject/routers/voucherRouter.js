import express from 'express';
import moment from 'moment'
import { connectDB } from '../config/connectDB.js';
import { validationVoucher, validationDataResult } from '../util/validate.js';

const router = express.Router();

const checkEmptyDataVoucher = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataVoucher ] = await connection.query('SELECT * FROM tb_voucher WHERE VoucID = ?', [req.params.VoucID]);
  await connection.end();
  if(dataVoucher.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลคูปอง ${req.params.VoucID} ในระบบ`});
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = 'SELECT * FROM tb_voucher WHERE 1=1';

    if(req.query.filters){
      const filterStatus = (req.query.filters.VoucStatus.length === 0) ? ['1', '0']: req.query.filters.VoucStatus;
      if(req.query.filters.VoucRangeDate){
        sqlCommand = sqlCommand + ` AND VoucStart >= ? AND VoucEnd <= ?`;
        paramsQuery = [...paramsQuery,
        ...[
          req.query.filters.VoucRangeDate[0],
          req.query.filters.VoucRangeDate[1]
          ]
        ]
      }
      sqlCommand = sqlCommand + ` AND VoucID LIKE ? AND VoucName LIKE ? AND VoucStatus IN (?)`;
      paramsQuery = [...paramsQuery,
      ...[
        `%${req.query.filters.VoucID || ''}%`, 
        `%${req.query.filters.VoucName || ''}%`, 
        filterStatus
        ]
      ];

      var [ TotalDataVouchers ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataVouchers ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    let current = 1;
    let pageSize = 10;
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataTable.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }

    const [ dataVouchers ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataVouchers,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataVouchers.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/:VoucCode', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataVoucher ] = await connection.query('SELECT * FROM tb_voucher WHERE VoucCode = ?', [req.params.VoucCode]);
    // if(dataVoucher.length <= 0){
    //   return res.status(200).json({msg: 'ไม่พบข้อมูลคูปอง'});
    // }
    
    return res.status(200).json(dataVoucher);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/', validationVoucher, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ sequenceVoucher ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbVoucher'`);
    const LastID = sequenceVoucher[0]['LastID'];
    let VoucID = 'V-00000';
    VoucID = VoucID.substr(0, VoucID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_voucher(VoucID, VoucCode, VoucName, VoucUsePoint, VoucDiscount, VoucType, VoucCondition,VoucStart, VoucEnd, VoucQuota) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
      VoucID,
      req.body.VoucCode,
      req.body.VoucName,
      req.body.VoucUsePoint,
      req.body.VoucDiscount,
      req.body.VoucType,
      req.body.VoucCondition,
      moment(req.body.VoucStart).format("YYYY-MM-DD HH:mm:ss"),
      moment(req.body.VoucEnd).format("YYYY-MM-DD HH:mm:ss"),
      req.body.VoucQuota
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbVoucher'`);
    await connection.commit();
    
    res.status(200).json({msg: 'เพิ่มข้อมูลคูปองสำเร็จ'});
  } catch (error) {
    console.error(error);
    await connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:VoucID', checkEmptyDataVoucher, validationVoucher , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];
    
    await connection.query('UPDATE tb_voucher SET VoucCode = ?, VoucName = ?, VoucUsePoint = ?, VoucDiscount= ?, VoucType = ?, VoucCondition = ?, VoucStart = ?, VoucEnd = ?, VoucQuota = ?, updatedAt = CURRENT_TIMESTAMP() WHERE VoucID = ?', [
      req.body.VoucCode,
      req.body.VoucName,
      req.body.VoucUsePoint,
      req.body.VoucDiscount,
      req.body.VoucType,
      req.body.VoucCondition,
      moment(req.body.VoucStart).format("YYYY-MM-DD HH:mm:ss"),
      moment(req.body.VoucEnd).format("YYYY-MM-DD HH:mm:ss"),
      req.body.VoucQuota,
      req.params.VoucID
    ]);

    res.status(200).json({msg: `แก้ไขข้อมูลคูปอง ${req.params.VoucID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:VoucID/Status', checkEmptyDataVoucher, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_voucher SET VoucStatus = (CASE WHEN VoucStatus = 1 THEN 0 ELSE 1 END) WHERE VoucID = ?', [
      req.params.VoucID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้งานคูปอง ${req.params.VoucID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:VoucID', checkEmptyDataVoucher, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_voucher WHERE VoucID = ?', [req.params.VoucID]);

    res.status(200).json({msg: `ลบข้อมูลคูปอง ${req.params.VoucID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;