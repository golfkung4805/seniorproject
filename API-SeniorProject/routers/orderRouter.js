import _ from 'lodash'
import moment from 'moment'
import express from 'express';
import { io } from '../server.js'
import cron from 'node-cron'
import { connectDB } from '../config/connectDB.js';
import { validationOrder, validationOrderDetail, validationProductReview, validationDataResult } from '../util/validate.js';

import Omise from 'omise';
const omise = Omise({
  publicKey: process.env.OMISE_PUBLIC_KEY || '',
  secretKey: process.env.OMISE_SECRET_KEY || ''
});

const checkEmptyDataOrder = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataOrder ] = await connection.query('SELECT * FROM tb_order WHERE ODID = ?', [req.params.ODID]);
  await connection.end();
  if(dataOrder.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลออเดอร์ ${req.params.ODID} ในระบบ`});
  req.body.DataOrder = dataOrder[0]
  next();
}

const checkEmptyDataOrderDetail = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataOrderDetail ] = await connection.query('SELECT * FROM tb_order_detail WHERE _ID = ?', [req.params._ID]);
  await connection.end();
  if(dataOrderDetail.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลออเดอร์อาหารในระบบ`});
  req.body.PDID = dataOrderDetail[0].PDID
  next();
}

const calculatorPoints = async (CusID, amount, status = '') => {
  const connection = await connectDB();
  let totalPoint = 0;

  const [ customer ] = await connection.query('SELECT * FROM tb_customer WHERE CusID = ?', [ CusID ]);
  if(status === 'successful' && !_.isEmpty(customer)){
    const [ getCondition ] = await connection.query('SELECT SysPointCondition FROM tb_sysconfig');
    const pointCondition = getCondition[0].SysPointCondition.split("/");

    totalPoint = ((Number.parseFloat(amount)/100) / Number.parseFloat(pointCondition[0])) * Number.parseFloat(pointCondition[1]);

    await connection.query(`UPDATE tb_customer SET CusPoint = CusPoint + ? WHERE CusID = ?`, [
      totalPoint,
      CusID
    ]);
  }
  await connection.end();
  return totalPoint;
}

const updateAverageRatingProduct = async (PDID) => {
  const connection = await connectDB();
  await connection.query('UPDATE tb_product SET PDRating = (SELECT ROUND(AVG(`ODReview`),1) FROM tb_order_detail WHERE PDID = ?) WHERE PDID = ?', [ PDID, PDID ]);
  await connection.end();
}

const router = express.Router();

cron.schedule('0 1 * * *', async () => {
  // schedule update status order overdue => 01:00 AM
  const connection = await connectDB();
  try {
    await connection.query("UPDATE tb_order SET ODStatus = 'overdue', updatedAt = CURRENT_TIMESTAMP() WHERE ODStatus = 'openOrder' AND createdAt LIKE ?", [
      `%${moment().add(-1, 'day').format('YYYY-MM-DD')}%`
    ])
  } catch (error) {
    console.error(error)
  } finally {
    await connection.end();
  }
});

const typeFormatSQL = {
  'date': "%H:00",
  'week': "%Y-%m-%d",
  'month': "%Y-%m-%d",
  'year': "%Y-%m",
}

const typeFormat = {
  'date_query': 'YYYY-MM-DD',
  'week_query': 'YYYY-MM-DD',
  'month_query': 'YYYY-MM',
  'year_query': 'YYYY',

  'date': 'HH:mm',
  'week': 'YYYY-MM-DD',
  'month': 'YYYY-MM-DD',
  'year': 'YYYY-MM',
};

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let sqlCommand = `SELECT ODID, TableID, CusID, EmpID, VoucCode, PayID, ODStatus, ODTotal, ODTax, VoucDiscount, ODTotalPoint, createdAt, updatedAt ,CAST((SELECT SUM(tb_order_detail.ODQty) FROM tb_order_detail WHERE ODID = tb_order.ODID) AS int)as countOrder FROM tb_order WHERE 1=1`;
    let paramsQuery = [];
    
    if(req.query.filters){
      const typePicker = req.query.filters.typePicker || 'date', datePicker = req.query.filters.datePicker || moment().format('YYYY-MM-DD');
      const filterStatus = req.query.filters.Status || ['openOrder', 'paid', 'unpaid', 'successful', 'failed', 'overdue'];
      sqlCommand = sqlCommand + ` AND ODID LIKE ? AND TableID LIKE ? AND CusID LIKE ? AND ODStatus IN (?)`;
      paramsQuery = [...paramsQuery, ...[
        `%${req.query.filters.ODID || ''}%`, 
        `%${req.query.filters.TableID || ''}%`, 
        `%${req.query.filters.CusID || ''}%`,
        filterStatus
      ]]

      if(typePicker === 'date' || typePicker === 'month' || typePicker === 'year'){
        sqlCommand += ` AND createdAt LIKE ?`;
        paramsQuery = [...paramsQuery,
          `%${moment(datePicker).format(typeFormat[`${typePicker}_query`])}%`
        ]
      }
      if(typePicker === 'week'){
        sqlCommand += ` AND WEEK(createdAt) = ?`;
        paramsQuery = [...paramsQuery,
          moment(datePicker).week()-1
        ]
      }

      var [ TotalDataOrder ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataOrder ] = await connection.query(sqlCommand);
    }

    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    
    let current = 1;
    let pageSize = 10;
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataOrder.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }

    const [ dataOrders ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataOrders,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataOrder.length
    });
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/kitchen', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataOrderDetail ] = await connection.query(`SELECT *, 
                                                      (SELECT PDImage FROM tb_product_image WHERE PDID = tb_order_detail.PDID ORDER BY PDImageIndex LIMIT 1)as PDImage 
                                                      FROM tb_order_detail 
                                                      WHERE createdAt LIKE ? AND (SELECT ODStatus FROM tb_order WHERE ODID = tb_order_detail.ODID) IN ('openOrder', 'paid', 'unpaid')
                                                      ORDER BY createdAt`, [`%${moment().format('YYYY-MM-DD')}%`]
                                                    );

    let kitchenOrder = [], kitchenOrderComplete = [], sameOrder = [], countComplete = 0;
    for (var i = 0; i < dataOrderDetail.length; i++) {
      if(_.isEmpty(sameOrder)) {
        sameOrder = [dataOrderDetail[i]];
        countComplete += (dataOrderDetail[i].ODStatus === 'waitQueue'? 0: 1);
      }
      
      if(!_.isEmpty(dataOrderDetail[i+1]) && _.last(sameOrder).Ref === dataOrderDetail[i+1].Ref){
        sameOrder = [...sameOrder, dataOrderDetail[i+1]];
        countComplete += (dataOrderDetail[i+1].ODStatus === 'waitQueue'? 0: 1);
      }else{
        if(sameOrder.length === countComplete){
          kitchenOrderComplete = [...kitchenOrderComplete, {countComplete, countOrder: sameOrder.length, order: sameOrder }];
          kitchenOrderComplete = _.orderBy(kitchenOrderComplete, (item) => {return item.order[0]._ID}, ['desc']);
        }else{
          kitchenOrder = [...kitchenOrder, {countComplete, countOrder: sameOrder.length, order: sameOrder }];
        }
        sameOrder = [dataOrderDetail[i+1]];
        countComplete = 0
        countComplete += (!_.isEmpty(dataOrderDetail[i+1]) && dataOrderDetail[i+1].ODStatus === 'waitQueue'? 0: 1);
      }
    }

    return res.status(200).json({kitchenOrder, kitchenOrderComplete});
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

// router.get('/:ODID', async (req, res) => {
//   try {
//     const [ dataOrder ] = await connection.query('SELECT * FROM tb_order WHERE ODID = ?', [req.params.ODID]);
//     if(dataOrder.length <= 0){
//       return res.status(200).json({msg: 'ไม่พบข้อมูลออเดอร์อาหาร'});
//     }
    
//     return res.status(200).json(dataOrder);
//   } catch (error) {
//     console.error(error)
//     res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
//   }
// });

router.get('/detail/:ODID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataOrderDetail ] = await connection.query('SELECT *, (SELECT PDImage FROM tb_product_image WHERE PDID = tb_order_detail.PDID ORDER BY PDImageIndex LIMIT 1)as PDImage FROM tb_order_detail WHERE ODID = ? ORDER BY _ID DESC', [req.params.ODID]);
    
    return res.status(200).json(dataOrderDetail);
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

// เพิ่มอาหารก่อนเพิ่มอาหารในออเดอร์ (ในกรณีที่สั่งอาหารครั้งแรก)
router.post('/', validationOrder, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0]
    
    const [ checkOrder ] = await connection.query(`SELECT * FROM tb_order WHERE TableID = ? AND createdAt LIKE ? AND ODStatus IN ('openOrder','paid','unpaid')`, [
      req.body.TableID,
      `%${moment().format('YYYY-MM-DD')}%`
    ])

    if(!_.isEmpty(checkOrder)){
      return res.status(401).json({msg: `ขณะนี้โต๊ะ ${checkOrder[0].TableID} กำลังถูกใช้งานอยู่`});
    }

    const [ sequenceOrder ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbOrder'`);
    const LastID = sequenceOrder[0]['LastID'];
    
    const date = new Date();
    const day = (String(date.getDate()).length < 2 ? "0" : "")  + date.getDate();
    const month = (String(date.getMonth()+1).length < 2 ? "0" : "") + (date.getMonth()+1);
    const year = date.getFullYear();

    let ODID = `OD-${day}${month}${year}:0000000`;
    ODID = ODID.substr(0, ODID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_order(ODID, TableID, CusID) VALUES (?, ?, ?)',[
      ODID,
      req.body.TableID,
      req.body.CusID
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbOrder'`);
    await connection.commit();

    const [ newOrder ] = await connection.query(`SELECT * FROM tb_order WHERE ODID = ?`, [ODID]);
    io.to('kitchen').emit('newOpenOrder', newOrder[0]);
    res.status(200).json({ODID});
  } catch (error) {
    console.error(error)
    await connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

// เพิ่มอาหารในออเดอร์
router.post('/:ODID',validationOrderDetail, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0]

    await connection.beginTransaction();
    const Ref = (Math.random()).toString(36).substring(3);
    let ODTotalPrice = 0, ODTotal = 0;
    let sqlCommand = `INSERT INTO tb_order_detail(ODID, PDID, PDName, ODSizeList, ODSizeListDisplay, ODPriceList, PDPrice, ODQty, ODTotalPrice, ODTotalTax, ODDesc, Ref) VALUES`;
    let paramsQuery = [];

    req.body.cartItems.map(async(item, index)=>{
      if(_.isEmpty(item.optionPrice)){
        ODTotalPrice = Number.parseFloat(item.PDPrice) * Number.parseInt(item.orderQty)
      }else{
        ODTotalPrice = (Number.parseFloat(item.PDPrice) + item.optionPrice.reduce((a, b)=> a+b)) * Number.parseInt(item.orderQty)
      }
      ODTotal += ODTotalPrice
      sqlCommand += ` (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        req.params.ODID, 
        item.PDID, 
        item.PDName,
        JSON.stringify(item.objectOptions), 
        JSON.stringify(item.optionShow),
        JSON.stringify(item.optionPrice), 
        Number.parseFloat(item.PDPrice), 
        Number.parseInt(item.orderQty), 
        Number.parseFloat(ODTotalPrice), 
        Number.parseFloat(ODTotalPrice) * 0.07,
        item.orderDesc,
        Ref
      ]];
    })

    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query('UPDATE tb_order SET ODTotal = ODTotal + ?, updatedAt = CURRENT_TIMESTAMP() WHERE ODID = ?', [
      Number.parseFloat(ODTotal),
      req.params.ODID
    ]);
    
    await connection.commit();

    const [ newOrder ] = await connection.query(`SELECT *, (SELECT PDImage FROM tb_product_image WHERE PDID = tb_order_detail.PDID ORDER BY PDImageIndex LIMIT 1)as PDImage FROM tb_order_detail WHERE Ref = ?`, [ Ref ]);

    io.to('kitchen').emit('newOrder', newOrder);
    res.status(200).json({msg: 'สั่งออเดอร์อาหารสำเร็จ'});
  } catch (error) {
    console.error(error)
    await connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/omise/webhooks', async (req, res) => {
  const connection = await connectDB();
  try {
    if(req.body.data.status === 'successful' || req.body.data.status === 'failed'){
      connection.beginTransaction();

      let getPoint = await calculatorPoints(req.body.data.metadata.CusID, req.body.data.amount, req.body.data.status);

      const thisStatus = req.body.data.status==="successful" ?'paid' : req.body.data.status
      await connection.query(`UPDATE tb_order SET VoucCode = ?, PayID = ?, ODPayDetail = ?, ODStatus = ?, VoucDiscount = ?, ODTotalPoint = ?, updatedAt = CURRENT_TIMESTAMP() WHERE ODID = ?`, [
        req.body.data.metadata.VoucCode,
        req.body.data.metadata.paymentMethod,
        JSON.stringify(req.body.data),
        thisStatus,
        req.body.data.metadata.VoucDiscount,
        getPoint,
        req.body.data.metadata.ODID
      ]);
      if(!_.isEmpty(req.body.data.metadata.VoucCode)){
        await connection.query(`UPDATE tb_customer_voucher SET VoucHisStatus = 0 WHERE CusID = ? AND VoucID = (SELECT VoucID FROM tb_voucher WHERE VoucCode = ?)`, [
          req.body.data.metadata.CusID,
          req.body.data.metadata.VoucCode,
        ]);
      }
      connection.commit();
      io.to('counter').emit('newOrderPayment', {data: req.body.data});
    }
  } catch (error) {
    console.error(error)
    connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.patch('/:ODID/confirmPayment', async (req, res) => {
  const connection = await connectDB();
  try {
    let sqlCommand = `UPDATE tb_order SET EmpID = ?,`
    if(_.isEmpty(req.body.Detail.PayID)){
      sqlCommand += ` PayID = 'checkout_counter', ODPayDetail = '{}',`
    }
    sqlCommand += ` ODStatus = 'successful', updatedAt = CURRENT_TIMESTAMP() WHERE ODID = ?`
    await connection.query(sqlCommand, [
      req.body.EmpID,
      req.body.Detail.ODID,
    ])
    if(_.isEmpty(req.body.Detail.PayID) || req.body.Detail.PayID === 'checkout_counter'){
      // socket emit to customer
    }
    res.status(200).json({msg: 'ยืนยันการชำระเงินสำเร็จ'});
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.patch('/:ODID/checkout', async (req, res) => {
  const connection = await connectDB();
  try {
    const { CusID, TableID, amount, VoucDiscount, VoucCode, token, paymentMethod } = req.body;
    if(paymentMethod === 'checkout_counter'){
      const getPoint = await calculatorPoints(CusID, amount, 'successful');
      await connection.query(`UPDATE tb_order SET VoucCode = ?, PayID = ?, ODPayDetail = ?, ODStatus = ?, VoucDiscount = ?, ODTotalPoint = ?, updatedAt = CURRENT_TIMESTAMP() WHERE ODID = ?`, [
        req.body.VoucCode,
        req.body.paymentMethod,
        '{}',
        'unpaid',
        req.body.VoucDiscount,
        getPoint,
        req.params.ODID
      ]);
      if(!_.isEmpty(req.body.VoucCode)){
        await connection.query(`UPDATE tb_customer_voucher SET VoucHisStatus = 0 WHERE CusID = ? AND VoucID = (SELECT VoucID FROM tb_voucher WHERE VoucCode = ?)`, [
          CusID,
          VoucCode
        ]);
      }
      res.status(200).json({msg: 'ชำระเงินผ่านหน้าเคาน์เตอร์'});
      io.to('counter').emit('newOrderPayment', {data: {...req.body, ODID: req.params.ODID}});
    }else if(paymentMethod === 'credit_card'){
      const customer = await omise.customers.create({
        email: 'test@mail.com',
        description: `${CusID}, (${req.params.ODID})`,
        card: token
      });

      const charge = await omise.charges.create({
        amount: amount,
        currency: "thb",
        customer: customer.id,
        metadata: { 'ODID': req.params.ODID, 
                    TableID, 
                    CusID,
                    paymentMethod,
                    VoucCode,
                    VoucDiscount
                  },
      });

      if(charge.status === 'successful'){
        
        
      }

      res.json({customer, charge});
    }else if(paymentMethod === 'internet_banking' || paymentMethod === 'rabbit_linepay' || paymentMethod === 'truemoney'){
      const charge = await omise.charges.create({
        amount: amount,
        description: `${CusID}, (${req.params.ODID})`,
        source: token,
        currency: "thb",
        metadata: { 'ODID': req.params.ODID, 
                    TableID, 
                    CusID,
                    paymentMethod,
                    VoucCode,
                    VoucDiscount
                  },
        return_uri: `${process.env.BASE_URL}/order/${TableID}`
      });

      res.send({ authorizeUri: charge.authorize_uri });
    }
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:ODID/status', checkEmptyDataOrder, async (req, res) => {
  const connection = await connectDB();
  try {
    if(req.body.status !== "openOrder" && req.body.status !== "successful" && req.body.status !== "failed" && 
    req.body.status !== "paid" && req.body.status !== "unpaid" && req.body.status !== "overdue"){
      throw "เกิดความผิดพลาดบางอย่าง"
    }
    let sqlCommand = `UPDATE tb_order SET`;

    let getPoint = 0;

    if(req.body.DataOrder.ODTotalPoint === 0 && req.body.status === "successful"){
      getPoint = await calculatorPoints(req.body.DataOrder.CusID, (req.body.DataOrder.ODTotal - req.body.DataOrder.VoucDiscount)*100, 'successful');
    }

    if(_.isEmpty(req.body.DataOrder.PayID) && req.body.status === "successful"){
      sqlCommand += ` PayID = 'checkout_counter',`
    }

    sqlCommand += ` ODStatus = ?, ODTotalPoint = ?, updatedAt = CURRENT_TIMESTAMP() WHERE ODID = ?`

    await connection.query(sqlCommand, [
      req.body.status,
      getPoint,
      req.params.ODID
    ]);

    res.status(200).json({msg: `แก้ไขสถานะออเดอร์ ${req.params.ODID} สำเร็จ`});
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/detail/:_ID/status', async (req, res) => {
  const connection = await connectDB();
  try {
    if(req.body.status !== "complete" && req.body.status !== "cancel" && req.body.status !== "waitQueue"){
      throw "เกิดความผิดพลาดบางอย่าง"
    }

    await connection.beginTransaction();
    await connection.query('UPDATE tb_order_detail SET ODStatus = ?, updatedAt = CURRENT_TIMESTAMP() WHERE _ID = ?', [
      req.body.status,
      req.params._ID
    ]);
    if(req.body.status !== "complete" && req.body.status !== "waitQueue"){
      await connection.query('UPDATE tb_order SET ODTotal = ODTotal - (SELECT ODTotalPrice FROM tb_order_detail WHERE _ID = ?), updatedAt = CURRENT_TIMESTAMP() WHERE ODID = (SELECT ODID FROM tb_order_detail WHERE _ID = ?)', [
        req.params._ID,
        req.params._ID
      ]);
    }
    await connection.commit();
    res.status(200).json({msg: 'แก้ไขสถานะอาหารสำเร็จ'});
  } catch (error) {
    console.error(error)
    await connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:CusID/check', async (req, res) => {
  const connection = await connectDB();
  try {
    
    res.status(200).json({msg: 'แก้ไขข้อมูลออเดอร์อาหารสำเร็จ'});
  } catch (error) {
    console.error(error)
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:_ID/review', validationProductReview , validationDataResult, checkEmptyDataOrderDetail, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    await connection.query('UPDATE tb_order_detail SET ODReview = ? WHERE _ID = ?', [
      req.body.ODReview,
      req.params._ID
    ]);
    updateAverageRatingProduct(req.body.PDID);
    await connection.commit();

    res.status(200).json({msg: `รีวิวออเดอร์ ${req.params._ID} สำเร็จ`})
  } catch (error) {
    console.error(error)
    await connection.rollback();
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการรีวิว', error});
  } finally {
    await connection.end();
  }
});

// router.delete('/:ODID', async (req, res) => {
//     try {
//         await connection.query('DELETE FROM tb_order WHERE ODID = ?', [req.params.ODID]);

//         res.status(200).json({msg: 'ลบข้อมูลออเดอร์อาหารสำเร็จ'});
//     } catch (error) {
//         console.error(error)
//         res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
//     }
// });

export default router;