import express from 'express';
import { connectDB } from '../config/connectDB.js';
import { validationPaymentMethod, validationDataResult } from '../util/validate.js';

const router = express.Router();

const checkEmptyDataPaymentMethod = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataPaymentMethod ] = await connection.query('SELECT * FROM tb_payment_method WHERE PayID = ?', [req.params.PayID]);
  await connection.end();
  if(dataPaymentMethod.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลช่องทางการชำระเงิน ${req.params.PayID} ในระบบ`});
  req.body.PayName = dataPaymentMethod[0].PayName
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = 'SELECT * FROM tb_payment_method WHERE 1=1';

    if(req.query.filters){
      const filterStatus = (req.query.filters.PayStatus.length === 0) ? ['1', '0']: req.query.filters.PayStatus;
      sqlCommand = sqlCommand + ` AND PayName LIKE ? AND PayStatus IN (?)`;
      paramsQuery = [...paramsQuery,
      ...[
        `%${req.query.filters.PayName}%`, 
        filterStatus
        ]
      ];
      var [ TotalDataPay ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataPay ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    let current = 1;
    let pageSize = 10;
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataPay.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }

    const [ dataPaymentMethod ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataPaymentMethod,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataPay.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
})

router.put('/:PayID/Status', checkEmptyDataPaymentMethod, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query(`UPDATE tb_payment_method SET PayStatus = (CASE WHEN PayStatus = 1 THEN 0 ELSE 1 END) WHERE PayID = ?`, [
        req.params.PayID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะตัวเลือกชำระเงิน ${req.body.PayName} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
})

export default router;