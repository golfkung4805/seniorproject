import express from 'express';
import QRCode from 'qrcode';
import fs from 'fs';
import path from 'path';
import pkg from 'canvas';
import { connectDB } from '../config/connectDB.js';
import { removeImage } from '../removeImage.js'
import { validationTable, validationDataResult } from '../util/validate.js';

const router = express.Router();

const { createCanvas, loadImage } = pkg;

const __dirname = path.resolve();
const pathRoot = path.join(__dirname, 'uploads/qrTable');

const create = async (dataForQRcode, center_image, width, cwidth) => {
  const canvas = createCanvas(width, width);
  QRCode.toCanvas(
    canvas,
    dataForQRcode,
    {
      errorCorrectionLevel: "H",
      width,
      margin: 1,
      color: {
          dark: "#000000",
          light: "#ffffff",
      },
    }
  );

  const ctx = canvas.getContext("2d");
  const img = await loadImage(center_image);
  const center = (width - cwidth) / 2;
  ctx.drawImage(img, center, center, cwidth, cwidth);

  return canvas.toDataURL("image/png");
}

const SaveBase64toImage = async (TableID) => {
  const qrCode = await create(
    `${process.env.BASE_URL}/order/${TableID}`,
    "https://gitlab.com/golfkung4805/seniorproject/-/raw/master/API-SeniorProject/demologo.png",
    // "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABEVBMVEX/////vgCfB6khKzb/vAD+vxf/ugCYAKP/1oacAKaXALD/uQCcAKf///3/8M7/wQDw3fEbJjL/13/z5PT/8tb/5Kr68vr/+u3nzOm5YsD//P/MkdH//PTr1e3cteD16vbWqdrIh83gvOOpMrL/5rP/6bzlxuf/9d+zUrv/ykf/35z/0mvDfcn/3ZQAFjgOHCrQm9WvRbf/xTP/8dK9bMT/1HXJi8//xjv/zVrAdMayTrqsObT/z2Dv7/AAFSUABhxES1Pg4OIAABSChooAHTi4jhvMzc+mIa+0trh/g4ifoaVdYmkxOUNiZ27lrA27vb/SnxREQTFVSy+xiR0AEDkBIjZoVyyJbibuswk2ODOceiJEw5mmAAAMzElEQVR4nO2d+1viOBfHCxZqhwLCiChUxQsgCIq38S46zgwzzm1335l5Z/f//0O2RaFJe5ImaVJcn35/2We0SD57kpNzTtJE0xIlSpQoUaJEiRIlSpQoUaJEiRIlSgRpvX4w2tpp1GbdDmXaMEzLkWFtzrolinRgpB9lGYNZt0WJjs30VEZr1q1RoGXDA0xb6Vk3R4FWLIQwbbzAoWiigGnzfNbtka51AyO0DmbdIOnyE45m3SDpqvkIL2bdIPlKY57mBY5D7QInfIFz/gbmTI3lWbdHvjaxgWguzbo98oU50xc4WTjaQgaiWZ91a1Sojkbeq7NujQoteN30RQbeGtpNX2Yn1bTBtJsar2fdFkj56lFzu9vvz8+Xy+e9jY36Wmt1iaviUpsQWiuqGikmu73dvTotZnNZV7qj1CvTlWEY1uVOfW2BlXMy6T8jP2MXurvFJ66Up8yrqcuwLJf0oL7MQlmzxiPRfCYmtJvlwzFbKiCPcAJqGtbOYCH0b47jGst6DvHMYv8wB8LBhE+Uo0GYBxkYjs19/yfWW5trm/G6nqNyhkxHJHyEvFij/+3WSm8d+efqYCftDGe3o8dWfVucT9HxKIQupGH2WO2xfG65JeLJB4+Vcj3JrhyG4tEJXUdirDD4yqV62sFDPxcD4uKJ41hC8UIJXXvshHmdgYnjjREVVxibp2x4DIQu48o65btejwzoQ0qrU5UMMx8LodtX3xC/bNUI2O/RiOHzjaDylRQHHxuhw2iR/OoIBkybDUWADh8HHjOhOxzBuX0d6qJjQjXlqWaGk4+Z0GVsQF+ZjtOG7Q5b/9R1JDRlJnSG1goQsLZI41B+AS5fztH5HCwnNs1lU5nD086pzk/ojEag2S3LBB5VkPk3aQPQZdMzu/OVwmI1n3cfX8wKEMILvkvHRpAR7tIRZJ/liHQOXGe+2c5jHygIEbo9Ffj2hZ5/zjd3JANWSOGZ7tB1j4BPiBKmzUvIpy41DkwvcLMMySVU+wzuoA7eSdOGPyNM6IwwOMJZ2jx3UotxvcBqyAUsgAbUs6mTAuVDwoRO1ksMxpeWG4P6QHbuNA+NQD3XaeZpn4pA6KSOcdZmqqdAD83q89WQz0UhdMZZfKtNQA91umeXMPjQD0YhjBGxEuyh2WKF5ZNHHqHhypwm6IyI8XTUcqCHZlOhfPZiodKfv5rGNLWl9dXlzUHPLbIEM1kiohlDvSkfmCT0bJ/mXqrN/m4nlctidcWM9/v1Vn3HZKW00srriPah7ufbJY6//FH3KgVWTDP4g7WWM6sxMVqXigGrGV9jsxnS9HfU7ejEqk0m+PzyOVB4CUpxubudwlvsdFDwuXzhhExHIHQsubbFYEil+y7bvu6WzSxCjzV3Q0saIKGjFgOjQocaACxDD5V1hoyYRKhpm6GMlqVqD3Qbb7mebQafaTIm/GRCTVtLQ8ltDEPRZ8HsYSBEs7vMBTcaoVbrhZjRCFnaEFMVdzLZ3cADTN2ThVDTVkd0M5q0YrGgbHyayHX9fCd89dKw7zsm1Qsfh6LsZN4RNtEHhqBd5uJjINQa1J4qv5+eYYC6b5LoM6w28RJqq6Sa6NiIsv0pFmzrKdzHUOtt4oTa0hZlMJo9qYAVDDCDxaHVM84OykyoaRcURKkbahbRfFA/xBKJrggfKyH2ColCZ2Oj8wQO2IaqGRIJtRUyosTg7RQFxLpoN6SiH52Qgihvg+k8YiY9hQBWO4IG5CHULokeVZYRC+ggzLaRX3BPEUKENdJiqKyRaKMcWSTbBQumCgi118ToRo47vUIAc169KVitUUbo28COSMqcuI1YCkkHA8UMlYRaj+RtjOhlKRt1o53pjyMNQX5CbAc7ZsRGZEKkjyJudDvSEBQgXCD0U2srKmATQclNvUw/OiAnoe9tGaSbRpww8kgw4xXVyhIAeQlJey+i+hoko9BPJz88ieZEBQkJ/jTizoQ26kcnCZMcQG5C7UDF7pIO0kcnRQtJgPyEq7ARzY0IgAWkjx4+/Sy47BQXobYDGjHSZkRkUp+Eo31ZgAKEBCNGiNyaHs0kmAFWRuMjJCQZESZ9xIT6Y9LblAcoQtgCjSj+8sw2YsLHgLstrYuKEcJzovh84U32T27GjhyLRiV8AwY2ogMRMWHusfrrX/uNnxCOTk3B2nDGb8IrmX1UjBDe+iwYuKGOdBxxV+QCihHWwS2lYkcreOW1x4B0UTKgGCE8JVoifwoBGq/B5KOm9MyEdx/vP326f/hwDf4W9KaGyErbrmfCDP5vtYQPt8ObfVc3w88fgN+vgIQCOaKNmHBbk5LTsxB+vLkpzU1UGn4JMg6ggSjiTBG34oYztnQLQoTXn4dziErDzw+BrroMDUSRt7q9qU93I9KzOAjvSvsY4O0d0LAl8F0n/lNcED+Ta8sNR4mE16USBvgFbho0IwpMF2VsqsinFJgwQHiLAc4NIQtqsKsR2OmG5IUViUkvjfDhBgMsfSU0DaoN85cUkdw+W8WKNYCKxaIEwuu3GODczUdC2xoQYZp3Sf/E66RnWLEG4vv2/v1vEUic8H7fR/id0LY1MG7jre177XU6KdXNFP/4c+/du72/vvEj4oT4IKTYECwq8h42dIR1Ulq4Vvzfj3HTSj/+4EbECD8MfYSlT4TGgZEp7/rMvNdJO2iaGAT8tjdp0M/fkQj9ndQxojfZX6OhDZgi8gamntWyXa1IM+Gvn5MG/fw/rxExws/+XjpX+jz95S3aY8HVUk4bVr2Bl23Ts8KfXoPmIhF+CRDO3Xx9cpBf3z5IJkT6pa5RJ/vfe16DfkQiDPA52t9/+HD34aG0v38fRsjpaZDEabdJNWEGIdyTbUOX8cZJpZz/oDYEPQ0nITIMK/TiU/HvacNKf0UivAUJJ6AoIZRccM74VaSThsRrxX/eTU34PhLhp4AvRUck6kvXIEK+pQt0eqCGM67+fvI1P7lNiBN+vKEQDtEsEYza+OLSMkciUfz9517JmQz3fvHy+Qjv/DM+otIt+iRUbePc/cVX9y2+/1Wa+8Uf0fijNspAvEGHIZg98RVM85ypUnEsfkAf4XeyEYfYg9BKMN9BEVJXX9gJCfNFwIRa9EoULQ5VSRiIvSejEK9mwGEpVzWxr6JiwUCoPcCIvmoGmB7yBW0qympMhNonCPGtr2QKFjH4FhDlLqHxEGr3AcTSjb8mDO1v41sEzscFCNW8v+/jFdPhrb8iDJ67Y5KPWgIUUnZSS+iYcVrWL+0PvwRrNfAw5DpLoRCXKyWszFx//Lo/dPXlnnlhhu8lr9gmC8r64fXd3R2cLNQknF8mb0eQOCFZUGLBu8jNE3fHT3gBrh7yHWkifyVUIiG8Z9/gK3iHZoSzJAT3CfMuPMU24QsQgn6Ge1tbXHwihOAKN3c1ODYT8hPWwGNeuN/uesaE4G4h7k6qYkuCJEJwBd8h5Fw6jC/w5iaEXybl36QwPi6dWTESgttMRHbqz/MoUgDESQi/gxj9pSC67CipFh8h4aUgNcdjeIqPkNBHlV/iEckv8RDWSC89Kb/wKcqWTB5CMKdwD45QRjZRlDidg3CD8PKh9NNYgzqJhbBBersyhqt0uhEqAsyE8HskMThSV0cxEBIBlZ9M54p3oUqAkHSodVwXzURwNWyExDfx47r8MMJLGEyEJCcjsh9RTFW1hG/Ih2HF4WbGEl+qCies7ZAB47sMib6lKBLhAuXEtrj6qCtRwFDCOu0sszjv5xSe9OmEC5e08+goV5fIV16FDWsb1MPopB+eT5fohEEhDDn6Ms5BOJZgCkUk3BzRz72M5TRoTAWxTB8mrDVC+GZyC7DYORIQ4ULPCj1iN4akMCihfhogfD3YYjjT25jJ7aNCb9BihOubvRHTkeWG3OMgmSVymMQT4dJ6q3E+Yj133pjZbeoCQzFzfn58sTWyDIP97oBZWdAVf6KYeWW5YmR7BIwzlPErf8qLyH+/hdKj2BkQeR0qL6Gl+mbDcEROK3ISWmllt/6xI/Id9clHCN/2FLt2eRB5CK1ZOlFMPCfSchBa5qyHoKdF9te+2QmNg2fRQ5+UZz7FhpXQVHbtpqi2GZf32QgtY+U5GfBR9hXTAd9MhMYo/mSQRQWWW3MZCE31a7zCYrhZNpTQNOsx12O4FH47MJ3Qcvie3wD0afuQykgjtIxRY9bNZ1LhjHLjE5HQMo2d2O65jyy7e5gjQMKEDt7lQMEVMirV7p5C164BhJaT718O4i6GSlG1eeKaEsfECC3HdsbofO3ZOxeK7EL/KqOPb9F7RHUIx2UM02EzRzv11n+Zbiq73az0T646Gfc65FdWOn25ctxrbC78xwZeokSJEiVKlChRokSJEiVKlChRokSJEiVKlChRIlj/ApgIIGDBCEskAAAAAElFTkSuQmCC",
    500,
    100
  );

  let base64String = qrCode; // Not a real image
  // Remove header
  let base64Image = base64String.split(';base64,').pop();
  fs.writeFile(`uploads/qrTable/${TableID}.png`, base64Image, {encoding: 'base64'}, (err) => {});
}

const checkEmptyDataTable = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataTable ] = await connection.query('SELECT * FROM tb_table WHERE TableID = ?', [req.params.TableID]);
  await connection.end();
  if(dataTable.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลโต๊ะ ${req.params.TableID} ในระบบ`});
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = 'SELECT * FROM tb_table WHERE 1=1';

    if(req.query.filters){
      const filterStatus = (req.query.filters.TableStatus.length === 0) ? ['1', '0']: req.query.filters.TableStatus;
      sqlCommand = sqlCommand + ` AND TableID LIKE ? AND TableName LIKE ? AND TableStatus IN (?)`;
      paramsQuery = [...paramsQuery,
      ...[
        `%${req.query.filters.TableID || ''}%`, 
        `%${req.query.filters.TableName || ''}%`, 
        filterStatus
        ]
      ];
      var [ TotalDataTable ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataTable ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    let current = 1;
    let pageSize = 10;
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataTable.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }

    const [ dataTables ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataTables,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataTable.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/:TableID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataTable ] = await connection.query('SELECT * FROM tb_table WHERE TableID = ?', [req.params.TableID]);
    res.status(200).json(dataTable);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/status', async (req, res) => {
  const connection = await connectDB();
  try {
    
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/image/:TableQR', function (req, res) {
  fs.stat(`${pathRoot}/${req.params.TableQR}`, (err, stat) => {
    if (stat && stat.isFile()) {
      res.sendFile(`${req.params.TableQR}`, { root: pathRoot });
      return;
    }
    res.sendFile(`nofile.png`, { root: pathRoot });
  });
  // res.sendFile(`/uploads/qrTable/TableID-0016.png`, { root: path.join(__dirname, 'public') });
});

router.post('/', validationTable, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ sequenceTable ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbTable'`);
    const LastID = sequenceTable[0]['LastID'];
    let TID = 'TableID-0000';
    TID = TID.substr(0, TID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_table(TableID, TableName, TableQR) VALUES (?, ?, ?)', [
      TID,
      req.body.TName,
      `${TID}.png`
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbTable'`);
    await connection.commit();

    // QRCode.toFile("uploads/qrTable/test.png","https://it63011270020.xyz",{width:500});
    SaveBase64toImage(TID);

    res.status(200).json({msg: 'เพิ่มข้อมูลโต๊ะอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/import', validationTable, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    const [ sequenceTable ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbTable'`);
    let LastID = parseInt(sequenceTable[0]['LastID']);
    let TID = 'TableID-0000';
    let sqlCommand = `INSERT INTO tb_table(TableID, TableName, TableQR) VALUES`;
    let paramsQuery = [];

    for(let i = 0; i < req.body.length; i++){
      TID = TID.substr(0, TID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        TID,
        req.body[i].TName,
        `${TID}.png`
      ]];
      SaveBase64toImage(TID);
        // QRCode.toFile("uploads/qrTable/test.png","https://it63011270020.xyz",{width:500});
      LastID += 1;
    }
    
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID + ? WHERE tableName = 'TbTable'`, [ req.body.length ]);
    await connection.commit();

    res.status(200).json({msg: 'นำเข้าข้อมูลโต๊ะอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:TableID', checkEmptyDataTable, validationTable , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    await connection.query('UPDATE tb_table SET TableName = ? WHERE TableID = ?', [
      req.body.TName,
      req.params.TableID
    ]);

    res.status(200).json({msg: `แก้ไขข้อมูลโต๊ะอาหาร ${req.params.TableID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:TableID/Status', checkEmptyDataTable, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_table SET TableStatus = (CASE WHEN TableStatus = 1 THEN 0 ELSE 1 END) WHERE TableID = ?', [
      req.params.TableID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้งานโต๊ะ ${req.params.TableID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:TableID', checkEmptyDataTable, async (req, res) => {
  const connection = await connectDB();
  try {
    const [ TableQR ] = await connection.query('SELECT TableQR FROM tb_table WHERE TableID = ?', [ req.params.TableID ]);
    removeImage(TableQR, pathRoot);

    await connection.query('DELETE FROM tb_table WHERE TableID = ?', [req.params.TableID]);

    res.status(200).json({msg: `ลบข้อมูลโต๊ะอาหาร ${req.params.TableID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;