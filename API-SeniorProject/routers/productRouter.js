import express from 'express';
import multer from 'multer';

import _ from 'lodash'
import fs from 'fs';
import path from 'path';

import { connectDB } from '../config/connectDB.js';
import { removeImage } from '../removeImage.js'
import { 
    validationProduct, 
    validationProductReview, 
    validationDataResult 
} from '../util/validate.js';

const router = express.Router();

const __dirname = path.resolve();
const pathRoot = path.join(__dirname, 'uploads/product');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, `uploads/product/`);
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.jpg`);
  }
});

const upload = multer({ storage });

const checkEmptyDataProduct = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataProduct ] = await connection.query('SELECT * FROM tb_product WHERE PDID = ?', [req.params.PDID]);
  await connection.end();
  if(dataProduct.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลอาหาร ${req.params.PDID} ในระบบ`});
  next();
}

// ! Manage Product
// ? อาหารทั้งหมด
router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
      // const search = (req.query.search) ? `PDName LIKE '%${req.query.search}%'` : '1'; // search ชื่ออาหาร
      // const order = (req.query.order) ? req.query.order : 'ASC'; // sort 
      // const type = (req.query.type) ? `AND CID = '${req.query.type}'` : ''; // sort ประเภทอาหาร

      // const [ dataProducts ] = await connection.query(`SELECT * FROM tb_product WHERE ${search} ${type} AND RDStatus = 1 ORDER BY PDPrice ${order}`);
    const [ dataCategory ] = await connection.query(`SELECT CID FROM tb_category`);
    // console.log(_.map(dataCategory, 'CID'))
    let paramsQuery = []
    let sqlCommand = 'SELECT *,(SELECT PDImage FROM tb_product_image WHERE PDID = tb_product.PDID ORDER BY PDImageIndex ASC LIMIT 0,1)as PDImage  FROM tb_product INNER JOIN tb_category ON tb_product.CID = tb_category.CID WHERE 1=1';
    if(req.query.filters){
      const filterStatus = (req.query.filters.PDStatus.length === 0)? ['1','0']: req.query.filters.PDStatus;
      const filterCategory = (req.query.filters.CName && req.query.filters.CName.length === 0)? _.map(dataCategory, 'CID'): req.query.filters.CName || _.map(dataCategory, 'CID');

      sqlCommand = sqlCommand + ` AND PDID LIKE ? AND PDName LIKE ? AND PDPrice LIKE ? AND PDStatus IN (?) AND tb_category.CID IN (?)`;
      paramsQuery = [...paramsQuery, 
      ...[`%${req.query.filters.PDID || ''}%`, 
          `%${req.query.filters.PDName || ''}%`, 
          // `%${req.query.filters.PDDesc}%`, 
          `%${req.query.filters.PDPrice || ''}%`,
          filterStatus,
          filterCategory
        ]
      ];
      var [ TotalDataProducts ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataProducts ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    let current = 1;
    let pageSize = 10;
    
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataProducts.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }
    
    const [ dataProducts ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataProducts,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataProducts.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

//  ? เลือกอาหาร Not use
router.get('/:PDID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataProduct ] = await connection.query('SELECT * FROM tb_product INNER JOIN tb_category ON tb_product.CID = tb_category.CID WHERE PDID = ?', [req.params.PDID]);
    if(dataProduct.length <= 0){
        return res.status(200).json({msg: 'ไม่พบข้อมูลอาหาร'});
    }

    const [ dataCategoryIngredients ] = await connection.query('SELECT CIID, CIName, CIMultiple FROM tb_category_ingredients WHERE CID = ?', [ dataProduct[0]['CID'] ]);
    dataProduct[0]['CategoryOption'] = [];
    
    for(let i = 0; i < dataCategoryIngredients.length; i++){
        const [ dataCategoryIngredientsOption ] = await connection.query('SELECT CIOID, CIID, CIOName, CIOPrice FROM tb_category_ingredients_option WHERE CIID = ?', [ dataCategoryIngredients[i]['CIID'] ]);
        dataProduct[0]['CategoryOption'][i] = {};
        dataProduct[0]['CategoryOption'][i]['CIID'] = dataCategoryIngredients[i]['CIID'];
        dataProduct[0]['CategoryOption'][i]['Name'] = dataCategoryIngredients[i]['CIName'];
        dataProduct[0]['CategoryOption'][i]['CIMultiple'] = dataCategoryIngredients[i]['CIMultiple'];
        dataProduct[0]['CategoryOption'][i]['Desc'] = dataCategoryIngredientsOption;
    }

    const [ dataImages ] = await connection.query('SELECT * FROM tb_product_image WHERE PDID = ?', [req.params.PDID]);
    // const [ dataRatings ] = await connection.query('SELECT * FROM tb_product_rating WHERE PDID = ?', [req.params.PDID]);
    dataProduct[0]['images'] = dataImages;
    // dataProduct[0]['ratings'] = dataRatings;

    res.status(200).json(dataProduct);
  } catch (error) {
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/ImageList/:PDID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ ProductImage ] = await connection.query('SELECT * FROM tb_product_image WHERE PDID = ? ORDER BY PDImageIndex ASC', [req.params.PDID]);

    res.status(200).json(ProductImage);
  } catch (error) {
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/image/:PDImage', function (req, res) {
  fs.stat(`${pathRoot}/${req.params.PDImage}`, (err, stat) => {
    if (stat && stat.isFile()) {
      res.sendFile(`${req.params.PDImage}`, { root: pathRoot });
      return;
    }
    res.sendFile(`error.png`, { root: pathRoot });
  });
});

// ? เพิ่มอาหาร
router.post('/', upload.array('image'), validationProduct, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    const [ sequenceProduct ] = await connection.query(`SELECT LastID FROM tb_sequence WHERE tableName = 'TbProduct'`);
    const LastID = sequenceProduct[0]['LastID'];
    let PDID = 'PD-00000';
    PDID = PDID.substr(0, PDID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_product(PDID, PDName, PDDesc, PDPrice, CID) VALUES (?, ?, ?, ?, ?)', [
      PDID,
      req.body.PDName,
      req.body.PDDesc,
      req.body.PDPrice,
      req.body.CID
    ]);

    for(let i = 0; i < req.files.length; i++){
      await connection.query('INSERT INTO tb_product_image(PDID, PDImage, PDImageIndex) VALUES (?, ?, ?)', [
        PDID,
        req.files[i].filename,
        (i+1),
      ]);
    }
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbProduct'`);
    await connection.commit();

    res.status(201).json({msg: 'เพิ่มข้อมูลอาหารสำเร็จ'});
  } catch (error) {
    console.error(error);
    req.files.forEach(filePath => {
      fs.unlinkSync(filePath['path']);
      return;
    });
    await connection.rollback();
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการเพิ่มข้อมูลอาหาร', error});
  } finally {
    await connection.end();
  }
});

// ? เพิ่มอาหาร แบบ import file
router.post('/import', async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    const [ sequenceProduct ] = await connection.query(`SELECT LastID FROM tb_sequence WHERE tableName = 'TbProduct'`);
    let LastID = parseInt(sequenceProduct[0]['LastID']);
    let PDID = 'PD-00000';
    let sqlCommand = `INSERT INTO tb_product(PDID, PDName, PDDesc, PDPrice, CID) VALUES`;
    let paramsQuery = [];

    for(let i = 0; i < req.body.length; i++){
      PDID = PDID.substr(0, PDID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?, ?, ?),`;
      paramsQuery = [...paramsQuery, ...[
        PDID,
        req.body[i].PDName,
        req.body[i].PDDesc,
        req.body[i].PDPrice,
        req.body[i].CID
      ]];
      LastID += 1;
    }
    
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID + ? WHERE tableName = 'TbProduct'`, [ req.body.length ]);
    await connection.commit();

    res.status(201).json({msg: 'นำเข้าข้อมูลอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการนำเข้าข้อมูลอาหาร', error});
  } finally {
    await connection.end();
  }
});

// ? แก้ไขอาหาร
router.put('/:PDID', checkEmptyDataProduct, upload.array('image'), validationProduct, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    await connection.query('UPDATE tb_product SET PDName = ?, PDDesc = ?, PDPrice = ?, CID = ? WHERE PDID = ?', [
      req.body.PDName,
      req.body.PDDesc,
      req.body.PDPrice,
      req.body.CID,
      req.params.PDID
    ]);

    let imageInList = {};
    let fileObjInList = {};
    const lengthImageList = (typeof req.body.imageList==="string")? 1: req.body.imageList.length;
    for(let i = 0; i < lengthImageList; i++) {
      imageInList = lengthImageList === 1? JSON.parse(req.body.imageList): JSON.parse(req.body.imageList[i]);
      if(imageInList.uid === ""){
        fileObjInList = req.files.find((x) => x.originalname === imageInList.name);
        await connection.query('INSERT INTO tb_product_image(PDID, PDImage, PDImageIndex) VALUES (?, ?, ?)', [
          req.params.PDID,
          fileObjInList.filename,
          (imageInList.index+1)
        ]);
      }else{
        await connection.query('UPDATE tb_product_image SET PDImageIndex = ? WHERE _ID = ?', [
          (imageInList.index+1),
          imageInList.uid
        ]);
      }
    }
    await connection.commit();

    res.status(200).json({msg: `แก้ไขข้อมูลอาหาร ${req.params.PDID} สำเร็จ`});
  } catch (error) {
    await connection.rollback();
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการแก้ไขข้อมูลอาหาร', error});
  } finally {
    await connection.end();
  }
});

router.put('/:PDID/Status', checkEmptyDataProduct, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_product SET PDStatus = (CASE WHEN PDStatus = 1 THEN 0 ELSE 1 END) WHERE PDID = ?', [
      req.params.PDID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้งานอาหาร ${req.params.PDID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

// ? ลบอาหาร
// ! เหลือ เลือก ลบ ราคา รูปภาพ ~ ~
router.delete('/:PDID', checkEmptyDataProduct, async (req, res) => {
  const connection = await connectDB();
  try {
    const [ PDImages ] = await connection.query('SELECT PDImage FROM tb_product_image WHERE PDID = ?', [req.params.PDID]);
    removeImage(PDImages, pathRoot);
  
    await connection.query('DELETE FROM tb_product WHERE PDID = ?', [req.params.PDID]);
    
    res.status(200).json({msg: `ลบข้อมูลอาหาร ${req.params.PDID} สำเร็จ`});
  } catch (error) {
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการลบข้อมูลอาหาร', error});
  } finally {
    await connection.end();
  }
});

router.delete('/image/:_ID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ pathPDImage ] = await connection.query('SELECT PDImage FROM tb_product_image WHERE _ID = ?', [req.params._ID]);
    removeImage(pathPDImage, pathRoot);
    
    await connection.query('DELETE FROM tb_product_image WHERE _ID = ?', [req.params._ID]);
    
    res.status(200).json({msg: 'ลบรูปภาพอาหารสำเร็จ'});
  } catch (error) {
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการลบรูปภาพอาหาร', error});
  } finally {
    await connection.end();
  }
});

// ! Manage Review Product ==========================================================
const updateAverageRatingProduct = async (PDID) => {
  const connection = await connectDB();
  await connection.query('UPDATE tb_product SET PDRating = (SELECT ROUND(AVG(`RevRating`),1) FROM tb_product_rating WHERE PDID = ?)', [PDID]);
  await connection.end();
}

router.post('/:PDID/:PDID/Review', validationProductReview, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_product_rating(PDID, RevRating, RevComment, PDID, ODD_ID) VALUES (?, ?, ?, ?, ?)', [
      req.params.PDID,
      req.body.RevRating,
      req.body.RevComment,
      req.params.PDID,
      'GET ODD_ID'
    ]);
    updateAverageRatingProduct(req.params.PDID);
    await connection.commit();
    
    res.status(201).json('เพิ่มรีวิวอาหารสำเร็จ');
  } catch (error) {
    await connection.rollback();
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการเพิ่มรีวิวอาหาร', error});
  } finally {
    await connection.end();
  }
});

router.put('/:PDID/:_ID/Review', validationProductReview , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataRating ] = await connection.query('SELECT PDID, PDID FROM tb_product_rating WHERE _ID = ?', [req.params._ID]);
    if(dataRating.length <= 0){
      return res.status(200).json({msg: 'ระบบไม่พบรีวิวของคุณ'})
    }
    if(req.params.PDID != dataRating[0]['PDID']){
      return res.status(401).json({msg: 'คุณไม่ได้แก้ไขรีวิวของคนอื่นได้'})
    }
    await connection.beginTransaction();
    await connection.query('UPDATE tb_product_rating SET RevRating = ?, RevComment = ? WHERE _ID = ?', [
      req.body.RevRating,
      req.body.RevComment,
      req.params._ID
    ]);
    updateAverageRatingProduct(dataRating[0]['PDID']);
    await connection.commit();

    res.status(200).json({msg: 'แก้ไขรีวิวสำเร็จ'})
  } catch (error) {
    await connection.rollback();
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการแก้ไขรีวิว', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:PDID/:_ID/Review', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataRating ] = await connection.query('SELECT PDID, PDID FROM tb_product_rating WHERE _ID = ?', [req.params._ID]);
    if(dataRating.length <= 0){
        return res.status(200).json({msg: 'ระบบไม่พบรีวิวของคุณ'})
    }
    if(req.params.PDID != dataRating[0]['PDID']){
        return res.status(401).json({msg: 'คุณไม่ได้ลบรีวิวของคนอื่นได้'})
    }
    await connection.query('DELETE FROM tb_product_rating WHERE _ID = ?', [req.params._ID]);
    updateAverageRatingProduct(dataRating[0]['PDID']);

    res.status(200).json({msg: 'ลบรีวิวอาหารสำเร็จ'});
  } catch (error) {
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการลบรีวิวอาหาร', error});
  } finally {
    await connection.end();
  }
});

export default router;