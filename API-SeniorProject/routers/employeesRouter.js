import express from 'express';
import bcrypt from 'bcryptjs';
import _ from 'lodash'

import { connectDB } from '../config/connectDB.js';

import { generateTokenEmp } from '../utils.js';
import { validationPerson, validationDataResult } from '../util/validate.js';

const router = express.Router();

const checkEmptyDataEmployees = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataEmployees ] = await connection.query('SELECT * FROM tb_employees WHERE EmpID = ?', [req.params.EmpID]);
  await connection.end();
  if(dataEmployees.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลพนักงาน ${req.params.EmpID} ในระบบ`});
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = `SELECT EmpID, EmpUsername, EmpGender, EmpFirstname, EmpLastname, EmpNickname, EmpTel, EmpEmail, EmpRole, EmpStatus, CONCAT(EmpGender, EmpFirstname,\' \',EmpLastname)as EmpName FROM tb_employees WHERE 1=1 AND EmpRole NOT IN('admin')`;
    if(req.query.filters){
      const filterRole = (req.query.filters.EmpRole.length === 0) ? ['counter', 'kitchen']: req.query.filters.EmpRole;
      const filterStatus = (req.query.filters.EmpStatus.length === 0) ? ['1', '0']: req.query.filters.EmpStatus;
      sqlCommand = sqlCommand + ` AND EmpID LIKE ? AND EmpUsername LIKE ? AND CONCAT(EmpGender, EmpFirstname,\' \',EmpLastname) LIKE ? AND EmpNickname LIKE ? AND EmpTel LIKE ? AND EmpEmail LIKE ? AND EmpRole IN (?) AND EmpStatus IN (?)`;
      paramsQuery = [...paramsQuery,
      ...[
        `%${req.query.filters.EmpID || ''}%`, 
        `%${req.query.filters.EmpUsername || ''}%`, 
        `%${req.query.filters.EmpName || ''}%`, 
        `%${req.query.filters.EmpNickname || ''}%`, 
        `%${req.query.filters.EmpTel || ''}%`, 
        `%${req.query.filters.EmpEmail || ''}%`,
        filterRole,
        filterStatus
        ]
      ];

      var [ TotalDataEmployees ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataEmployees ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    if(req.query.pagination){
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(parseInt(req.query.pagination.current)-1)*10, parseInt(req.query.pagination.pageSize)]];
    }

    const [ dataEmployees ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataEmployees,
      "current": parseInt(req.query.pagination.current),
      "pageSize": parseInt(req.query.pagination.pageSize),
      "total": TotalDataEmployees.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/:EmpID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ user ] = await connection.query('SELECT *, CONCAT(EmpGender, EmpFirstname,\' \',EmpLastname)as EmpName FROM tb_employees WHERE EmpID = ?', [ req.params.EmpID ]);
    if(!_.isEmpty(user)){
      res.status(200).json({
        _id: user[0].EmpID,
        username: user[0].EmpUsername,
        name: user[0].EmpName,
        nickname: user[0].EmpNickname,
        tel: user[0].EmpTel,
        email: user[0].EmpEmail,
        isRole: user[0].EmpRole,
        token: generateTokenEmp(user[0]),
      });
      return;
    }
    res.status(401).json({ msg: 'เกิดความผิดพลาดบางอย่าง' });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
})

router.post('/', validationPerson, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ sequenceEmployees ] = await connection.query(`SELECT * FROM tb_sequence WHERE tableName = 'TbEmployees'`);
    const LastID = sequenceEmployees[0]['LastID'];
    let EmpID = 'Emp-00000';
    EmpID = EmpID.substr(0, EmpID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_employees(EmpID, EmpUsername, EmpPasswd, EmpGender, EmpFirstname, EmpLastname, EmpNickname, EmpTel, EmpEmail, EmpRole)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
      EmpID,
      req.body.Username,
      bcrypt.hashSync(req.body.Passwd),
      req.body.Gender,
      req.body.Firstname,
      req.body.Lastname,
      req.body.Nickname,
      req.body.Tel,
      req.body.Email,
      req.body.Role
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbEmployees'`);
    await connection.commit();
    
    res.status(200).json({msg: 'เพิ่มข้อมูลพนักงานสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/import', validationPerson, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    const [ sequenceEmployees ] = await connection.query(`SELECT * FROM tb_sequence WHERE tableName = 'TbEmployees'`);
    let LastID = sequenceEmployees[0]['LastID'];
    let EmpID = 'Emp-00000';
    let sqlCommand = `INSERT INTO tb_employees(EmpID, EmpUsername, EmpPasswd, EmpGender, EmpFirstname, EmpLastname, EmpNickname, EmpTel, EmpEmail) VALUES`;
    let paramsQuery = [];

    for(let i = 0; i < req.body.length; i++){
      EmpID = EmpID.substr(0, EmpID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?, ?, ?, ?, ?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        EmpID,
        req.body[i].Username,
        bcrypt.hashSync(req.body[i].Passwd),
        req.body[i].Gender,
        req.body[i].Firstname,
        req.body[i].Lastname,
        req.body[i].Nickname,
        req.body[i].Tel,
        req.body[i].Email
      ]];
      LastID += 1;
    }
    
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID + ? WHERE tableName = 'TbEmployees'`, [ req.body.length ]);
    await connection.commit();

    res.status(200).json({msg: 'นำเข้าข้อมูลพนักงานสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการนำเข้าข้อมูล', error});
  } finally {
    await connection.end();
  }
});

router.put('/:EmpID', checkEmptyDataEmployees, validationPerson , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    let paramsQuery = [];
    let sqlCommand = 'UPDATE tb_employees SET';
    if(req.body.Passwd!=='-'){
      sqlCommand = sqlCommand + ' EmpPasswd = ?,';
      paramsQuery = [...paramsQuery,
        bcrypt.hashSync(req.body.Passwd),
      ];
    }
    if(req.body.Role!=='-'){
      sqlCommand = sqlCommand + ' EmpRole = ?,';
      paramsQuery = [...paramsQuery,
        req.body.Role,
      ];
    }
    sqlCommand = sqlCommand + ' EmpGender = ?, EmpFirstname = ?, EmpLastname = ?, EmpNickname = ?, EmpTel = ?, EmpEmail = ?, updatedAt = CURRENT_TIMESTAMP() WHERE EmpID = ?';
    paramsQuery = [...paramsQuery, 
      req.body.Gender,
      req.body.Firstname,
      req.body.Lastname,
      req.body.Nickname,
      req.body.Tel,
      req.body.Email,
      req.params.EmpID
    ];

    await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({msg: `แก้ไขข้อมูลพนักงาน ${req.params.EmpID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:EmpID/Status', checkEmptyDataEmployees, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_employees SET EmpStatus = (CASE WHEN EmpStatus = 1 THEN 0 ELSE 1 END) WHERE EmpID = ?', [
      req.params.EmpID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้งานพนักงาน ${req.params.EmpID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:EmpID', checkEmptyDataEmployees, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_employees WHERE EmpID = ?', [req.params.EmpID]);

    res.status(200).json({msg: `ลบข้อมูลพนักงาน ${req.params.EmpID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/signin', async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ user ] = await connection.query('SELECT *, CONCAT(EmpGender, EmpFirstname,\' \',EmpLastname)as EmpName FROM tb_employees WHERE EmpUsername = ? OR EmpEmail = ? OR EmpTel = ?', [
      req.body.Username,
      req.body.Username,
      req.body.Username
    ]);
    if(!_.isEmpty(user)){
      if(user[0].EmpStatus === 0) return res.status(401).json({msg: `บัญชีนี้ถูกระงับการใช้งานกรุณาติดต่อเจ้าของร้าน`});
      if(bcrypt.compareSync(req.body.Passwd, user[0].EmpPasswd)){
        res.status(200).json({
          _id: user[0].EmpID,
          username: user[0].EmpUsername,
          name: user[0].EmpName,
          nickname: user[0].EmpNickname,
          tel: user[0].EmpTel,
          email: user[0].EmpEmail,
          isRole: user[0].EmpRole,
          token: generateTokenEmp(user[0]),
        });
        return;
      }
    }
    res.status(401).json({ msg: 'อีเมลหรือรหัสผ่านไม่ถูกต้อง' });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;