import express from 'express';
import multer from 'multer';
import _ from 'lodash'
import { connectDB } from '../config/connectDB.js';
import { validationCategory, validationCategoryIngredientsOption, validationDataResult } from '../util/validate.js';

const router = express.Router();

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, `uploads/`);
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.jpg`);
  }
});
const upload = multer({ storage });

const checkEmptyDataCategory = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataCategory ] = await connection.query('SELECT * FROM tb_category WHERE CID = ?', [req.params.CID]);
  await connection.end();
  if(dataCategory.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลประเภทอาหาร ${req.params.CID} ในระบบ`});
  next();
}

const checkEmptyDataCategoryIngredient = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataCategoryIngredient ] = await connection.query('SELECT * FROM tb_category_ingredients WHERE CIID = ?', [req.params.CIID]);
  await connection.end();
  if(dataCategoryIngredient.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลส่วนประกอบ ${req.params.CIID} ในระบบ`});
  next();
}

const checkEmptyDataCategoryOption = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataCategoryOption ] = await connection.query('SELECT * FROM tb_category_ingredients_option WHERE CIOID = ?', [req.params.CIOID]);
  await connection.end();
  if(dataCategoryOption.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลตัวเลือก ${req.params.CIOID} ในระบบ`});
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = 'SELECT * FROM tb_category WHERE 1=1';
    
    if(req.query.filters){
      const filterStatus = (req.query.filters.CStatus.length === 0) ? ['1', '0']: req.query.filters.CStatus;
      sqlCommand = sqlCommand + ` AND CID LIKE ? AND CName LIKE ? AND CStatus IN (?)`;
      paramsQuery = [...paramsQuery,
      ...[
        `%${req.query.filters.CID || ''}%`, 
        `%${req.query.filters.CName || ''}%`, 
        filterStatus
        ]
      ];

      var [ TotalDataCategorys ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataCategorys ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    let current = 1;
    let pageSize = 10;
    if(req.query.pagination){
      current = parseInt(req.query.pagination.current) || 1;
      pageSize = (req.query.pagination.pageSize === "all")? TotalDataCategorys.length: parseInt(req.query.pagination.pageSize);
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(current-1)*pageSize, pageSize]];
    }
    
    const [ dataCategorys ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataCategorys,
      "current": current,
      "pageSize": pageSize,
      "total": TotalDataCategorys.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/ingredient/:CID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataCategoryIngredients ] = await connection.query('SELECT *, (SELECT COUNT(*) FROM tb_category_ingredients_option WHERE CIID = tb_category_ingredients.CIID)as isEmpty FROM tb_category_ingredients WHERE CID = ?', [ req.params.CID ]);

    return res.status(200).json(dataCategoryIngredients);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/option/:CIID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataCategoryIngredientsOption ] = await connection.query('SELECT * FROM tb_category_ingredients_option WHERE CIID = ?', [ req.params.CIID ]);

    return res.status(200).json(dataCategoryIngredientsOption);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/', validationCategory, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ sequenceCategory ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCategory'`);
    let LastID = sequenceCategory[0]['LastID'];
    let CID = 'C-00000';
    CID = CID.substr(0, CID.length - String(LastID).length) + LastID;

    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_category(CID, CName, CIcon) VALUES (?, ?, ?)', [
      CID,
      req.body.CName,
      req.body.CIcon
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbCategory'`);

    const [ sequenceCategoryIngredients ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCategoryIngredients'`);
    LastID = sequenceCategoryIngredients[0]['LastID'];
    let CIID = 'CI-00000';

    let sqlCommand = `INSERT INTO tb_category_ingredients(CIID, CID, CIName, CIMultiple) VALUES`;
    let paramsQuery = [];

    for (var i = 0; i < req.body.Ingredient.length; i++){
      CIID = CIID.substr(0, CIID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        CIID,
        CID,
        req.body.Ingredient[i].CIName,
        req.body.Ingredient[i].CIMultiple
      ]];
      LastID += 1;
    }
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = ? WHERE tableName = 'TbCategoryIngredients'`, [ LastID ]);
    await connection.commit();
    
    res.status(200).json({CID, msg: 'เพิ่มข้อมูลประเภทอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/option/', validationCategoryIngredientsOption, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];
    
    const [ sequenceCategoryIngredientsOption ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCategoryIngredientsOption'`);
    let LastID = sequenceCategoryIngredientsOption[0]['LastID'];
    let CIOID = 'CIO-00000';

    let sqlCommand = `INSERT INTO tb_category_ingredients_option(CIOID, CIID, CIOName, CIOPrice) VALUES`;
    let paramsQuery = [];

    await connection.beginTransaction();

    for (var i = 0; i < req.body.Option.length; i++){
      CIOID = CIOID.substr(0, CIOID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        CIOID,
        req.body.CIID,
        req.body.Option[i].CIOName,
        req.body.Option[i].CIOPrice
      ]];
      LastID += 1;
    }
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = ? WHERE tableName = 'TbCategoryIngredientsOption'`, [ LastID ]);
    await connection.commit();

    res.status(200).json({CID: req.body.CID[0], CIID: req.body.CIID, msg: 'เพิ่มข้อมูลตัวเลือกประเภทอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/import', async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body.CategoryData;

    await connection.beginTransaction();
    const [ sequenceCategory ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` IN ('TbCategory', 'TbCategoryIngredients', 'TbCategoryIngredientsOption')`);
    
    let LastCategoryID = parseInt(sequenceCategory.find((x)=>x.tableName === 'TbCategory')['LastID']);
    let LastCategoryIngredientsID = parseInt(sequenceCategory.find((x)=>x.tableName === 'TbCategoryIngredients')['LastID']);
    let LastCategoryIngredientsOptionID = parseInt(sequenceCategory.find((x)=>x.tableName === 'TbCategoryIngredientsOption')['LastID']);

    let CID = 'C-00000';

    let sqlCommand = `INSERT INTO tb_category(CID, CName) VALUES`;
    let paramsQuery = [];
    let sqlCommandIngredient = `INSERT INTO tb_category_ingredients(CIID, CID, CIName) VALUES`;
    let paramsQueryIngredient = [];
    let sqlCommandIngredientOption = `INSERT INTO tb_category_ingredients_option(CIOID, CIID, CIOName, CIOPrice) VALUES`;
    let paramsQueryIngredientOption = [];

    for(let i = 1; i < req.body.length; i++){
      if(_.isEmpty(req.body[i][0]) || _.isEmpty(req.body[i][1])){
        throw 'ข้อมูลประเภทหรือส่วนประกอบต้องไม่ว่าง';
      }
      
      CID = CID.substr(0, CID.length - String(LastCategoryID).length) + LastCategoryID;
      sqlCommand += ` (?, ?),`
      paramsQuery = [...paramsQuery, ...[
        CID,
        req.body[i][0]
      ]];
      
      const lenData = Math.floor((req.body[i].length-1)/3);
      let index = 1;
      let CIID = 'CI-00000';
      let splitName = [], splitPrice = [];
      
      for(let j = 1; j <= lenData; j++){
        CIID = CIID.substr(0, CIID.length - String(LastCategoryIngredientsID).length) + LastCategoryIngredientsID;
        sqlCommandIngredient += ` (?, ?, ?),`
        paramsQueryIngredient = [...paramsQueryIngredient, ...[
          CIID,
          CID,
          req.body[i][index]
        ]];

        let CIOID = 'CIO-00000';

        splitName = req.body[i][index+1].toString().replace(' ','').split(',');
        splitPrice = req.body[i][index+2].toString().replace(' ','').split(',');
        
        if(splitName.length != splitPrice.length){
          throw 'จำนวนข้อมูลของตัวเลือกและราคาของตัวเลือกไม่ตรงกัน';
        }
        if(splitName.filter((name) => name === "").length !== 0 && splitPrice.filter((price) => price === "").length !== 0){
          throw 'ข้อมูลตัวเลือกหรือราคาตัวเลือกไม่สามารถเป็นค่าว่างได้';
        }
        for(let k = 0; k < splitName.length; k++) {
          CIOID = CIOID.substr(0, CIOID.length - String(LastCategoryIngredientsOptionID).length) + LastCategoryIngredientsOptionID;
          sqlCommandIngredientOption += ` (?, ?, ?, ?),`
          paramsQueryIngredientOption = [...paramsQueryIngredientOption, ...[
            CIOID,
            CIID,
            splitName[k],
            splitPrice[k]
          ]];
          LastCategoryIngredientsOptionID += 1;
        }
        index += 3;
        LastCategoryIngredientsID += 1;
      }
      LastCategoryID += 1;
    }
    
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    const [a] = await connection.query(sqlCommandIngredient.slice(0, -1), paramsQueryIngredient);
    const [b] = await connection.query(sqlCommandIngredientOption.slice(0, -1), paramsQueryIngredientOption);

    await connection.query(`UPDATE tb_sequence JOIN (
      SELECT 'TbCategory' as tableName, ? as newLastID
      UNION ALL
      SELECT 'TbCategoryIngredients', ?
      UNION ALL
      SELECT 'TbCategoryIngredientsOption', ?
    ) newVals ON tb_sequence.tableName = newVals.tableName SET LastID = newLastID;`, [ LastCategoryID, LastCategoryIngredientsID, LastCategoryIngredientsOptionID ]);
    await connection.commit();
      
    res.status(200).json({msg: 'นำเข้าข้อมูลประเภทอาหารสำเร็จ'});
  } catch (error) {
    console.error(error);
    await connection.rollback();
    let msg = (typeof error === 'string')? error: 'เกิดความผิดพลาดบางอย่าง'
    res.status(400).json({msg, error});
  } finally {
    await connection.end();
  }
});

router.put('/:CID', checkEmptyDataCategory, validationCategory , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    await connection.beginTransaction();
    await connection.query('UPDATE tb_category SET CName = ?, CIcon = ? WHERE CID = ?', [
      req.body.CName,
      req.body.CIcon,
      req.params.CID
    ]);

    const [ sequenceCategoryIngredients ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCategoryIngredients'`);
    let LastID = sequenceCategoryIngredients[0]['LastID'];
    let CIID = 'CI-00000';

    let sqlCommandInsert = `INSERT INTO tb_category_ingredients(CIID, CID, CIName, CIMultiple) VALUES`;
    let paramsQueryInsert = [];

    let sqlCommandUpdate = `UPDATE tb_category_ingredients JOIN (`;
    let paramsQueryUpdate = [];

    for (var i = 0; i < req.body.Ingredient.length; i++){
      // TODO: check data in object, if new data to insert data, if old data to update data
      if(Object.keys(req.body.Ingredient[i]).length === 1 || Object.keys(req.body.Ingredient[i]).length === 2){ // ? 1 = new data
        CIID = CIID.substr(0, CIID.length - String(LastID).length) + LastID;
        sqlCommandInsert += ` (?, ?, ?, ?),`
        paramsQueryInsert = [...paramsQueryInsert, ...[
          CIID,
          req.params.CID,
          req.body.Ingredient[i].CIName,
          req.body.Ingredient[i].CIMultiple || false
        ]];
        LastID += 1;
      }else{
        if(_.isEmpty(paramsQueryUpdate)){
          sqlCommandUpdate += ` SELECT ? as newCIID, ? as newCIName, ? as newCIMultiple`;
        }else{
          sqlCommandUpdate += ` UNION ALL SELECT ?, ?, ?`;
        }
        paramsQueryUpdate = [...paramsQueryUpdate, ...[
          req.body.Ingredient[i].CIID,
          req.body.Ingredient[i].CIName,
          req.body.Ingredient[i].CIMultiple,
        ]];
      }
    }

    if(!_.isEmpty(paramsQueryInsert)){
      await connection.query(sqlCommandInsert.slice(0, -1), paramsQueryInsert);
      await connection.query(`UPDATE tb_sequence SET LastID = ? WHERE tableName = 'TbCategoryIngredients'`, [ LastID ]);
    }

    if(!_.isEmpty(paramsQueryUpdate)){
      sqlCommandUpdate += `) newValues ON tb_category_ingredients.CIID = newValues.newCIID SET CIName = newCIName, CIMultiple = newCIMultiple;`
      await connection.query(sqlCommandUpdate, paramsQueryUpdate);
    }
    await connection.commit();

    res.status(200).json({CID: req.params.CID, msg: `แก้ไขข้อมูลประเภทอาหาร ${req.params.CID} สำเร็จ`});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:CID/Status', checkEmptyDataCategory, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_category SET CStatus = (CASE WHEN CStatus = 1 THEN 0 ELSE 1 END) WHERE CID = ?', [
      req.params.CID
    ])
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้ประเภทอาหาร ${req.params.CID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/option/:CIID', checkEmptyDataCategoryIngredient, validationCategoryIngredientsOption , validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0]
    
    await connection.beginTransaction();

    const [ sequenceCategoryIngredientsOption ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCategoryIngredientsOption'`);
    let LastID = sequenceCategoryIngredientsOption[0]['LastID'];
    let CIOID = 'CIO-00000';

    let sqlCommandInsert = `INSERT INTO tb_category_ingredients_option(CIOID, CIID, CIOName, CIOPrice) VALUES`;
    let paramsQueryInsert = [];

    let sqlCommandUpdate = `UPDATE tb_category_ingredients_option JOIN (`;
    let paramsQueryUpdate = [];

    for (var i = 0; i < req.body.Option.length; i++){
      // TODO: check data in object, if new data to insert data, if old data to update data
      if(Object.keys(req.body.Option[i]).length === 2){ // ? 1 = new data
        CIOID = CIOID.substr(0, CIOID.length - String(LastID).length) + LastID;
        sqlCommandInsert += ` (?, ?, ?, ?),`
        paramsQueryInsert = [...paramsQueryInsert, ...[
          CIOID,
          req.params.CIID,
          req.body.Option[i].CIOName,
          req.body.Option[i].CIOPrice
        ]];
        LastID += 1;
      }else{
        if(_.isEmpty(paramsQueryUpdate)){
          sqlCommandUpdate += ` SELECT ? as newCIOID, ? as newCIOName, ? as newCIOPrice`;
        }else{
          sqlCommandUpdate += ` UNION ALL SELECT ?, ?, ?`;
        }
        paramsQueryUpdate = [...paramsQueryUpdate, ...[
          req.body.Option[i].CIOID,
          req.body.Option[i].CIOName,
          req.body.Option[i].CIOPrice,
        ]];
      }
    }

    if(!_.isEmpty(paramsQueryInsert)){
      await connection.query(sqlCommandInsert.slice(0, -1), paramsQueryInsert);
      await connection.query(`UPDATE tb_sequence SET LastID = ? WHERE tableName = 'TbCategoryIngredientsOption'`, [ LastID ]);
    }

    if(!_.isEmpty(paramsQueryUpdate)){
      sqlCommandUpdate += `) newValues ON tb_category_ingredients_option.CIOID = newValues.newCIOID SET CIOName = newCIOName, CIOPrice = newCIOPrice;`
      await connection.query(sqlCommandUpdate, paramsQueryUpdate);
    }
    await connection.commit();

    res.status(200).json({CIID: req.params.CIID, msg: 'แก้ไขข้อมูลตัวเลือกประเภทอาหารสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:CID', checkEmptyDataCategory, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_category WHERE CID = ?', [req.params.CID]);

    res.status(200).json({msg: `ลบข้อมูลประเภทอาหาร ${req.params.CID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/ingredient/:CIID', checkEmptyDataCategoryIngredient, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_category_ingredients WHERE CIID = ?', [req.params.CIID]);

    res.status(200).json({CIID: req.params.CIID, msg: `ลบข้อมูลส่วนประกอบ ${req.params.CIID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/option/:CIOID', checkEmptyDataCategoryOption, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_category_ingredients_option WHERE CIOID = ?', [req.params.CIOID]);

    res.status(200).json({CIOID: req.params.CIOID, msg: `ลบข้อมูลตัวเลือกในส่วนประกอบ ${req.params.CIOID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;