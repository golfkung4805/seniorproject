import express from 'express';

import { connectDB } from '../config/connectDB.js';

const router = express.Router();

const checkEmptyDataCustomer = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataCustomer ] = await connection.query('SELECT * FROM tb_customer WHERE CusID = ?', [req.params.CusID]);
  await connection.end();
  if(dataCustomer.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลสมาชิก ${req.params.CusID} ในระบบ`});
  req.body.CusPoint = dataCustomer[0].CusPoint
  next();
}

const checkEmptyDataVoucher = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataVoucher ] = await connection.query('SELECT * FROM tb_voucher WHERE VoucID = ?', [req.body.VoucID]);
  await connection.end();
  if(dataVoucher.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลคูปอง ${req.body.VoucID} ในระบบ`});
  if(dataVoucher[0].VoucUse + 1 > dataVoucher[0].VoucQuota) return res.status(202).json({msg: `สิทธิ์แล้วคูปองนี้ครบที่กำหนดไว้แล้ว`});
  req.body.VoucCode = dataVoucher[0].VoucCode
  req.body.UsePoint = dataVoucher[0].VoucUsePoint
  next();
}

const checkVoucherCustomer = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataVoucher ] = await connection.query('SELECT * FROM tb_customer_voucher WHERE CusID = ? AND VoucID = ?', [
    req.params.VoucID,
    req.body.VoucID
  ]);
  await connection.end();
  if(dataVoucher.length > 0) return res.status(202).json({msg: `คุณเคยรับโค้ดส่วนลดนี้ไปแล้ว`});
  next();
}

router.get('/:CusID', async(req, res) => {
  const connection = await connectDB();
  try {
    const [ dataVoucher ] = await connection.query('SELECT * FROM tb_customer_voucher LEFT JOIN tb_voucher ON tb_customer_voucher.VoucID = tb_voucher.VoucID WHERE CusID = ?', [req.params.CusID]);
    
    res.status(201).json(dataVoucher);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/:CusID', checkEmptyDataCustomer, checkEmptyDataVoucher, checkVoucherCustomer, async(req, res) => {
  const connection = await connectDB();
  try {
    if(req.body.CusPoint < req.body.UsePoint){
      return res.status(202).json({msg: `พ้อยท์ของคุณคงเหลือ ${req.body.CusPoint} พ้อยท์`});
    }

    await connection.beginTransaction();

    await connection.query(`UPDATE tb_customer SET CusPoint = CusPoint - ? WHERE CusID = ?`, [
      req.body.UsePoint,
      req.params.CusID
    ]);

    await connection.query(`INSERT INTO tb_customer_voucher(CusID, VoucID, VoucHisUsePoint) VALUES(?, ?, ?)`, [
      req.params.CusID,
      req.body.VoucID,
      req.body.UsePoint
    ]);

    await connection.query(`UPDATE tb_voucher SET VoucUse = VoucUse+1 WHERE VoucID = ?`, [
      req.body.VoucID
    ]);

    await connection.commit();

    res.status(201).json({msg: `แลกโค้ดส่วนลด ${req.body.VoucCode} สำเร็จ`});
  } catch (error) {
    console.error(error);
    await connection.rollback();
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;