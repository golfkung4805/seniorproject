import express from 'express';
import moment from 'moment'

import { connectDB } from '../config/connectDB.js';

const router = express.Router();

router.get('/:SysID', async(req, res) => {
  const connection = await connectDB();
  try {
    const [ dataStore ] = await connection.query(`SELECT * FROM tb_sysconfig WHERE SysID = ?`, [
      req.params.SysID
    ]);
    res.status(200).json(dataStore);
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:SysID', async(req, res) => {
  const connection = await connectDB();
  try {
    await connection.query(`UPDATE tb_sysconfig SET SysName = ?, SysOpen = ?, SysClose = ?, SysPointType = ?, SysPointCondition = ?, SysKitchenOpen = ?, SysKitchenClose = ? WHERE SysID = ?`,[
      req.body.SysName,
      moment(req.body.SysOpenClose[0]).format('HH:mm'),
      moment(req.body.SysOpenClose[1]).format('HH:mm'),
      req.body.SysPointType,
      `${req.body.SysPointCondition.Money}/${req.body.SysPointCondition.Point}`,
      moment(req.body.SysKitchenOpenClose[0]).format('HH:mm'),
      moment(req.body.SysKitchenOpenClose[1]).format('HH:mm'),
      req.params.SysID,
    ]);

    res.status(201).json({msg: 'แก้ไขข้อมูลร้านสำเร็จ'});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;