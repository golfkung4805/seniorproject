import express from 'express';
import bcrypt from 'bcryptjs';
import _ from 'lodash'

import { connectDB } from '../config/connectDB.js';

import { generateTokenCus, generateTokenCusSocialLogin } from '../utils.js';
import { validationPerson, validationDataResult } from '../util/validate.js';

const router = express.Router();

const checkEmptyDataCustomer = async (req, res, next) => {
  const connection = await connectDB();
  const [ dataCustomer ] = await connection.query('SELECT * FROM tb_customer WHERE CusID = ?', [req.params.CusID]);
  await connection.end();
  if(dataCustomer.length <= 0) return res.status(404).json({msg: `ไม่พบข้อมูลสมาชิก ${req.params.CusID} ในระบบ`});
  next();
}

router.get('/', async (req, res) => {
  const connection = await connectDB();
  try {
    let paramsQuery = [];
    let sqlCommand = 'SELECT CusID, CusUsername, CusGender, CusFirstname, CusLastname, CusNickname, CusTel, CusEmail, CusStatus, CusPoint, CONCAT(CusGender, CusFirstname,\' \',CusLastname)as CusName FROM tb_customer WHERE 1=1';
    if(req.query.filters){
      const filterStatus = (req.query.filters.CusStatus.length === 0)? ['1','0']: req.query.filters.CusStatus
      sqlCommand = sqlCommand + ` AND CusID LIKE ? AND CusUsername LIKE ? AND CONCAT(CusGender, CusFirstname,\' \',CusLastname) LIKE ? AND CusNickname LIKE ? AND CusTel LIKE ? AND CusEmail LIKE ? AND CusStatus IN (?)`;
      paramsQuery = [...paramsQuery, 
      ...[`%${req.query.filters.CusID}%`, 
          `%${req.query.filters.CusUsername}%`, 
          `%${req.query.filters.CusName}%`, 
          `%${req.query.filters.CusNickname}%`, 
          `%${req.query.filters.CusTel}%`, 
          `%${req.query.filters.CusEmail}%`,
          filterStatus
        ]
      ];

      var [ TotalDataCustomers ] = await connection.query(sqlCommand, paramsQuery);
    }else{
      var [ TotalDataCustomers ] = await connection.query(sqlCommand);
    }
    if(req.query.sorter && req.query.sorter.order){
      // !! ⚠️⚠️⚠️ ${req.query.sorter.field} is at risk of being SQL injection ⚠️⚠️⚠️
      sqlCommand = sqlCommand + ` ORDER BY \`${req.query.sorter.field}\` ${req.query.sorter.order==='ascend'?'ASC':'DESC'}`;
    }
    if(req.query.pagination){
      sqlCommand = sqlCommand + ' LIMIT ?, ?';
      paramsQuery = [...paramsQuery, ...[(parseInt(req.query.pagination.current)-1)*10, parseInt(req.query.pagination.pageSize)]];
    }
    const [ dataCustomers ] = await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({
      "data": dataCustomers,
      "current": parseInt(req.query.pagination.current),
      "pageSize": parseInt(req.query.pagination.pageSize),
      "total": TotalDataCustomers.length
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.get('/:CusID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ user ] = await connection.query('SELECT *, CONCAT(CusGender, CusFirstname,\' \',CusLastname)as CusName FROM tb_customer WHERE CusID = ?', [ req.params.CusID ]);
    if(!_.isEmpty(user)){
      res.status(200).json({
        CusID: user[0].CusID,
        CusNickname: user[0].CusNickname,
        CusPoint: user[0].CusPoint,
        token: generateTokenCus(user[0]),
      });
      return;
    }
    res.status(401).json({ msg: 'เกิดความผิดพลาดบางอย่าง' });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
})

// TODO: :CusID check authorization
router.get('/:CusID/order', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataOrderCustomer ] = await connection.query('SELECT * FROM tb_order WHERE CusID = ? ORDER BY createdAt DESC', [req.params.CusID])
    res.status(200).json({"data": dataOrderCustomer})
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการนำเข้าข้อมูล', error});
  } finally {
    await connection.end();
  }
});
// TODO: :CusID check authorization
router.get('/:CusID/order/:ODID', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ dataOrderDetailCustomer ] = await connection.query('SELECT * FROM tb_order_detail WHERE ODID = ? ORDER BY _ID DESC', [req.params.ODID])
    res.status(200).json({"data": dataOrderDetailCustomer})
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดข้อผิดพลาดในการนำเข้าข้อมูล', error});
  } finally {
    await connection.end();
  }
});

router.post('/', validationPerson, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    const [ sequenceCustomer ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCustomer'`);
    const LastID = sequenceCustomer[0]['LastID'];
    let CusID = 'Cus-00000';
    CusID = CusID.substr(0, CusID.length - String(LastID).length) + LastID;
    
    await connection.beginTransaction();
    await connection.query('INSERT INTO tb_customer(CusID, CusUsername, CusPasswd, CusGender, CusFirstname, CusLastname, CusNickname, CusTel, CusEmail)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', [
      CusID,
      req.body.Username,
      bcrypt.hashSync(req.body.Passwd),
      req.body.Gender,
      req.body.Firstname,
      req.body.Lastname,
      req.body.Nickname,
      req.body.Tel,
      req.body.Email
    ]);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID+1 WHERE tableName = 'TbCustomer'`);
    await connection.commit();
    
    res.status(201).json({msg: 'เพิ่มข้อมูลสมาชิกสำเร็จ'});
  } catch (error) {
    await connection.rollback();
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/import', validationPerson, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.beginTransaction();
    const [ sequenceCustomer ] = await connection.query(`SELECT * FROM tb_sequence WHERE \`tableName\` = 'TbCustomer'`);
    let LastID = sequenceCustomer[0]['LastID'];
    let CusID = 'Cus-00000';
    let sqlCommand = `INSERT INTO tb_customer(CusID, CusUsername, CusPasswd, CusGender, CusFirstname, CusLastname, CusNickname, CusTel, CusEmail) VALUES`;
    let paramsQuery = [];

    for(let i = 0; i < req.body.length; i++){
      CusID = CusID.substr(0, CusID.length - String(LastID).length) + LastID;
      sqlCommand += ` (?, ?, ?, ?, ?, ?, ?, ?, ?),`
      paramsQuery = [...paramsQuery, ...[
        CusID,
        req.body[i].Username,
        bcrypt.hashSync(req.body[i].Passwd),
        req.body[i].Gender,
        req.body[i].Firstname,
        req.body[i].Lastname,
        req.body[i].Nickname,
        req.body[i].Tel,
        req.body[i].Email
      ]];
      LastID += 1;
    }
    
    await connection.query(sqlCommand.slice(0, -1), paramsQuery);
    await connection.query(`UPDATE tb_sequence SET LastID = LastID + ? WHERE tableName = 'TbCustomer'`, [ req.body.length ]);
    await connection.commit();

    res.status(200).json({msg: 'นำเข้าข้อมูลสมาชิกสำเร็จ'});
  } catch (error) {
      await connection.rollback();
      console.error(error);
      res.status(400).json({msg: 'เกิดข้อผิดพลาดในการนำเข้าข้อมูล', error});
  } finally {
    await connection.end();
  }
});

router.put('/:CusID', checkEmptyDataCustomer, validationPerson, validationDataResult, async (req, res) => {
  const connection = await connectDB();
  try {
    req.body = req.body[0];

    let paramsQuery = [];
    let sqlCommand = 'UPDATE tb_customer SET';
    if(req.body.Passwd!=='-'){
      sqlCommand = sqlCommand + ' CusPasswd = ?,';
      paramsQuery = [...paramsQuery, 
        bcrypt.hashSync(req.body.Passwd),
      ];
    }
    sqlCommand = sqlCommand + ' CusGender = ?, CusFirstname = ?, CusLastname = ?, CusNickname = ?, CusTel = ?, CusEmail = ?, updatedAt = CURRENT_TIMESTAMP() WHERE CusID = ?'
    paramsQuery = [...paramsQuery, 
      req.body.Gender,
      req.body.Firstname,
      req.body.Lastname,
      req.body.Nickname,
      req.body.Tel,
      req.body.Email,
      req.params.CusID
    ];
    
    await connection.query(sqlCommand, paramsQuery);

    res.status(200).json({msg: `แก้ไขข้อมูลสมาชิก ${req.params.CusID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.put('/:CusID/Status', checkEmptyDataCustomer, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('UPDATE tb_customer SET CusStatus = (CASE WHEN CusStatus = 1 THEN 0 ELSE 1 END) WHERE CusID = ?', [
      req.params.CusID
    ]);
    
    res.status(200).json({msg: `เปลี่ยนสถานะการใช้งานสมาชิก ${req.params.CusID} สำเร็จ`});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.delete('/:CusID', checkEmptyDataCustomer, async (req, res) => {
  const connection = await connectDB();
  try {
    await connection.query('DELETE FROM tb_customer WHERE CusID = ?', [req.params.CusID]);

    res.status(200).json({msg: `ลบข้อมูลสมาชิก ${req.params.CusID} สำเร็จ `});
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/signin', async (req, res) => {
  const connection = await connectDB();
  try {
    const [ user ] = await connection.query('SELECT *, CONCAT(CusGender, CusFirstname,\' \',CusLastname)as CusName FROM tb_customer WHERE CusUsername = ? OR CusEmail = ? OR CusTel = ?', [
      req.body.Username,
      req.body.Username,
      req.body.Username
    ]);
    if(!_.isEmpty(user)){
      if(user[0].CusStatus === 0) return res.status(401).json({msg: `บัญชีนี้ถูกระงับการใช้งานกรุณาติดต่อเคาน์เตอร์`});
      if(bcrypt.compareSync(req.body.Passwd, user[0].CusPasswd)){
        res.status(200).json({
          CusID: user[0].CusID,
          CusNickname: user[0].CusNickname,
          CusPoint: user[0].CusPoint,
          token: generateTokenCus(user[0]),
        });
        return;
      }
    }
    res.status(401).json({ msg: 'อีเมลหรือรหัสผ่านไม่ถูกต้อง' });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/signin/facebook', async (req, res) => {
  const connection = await connectDB();
  try {
    const myName = req.body.name.split(" ");
    let profileObj = {};

    const [ dataCustomer ] = await connection.query(`SELECT * FROM tb_customer WHERE CusID = ? OR CusEmail = ?`, [
      req.body.id,
      req.body.email
    ]);

    if(_.isEmpty(dataCustomer)){
      await connection.query(`INSERT INTO tb_customer(CusID, CusFirstname, CusLastname, CusNickname, CusEmail, CusPicture)  VALUES(?, ?, ?, ?, ?, ?)`, [
        req.body.id,
        myName[0],
        myName[2],
        myName[0],
        req.body.email,
        req.body.picture.data.url
      ]);

      profileObj = {
        CusID: req.body.id,
        CusNickname: myName[0],
        CusEmail: req.body.email,
        CusPoint: 0,
        CusPicture: req.body.picture.data.url
      }
    }
    
    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusStatus === 0) return res.status(401).json({msg: `บัญชีนี้ถูกระงับการใช้งานกรุณาติดต่อเคาน์เตอร์`});

    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusID !== req.body.id){
      profileObj = {
        CusID: dataCustomer[0].CusID,
        CusNickname: dataCustomer[0].CusNickname,
        CusEmail: req.body.email,
        CusPoint: dataCustomer[0].CusPoint,
        CusPicture: dataCustomer[0].CusPicture
      }
    }

    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusID === req.body.id){
      profileObj = {
        CusID: req.body.id,
        CusNickname: myName[0],
        CusEmail: req.body.email,
        CusPoint: dataCustomer[0].CusPoint,
        CusPicture: req.body.picture.data.url
      }
    }
    
    res.status(200).json({
      ...profileObj,
      token: generateTokenCusSocialLogin(profileObj),
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

router.post('/signin/google', async (req, res) => {
  const connection = await connectDB();
  try {
    let profileObj = {};
    const [ dataCustomer ] = await connection.query(`SELECT * FROM tb_customer WHERE CusID = ? OR CusEmail = ?`, [
      req.body.googleId,
      req.body.email
    ]);

    if(_.isEmpty(dataCustomer)){
      await connection.query(`INSERT INTO tb_customer(CusID, CusFirstname, CusLastname, CusNickname, CusEmail, CusPicture)  VALUES(?, ?, ?, ?, ?, ?)`, [
        req.body.googleId,
        req.body.givenName,
        req.body.familyName,
        req.body.givenName,
        req.body.email,
        req.body.imageUrl
      ]);

      profileObj = {
        CusID: req.body.googleId,
        CusNickname: req.body.givenName,
        CusEmail: req.body.email,
        CusPoint: 0,
        CusPicture: req.body.imageUrl
      }
    }
    
    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusStatus === 0) return res.status(401).json({msg: `บัญชีนี้ถูกระงับการใช้งานกรุณาติดต่อเคาน์เตอร์`});

    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusID !== req.body.googleId){
      profileObj = {
        CusID: dataCustomer[0].CusID,
        CusNickname: dataCustomer[0].CusNickname,
        CusEmail: req.body.email,
        CusPoint: dataCustomer[0].CusPoint,
        CusPicture: dataCustomer[0].CusPicture
      }
    }

    if(!_.isEmpty(dataCustomer) && dataCustomer[0].CusID === req.body.googleId){
      profileObj = {
        CusID: req.body.googleId,
        CusNickname: req.body.givenName,
        CusEmail: req.body.email,
        CusPoint: dataCustomer[0].CusPoint,
        CusPicture: req.body.imageUrl
      }
    }
    
    res.status(200).json({
      ...profileObj,
      token: generateTokenCusSocialLogin(profileObj),
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({msg: 'เกิดความผิดพลาดบางอย่าง', error});
  } finally {
    await connection.end();
  }
});

export default router;