import _ from 'lodash'

import { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector} from 'react-redux';
import Script from 'next/script'
import { 
  listCategory
} from '../redux/actions/categoryActions';


const PosMenu = () => {

  const dispatch = useDispatch();

  const categoryList = useSelector((state) => state.categoryList)
  const { loading, data } = categoryList

  const CATEGORY_LIST = () => {
    dispatch(listCategory({pagination: {pageSize: 'all'}, filters: { CStatus: ['1'] } }))
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      CATEGORY_LIST()
    }
  },[])

  const btnPosMenu = (e) => {
    e.preventDefault()
    let a = document.querySelectorAll('a[data-filter]')
    // console.log(a)
  }

  return(
    <>
    <Script
      src="https://code.jquery.com/jquery-3.6.0.min.js"
      onLoad={() => {
        let handleFilter = function() {
          "use strict";
      
          $(document).on('click', '.pos-menu [data-filter]', function(e) {
            e.preventDefault();
    
            let targetType = $(this).attr('data-filter');
    
            $(this).addClass('active');
            $('.pos-menu [data-filter]').not(this).removeClass('active');
            if (targetType == 'all') {
              $('.pos-content [data-type]').removeClass('d-none');
            } else {
              $('.pos-content [data-type="' + targetType + '"]').removeClass('d-none');
              $('.pos-content [data-type]').not('.pos-content [data-type="' + targetType + '"]').addClass('d-none');
            }
          });
        };
        
        $(document).ready(function() {
            handleFilter();
        });
      }}
    />
    <div className="pos-menu">
      <div className="logo">
        <a href="#">
          <div className="logo-img">
            <img src="../assets/img/pos/logo.svg" />
          </div>
          <div className="logo-text">
            SR &amp; Menu
          </div>
        </a>
      </div>
      <div className="nav-container">
        <div data-scrollbar="true" data-height="100%" data-skip-mobile="true">
          <ul className="nav nav-tabs">
            <li className="nav-item">
              <a className="nav-link active" href="#" data-filter="all">
                <i className="fa fa-fw fa-utensils mr-1 ml-n2" /> อาหารทั้งหมด
              </a>
            </li>
            {
              (loading===false)?
              data.map((value, index)=>{
                return (
                  <li className="nav-item" key={index}>
                    <a className="nav-link" href="" data-filter={value.CID} onClick={(e)=> btnPosMenu(e)}>
                      <i className={`fa fa-fw ${value.CIcon} mr-1 ml-n2`} /> {value.CName}
                    </a>
                  </li>
                )
              }): ''
            }
          </ul>
        </div>
      </div>
    </div>
    </>
  )
}

export default PosMenu