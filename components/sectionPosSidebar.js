import _ from 'lodash'
import { 
  v4 as uuidv4
} from 'uuid'
import axios from 'axios'
import moment from 'moment';
import Swal from 'sweetalert2'
import jwt from "jsonwebtoken";

import { useRouter } from 'next/router'
import Script from 'next/script'
import { useState, useEffect, useMemo} from 'react';
import { useDispatch, useSelector} from 'react-redux';

import { useTranslation, i18n } from 'next-i18next'

import { Modal, Table, Typography, Input, Radio, Rate, Space, Select, Button, Tooltip, Spin, message } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';

import { 
  minusToCart,
  plusToCart,
  removeFromCart,
  cancelRemoveFromCart,
  showProductDetail
} from '../redux/actions/cartActions';

import {
  createOrders
} from '../redux/actions/orderActions'

import { listOrderCustomer } from '../redux/actions/customerActions'

import { 
  modalOpenLocal
} from '../redux/actions/localActions';

import { CART_EMPTY } from '../redux/constants/cartConstants'
import { ORDER_CREATE_RESET } from '../redux/constants/orderConstants'

import { CUSTOMER_UPDATE_PROFILE_SUCCESS } from '../redux/constants/customerConstants'

import { ToCurrency } from './converterCurrency'

const PosSidebar = ({form}) => {
  const { t } = useTranslation('customerOrder')

  const router = useRouter()

  const thisPath = router.asPath

  const { TableID } = router.query

  const { Text } = Typography;

  const { Option } = Select;

  const { Search } = Input;

  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart)
  const { cartItems } = cart

  const customerSignIn = useSelector((status)=> status.customerSignIn)
  const { cusInfo } = customerSignIn

  const localSocket = useSelector((status)=> status.localSocket)
  const { isSocket: socket } = localSocket

  const customerOrderList = useSelector((status)=> status.customerOrderList)
  const { loading: loadingOrderList, data: orderList, error: errorOrderList } = customerOrderList
  
  const orderCreate = useSelector((status)=> status.orderCreate)
  const { loading: loadingCreate, success: successCreate, product: createdProduct, error: errorCreate } = orderCreate

  const lastOrder = orderList.find((x)=>x.ODStatus === "openOrder")
  
  const [ paymentMethodList, setPaymentMethodList ] = useState([])
  const [ optionsVoucher, setOptionsVoucher ] = useState([]);
  const [ listVoucher, setListVoucher ] = useState([]);
  const [ reviewRateList, setReviewRateList ] =  useState({value: 0})
  const [ expandedRowKeysReview, setExpandedRowKeysReview ] = useState([])
  const [ modalOrderHistory, setModalOrderDetail ] = useState({isModalVisible:false, dataOrder: [], dataOrderDetail: [], voucher: {}, paymentMethod: 'checkout_counter'})
  const { isModalVisible, dataOrder, dataOrderDetail, voucher, paymentMethod } = modalOrderHistory

  const SwalFire = (icon = "", title = "", text = "") => {
    Swal.fire({
      icon,
      title,
      text: text.msg || text
    })
  }
  
  const onChangePaymentMethod = (e) => {
    setModalOrderDetail({...modalOrderHistory, paymentMethod: e.target.value})
  }
  
  const onSearchVoucher = async(value = "") => {
    if(!_.isEmpty(value)){
      try {
        // check order total minimum
        const getDataVoucher = listVoucher.find((x)=>x.VoucCode === value)
        // console.log(getDataVoucher)
        if(!_.isEmpty(getDataVoucher) && dataOrder.ODTotal >= getDataVoucher.VoucCondition){
          // setVoucher(data[0])
          setModalOrderDetail({...modalOrderHistory, voucher: getDataVoucher})
          return 
        }else{
          message.warning(t('order_sidebar.model_order_detail.warningVoucher'))
        }
      } catch (error) {
        console.error(error)
      }
    }else{
      message.warning(t('order_sidebar.model_order_detail.warningVoucherNotFound'))
    }
    setModalOrderDetail({...modalOrderHistory, voucher: []})
  }

  const omiseCardConfigure = () => {
    OmiseCard.configure({
      defaultPaymentMethod: paymentMethod,
      otherPaymentMethods: ['credit_card', 'internet_banking', 'truemoney', 'rabbit_linepay']
    });
    OmiseCard.configureButton("#omise-card");
    OmiseCard.attach();
  }

  const omiseCardHandler = (discount, amount) => {
    OmiseCard.open({
      frameDescription: `Invoice #${dataOrder.ODID}`,
      amount: amount*100,
      onCreateTokenSuccess: async (token) => {
        try {
          const { data } = await axios.patch(`${process.env.API_URL}/v1/order/${dataOrder.ODID}/checkout`, {
            CusID: cusInfo.CusID,
            TableID: dataOrder.TableID,
            amount: amount*100,
            VoucCode: voucher.VoucCode,
            VoucDiscount: discount,
            token,
            paymentMethod
          })

          if(paymentMethod === "credit_card" && data.charge.status === "successful"){
            dataOrder.ODStatus = "paid"
            dataOrder.PayID = "credit_card"
            setModalOrderDetail({...modalOrderHistory, dataOrder: dataOrder})
          }else if(paymentMethod === "internet_banking" || paymentMethod === "rabbit_linepay" || paymentMethod === "truemoney"){
            router.push(data.authorizeUri)
          }else{
            message.warning(t('order_sidebar.model_order_detail.warningCorrectPay'))
          }
          setModalOrderDetail({...modalOrderHistory, isModalVisible: false})
        } catch (error) {
          console.error(error)
        }
      },
      onFormClosed: () => {}
    });
  }

  const showModalOrderHistory = async (order) => {
    try {
      const { data: dataOrderDetail } = await axios.get(`${process.env.API_URL}/v1/customer/${cusInfo.CusID}/order/${order.ODID}`)
      let { data: dataCustomerVoucher } = await axios.get(`${process.env.API_URL}/v1/customer/voucher/${cusInfo.CusID}`)
      setModalOrderDetail({...modalOrderHistory, isModalVisible: true, dataOrder: order, dataOrderDetail: dataOrderDetail.data})
      dataCustomerVoucher = dataCustomerVoucher.filter((x)=>x.VoucHisStatus === 1)
      const options = dataCustomerVoucher.map((value)=>{
        return {label: `${t('order_sidebar.model_order_detail.showVoucher', { VoucDiscount: value.VoucDiscount, VoucType: value.VoucType, VoucCondition: value.VoucCondition })}`, value: value.VoucCode, disabled: (dataOrder.ODTotal < value.VoucCondition)? true: false }
      })
      console.log(options)
      setListVoucher(dataCustomerVoucher)
      setOptionsVoucher(options)
    } catch (error) {
      console.error(error)
    }
  };

  const handlePay = async (e) => {
    if(!_.isEmpty(dataOrderDetail.filter((x)=>x.ODStatus==="waitQueue"))){
      return SwalFire('warning', t('common:alertWarning'), t('order_sidebar.model_order_detail.warningPay'))
    }
    let discount = !_.isEmpty(voucher)? voucher.VoucType==="%"? (dataOrder.ODTotal * voucher.VoucDiscount)/100: voucher.VoucDiscount: 0
    let amount = dataOrder.ODTotal-(dataOrder.ODTotal>discount? discount: dataOrder.ODTotal)

    if(paymentMethod === 'checkout_counter'){
      try {
        const data = await axios.patch(`${process.env.API_URL}/v1/order/${dataOrder.ODID}/checkout`, {
          CusID: cusInfo.CusID,
          TableID: dataOrder.TableID,
          amount: amount*100,
          VoucCode: voucher.VoucCode,
          VoucDiscount: discount,
          paymentMethod
        })
        if(data.status === 200){
          dataOrder.ODStatus = "unpaid"
          dataOrder.PayID = "checkout_counter"
          setModalOrderDetail({...modalOrderHistory, dataOrder: dataOrder})
        }
      } catch (error) {
        console.error(error)
      }
    }else{
      e.preventDefault()

      omiseCardConfigure()
      omiseCardHandler(discount, amount)
    }
  };

  const handleCancelOrderHistory = () => {
    setReviewRateList({value: 0})
    setModalOrderDetail({...modalOrderHistory, isModalVisible: false, voucher: [], paymentMethod: 'checkout_counter'})
  };

  const columnsOrderHistory = [
    {
      title: t('order_sidebar.table_order_detail.name'),
      dataIndex: 'PDName',
      key: 'PDName'
    },
    {
      title: t('order_sidebar.table_order_detail.price'),
      dataIndex: 'PDPrice',
      key: 'PDPrice',
    },
    {
      title: t('order_sidebar.table_order_detail.option'),
      dataIndex: 'ODSizeListDisplay',
      key: 'ODSizeListDisplay',
      render: (text, render) => (
        JSON.parse(render.ODSizeListDisplay).map((option, index) => {
          return (<div key={index}>{option}</div>)
        })
      )
    },
    {
      title: t('order_sidebar.table_order_detail.qty'),
      dataIndex: 'ODQty',
      key: 'ODQty',
    },
    {
      title: t('order_sidebar.table_order_detail.total'),
      dataIndex: 'ODTotalPrice',
      key: 'ODTotalPrice',
    },
    {
      title: t('order_sidebar.table_order_detail.action'),
      key: 'action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <button type="button" className="ant-btn btn btn-lime" onClick={()=> handleOrderAgain(record.PDID, record._ID, record.ODID)}><span>{t('order_sidebar.model_order_detail.btn_order_again', {PDID: record.PDID})}</span></button>
        </Space>
      ),
    },
    {
      title: t('order_sidebar.table_order_detail.status.text'),
      dataIndex: 'ODStatus',
      key: 'ODStatus',
      align: 'center',
      render: (text, record) => (
        <>{record.ODStatus === 'complete'? t('order_sidebar.table_order_detail.status.completed') :record.ODStatus === 'cancel'? t('order_sidebar.table_order_detail.status.cancelled') : t('order_sidebar.table_order_detail.status.waitQueue')}</>
      )
    },
  ];

  const handleOrderAgain = async(PDID, _ID, ODID) => {
    try {
      dispatch(showProductDetail(PDID))
      const { data } = await axios.get(`${process.env.API_URL}/v1/customer/${cusInfo.CusID}/order/${ODID}`)
      const thisProduct = data.data.find((x)=> x._ID === _ID)
      form.resetFields()
      form.setFieldsValue({qty: thisProduct.ODQty, ...JSON.parse(thisProduct.ODSizeList), description: thisProduct.ODDesc})
      dispatch(modalOpenLocal())
    } catch (error) {
      console.error(error)
    }
  }

  const handleOpenModalProduct = async (PDID) => {
    dispatch(showProductDetail(PDID))
    const thisProduct = cartItems.find((x)=> x.PDID === PDID)
    form.resetFields()
    form.setFieldsValue({qty: thisProduct.orderQty, ...thisProduct.objectOptions, description: thisProduct.orderDesc})
    dispatch(modalOpenLocal(false, true))
  }
  
  const handleOrderMinus = (PDID) => {
    dispatch(minusToCart(PDID))
  }

  const handleOrderPlus = (PDID) => {
    dispatch(plusToCart(PDID))
  }

  const handleCancelRemoveOrderProduct = (PDID) => {
    dispatch(cancelRemoveFromCart(PDID))
  }
  
  const handleConfirmRemoveOrderProduct = (PDID) => {
    dispatch(removeFromCart(PDID))
  }

  const handleService = async () => {
    const { value: textService } = await Swal.fire({
      // icon: 'error',
      iconHtml: '🚨',
      title: t('order_sidebar.titleService'),
      input: 'text',
      // inputLabel: 'Your Requirement',
      inputPlaceholder: t('order_sidebar.placeholderService'),
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return t('order_sidebar.warningService')
        }
      }
    })
    if(textService){
      // console.log('handleService==>', textService)
      socket.emit('newService', {
        _ID: uuidv4(),
        CusID: cusInfo.CusID,
        TableID,
        message: textService,
        serviceAt: moment(),
        isSee: false,
      });
    }
  }

  const handleBill = () => {
    document.getElementById('tabOrderHistoryTab').click()
    // console.log('handleBill')
  }

  const handleSubmit = () => {
    let filterOrderStatus = orderList.filter((x)=>x.ODStatus !== "successful" && moment(x.updatedAt).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD'));
    if(!_.isEmpty(filterOrderStatus) && !_.isEmpty(lastOrder) && lastOrder.ODID !== filterOrderStatus.at(-1).ODID){
      Swal.fire({
        icon: 'warning',
        html: `<h1>${filterOrderStatus.at(-1).ODID} <br/> ${t('order_sidebar.warningWaitPay')}</h1>`,
        // text: text
      })
    }else{
      if(_.isEmpty(cartItems)){
        message.warning(t('order_sidebar.warningSelectProduct'))
      }else{
        dispatch(createOrders({lastOrder, cartItems, TableID}))
      }
    }
    // console.log('handleSubmit')
  }

  const onChangeRate = (value, _ID) => {
    setReviewRateList({_ID, value})
  }

  const handleSubmitReview = (reviewRateList) => {
    if(reviewRateList.value <= 0 || reviewRateList.value > 5) return SwalFire('warning', t('common:alertWarning'), t('order_sidebar.model_order_detail.warningReview'))
    Swal.fire({
      icon: 'warning',
      title: t('common:alertWarning'),
      html: t('order_sidebar.model_order_detail.warningTextReview', { _ID: reviewRateList._ID, value: reviewRateList.value }),
      showCancelButton: true,
    }).then(async(result) => {
      if (result.isConfirmed) {
        try {
          const res = await axios.put(`${process.env.API_URL}/v1/order/${reviewRateList._ID}/review`, {
            ODReview: reviewRateList.value
          })
          if(res.status === 200){
            // console.log(res)
            setModalOrderDetail({...modalOrderHistory, dataOrderDetail: dataOrderDetail.map((x)=>x._ID === reviewRateList._ID? {...x, ODReview: reviewRateList.value}: x)})
          }
          if(res.status === 404){
            console.error(res.status)
          }
        } catch (error) {
          console.error(error)
        }
      }
    })
  }
  
  const handleOnExpandReview = (expanded, record) => {
    if(expanded){
      setExpandedRowKeysReview([record._ID]) 
      setReviewRateList({value: 0, _ID: record._ID})
    }else{
      setExpandedRowKeysReview([]) 
      setReviewRateList({value: 0})
    }
  }

  const expandedRowRender = (record = NULL, index, indent, expanded) => {
    const columns = [
      {
        title: 'Review',
        key: 'ODReview',
        render: (text, record) => (
          <>
            <Rate 
              allowHalf 
              allowClear 
              className="mr-2"
              defaultValue={record.ODReview} 
              disabled={dataOrder.ODStatus !== 'successful' || record.ODReview !== null || record.ODStatus !== 'complete'} 
              onChange={(value)=> onChangeRate(value, record._ID)}
            />
            { `${record.ODReview || reviewRateList.value} ${t('order_sidebar.model_order_detail.reviewStar')}` } 
          </>
        ),
      },
      {
        title: t('order_sidebar.table_order_detail.status.text'),
        dataIndex: 'ODStatus',
        key: 'ODStatus',
        align: 'center',
        render: (text, record) => (
          <Button onClick={()=> handleSubmitReview(reviewRateList)} disabled={dataOrder.ODStatus !== 'successful' || record.ODReview !== null || record.ODStatus !== 'complete'}>รีวิวดาว</Button>
        )
      },
    ];

    return <Table rowKey={record => record._ID} columns={columns} dataSource={[record]} size="small" pagination={false} />;
  };

  useMemo(async()=>{
    try {
      const { data } = await axios.get(`${process.env.API_URL}/v1/paymentmethod?pagination[current]=1&pagination[pageSize]=10&pagination[total]=5&filters[PayName]=&filters[PayStatus][0]=1&sorter[column][title]=Payment Method&sorter[column][dataIndex]=PayName&sorter[column][key]=PayName&sorter[column][sorter]=true&sorter[column][sortDirections][0]=descend&sorter[column][sortDirections][1]=ascend&sorter[order]=ascend&sorter[field]=PayName&sorter[columnKey]=PayName`)
      setPaymentMethodList(data.data)
    } catch (error) {
      console.error(error)
    }
  }, [])

  useEffect(()=>{
    
  }, [])

  useEffect(()=>{
    if(!_.isEmpty(socket)){
      socket.on('kitchenChangeOrder', (order)=>{
        if(order.Order.ODStatus === 'cancel'){
          dispatch(listOrderCustomer(cusInfo.CusID))
        }
      })
      socket.on('confirmPayment', async(data)=>{
        dispatch(listOrderCustomer(cusInfo.CusID))
        // const decoded = jwt.verify(cusInfo.token, process.env.JWT_SECRET_KEY);
        // const updatedCus = await axios.get(`${process.env.API_URL}/v1/customer/${decoded.CusID}`)
        // if(updatedCus.status === 200){
        //   awaitdispatch({ type: CUSTOMER_UPDATE_PROFILE_SUCCESS, payload: updatedCus.data })
        //   location.replace(`${process.env.BASE_URL}${thisPath}`)
        // }
      })
    }
  },[socket])

  useEffect(()=>{
    if(successCreate){
      dispatch(listOrderCustomer(cusInfo.CusID))
      dispatch({ type: CART_EMPTY})
      dispatch({ type: ORDER_CREATE_RESET})
      document.getElementById('tabOrderHistoryTab').click()
    }

    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: CART_EMPTY})
      dispatch({ type: ORDER_CREATE_RESET})
    }
  },[successCreate])

  try {
    let discount = !_.isEmpty(voucher)
                      ? voucher.VoucType==="%"
                        ? (dataOrder.ODTotal * voucher.VoucDiscount)/100
                        : voucher.VoucDiscount
                      : 0
    let total = 0, subTotal = 0
    let OmiseCard

    return(
      <>
        <Script
          src="https://cdn.omise.co/omise.js"
          onLoad={() => {
            OmiseCard = window.OmiseCard
            OmiseCard.configure({
              publicKey: process.env.OMISE_PUBLIC_KEY,
              frameLabel: "Siri Shop",
              submitLabel: "PAY NOW",
              currency: "thb"
            });
          }}
        />
        <div className="pos-sidebar" id="pos-sidebar">
          <div className="pos-sidebar-header">
            <div className="back-btn">
              <button type="button" data-dismiss-class="pos-mobile-sidebar-toggled" data-target="#pos-customer" className="btn">
                <svg viewBox="0 0 16 16" className="bi bi-chevron-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                </svg>
              </button>
            </div>
            <div className="icon">
              <img src="../assets/img/pos/icon-table.svg" />
            </div>
            <div className="title">{ t('titleTable', { TableID: TableID.slice(8) }) }</div>
            <div className="order"><b>#{(_.isEmpty(lastOrder)? `${t('order_sidebar.lastOrderStatus')}`: lastOrder.ODID)}</b></div>
          </div>
          <div className="pos-sidebar-nav">
            <ul className="nav nav-tabs nav-fill">
              <li className="nav-item">
                <a className="nav-link active" id="tabNewOrderTab" href="#" data-toggle="tab" data-target="#newOrderTab">{t('order_sidebar.new_order')} ({cartItems.reduce((a, b)=> a + b.orderQty, 0)})</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" id="tabOrderHistoryTab" href="#" data-toggle="tab" data-target="#orderHistoryTab">{t('order_sidebar.order_history')} ({orderList.length})</a>
              </li>
            </ul>
          </div>
          <div className="pos-sidebar-body tab-content" data-scrollbar="true" data-height="100%">
            <div className="tab-pane fade h-100 show active" id="newOrderTab">
              {(cartItems.length>0)?
                <Spin spinning={loadingCreate}>
                  <div className="pos-table">
                    {cartItems.map((data, index)=>{
                      total = (data.PDPrice + (!_.isEmpty(data.optionPrice)?data.optionPrice.reduce((a, b)=> a+ b, 0) : 0)) *data.orderQty
                      subTotal = subTotal + total
                      return (
                        <div className="row pos-table-row" key={index}>
                          <div className="col-9">
                            <div className="pos-product-thumb" >
                              <div className="img" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/${data.PDImage})`, cursor: 'pointer'}} onClick={()=> handleOpenModalProduct(data.PDID)}/>
                              <div className="info">
                                <div className="title" style={{cursor: 'pointer'}} onClick={()=> handleOpenModalProduct(data.PDID)}>{data.PDName}</div>
                                <div className="single-price">฿ {data.PDPrice.toFixed(2)} .-</div>
                                <div className="desc" style={{maxHeight: '157.5px'}}>
                                  {data.optionShow.map((txt, txtIndex)=>{
                                    return (
                                      <div key={txtIndex}>{txt}<br/></div>
                                    )
                                  })}
                                  <div>{data.orderDesc}</div>
                                </div>
                                <div className="input-group qty">
                                  <div className="input-group-append">
                                    <button className="btn btn-default" onClick={()=> handleOrderMinus(data.PDID) }><i className="fa fa-minus" /></button>
                                  </div>
                                  <input type="text" className="form-control" placeholder={data.orderQty} readOnly/>
                                  <div className="input-group-prepend">
                                    <button className="btn btn-default" onClick={()=> handleOrderPlus(data.PDID) }><i className="fa fa-plus" /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-3 total-price">฿ {total.toFixed(2)} .-</div>
                          { (data.checkRemove)?
                            <div className="pos-remove-confirmation">
                              <svg width="2em" height="2em" viewBox="0 0 16 16" className="mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                              </svg>
                              {t('order_sidebar.remove_item')}
                              <button className="btn btn-white ml-auto mr-2" onClick={()=> handleCancelRemoveOrderProduct(data.PDID)}>{t('order_sidebar.remove_item_no')}</button>
                              <button className="btn btn-danger" onClick={()=> handleConfirmRemoveOrderProduct(data.PDID)}>{t('order_sidebar.remove_item_yes')}</button>
                            </div>
                          : '' }
                        </div>
                      )
                    })}
                  </div>
                </Spin>
                :<div className=" h-100 d-flex align-items-center justify-content-center text-center p-20">
                  <div>
                    <div className="mb-3 mt-n5">
                      <svg width="6em" height="6em" viewBox="0 0 16 16" className="text-gray-300" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z" />
                        <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z" />
                      </svg>
                    </div>
                    <h4>{t('order_sidebar.new_order_found')}</h4>
                  </div>
                </div>
              }
            </div>
            
            <div className="tab-pane fade h-100" id="orderHistoryTab">
              {(loadingOrderList || _.isEmpty(orderList))?
                <div className=" h-100 d-flex align-items-center justify-content-center text-center p-20">
                  <div>
                    <div className="mb-3 mt-n5">
                      <svg width="6em" height="6em" viewBox="0 0 16 16" className="text-gray-300" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z" />
                        <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z" />
                      </svg>
                    </div>
                    <h4>{loadingOrderList? t('order_sidebar.order_history_loading'): t('order_sidebar.order_history_found')}</h4>
                  </div>
                </div>:
                <div className="pos-table">
                  {!_.isEmpty(orderList) && orderList.map((data, index)=>{
                    return(
                      <div className="row pos-table-row" key={index}>
                        <div className="col-9">
                          <div className="pos-product-thumb d-flex align-items-center justify-content-center" style={{cursor: 'pointer'}} onClick={()=> showModalOrderHistory(data)}>
                            { data.ODStatus === "openOrder"
                              ? <div className="img" style={{backgroundImage: `url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU-Ct_2bXbM34u1o2Ghk4-Qqi1qqldZ1zWGu9A5S-hrOcxpHkfw1ij_VtlPgwP5w2NdS4&usqp=CAU)`}}/>
                              :data.ODStatus === "successful" || data.ODStatus === "paid"
                                ? <div className="img" style={{backgroundImage: `url(https://cdn3.iconfinder.com/data/icons/shopping-mix/168/paid_invoice_stamp_bills-512.png)`}}/>
                                : data.ODStatus === "failed"
                                  ? <div className="img" style={{backgroundImage: `url(https://www.paidmembershipspro.com/wp-content/uploads/2017/07/Failed-Payment-Limit.png)`}}/>
                                  : data.ODStatus === "unpaid"
                                  ? <div className="img" style={{backgroundImage: `url(https://pnglux.com/wp-content/uploads/2021/03/1615836827_Hourglass-PNG-Transparent.png)`, backgroundSize: '50%'}}/>
                                  : <div className="img" style={{backgroundImage: `url(https://cdn.iconscout.com/icon/free/png-256/error-1659451-1409973.png)`}}/>
                            }
                            <div className="info">
                              <div className="title">{data.ODID}</div>
                              <div className="single-price">{t('order_sidebar.order_history_point')}: {ToCurrency(data.ODTotalPoint)} 💰</div>
                              <div className="single-price">{t('order_sidebar.order_history_total')}: {ToCurrency(data.ODTotal-data.VoucDiscount)} ฿</div>
                              <div className="single-price">{t('order_sidebar.order_history_date')}: {moment(data.updatedAt).format("DD/MM/YYYY - HH:mm:ss")}</div>
                            </div>
                          </div>
                        </div>
                        <div className="col-3 total-price d-flex align-items-center justify-content-center">
                          { data.ODStatus === "openOrder"
                            ? <span className="label label-yellow">{t('order_sidebar.order_history_not_paid')}</span>
                            :data.ODStatus === "successful" || data.ODStatus === "paid"
                              ? <span className="label label-green">{t('order_sidebar.order_history_paid_successful')}</span>
                              : data.ODStatus === "failed" || data.ODStatus === "unpaid"
                                ? <span className="label label-warning">{data.ODStatus === "unpaid"? t('order_sidebar.order_history_unpaid'): t('order_sidebar.order_history_paid_failed')}</span>
                                : <span className="label label-danger">{t('order_sidebar.order_history_error')}</span>
                          }
                        </div>
                      </div>
                    )
                  })}
                </div>
              }
            </div>
          </div>
          <div className="pos-sidebar-footer">
            <div className="subtotal">
              <div className="text">{t('order_sidebar.order_subtotal')}</div>
              <div className="price">{ToCurrency(subTotal)} .-</div>
            </div>
            {/* <div className="taxes">
              <div className="text">{t('order_sidebar.order_taxes')} (7%)</div>
              <div className="price">{ToCurrency(subTotal*0.07))} .-</div>
            </div> 
            <div className="total">
              <div className="text">{t('order_sidebar.order_total')}</div>
              <div className="price">{ToCurrency(subTotal+(subTotal*0.07))} ฿</div>
            </div> */}
            <div className="btn-row">
              <button className="btn btn-default" onClick={()=> handleService() }><i className="fa fa-bell fa-fw fa-lg" /> {t('order_sidebar.btn_service')}</button>
              <button className="btn btn-default" onClick={()=> handleBill() }><i className="fa fa-file-invoice-dollar fa-fw fa-lg" /> {t('order_sidebar.btn_bill')}</button>
              <button className="btn btn-success" onClick={()=> handleSubmit() } disabled={_.isEmpty(cartItems)? true: false}><i className="fa fa-check fa-fw fa-lg" /> {t('order_sidebar.btn_submit_order')}</button>
            </div>
          </div>
        </div>
        
        <Modal 
          title={<>
                  {t('order_sidebar.model_order_detail.title')} {`${dataOrder.ODID}`} 
                  <span className={`label label-${ dataOrder.ODStatus === "openOrder"? 'yellow':dataOrder.ODStatus === "successful" || dataOrder.ODStatus === "paid"? 'green': dataOrder.ODStatus === "unpaid" || dataOrder.ODStatus === "failed"? 'warning': 'danger'} ml-1`}>
                    {t(`order_sidebar.${ dataOrder.ODStatus === "openOrder"? 'order_history_not_paid':dataOrder.ODStatus === "successful" || dataOrder.ODStatus === "paid"? 'order_history_paid_successful': dataOrder.ODStatus === "unpaid"? 'order_history_unpaid': dataOrder.ODStatus === "failed"? 'order_history_paid_failed': 'order_history_error'}`)}
                  </span>
                </>}
          zIndex={1049}
          width={1000}
          visible={isModalVisible} 
          onCancel={handleCancelOrderHistory}
          footer={dataOrder.ODStatus==="openOrder"? 
          [
            <Button key="back" onClick={handleCancelOrderHistory}>
              {t('order_sidebar.model_order_detail.btn_cancel')}
            </Button>,
            <form id="test" style={{display: 'inline-block', marginLeft: '15px'}} key="submit">
              <Button 
                id="omise-card"
                type="primary" 
                htmlType="button"
                disabled={dataOrder.ODTotal <= 0? true: false}
                // loading={true} 
                onClick={(e)=> handlePay(e)}
              >
                {t('order_sidebar.model_order_detail.btn_pay')}
              </Button>
            </form>
          ]:[
            <Button key="back" onClick={handleCancelOrderHistory}>
              {t('order_sidebar.model_order_detail.btn_cancel')}
            </Button>
          ]}
        >
          <Table 
            rowKey={record => record._ID}
            columns={columnsOrderHistory} 
            dataSource={dataOrderDetail} 
            scroll={{ x: '100vh' }}
            size="small"
            bordered
            pagination={false}
            summary={pageData => {
              const orderPaymentMethod = paymentMethodList.find((x)=>x.PayID ===dataOrder.PayID)
              return (
                <>
                  { dataOrder.ODStatus==="openOrder"?
                    <Table.Summary.Row>
                      <Table.Summary.Cell colSpan={8} className="pt-0 pb-0">
                        <div key="search-voucher" className="search-voucher">
                          {t('order_sidebar.model_order_detail.voucher')}:
                          <Select allowClear autoFocus style={{ width: 300 }} className="ml-3" onChange={onSearchVoucher} options={optionsVoucher}>

                          </Select>
                          {/* <Search placeholder={t('order_sidebar.model_order_detail.search_voucher')} allowClear onSearch={onSearchVoucher} enterButton={false} style={{ width: 200, verticalAlign: 'middle' }} className="ml-3"/> */}
                        </div>
                        <div key="payment-method" className="payment-method option-row" style={{borderTop: '1px solid #e9ecef'}}>
                          {t('order_sidebar.model_order_detail.payment_method')} <Tooltip title={`${t('order_sidebar.model_order_detail.payment_tooltip')} Checkout Counter`}> <InfoCircleOutlined /> </Tooltip>
                          <Radio.Group onChange={onChangePaymentMethod} value={paymentMethod} className="option-list ml-3">
                            {paymentMethodList.map((data)=>{
                              return(
                                <Radio key={data.PayID} value={data.PayID} className="option" disabled={(ToCurrency(dataOrder.ODTotal-dataOrder.VoucDiscount) <= 20 && data.PayID !== "checkout_counter")? true: false}>
                                  <div className="option-label">
                                    {data.PayName}
                                  </div>
                                </Radio>
                              )
                            })}
                          </Radio.Group>
                        </div>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  : null }
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.order_subtotal')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1}>
                      <Text type="danger">฿{ToCurrency(dataOrder.ODTotal)}</Text>
                    </Table.Summary.Cell>
                  </Table.Summary.Row>

                  { dataOrder.ODStatus==="successful" || dataOrder.ODStatus === "paid" || dataOrder.ODStatus === "unpaid" && !_.isEmpty(dataOrder.VoucCode)?
                    <>
                      <Table.Summary.Row>
                        <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.model_order_detail.voucher')}</Table.Summary.Cell>
                        <Table.Summary.Cell colSpan={1}>
                          <Text type="">-฿{ToCurrency(dataOrder.VoucDiscount)}</Text>
                        </Table.Summary.Cell>
                      </Table.Summary.Row>
                      <Table.Summary.Row>
                        <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.order_total')}</Table.Summary.Cell>
                        <Table.Summary.Cell colSpan={1}>
                          <Text type="danger">฿{ToCurrency(dataOrder.ODTotal-dataOrder.VoucDiscount)}</Text>
                        </Table.Summary.Cell>
                      </Table.Summary.Row>
                    </>
                    :
                    <>
                      <Table.Summary.Row>
                        <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.model_order_detail.voucher')} {!_.isEmpty(voucher)? <>{voucher.VoucCode}<Tooltip title={voucher.VoucName}> <InfoCircleOutlined /> </Tooltip></>: null}</Table.Summary.Cell>
                        <Table.Summary.Cell colSpan={1}>
                          <Text type="">-฿{ToCurrency(discount)}</Text>
                        </Table.Summary.Cell>
                      </Table.Summary.Row> 
                      <Table.Summary.Row>
                        <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.order_total')}</Table.Summary.Cell>
                        <Table.Summary.Cell colSpan={1}>
                          <Text type="danger">฿{ToCurrency(dataOrder.ODTotal-(dataOrder.ODTotal>discount? discount: dataOrder.ODTotal))}</Text>
                        </Table.Summary.Cell>
                      </Table.Summary.Row>
                    </>
                  }
                  {/* <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right pr-3">{t('order_sidebar.order_taxes')} (7%)</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1}>
                      <Text type="danger">฿{tax}</Text>
                    </Table.Summary.Cell>
                  </Table.Summary.Row> */}
                  {/* <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right pr-3">{t('order_sidebar.order_total')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1}>
                      <Text type="danger">฿{dataOrder.ODTotal-(dataOrder.ODTotal>discount? discount: dataOrder.ODTotal)}</Text>
                    </Table.Summary.Cell>
                  </Table.Summary.Row> */}

                  { dataOrder.ODStatus==="successful" || dataOrder.ODStatus === "paid" || dataOrder.ODStatus === "unpaid" || dataOrder.ODStatus==="failed"?
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={7} className="text-right pr-3">{t('order_sidebar.model_order_detail.payment_method')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1}>
                      <Text type="">{orderPaymentMethod.PayName || `-`}</Text>
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                  :null }
                  
                </>
              )
            }}
            expandedRowKeys={expandedRowKeysReview}
            expandable={{ expandedRowRender }}
            // expandable={{ expandedRowRender: expandedRowOrder }}
            onExpand={handleOnExpandReview}
          />
        </Modal>
      </>
    )
  } catch (error) {
    console.error(error)
    dispatch({ type: CART_EMPTY})
    return null
  }
}

export default PosSidebar