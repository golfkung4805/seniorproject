const currencyOptions = new Intl.NumberFormat('th-TH', {
  style: 'currency',
  currency: 'THB',
}).resolvedOptions()

const ToCurrency = (number = 0) => {
  return number.toLocaleString('th-TH', { minimumFractionDigits: 2, maximumFractionDigits: 2 })
}

const ToCurrencyRounding = (number = 0) => { // 12345.678 => 12345.68
  return number.toLocaleString('th-TH', { ...currencyOptions, style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 })
}

const currencyToFloat = (number = 0) => {
  return Number.parseFloat(number.replace(/\d/g ,''))
}

export {
  ToCurrency,
  ToCurrencyRounding,
  currencyToFloat
}