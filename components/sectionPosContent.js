import _ from 'lodash'

import { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector} from 'react-redux';

import { useTranslation, i18n } from 'next-i18next'

import { Modal, Button, Form, Input, InputNumber, Checkbox, Radio, Carousel, Spin, Card, Rate } from 'antd';

import { 
  listProduct
} from '../redux/actions/productActions';

import { 
  addToCart,
  showProductDetail
} from '../redux/actions/cartActions';

import { 
  modalOpenLocal,
  modalCloseLocal
} from '../redux/actions/localActions';

import {
  CART_PRODUCT_DETAIL_RESET
} from '../redux/constants/cartConstants'

import { ToCurrency } from './converterCurrency'

const PosContent = ({form}) => {

  const { t } = useTranslation('customerOrder')

  const { TextArea } = Input;

  const dispatch = useDispatch();

  const modalProductDetail = useSelector((state) => state.modalProductDetail)
  const { productDetail } = modalProductDetail

  const localModal = useSelector((state) => state.localModal)
  const { isUpdate, isVisible } = localModal

  const productList = useSelector((state) => state.productList)
  const { loading, data } = productList

  const handleOpenModalProduct = async (value) => {
    dispatch(showProductDetail(value.PDID))
    form.resetFields()
    form.setFieldsValue({qty: 1})
    dispatch(modalOpenLocal())
  }

  const handleCloseModalProduct = async (value) => {
    dispatch(modalCloseLocal())
    dispatch({type: CART_PRODUCT_DETAIL_RESET})
  }

  const handleProductMinus = () => {
    form.setFieldsValue({qty: (parseInt(qty.value) <= 1)? 1: parseInt(qty.value)-1})
  }

  const handleProductPlus = () => {
    form.setFieldsValue({qty: (parseInt(qty.value) >= 20)? 20: parseInt(qty.value)+1})
  }

  const handleAddToCart = () => {
    form.submit();
  }

  const onFormFinish = (values) => {
    // TODO: START _.pull remove ['PDID','qty','description'] in array and _.pick CIID in object values
    let objectOptions = _.pick(values, _.pull(_.keys(values), 'PDID','qty','description'))
    // console.log(JSON.stringify(ODSizeList))
    // TODO: END

    let getPrice = []
    let optionShow = []
    const getKeyOption = _.pull(_.keys(values), 'PDID','qty','description');
    getKeyOption.map((key)=>{
      if(!_.isEmpty(values[key])){
        if(_.isArray(values[key])){ // Checkbox
          values[key].map((optionValue)=>{
            productDetail.CategoryOption.map((data)=>{
              const [ price ] = data.Desc.filter((x)=> x.CIOName === optionValue)
              if(!_.isEmpty(price)){
                getPrice = [...getPrice, price.CIOPrice]
                optionShow = [...optionShow, `- ${data.Name}: ${price.CIOName} +${price.CIOPrice}฿`]
              }
            })
          })
        }else{ // Radio
          productDetail.CategoryOption.map((data)=>{
            const [ price ] = data.Desc.filter((x)=> x.CIOName === values[key])
            if(!_.isEmpty(price)){
              getPrice = [...getPrice, price.CIOPrice]
              optionShow = [...optionShow, `- ${data.Name}: ${price.CIOName} +${price.CIOPrice}฿`]
            }
          })
        }
      }
    })

    values = _.assign({
      PDID: productDetail.PDID,
      PDName: productDetail.PDName,
      PDPrice: productDetail.PDPrice,
      PDImage: !_.isEmpty(productDetail.images)? productDetail.images[0].PDImage :'error.png',
      objectOptions
    }, {optionPrice: getPrice}, {optionShow}, values) 

    dispatch(addToCart(values))
    dispatch(modalCloseLocal())
    dispatch({type: CART_PRODUCT_DETAIL_RESET})
    document.getElementById('tabNewOrderTab').click()
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };

  const PRODUCT_LIST = () => {
    dispatch(listProduct({pagination: {pageSize: 'all'}, filters: { PDStatus: ['1'] } }))
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      PRODUCT_LIST()
    }
  },[])
  
  return(
    <>
      <div className="pos-content">
        <div className="pos-content-container" data-scrollbar="true" data-height="100%" data-skip-mobile="true">
          <Spin spinning={loading} />
          <div className="product-row">
            {
              (loading===false)?
              data.map((value, index)=>{
                return (
                  <div className="product-container" data-type={value.CID} key={index}>
                    <Card
                      hoverable
                      className="product"
                      bodyStyle={{padding: 0}}
                      cover={<div className="img" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/${value.PDImage})`}} />}
                      onClick={() => handleOpenModalProduct(value) }
                    >
                      <div className="text">
                        <div className="title">{value.PDName}</div>
                        <div className="desc">{value.PDDesc}</div>
                        <div className="price">{ToCurrency(value.PDPrice)} ฿</div>
                        <Rate allowHalf disabled defaultValue={Number.parseFloat((value.PDRating))} />
                      </div>
                    </Card>
                  </div>
                )
              }): ''
            }
          </div>
        </div>
      </div>

      <Modal
        bodyStyle={{padding: 0}}
        zIndex={1050}
        visible={isVisible}
        onCancel={() => handleCloseModalProduct()}
        width={1000}
        footer={null}
      >
        <div className="pos-product">
          <div className="pos-product-img">
          <Carousel 
            autoplay={true}
            arrows
            className={{background: '#000'}}
          >
            {(!_.isEmpty(productDetail.images) > 0) ?
              productDetail.images.map((value, index)=>{
                return(
                  <div key={index}>
                    <div className="img" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/${value.PDImage})`}} />
                  </div>
                )
              }):
              <div>
                <div className="img" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/error.png)`}} />
              </div>
            }
          </Carousel>
          </div>
          
          <div className="pos-product-info">
            <Form
              form={form}
              initialValues={{description:''}}
              onFinish={onFormFinish}
              onFinishFailed={onFormFinishFailed}
            >
              <div className="title">{productDetail.PDName}</div>
              <div className="desc">{productDetail.PDDesc}</div>
              <div className="desc"><Rate allowHalf disabled value={Number.parseFloat((productDetail.PDRating))} /></div>
              <div className="price">{ToCurrency(productDetail.PDPrice)} ฿</div>
              <hr />
              <div className="option-row">
              <div className="input-group qty">
                <button type="button" className="btn btn-default" onClick={()=> handleProductMinus() }><i className="fa fa-minus" /></button>
                <Form.Item
                  name="qty"
                  rules={[{ required: true }]}
                >
                    <InputNumber className="form-control border-0 text-center" min={1} max={20} style={{width: "100px" }}/>
                </Form.Item>
                <button type="button" className="btn btn-default" onClick={()=> handleProductPlus() }><i className="fa fa-plus" /></button>
                </div>
              </div>

              {(!_.isEmpty(productDetail.CategoryOption))?
                productDetail.CategoryOption.map((value, index)=>{
                  return(
                    <div className="option-row" key={index}>
                      <div className="option-title">{value.Name}</div>
                        <Form.Item
                          name={value.CIID}
                          rules={[{ required: true, message: `${t('order_content.err_pick_option')} ${value.Name}!`} ]}
                        >
                        { (value.CIMultiple)? 
                          <Checkbox.Group className="option-list">
                            {(!_.isEmpty(value.Desc))?
                              value.Desc.map((value, index)=>{
                                return(
                                  <Checkbox  value={value.CIOName} className="option" key={index}>
                                    <div className="option-label">
                                      <span className="option-text">{value.CIOName}</span>
                                      <span className="option-price">+{ToCurrency(value.CIOPrice)} ฿</span>
                                    </div>
                                  </Checkbox>
                                )
                              }): ''
                            }
                          </Checkbox.Group>:
                        <Radio.Group className="option-list">
                          {(!_.isEmpty(value.Desc))?
                            value.Desc.map((value, index)=>{
                              return(
                                <Radio  value={value.CIOName} className="option" key={index}>
                                  <div className="option-label">
                                    <span className="option-text">{value.CIOName}</span>
                                    <span className="option-price">+{ToCurrency(value.CIOPrice)} ฿</span>
                                  </div>
                                </Radio>
                              )
                            }): ''
                          }
                        </Radio.Group>}
                      </Form.Item>
                    </div>
                  )
                })
              :''}
              <div className="option-row">
                <div className="option-title">รายละเอียด -อื่นๆ</div>
                <div className="option-list">
                  <div className="option" style={{width: '100%', maxWidth: '100%', flex: '0 0 100%'}}>
                    <Form.Item
                      name="description"
                      // rules={[{ required: true, whitespace: true }]}
                    >
                      <TextArea rows={4} className="form-control" style={{resize: 'none'}}/>
                    </Form.Item>
                  </div>
                </div>
              </div>

              <div className="btn-row">
                <button type="button" className="btn btn-default" onClick={() => handleCloseModalProduct()}>{t('order_content.model_btn_cancel')}</button>
                <button type="button" className="btn btn-success" onClick={()=> handleAddToCart() }>{isUpdate? t('order_content.model_btn_update'): t('order_content.model_btn_add')} <i className="fa fa-plus fa-fw ml-2" /></button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default PosContent