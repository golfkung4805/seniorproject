import Header from './header';
import Footer from './footer';

const MainManageLayout = ( props ) => {

  return (
    <>
      {/* <!-- begin #page-container --> */}
      <div id="page-container" className="page-without-sidebar page-header-fixed page-with-light-sidebar">
        <Header />
          { props.children }
        <Footer />

        {/* begin scroll to top btn */}
        <a href='#' onClick={e => e.preventDefault()} className="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i className="fa fa-angle-up" /></a>
        {/* end scroll to top btn */}

      </div>
      
      <script src="/js/app.min.js"></script>
      <script src="/js/default.min.js"></script>
    </>
  )
}

export default MainManageLayout;