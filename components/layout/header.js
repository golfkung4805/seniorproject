import Link from 'next/link'

import qs from 'qs'
import axios from 'axios'
import { 
  v4 as uuidv4, 
  version as uuidVersion, 
  validate as uuidValidate 
} from 'uuid'
import _ from 'lodash';
import Swal from 'sweetalert2'
import jwt from "jsonwebtoken";
import { Howl, Howler } from 'howler';
import Timer from 'react-compound-timer'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { GoogleLogin } from 'react-google-login';
import { Col, Divider, Row, Form, Input, Button, notification, Modal, Select } from 'antd';
import { FacebookOutlined, GoogleOutlined, createFromIconfontCN,  UserOutlined, LockOutlined   } from '@ant-design/icons'
import moment from 'moment'
import 'moment/locale/th'

import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { useTranslation, i18n } from 'next-i18next'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { 
  CUSTOMER_UPDATE_PROFILE_SUCCESS,
  CUSTOMER_SIGNIN_SUCCESS,
  CUSTOMER_SIGNIN_RESET,
  CUSTOMER_CREATE_RESET,
  CUSTOMER_UPDATE_RESET,
} from '../../redux/constants/customerConstants'

import { 
  selectManageLocal
} from '../../redux/actions/localActions';

import { signIn, createCustomer, updateCustomer } from '../../redux/actions/customerActions'

let allNotification = []

const Header = () => {
  moment.locale(i18n.language)

  const router = useRouter()

  const { TableID } = router.query

  const thisPath = router.asPath

  const { Option } = Select;

  const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
  });

  const { t } = useTranslation('header')
  
  const dispatch = useDispatch();

  const [ formLogin ] = Form.useForm()
  const [ formRegister ] = Form.useForm()

  const localSelectManage = useSelector((state) => state.localSelectManage)
  const { isSelectManage: isActive } = localSelectManage


  const customerSignIn = useSelector((status)=> status.customerSignIn)
  const { cusInfo, loading: signInLoading, error: signInError, success: signInSuccess } = customerSignIn

  const customerCreate = useSelector((state) => state.customerCreate)
  const { loading: loadingCreate, success: successCreate, customer: createdCustomer, error: errorCreate } = customerCreate

  const customerUpdate = useSelector((state) => state.customerUpdate)
  const { loading: loadingUpdate, success: successUpdate, customer: updatedCustomer, error: errorUpdate } = customerUpdate

  const localSocket = useSelector((status)=> status.localSocket)
  const { isSocket: socket } = localSocket

  const [ tableUse, setTableUse ] = useState([])
  const [ notificationList, setNotificationList ] = useState([])
  const [ isLoginVisible, setIsLoginVisible ] = useState(false);
  const [ isRegister, setIsRegister ] = useState({ isVisible: false, isUpdate: false });

  const uuidValidateV4 = (uuid) => {
    return uuidValidate(uuid) && uuidVersion(uuid) === 4;
  }

  const SwalFire = (icon = "", title = "", text = "") => {
    if(icon === 'success') {
      // dispatch(modalCloseLocal())
    }
    Swal.fire({
      icon,
      title,
      text: text.msg || text
    })
  }

  const handleOpenNotification = (list, index) => {
    list.isSee = !list.isSee
    const updateNotification = notificationList.map((x)=>x.Order._ID === list.Order._ID ?list :x)
    setNotificationList(updateNotification)
    localStorage.setItem('notification', JSON.stringify(updateNotification))
    // console.log('handle.OpenNotification=>', list)
  }
  
  const handleRemoveNotifications = (e) => {
    e.preventDefault()
    localStorage.setItem('notification', JSON.stringify([]))
    setNotificationList([])
  }

  const checkStatusTable = async () => {
    const { data: getOrderData } = await axios.get(`${process.env.API_URL}/v1/order`, { params: {
      filters: { Status: 'openOrder', Date: moment().format('YYYY-MM-DD') },
      pagination: { current: 1, pageSize: 'all', total: 10 }
    }, paramsSerializer: params => qs.stringify(params) });
    const getTableUse = getOrderData.data.find((x)=>x.TableID === TableID || x.CusID === cusInfo.CusID)
    setTableUse(getTableUse)
    if(_.isEmpty(getTableUse)){
      return true;
    }else{
      return false;
    }
  }

  const showModalLogin = async () => {
    if(await checkStatusTable()){
      formLogin.resetFields();
      setIsLoginVisible(true);
    }else{
      SwalFire('warning', t('common:alertWarning'), t('warningLogin'))
    }
    
  };

  const responseLoginFacebook = async(response) => {
      // console.log('responseLoginFacebook');
      // console.log(response)
      const data = await axios.post(`${process.env.API_URL}/v1/customer/signin/facebook`, response)
      if(data.status === 200){
        dispatch({ type: CUSTOMER_SIGNIN_SUCCESS, payload: data.data })
      }
      if(data.status === 401){
        SwalFire('warning', t('common:alertWarning'), data.data)
      }
  }

  const responseLoginGoogle = async(response) => {
    // console.log('responseLoginGoogle');
    // console.log(response)
    const data = await axios.post(`${process.env.API_URL}/v1/customer/signin/google`, response.profileObj)
    if(data.status === 200){
      dispatch({ type: CUSTOMER_SIGNIN_SUCCESS, payload: data.data })
    }
    if(data.status === 401){
      SwalFire('warning', t('common:alertWarning'), data.data)
    }
  }

  const handleLogin = () => {
    // console.log('handleLogin')
    formLogin.submit()
  }

  const onLoginFinish = (values) => {
    dispatch(signIn(values))
  }

  const handleLogOut = async() => {
    if(await checkStatusTable()){
      dispatch({type: 'CUSTOMER_ANONYMOUS', payload: {CusID: uuidv4()}})
      location.replace(`${process.env.BASE_URL}${thisPath}`)
    }else{
      SwalFire('warning', t('common:alertWarning'), t('warningLogout'))
    }
  }

  const showModalRegister = () => {
    setIsLoginVisible(false);
    setIsRegister({...isRegister, isVisible: true})
  }

  const showModalUpdateProfile = () => {
    const decoded = jwt.verify(cusInfo.token, process.env.JWT_SECRET_KEY);
    // console.log(decoded)
    formRegister.setFieldsValue(decoded)
    setIsRegister({isVisible: true, isUpdate: true})
  }

  const handleRegister = () => {
    // console.log('handleRegister')
    formRegister.submit();
  }

  const onRegisterFinish = (values) => {
    if(isRegister.isUpdate){
      dispatch(updateCustomer(values))
    }else{
      dispatch(createCustomer(values))
    }
  }

  const ISSERVER = typeof window === "undefined";

  useEffect(()=>{
    try {
      let GetCusInfo = JSON.parse(localStorage.getItem("persist:authCus"))
      if(!_.isEmpty(GetCusInfo) && !_.isEmpty(JSON.parse(GetCusInfo.cusInfo))){
        GetCusInfo = JSON.parse(GetCusInfo.cusInfo)
        if(!_.isEmpty(GetCusInfo.token)){
          jwt.verify(GetCusInfo.token, process.env.JWT_SECRET_KEY);
        }
      }else{
        dispatch({type: 'CUSTOMER_ANONYMOUS', payload: {CusID: uuidv4()}})
      }
    } catch (error) {
      console.error(error)
      Swal.fire({
        icon: 'warning',
        title: t('common:alertWarning'),
        text: `${error.name}: ${error.message}`
      }).then((result) => {
        if (result.isConfirmed || result.isDismissed) {
          dispatch({type: 'CUSTOMER_ANONYMOUS', payload: {CusID: uuidv4()}})
          location.replace(`${process.env.BASE_URL}/table`)
        }
      })
    }
  },[])

  useEffect(async()=>{
    if(!_.isEmpty(cusInfo) && router.pathname !== "/order/[TableID]"){
      await checkStatusTable()
    }
  },[cusInfo])

  useEffect(()=>{
    const isPathname = (router.pathname).split('/')
    dispatch(selectManageLocal(isPathname[1]))
    if(!ISSERVER) {
      if(_.isEmpty(allNotification)){
        // const a = JSON.parse(localStorage.getItem("persist:root"));
        // console.log(JSON.parse(a.customerSignIn).cusInfo)
        allNotification = JSON.parse(localStorage.getItem("notification")) || []
        if(allNotification.find((x)=>x.CusID !== cusInfo.CusID)){
          localStorage.setItem("notification", "[]")
          allNotification = []
        }
        setNotificationList(allNotification)
      }
    }

    // if(_.isEmpty(cusInfo) || _.isEmpty(cusInfo.CusID)){
    //   dispatch({type: 'CUSTOMER_ANONYMOUS', payload: {CusID: uuidv4()}})
    // }
  },[router])

  useEffect(()=>{
    if(!_.isEmpty(socket)){
      socket.on('kitchenChangeOrder', (order)=>{
        new Howl({
          src: '../../notification.mp3',
          html5: true,
          autoplay: true,
          volume: 0.3
        })
        
        notification.success({
          message: `${order.Order.ODID}`,
          description: t('notificationOrder', { PDName: order.Order.PDName, Status: order.Order.ODStatus==='complete'?t('notificationStatusComplete') :t('notificationStatusCancel') }),
        })
        // if(Notification.permission === 'granted'){
        //   const a = new Notification("New message from SIRI!",{
        //     body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        //     icon: "https://gitlab.com/uploads/-/system/user/avatar/7360622/avatar.png"
        //   })
        // }else if(Notification.permission !== 'denied'){
        //   Notification.requestPermission().then(permission => {
        //     if(permission == 'granted'){
        //       const a = new Notification("New message from SIRI!",{
        //         body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        //         icon: "https://gitlab.com/uploads/-/system/user/avatar/7360622/avatar.png"
        //       })
        //     }
        //   })
        // }

        allNotification = [...allNotification, order]
        setNotificationList(allNotification)
        localStorage.setItem('notification', JSON.stringify(allNotification))
        // console.log(order)
      })

      socket.on('kitchenNextQueueOrder', (nextOrder)=>{
        new Howl({
          src: '../../notification.mp3',
          html5: true,
          autoplay: true,
          volume: 0.3
        })

        notification.info({
          message: `${nextOrder.Order.ODID}`,
          description: t('notificationNextOrder', { Ref: nextOrder.Order.Ref }),
        })

        allNotification = [...allNotification, nextOrder]
        setNotificationList(allNotification)
        localStorage.setItem('notification', JSON.stringify(allNotification))
      })
    }
  }, [socket])

  useEffect(async()=>{
    if(signInSuccess){
      SwalFire('success', t('common:alertSuccess'), t('loginSuccess'))
      dispatch({ type: CUSTOMER_SIGNIN_RESET })
      
      location.replace(`${process.env.BASE_URL}${thisPath}`)
      setIsLoginVisible(false);
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdCustomer)
      dispatch({ type: CUSTOMER_CREATE_RESET })
      setIsRegister({ isVisible: false, isUpdate: false })
    }
    if(successUpdate){
      const decoded = jwt.verify(cusInfo.token, process.env.JWT_SECRET_KEY);
      const updatedCus = await axios.get(`${process.env.API_URL}/v1/customer/${decoded.CusID}`)
      if(updatedCus.status === 200){
        dispatch({ type: CUSTOMER_UPDATE_PROFILE_SUCCESS, payload: updatedCus.data })
        location.replace(`${process.env.BASE_URL}${thisPath}`)
      }
      SwalFire('success', t('common:alertSuccess'), updatedCustomer)
      dispatch({ type: CUSTOMER_UPDATE_RESET })
      setIsRegister({ isVisible: false, isUpdate: false })
    }

    if(signInSuccess === false){
      SwalFire('error', t('common:alertError'), signInError)
      dispatch({ type: CUSTOMER_SIGNIN_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: CUSTOMER_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: CUSTOMER_UPDATE_RESET })
    }
  },[
    signInSuccess,
    successCreate,
    successUpdate,
  ])
  
  return (
    <>
      {/* begin #header */}
      <div id="header" className="header navbar-default">
        {/* begin navbar-header */}
        <div className="navbar-header">
          <a className="navbar-brand" style={{justifyContent: 'start'}} onClick={()=>{(!_.isEmpty(tableUse) && router.pathname !== "/order/[TableID]")? router.push(`${process.env.BASE_URL}/order/${tableUse.TableID}`): ``}}><span className="navbar-logo" /> <b className="pr-1">SR</b>Restaurant</a>
          <button type="button" className="navbar-toggle pt-0 pb-0 mr-0" data-toggle="collapse" data-target="#top-navbar">
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>

        </div>
        {/* end navbar-header */}
        {/* begin navbar-collapse */}
        <div className="collapse navbar-collapse" id="top-navbar">
          <ul className="nav navbar-nav">
            <li className={`dropdown dropdown-lg ${(isActive==="table")?"active":""}`}>
              <Link href='/table'>
                <a><i className="fa fa-table fa-fw mr-1 mr-md-0" /> Table</a>
              </Link>
            </li>
            <li className={`dropdown dropdown-lg ${(isActive==="voucher")?"active":""}`}>
              <Link href='/voucher'>
                <a><i className="fa fa-gift fa-fw mr-1 mr-md-0" /> Voucher</a>
              </Link>
            </li>
            <li className={`dropdown dropdown-lg ${(isActive==="order")?"active":""}`}>
              <Link href='/order'>
                <a><i className="fa fa-concierge-bell fa-fw mr-1 mr-md-0" /> Order</a>
              </Link>
            </li>
            <li className="dropdown navbar-language">
              <a href="#" className="dropdown-toggle pr-1 pl-1 pr-sm-3 pl-sm-3" data-toggle="dropdown" aria-expanded="false">
                <span className={`flag-icon flag-icon-${ i18n.language === 'th' ? 'th' : 'us' }`} />
                <span className="name d-none d-sm-inline"> { i18n.language === 'th' ? 'ไทย' : 'EN' } </span> <b className="caret" />
              </a>
              <div className="dropdown-menu dropdown-menu-right">
                {/* <Link href={ thisPath } locale='th'>  */}
                  <a className="dropdown-item" onClick={()=> location.replace(`${process.env.BASE_URL}/${thisPath}`) }><span className="flag-icon flag-icon-th" /> ภาษาไทย</a>
                {/* </Link> */}
                {/* <Link href={ thisPath } locale='en'>  */}
                  <a className="dropdown-item" onClick={()=> location.replace(`${process.env.BASE_URL}/en/${thisPath}`) }><span className="flag-icon flag-icon-us" /> English</a>
                {/* </Link> */}
              </div>
            </li>
          </ul>
        </div>
        {/* end navbar-collapse */}

        {/* begin header-nav */}
        <ul className="navbar-nav navbar-right">
          { (router.pathname==="/order/[TableID]") && (
            <li className="dropdown">
              <a href="#" data-toggle="dropdown" className="dropdown-toggle f-s-14">
                <i className="fa fa-bell" />
                { notificationList.reduce((a, b)=> a + !b.isSee, 0) > 0 && (<span className="label">{notificationList.reduce((a, b)=> a + !b.isSee, 0)}</span>) }
              </a>
              <div className="dropdown-menu media-list dropdown-menu-right">
                <div className="dropdown-header">{t('notification.text')} ({notificationList.length})</div>
                  { notificationList.length < 1 && (<div className="dropdown-item media">
                    <div className="w-100 text-center">
                      <h6 className="media-heading">{t('notificationEmpty')}</h6>
                    </div>
                  </div>) }
                  {notificationList.map((list, index)=>{
                    return(
                      <a onClick={() => handleOpenNotification(list, index)} className="dropdown-item media" key={index}>
                        <div className="media-left">
                          <i className="fa fa-bell media-object bg-silver-darker" />
                          {list.isSee === false && (<i className="media-object-icon bg-red" />)}
                          {/* <img src="/assets/img/user/user-3.jpg" className="media-object" />
                          <i className="fa fa-plus media-object bg-silver-darker" />
                          <i className="fab fa-facebook-messenger text-blue media-object-icon" />
                          <i className="fab fa-google text-warning media-object-icon f-s-14" /> */}
                        </div>
                        <div className="media-body">
                          <h6 className="media-heading">{t('notification.order', { ODID: list.Order.ODID })} {/* <i className="fa fa-exclamation-circle text-danger" /> */}</h6>
                          {list.Type === "ChangeOrder" && (<p>{ t('notificationOrder', { PDName: list.Order.PDName, Status: list.Order.ODStatus==='1'?t('notificationStatusComplete') :t('notificationStatusCancel') }) }</p>)}
                          {list.Type === "NextQueue" && (<p>{ t('notificationNextOrder', { Ref: list.Order.Ref }) }</p>)}
                          <div className="text-muted f-s-10">
                            {<Timer initialTime={moment().diff(moment(list.Order.updatedAt), 'minutes', true) * 60000}>
                              <Timer.Hours formatValue={value => `${(value > 0)? `${value} ${t('notification.hour')} `:''}`} />
                              <Timer.Minutes formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('notification.minute')} `} />
                              <Timer.Seconds formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('notification.seconds')}`} />
                            </Timer>}
                          </div>
                        </div>
                      </a>
                    )
                  })}

                <div className="dropdown-footer text-center">
                  <a onClick={(e) => handleRemoveNotifications(e)}>{t('notificationClear')} </a>
                </div>
              </div>
            </li>
          ) }

          {!uuidValidateV4(cusInfo.CusID) && (<li className="dropdown">
            <a className="dropdown-toggle f-s-14">
              {t('point')}: {cusInfo.CusPoint}
            </a>
          </li>)}

         
          <li className="dropdown navbar-user">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <img src={(_.isEmpty(cusInfo.CusPicture))?`/assets/img/user/user-3.jpg`: cusInfo.CusPicture} /> 
              <span className="d-none d-md-inline">{uuidValidateV4(cusInfo.CusID)?cusInfo.CusID: `[${cusInfo.CusID}] ${cusInfo.CusNickname}`}</span> <b className="caret" />
            </a>
            <div className="dropdown-menu dropdown-menu-right">
            {cusInfo.isLogin?
              <>
                { cusInfo.CusID.slice(0, 3) === "Cus" && (
                  <>
                    <a onClick={() => showModalUpdateProfile()} className="dropdown-item">{t('navbarUser.editProfile')}</a>
                    <div className="dropdown-divider" />
                  </>
                ) }
                {/* <a href='#' onClick={e => e.preventDefault()} className="dropdown-item"><span className="badge badge-danger pull-right">2</span> Inbox</a>
                <a href='#' onClick={e => e.preventDefault()} className="dropdown-item">Calendar</a>
                <a href='#' onClick={e => e.preventDefault()} className="dropdown-item">Setting</a> */}
                
                <a onClick={()=> handleLogOut()} className="dropdown-item">{t('navbarUser.logOut')}</a>
              </>:
              <>
                <a onClick={()=> showModalLogin()} className="dropdown-item">{t('navbarUser.logIn')}</a>
                <div className="dropdown-divider" />
                <a onClick={()=> showModalRegister()} className="dropdown-item">{t('navbarUser.register')}</a>
              </>
            }
            </div>
          </li>
          { !_.isEmpty(tableUse) && router.pathname !== "/order/[TableID]" && (<li className="dropdown">
            <Link href={`${process.env.BASE_URL}/order/${tableUse.TableID}`}> 
              <a className="dropdown-toggle f-s-14">
                {t('header:navbarUser.backTable')}
              </a>
            </Link>
          </li>) }
          
        </ul>
        {/* end header-nav */}
      </div>
      {/* end #header */}
      <Modal 
        title={t('navbarUser.logIn')}
        className="modal-sm"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        visible={isLoginVisible} 
        footer={null}
        onCancel={()=> setIsLoginVisible(false)}
      >
        <Form
          layout="vertical"
          form={formLogin}
          onFinish={onLoginFinish}
        >
          <Row gutter={24}>
            <Col xs={12} justify="space-around" align="middle">
              <FacebookLogin
                appId={process.env.FACEBOOK_APP_ID}
                fields="name,email,picture"
                callback={responseLoginFacebook} 
                render={(renderProps) => (
                  <Button size="large" icon={<IconFont type="icon-facebook" className="align-middle"/>} onClick={renderProps.onClick}>Facebook</Button>
                )}
              />
            </Col>
            <Col xs={12} justify="space-around" align="middle">
              <GoogleLogin
                clientId={process.env.GOOGLE_CLIENT_ID}
                onSuccess={responseLoginGoogle}
                onFailure={responseLoginGoogle}
                cookiePolicy={'single_host_origin'}
                render={(renderProps)=> (
                  <Button className="ml-1" size="large" icon={<GoogleOutlined className="align-middle"/>} danger onClick={renderProps.onClick}>Google</Button>
                )}
              />
            </Col>
            <Col xs={24} justify="space-around" align="middle">
            <Divider>{t('navbarUser.dividerOr')}</Divider>
            </Col>
            <Col xs={24}>
              <Form.Item
                name="username" 
                rules={[{ required: true, whitespace: true, message: t('warningUsername') }]} 
              >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder={t('navbarUser.txtUsername')} />
              </Form.Item>
            </Col>
            <Col xs={24}>
              <Form.Item
                name="password" 
                rules={[{ required: true, whitespace: true, message: t('warningPassword') }]} 
              >
                <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder={t('navbarUser.txtPassword')} />
              </Form.Item>
            </Col>
            <Col xs={24}>
              <Form.Item className="mb-2">
                <Button type="primary" className="w-100" loading={signInLoading} onClick={()=> handleLogin()}>{t('navbarUser.logIn')}</Button>
              </Form.Item>
            </Col>  
            <Col xs={24}>
              <Form.Item className="mb-2">
                <Button className="w-100" onClick={()=> showModalRegister()}>{t('navbarUser.register')}</Button>
              </Form.Item>
            </Col>          
          </Row>
        </Form>
      </Modal>

      <Modal 
        title={isRegister.isUpdate?t('navbarUser.editProfile') :t('navbarUser.register') }
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        visible={isRegister.isVisible} 
        onCancel={()=> setIsRegister({...isRegister, isVisible: false})}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isRegister.isUpdate? "warning": "primary"}`} 
            // loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            onClick={handleRegister}
          >
            {isRegister.isUpdate? t('navbarUser.editProfile'): t('navbarUser.register')}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={()=> setIsRegister({...isRegister, isVisible: false})}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        <Form
          layout="vertical"
          form={formRegister}
          onFinish={onRegisterFinish}
        >
          <Row gutter={24}>
            <Col xs={24} lg={24} xl={4}>
              <Form.Item 
                label={t('customer:table.id')} 
                name="CusID"
              >
                <Input disabled={true} />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={6}>
              <Form.Item 
                label={t('customer:table.username')} 
                name="CusUsername"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input disabled={isRegister.isUpdate? true: false}/>
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={6}>
              <Form.Item 
                label={t('customer:table.password')} 
                name="CusPasswd"
              >
                <Input.Password />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={8}>
              <Form.Item 
                label={t('customer:table.email')} 
                name="CusEmail"
                rules={[{  type: 'email', required: true, whitespace: true }]}
              >
                <Input disabled={isRegister.isUpdate? true: false} />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={4}>
              <Form.Item 
                label={t('customer:table.gender')}  
                name="CusGender"
                rules={[{ type:'enum', required: true, whitespace: true, enum: ['นาย', 'นาง', 'นางสาว'] }]}
              >
                <Select
                  showSearch
                  allowClear
                >
                  <Option value="นาย">นาย</Option>
                  <Option value="นาง">นาง</Option>
                  <Option value="นางสาว">นางสาว</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('customer:table.firstname')} 
                name="CusFirstname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('customer:table.lastname')} 
                name="CusLastname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('customer:table.nickname')} 
                name="CusNickname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('customer:table.tell')} 
                name="CusTel"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  )
}

export default Header