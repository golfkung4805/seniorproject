import { useEffect, useState } from 'react'

import _ from 'lodash'
import Swal from 'sweetalert2'
import { io } from "socket.io-client"
import moment from 'moment'
import 'moment/locale/th'
import { notification, Form, Modal, Select, Row, Col, Button, Input } from 'antd';

import Timer from 'react-compound-timer'
import { Howl, Howler } from 'howler';
import jwt from "jsonwebtoken";

import Link from 'next/link'
import { useRouter } from 'next/router'
import { useTranslation, i18n } from 'next-i18next'
import { useDispatch, useSelector } from 'react-redux'

import { 
  EMPLOYEE_UPDATE_RESET
} from '../../../redux/constants/employeeConstants'

import { updateEmployee } from '../../../redux/actions/employeeActions';

import { EMPLOYEE_SIGNIN_SUCCESS, EMPLOYEE_SIGNOUT } from '../../../redux/constants/employeeConstants'
import axios from 'axios'

let allNotification = [], GetEmpInfo = {}

const Header = () => {
  moment.locale(i18n.language)

  const router = useRouter()
  const thisPath = router.pathname

  const { t } = useTranslation('header')
  
  const dispatch = useDispatch()

  const [ formRegister ] = Form.useForm()

  const { Option } = Select;

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo } = employeeSignIn

  const employeeUpdate = useSelector((state) => state.employeeUpdate)
  const { loading: loadingUpdate, success: successUpdate, employee: updatedEmployee, error: errorUpdate } = employeeUpdate

  const [ socket, setSocket ] = useState({})
  const [ employeeInfo, setEmployeeInfo ] = useState({})
  const [ notificationList, setNotificationList ] = useState([])
  const [ dataEmpUpdate, setDataEmpUpdate ] = useState({})
  const [ isUpdateProfileVisible, setIsUpdateProfileVisible ] = useState(false);

  const SwalFire = (icon, title, text) => {
    // if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg || text
    })
  }

  const handleOpenNotification = (list, index) => {
    list.isSee = !list.isSee
    const updateNotification = notificationList.map((x)=>x._ID === list._ID ?list :x)
    setNotificationList(updateNotification)
    localStorage.setItem('manageNotification', JSON.stringify(updateNotification))
    // console.log('handle.OpenNotification=>', list)
  }

  const handleRemoveNotifications = (e) => {
    e.preventDefault()
    localStorage.setItem('manageNotification', JSON.stringify([]))
    setNotificationList([])
  }
  const showModalUpdateProfile = () => {
    const decoded = jwt.verify(empInfo.token, process.env.JWT_SECRET_KEY);
    formRegister.setFieldsValue(decoded)
    setIsUpdateProfileVisible(true)
  }

  const handleRegister = () => {
    // console.log('handleRegister')
    formRegister.submit();
  }

  const onRegisterFinish = (values) => {
    setDataEmpUpdate(values)
    dispatch(updateEmployee(values))
  }

  const handleSingOut = () => {
    dispatch({type: EMPLOYEE_SIGNOUT})
    location.replace(`${process.env.BASE_URL}/manage/login`)
  }

  const ISSERVER = typeof window === "undefined";

  useEffect(()=>{
    try {
      GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo)
      // console.log(GetEmpInfo)
      const decoded = jwt.verify(GetEmpInfo.token, process.env.JWT_SECRET_KEY);
      setEmployeeInfo(decoded);
    } catch (error) {
      console.error(error)
      dispatch({type: EMPLOYEE_SIGNOUT})
      location.replace(`${process.env.BASE_URL}/manage/login`)
    }
  },[])

  useEffect(()=>{
    if(!ISSERVER) {
      allNotification = JSON.parse(localStorage.getItem("manageNotification")) || []
      setNotificationList(allNotification)
    }
  },[router])

  useEffect(()=>{
    if(_.isEmpty(socket)){
      if(!_.isEmpty(GetEmpInfo)){
        GetEmpInfo = jwt.verify(GetEmpInfo.token, process.env.JWT_SECRET_KEY);
      }
      const sk = io(process.env.API_URL)
      sk.emit('online', {
        CusID: GetEmpInfo._id,
        isLogin: true,
      })
      setSocket(sk)
      sk.on('newService', newService => {
        new Howl({
          src: '../../../notification.mp3',
          html5: true,
          autoplay: true,
          volume: 0.3
        })
        
        notification.warning({
          message: `${newService.TableID}`,
          description: `${t('notificationRequirement')}: ${newService.message}`,
        })

        allNotification = [...allNotification, newService]
        setNotificationList(allNotification)
        localStorage.setItem('manageNotification', JSON.stringify(allNotification))
        // console.log(newService)
      })
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  useEffect(async()=>{
    if(successUpdate){
      if(!_.isEmpty(dataEmpUpdate)){
        const decoded = jwt.verify(empInfo.token, process.env.JWT_SECRET_KEY);
        const updatedEmp = await axios.get(`${process.env.API_URL}/v1/employee/${decoded._id}`)
        if(updatedEmp.status === 200){
          dispatch({ type: EMPLOYEE_SIGNIN_SUCCESS, payload: updatedEmp.data })
        }
        setDataEmpUpdate({})
      }
      setIsUpdateProfileVisible(false)
      dispatch({ type: EMPLOYEE_UPDATE_RESET })
    }
    if(successUpdate && thisPath !== "/manage/employee"){
      SwalFire('success', t('common:alertSuccess'), updatedEmployee)
      dispatch({ type: EMPLOYEE_UPDATE_RESET })
    }

    if(successUpdate === false && thisPath !== "/manage/employee"){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: EMPLOYEE_UPDATE_RESET })
    }
  },[
    successUpdate,
  ])

  return (
    <>
      {/* begin #header */}
      <div id="header" className="header navbar-default">
        {/* begin navbar-header */}
        <div className="navbar-header">
          <Link href='/manage/'> 
            <a className="navbar-brand"><span className="navbar-logo" /> <b className="pr-1">SR</b> RestaurantManage</a>
          </Link>
          <button type="button" className="navbar-toggle" data-click="sidebar-toggled">
            <span className="icon-bar" />
            <span className="icon-bar" />
            <span className="icon-bar" />
          </button>
        </div>
        {/* end navbar-header */}{/* begin header-nav */}
        <ul className="navbar-nav navbar-right">
          <li className="dropdown">
            <a href="#" data-toggle="dropdown" className="dropdown-toggle f-s-14">
              <i className="fa fa-bell" />
              { notificationList.reduce((a, b)=> a + !b.isSee, 0) > 0 && (<span className="label">{notificationList.reduce((a, b)=> a + !b.isSee, 0)}</span>) }
            </a>
            <div className="dropdown-menu media-list dropdown-menu-right" style={{minWidth: "350px"}} >
              <div className="dropdown-header">{t('notification.text')} ({notificationList.length})</div>
              { notificationList.length < 1 && (<div className="dropdown-item media">
                <div className="w-100 text-center">
                  <h6 className="media-heading">{t('notificationEmpty')}</h6>
                </div>
              </div>) }
              {notificationList.map((list, index)=>{
                return(
                  <a onClick={() => handleOpenNotification(list, index)} className="dropdown-item media" key={index}>
                    <div className="media-left">
                      <i className="fa fa-bell media-object bg-silver-darker" />
                      {list.isSee === false && (<i className="media-object-icon bg-red" />)}
                      {/* <img src="/assets/img/user/user-3.jpg" className="media-object" />
                      <i className="fa fa-plus media-object bg-silver-darker" />
                      <i className="fab fa-facebook-messenger text-blue media-object-icon" />
                      <i className="fab fa-google text-warning media-object-icon f-s-14" /> */}
                    </div>
                    <div className="media-body">
                      <h6 className="media-heading">{t('notificationRequirement')}: {list.TableID} {/* <i className="fa fa-exclamation-circle text-danger" /> */}</h6>
                      <p>{`${list.CusID}: ${list.message}`}</p>
                      <div className="text-muted f-s-10">
                        {<Timer initialTime={moment().diff(moment(list.serviceAt), 'minutes', true) * 60000}>
                          <Timer.Hours formatValue={value => `${(value > 0)? `${value} ${t('notification.hour')} `:''}`} />
                          <Timer.Minutes formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('notification.minute')} `} />
                          <Timer.Seconds formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('notification.seconds')}`} />
                        </Timer>}
                      </div>
                    </div>
                  </a>
                )
              })}
              
              <div className="dropdown-footer text-center">
                <a onClick={(e) => handleRemoveNotifications(e)}>{t('notificationClear')} </a>
              </div>
            </div>
          </li>
          <li className="dropdown navbar-language">
            <a href="#" className="dropdown-toggle pr-1 pl-1 pr-sm-3 pl-sm-3" data-toggle="dropdown" aria-expanded="false">
              <span className={`flag-icon flag-icon-${ i18n.language === 'th' ? 'th' : 'us' }`} />
              <span className="name d-none d-sm-inline"> { i18n.language === 'th' ? 'ไทย' : 'EN' } </span> <b className="caret" />
            </a>
            <div className="dropdown-menu">
              <Link href={ thisPath } locale='th'> 
                <a className="dropdown-item"><span className="flag-icon flag-icon-th" /> ภาษาไทย</a>
              </Link>
              <Link href={ thisPath } locale='en'> 
                <a className="dropdown-item"><span className="flag-icon flag-icon-us" /> English</a>
              </Link>
            </div>
          </li>

          <li className="dropdown navbar-user">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <img src="/assets/img/user/user-3.jpg" /> 
              <span className="d-none d-md-inline">{`${employeeInfo._id}: ${empInfo.name}`}</span> <b className="caret" />
            </a>
            <div className="dropdown-menu dropdown-menu-right">
              <a onClick={()=> showModalUpdateProfile()} className="dropdown-item">{t('navbarUser.editProfile')}</a>
              {/* <a href='#' onClick={e => e.preventDefault()} className="dropdown-item"><span className="badge badge-danger pull-right">2</span> Inbox</a>
              <a href='#' onClick={e => e.preventDefault()} className="dropdown-item">Calendar</a>
              <a href='#' onClick={e => e.preventDefault()} className="dropdown-item">Setting</a> */}
              <div className="dropdown-divider" />
              <a onClick={() => handleSingOut()}  className="dropdown-item">{t('navbarUser.logOut')}</a>
            </div>
          </li>
        </ul>
        {/* end header-nav */}
      </div>
      {/* end #header */}

      <Modal 
        title={t('navbarUser.editProfile')}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        visible={isUpdateProfileVisible} 
        onCancel={()=> setIsUpdateProfileVisible(false)}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-warning`} 
            // loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            onClick={handleRegister}
          >
            {t('navbarUser.editProfile')}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={()=> setIsUpdateProfileVisible(false)}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        <Form
          layout="vertical"
          form={formRegister}
          onFinish={onRegisterFinish}
        >
          <Row gutter={24}>
            <Col xs={24} lg={24} xl={4}>
              <Form.Item 
                label={t('employee:table.id')} 
                name="EmpID"
              >
                <Input disabled={true} />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={6}>
              <Form.Item 
                label={t('employee:table.username')} 
                name="EmpUsername"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input disabled/>
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={6}>
              <Form.Item 
                label={t('employee:table.password')} 
                name="EmpPasswd"
              >
                <Input.Password />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={8}>
              <Form.Item 
                label={t('employee:table.email')} 
                name="EmpEmail"
                rules={[{  type: 'email', required: true, whitespace: true }]}
              >
                <Input disabled />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={4}>
              <Form.Item 
                label={t('employee:table.gender')}  
                name="EmpGender"
                rules={[{ type:'enum', required: true, whitespace: true, enum: ['นาย', 'นาง', 'นางสาว'] }]}
              >
                <Select
                  showSearch
                  allowClear
                >
                  <Option value="นาย">นาย</Option>
                  <Option value="นาง">นาง</Option>
                  <Option value="นางสาว">นางสาว</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('employee:table.firstname')} 
                name="EmpFirstname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('employee:table.lastname')} 
                name="EmpLastname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('employee:table.nickname')} 
                name="EmpNickname"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} lg={24} xl={5}>
              <Form.Item 
                label={t('employee:table.tell')} 
                name="EmpTel"
                rules={[{ required: true, whitespace: true }]}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  )
}

export default Header