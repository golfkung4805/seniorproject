import { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd';

import { Tooltip, Upload, message } from 'antd'
import { ExcelRenderer } from "react-excel-renderer";

const checkColumnFileImport = async (t, maxUpload, column = [], fileList, fileObj, fileListObj) => {
  const newObjLength = fileListObj.length
  if((fileList.length + newObjLength) <= maxUpload){
    if( _.isEmpty(column) ){
      return true
    }else{
      const resp = await ExcelRenderer(fileObj, (err, resp)=>{ if (err) console.log(err) })
      if( _.isEqual(column ,_.uniq(resp.rows[0])) ){
        return true
      }else{
        message.error(t('common:upload.column_not_match'));
        return Upload.LIST_IGNORE
      }
    }
    
  }else{
    message.error(t('common:upload.max_upload', {max: maxUpload}));
    return Upload.LIST_IGNORE
  }
}

const type = 'DragableUploadList';
const DragableUploadListItem = ({ originNode, moveRow, file, fileList }) => {
  const ref = useRef();
  const index = fileList.indexOf(file);

  const [{ isOver, dropClassName }, drop] = useDrop({
    accept: type,
    collect: monitor => {
      const { index: dragIndex } = monitor.getItem() || {};
      if (dragIndex === index) {
        return {};
      }
      return {
        isOver: monitor.isOver(),
        dropClassName: dragIndex < index ? ' drop-over-downward' : ' drop-over-upward',
      };
    },
    drop: item => {
      moveRow(item.index, index);
    },
  });
  const [, drag] = useDrag({
    type,
    item: { index },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });
  drop(drag(ref));
  const errorNode = <Tooltip title="Upload Error">{originNode.props.children}</Tooltip>;
  return (
    <div
      ref={ref}
      className={`ant-upload-draggable-list-item ${isOver ? dropClassName : ''}`}
      style={{ cursor: 'move' }}
    >
      {file.status === 'error' ? errorNode : originNode}
    </div>
  );
}

export {
  checkColumnFileImport,
  DragableUploadListItem
}