import Link from 'next/link'
import { useMemo, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation, i18n } from 'next-i18next'

import { 
  selectManageLocal
} from '../../../redux/actions/localActions';
import _ from 'lodash'

const Sidebar = () => {

  const { t } = useTranslation('sidebar')
  const router = useRouter()
  const dispatch = useDispatch()
  
  const localSelectManage = useSelector((state) => state.localSelectManage)
  const { isSelectManage: isActive } = localSelectManage

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo } = employeeSignIn

  let isPathname = (router.pathname).split('/')
  isPathname = isPathname.length === 3? isPathname[2]: isPathname[1]

  useEffect(()=>{
    isPathname = (router.pathname).split('/')
    isPathname = isPathname.length === 3? isPathname[2]: isPathname[1]
    const rolePage = {
      'admin': [ 'manage', 'employee', 'customer', 'category', 'product', 'table', 'voucher', 'payment', 'order', 'counter', 'kitchen', 'setting' ],
      'counter': [ 'customer', 'product', 'table', 'voucher', 'payment', 'order', 'counter', 'kitchen' ],
      'kitchen': [ 'kitchen' ],
    }
    dispatch(selectManageLocal(isPathname))
  },[router])

  useEffect(()=>{
    if(!_.isEmpty(empInfo)){
      const rolePage = {
        'admin': [ 'manage', 'employee', 'customer', 'category', 'product', 'table', 'voucher', 'payment', 'order', 'counter', 'kitchen', 'setting' ],
        'counter': [ 'customer', 'product', 'table', 'voucher', 'payment', 'order', 'counter', 'kitchen' ],
        'kitchen': [ 'kitchen' ],
      }
      if(_.isEmpty(rolePage[empInfo.isRole].find((x)=>x===isPathname))){
        location.replace(`${process.env.BASE_URL}/manage/${empInfo.isRole}`)
      }
    }
  },[router, empInfo])

  return(
    <>
      <div>
        {/* begin #sidebar */}
        <div id="sidebar" className="sidebar">
          {/* begin sidebar scrollbar */}
          <div data-scrollbar="true" data-height="100%">
            {/* begin sidebar user */}
            <ul className="nav">
              <li className="nav-profile">
                <a href='#' onClick={e => e.preventDefault()} data-toggle="nav-profile">
                  <div className="cover with-shadow" />
                  <div className="image">
                    <img src="/assets/img/user/user-3.jpg" />
                  </div>
                  <div className="info">
                    {/* <b className="caret pull-right" /> */}
                    {empInfo.name}
                    <small>SR:Restaurant Manage</small>
                  </div>
                </a>
              </li>
              {/* <li>
                <ul className="nav nav-profile">
                  <li><a href='#' onClick={e => e.preventDefault()}><i className="fa fa-cog" /> Settings</a></li>
                  <li><a href='#' onClick={e => e.preventDefault()}><i className="fa fa-pencil-alt" /> Send Feedback</a></li>
                  <li><a href='#' onClick={e => e.preventDefault()}><i className="fa fa-question-circle" /> Helps</a></li>
                </ul>
              </li> */}
            </ul>
            {/* end sidebar user */}
            {/* begin sidebar nav */}
            <ul className="nav">
              <li className="nav-header">{t('sidebar_header')}</li>
              {empInfo.isRole === 'admin' && (<li className={(isActive==="manage")?"active":""}>
                <Link href='/manage'>
                  <a>
                    <i className="fas fa-chart-bar" />
                    <span>{t('sidebar_dashboard')}</span> 
                  </a>
                </Link>
              </li>)}
              {empInfo.isRole === 'admin' && (<li className={(isActive==="employee")?"active":""}>
                <Link href='/manage/employee'>
                  <a>
                    <i className="fas fa-user" />
                    <span>{t('sidebar_employee')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="customer")?"active":""}>
                <Link href='/manage/customer'>
                  <a>
                    <i className="fas fa-users" />
                    <span>{t('sidebar_customer')}</span> 
                  </a>
                </Link>
              </li>)}
              {empInfo.isRole === 'admin' && (<li className={(isActive==="category")?"active":""}>
                <Link href='/manage/category'>
                  <a>
                    <i className="fas fa-tags" />
                    <span>{t('sidebar_category')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="product")?"active":""}>
                <Link href='/manage/product'>
                  <a>
                    <i className="fas fa-utensils" />
                    <span>{t('sidebar_product')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="table")?"active":""}>
                <Link href='/manage/table'>
                  <a>
                    <i className="fas fa-map-pin" />
                    <span>{t('sidebar_table')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="voucher")?"active":""}>
                <Link href='/manage/voucher'>
                  <a>
                    <i className="far fa-money-bill-alt" />
                    <span>{t('sidebar_voucher')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="payment")?"active":""}>
                <Link href='/manage/payment'>
                  <a>
                    <i className="fas fa-dollar-sign" />
                    <span>{t('sidebar_payment')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="order")?"active":""}>
                <Link href='/manage/order'>
                  <a>
                    <i className="fas fa-shopping-basket" />
                    <span>{t('sidebar_order')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter') && (<li className={(isActive==="counter")?"active":""}>
                <Link href='/manage/counter'>
                  <a>
                    <i className="fas fa-calculator" />
                    <span>{t('sidebar_counter')}</span> 
                  </a>
                </Link>
              </li>)}
              {(empInfo.isRole === 'admin' || empInfo.isRole === 'counter' || empInfo.isRole === 'kitchen') && (<li className={(isActive==="kitchen")?"active":""}>
                <Link href='/manage/kitchen'>
                  <a>
                    <i className="fas fa-utensils" />
                    <span>{t('sidebar_kitchen')}</span> 
                  </a>
                </Link>
              </li>)}
              {empInfo.isRole === 'admin' && (<li className={(isActive==="setting")?"active":""}>
                <Link href='/manage/setting'>
                  <a>
                    <i className="fas fa-cogs" />
                    <span>{t('sidebar_setting')}</span> 
                  </a>
                </Link>
              </li>)}
              {/* <li>
                <a>
                  <i className="fab fa-simplybuilt" />
                  <span>Widgets <span className="label label-theme">NEW</span></span> 
                </a>
              </li>
              <li className="has-sub">
                <a href='#' onClick={e => e.preventDefault()}>
                  <b className="caret" />
                  <span className="badge pull-right">10</span>
                  <i className="fa fa-align-left" />
                  <span>Menu Level</span>
                </a>
                <ul className="sub-menu">
                  <li className="has-sub">
                    <a href='#' onClick={e => e.preventDefault()}>
                      <b className="caret" />
                      Menu 1.1
                    </a>
                    <ul className="sub-menu">
                      <li className="has-sub">
                        <a href='#' onClick={e => e.preventDefault()}>
                          <b className="caret" />
                          Menu 2.1
                        </a>
                        <ul className="sub-menu">
                          <li><a href='#' onClick={e => e.preventDefault()}>Menu 3.1</a></li>
                          <li><a href='#' onClick={e => e.preventDefault()}>Menu 3.2</a></li>
                        </ul>
                      </li>
                      <li><a href='#' onClick={e => e.preventDefault()}>Menu 2.2</a></li>
                      <li><a href='#' onClick={e => e.preventDefault()}>Menu 2.3</a></li>
                    </ul>
                  </li>
                  <li><a href='#' onClick={e => e.preventDefault()}>Menu 1.2</a></li>
                  <li><a href='#' onClick={e => e.preventDefault()}>Menu 1.3</a></li>
                </ul>
              </li> */}
              {/* begin sidebar minify button */}
              <li><a href='#' onClick={e => e.preventDefault()} className="sidebar-minify-btn" data-click="sidebar-minify"><i className="fa fa-angle-double-left" /></a></li>
              {/* end sidebar minify button */}
            </ul>
            {/* end sidebar nav */}
          </div>
          {/* end sidebar scrollbar */}
        </div>
        <div className="sidebar-bg" />
        {/* end #sidebar */}
      </div>
    </>
  )
}

export default Sidebar