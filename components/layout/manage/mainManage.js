import Header from './header';
import Sidebar from './sidebar';
import Footer from './footer';

const MainManageLayout = ( props ) => {
  return (
    <>
      {/* begin #page-loader */}
      {/* <div id="page-loader" className="fade show">
        <span className="spinner" />
      </div> */}
      {/* end #page-loader */}

      {/* <!-- begin #page-container --> */}
      <div id="page-container" className="page-sidebar-fixed page-header-fixed page-with-light-sidebar">
        <Header />
        <Sidebar />
          { props.children }
        <Footer />

        {/* begin scroll to top btn */}
        <a href='#' onClick={e => e.preventDefault()} className="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i className="fa fa-angle-up" /></a>
        {/* end scroll to top btn */}

      </div>
      
      <script src="/js/app.min.js"></script>
      <script src="/js/default.min.js"></script>
    </>
  )
}

export default MainManageLayout;