import _ from 'lodash'
import moment from 'moment';

import { SearchOutlined } from "@ant-design/icons"
import { Button, Input, DatePicker, Space } from 'antd'

const { RangePicker } = DatePicker;

const getColumnSearchProps = (t, dataIndex) => ({
  filterDropdown: ({
    setSelectedKeys,
    selectedKeys,
    confirm,
    clearFilters
  }) => (
    <div style={{ padding: 8 }}>
      <Input
        placeholder={`${t('common:table.search')} ${dataIndex}`}
        value={selectedKeys[0]}
        onChange={(e) =>
          setSelectedKeys(e.target.value ? [e.target.value] : [])
        }
        onPressEnter={() =>
          handleTableSearch(selectedKeys, confirm, dataIndex)
        }
        style={{ marginBottom: 8, display: "block" }}
      />
      <Space>
        <Button
          className="btn btn-success"
          onClick={() => handleTableSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          // size="small"
          // style={{ width: 90 }}
        >
          {t('common:table.search')}
        </Button>
        <Button
          className="btn btn-white"
          onClick={() => handleTableReset(clearFilters)}
          // size="small"
          // style={{ width: 90 }}
        >
          {t('common:table.reset')}
        </Button>
        <Button
          className="btn btn-link"
          // type="link"
          // size="small"
          onClick={() => {
            confirm({ closeDropdown: false });
          }}
        >
          {t('common:table.filter')}
        </Button>
      </Space>
    </div>
  ),
  filterIcon: (filtered) => (
    <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
  ),
});

const getColumnSearchDateProps = (t, dataIndex) => ({
  filterDropdown: ({
    setSelectedKeys,
    selectedKeys,
    confirm,
    clearFilters
  }) => (
    <div style={{ padding: 8 }}>
      <Space style={{ display: "block"  }}>
      <RangePicker 
        format={['DD/MM/YYYY', 'DD/MM/YYYY']} 
        style={{ marginBottom: 8 }} 
        value={(!_.isEmpty(selectedKeys))? [moment(selectedKeys[0]), moment(selectedKeys[1])]: ''}
        onChange={(value, dateString) => {
          if(!_.isEmpty(dateString[0])){
            dateString[0] = moment(dateString[0], 'DD/MM/YYYY').format('YYYY-MM-DD')
            dateString[1] = moment(dateString[1], 'DD/MM/YYYY').format('YYYY-MM-DD')
          }
          setSelectedKeys(!_.isEmpty(dateString[0])? dateString: [])
          handleTableSearch(selectedKeys, confirm, dataIndex)
        }}
      />
      </Space>
      <Space>
        <Button
          className="btn btn-success"
          onClick={() => handleTableSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          // size="small"
          // style={{ width: 90 }}
        >
          {t('common:table.search')}
        </Button>
        <Button
          className="btn btn-white"
          onClick={() => handleTableReset(clearFilters)}
          // size="small"
          // style={{ width: 90 }}
        >
          {t('common:table.reset')}
        </Button>
        <Button
          className="btn btn-link"
          // type="link"
          // size="small"
          onClick={() => {
            confirm({ closeDropdown: false });
          }}
        >
          {t('common:table.filter')}
        </Button>
      </Space>
    </div>
  ),
  filterIcon: (filtered) => (
    <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
  ),
});

const handleTableSearch = (selectedKeys, confirm, dataIndex) => {
  confirm();
};

const handleTableReset = (clearFilters) => {
  clearFilters();
};

export {
  getColumnSearchProps,
  getColumnSearchDateProps
}