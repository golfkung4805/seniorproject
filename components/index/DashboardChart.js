import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const MyChart = ({dataDashboard}) => {
  return (
    <ResponsiveContainer>
      <LineChart
        data={dataDashboard}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="DateTime" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="subTotal" name="Total" stroke="#82ca9d" strokeWidth={2} activeDot={{ r: 8 }} />
          {/* <Line type="monotone" dataKey="orderTotal" stroke="#8884d8" /> */}
      </LineChart>
    </ResponsiveContainer>
  )
}

export default MyChart