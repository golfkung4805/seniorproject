import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ffC49F'];

const MyProductChart = ({dataDashboard}) => {
  return (
    <ResponsiveContainer>
      <BarChart data={dataDashboard}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="PDName" />
        <YAxis />
        <Tooltip />
        <Bar dataKey="subQty" fill="#8884d8" label={{ position: 'top' }}>
          {dataDashboard.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  )
}

export default MyProductChart
