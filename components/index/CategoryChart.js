import { PieChart, Pie, Cell, Legend, Tooltip, ResponsiveContainer } from 'recharts';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ffC49F'];

const MyCategoryChart = ({dataDashboard}) => {

  return (
    <ResponsiveContainer>
      <PieChart>
        <Pie
          dataKey="subTotal"
          data={dataDashboard}
          fill="#82ca9d"
          // cx={500} 
          // cy={200} 
          // innerRadius={80} 
          label
        >
          {dataDashboard.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  )
}

export default MyCategoryChart
