This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, create configuration to `.env.local` file in the root of project:

```shell
BASE_URL=
API_URL=
GOOGLE_CLIENT_ID=
FACEBOOK_APP_ID=
OMISE_PUBLIC_KEY=
JWT_SECRET_KEY=
```
and, create configuration to `.env` file in the API-SeniorProject of project:

```shell
HOST_NAME=
PORT=

DB_HOST=
DB_USER=
DB_PASS=
DB_DATABASE=

OMISE_PUBLIC_KEY=
OMISE_SECRET_KEY=

JWT_SECRET_KEY=
```

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Redux Documentation](https://redux.js.org/) - learn about Redux tutorial.
- [Express.js Documentation](http://expressjs.com/) - learn about Express.js features and API.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Learn More Plugin
- Ant Design [ Documentation](https://ant.design/) for more details.
- i18n MultiLanguage [ Documentation](https://github.com/isaachinman/next-i18next) for more details.


## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
