import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'

import qs from 'qs'
import _ from 'lodash'
import axios from 'axios'
import moment from 'moment';
import { io } from "socket.io-client"
import Timer from 'react-compound-timer'

import { useState, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { ToCurrency } from '../components/converterCurrency'

import { listTable } from '../redux/actions/tableActions'

import Layout from '../components/layout/main'

let allTableList = [], allOrderDetail = []

const Table = () => {

  const { t } = useTranslation('customerTable')

  const pageManage = {
    page_manage: t('page_manage')
  }
  
  const router = useRouter()
  
  const dispatch = useDispatch();

  const customerSignIn = useSelector((status)=> status.customerSignIn)
  const { cusInfo } = customerSignIn

  const tableData = useSelector((state) => state.tableList)
  const { data: tableList } = tableData

  const [ tableOrderList, setTableOrderList ] = useState([])
  const [ socket, setSocket ] = useState({})

  useMemo(()=>{
    dispatch(listTable({ filters: { TableStatus: ['1'] } }))
  },[])

  useEffect(async()=>{
    if(!_.isEmpty(tableList)){
      try {
        const { data: orderData } = await axios.get(`${process.env.API_URL}/v1/order`, { params: {
          filters: { Status: ['openOrder', 'paid', 'unpaid'], Date: moment().format('YYYY-MM-DD') },
          pagination: { current: 1, pageSize: 'all', total: 10 }
        }, paramsSerializer: params => qs.stringify(params) });
        
        tableList.map((table, index)=>{
          const checkTableUse = orderData.data.find((x)=>x.TableID === table.TableID) || {}
          tableList[index] = {...table, checkTableUse}
        })
        allTableList = tableList
        setTableOrderList(allTableList)
        // console.log(tableList)
      } catch (error) {
        console.error(error)
      }
    }
  },[tableList])

  useEffect(()=>{
    try {
      if(_.isEmpty(socket)){
        const sk = io(process.env.API_URL)
        sk.emit('online', {
          CusID: "VIEW_ALL_STATUS_TABLE",
          isLogin: true,
        })
        setSocket(sk)
        sk.on('newOpenOrder', newOpenOrder => {
          // console.log(newOpenOrder)
          allTableList = allTableList.map((x)=>x.TableID === newOpenOrder.TableID? {...x, checkTableUse: newOpenOrder}: x)
          setTableOrderList(allTableList)
        })
        sk.on('newOrder', newOrders => {
          // console.log(newOrders)
          let changeDataTable = allTableList.find((x)=>x.checkTableUse.ODID === newOrders.at(0).ODID)
          changeDataTable.checkTableUse.ODTotal += _.sumBy(newOrders, 'ODTotalPrice')
          changeDataTable.checkTableUse.countOrder = changeDataTable.checkTableUse.countOrder || 0
          changeDataTable.checkTableUse.countOrder += newOrders.length
          allTableList = allTableList.map((x)=>x.checkTableUse.ODID === newOrders.at(0).ODID? changeDataTable: x)
          setTableOrderList(allTableList)
        })
        sk.on('newOrderPayment', newPayment => {
          newPayment = _.isEmpty(newPayment.data.metadata)? newPayment.data: newPayment.data.metadata
          // console.log(newPayment)
          const ODStatus = newPayment.paymentMethod === 'checkout_counter'? 'unpaid': 'paid'
          allTableList = allTableList.map((x)=>x.checkTableUse.ODID===newPayment.ODID? {...x, checkTableUse:{...x.checkTableUse,
            PayID: newPayment.paymentMethod,
            ODPayDetail: newPayment.data || '{}',
            ODStatus,
            VoucDiscount: newPayment.VoucDiscount,
            updatedAt: moment().format()
          }}: x)
          setTableOrderList(allTableList)
        })
        sk.on('confirmPayment', data => {
          // console.log(data)
          allTableList = allTableList.map((x)=>x.TableID === data.TableID? {...x, checkTableUse: {}}: x)
          setTableOrderList(allTableList)
        })
      }
    } catch (error) {
      console.log(error)
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  return (
    <Layout>
      <Head>
        <title>{t('common:titleCustomer', pageManage)}</title>
      </Head>
      {/* begin #content */}
      <div id="content" className="content">
        {/* begin panel */}
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('panel_title')}</h4>
            <div className="panel-heading-btn">
              {/* <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button> */}
              {/* <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button> */}
            </div>
          </div>
          <div className="panel-body">
            
          <div id="page-container" className="page-empty bg-white page-content-full-height mt-0 p-0">
              <div id="content" className="content m-0 p-0">
                <div className="pos pos-counter pt-0" id="pos-counter">
                  <div className="pos-counter-body">
                    <div className="pos-counter-content">
                      <div className="pos-counter-content-container" data-scrollbar="true" data-height="100%" data-skip-mobile="true">
                        <div className="table-row">
                          
                          {tableOrderList.map((table)=>{
                            // console.log('table=>',table)
                            const updatedAt = (table.checkTableUse.ODStatus === 'unpaid' || table.checkTableUse.ODStatus === 'paid')? moment(table.checkTableUse.updatedAt): moment()
                            const createdAt = updatedAt.diff(moment(table.checkTableUse.createdAt), 'minutes', true)
                            const stopTime = moment.duration(createdAt, 'minutes')
                            const ODTotal = ToCurrency(table.checkTableUse.ODTotal) || '-'
                            const countOrder = table.checkTableUse.countOrder || t('counter:txtEmpty')

                            return(
                              <div key={table.TableID} 
                                className={`table ${_.isEmpty(table.checkTableUse)?'available' :'in-use'}`}  
                                onClick={()=> { (!_.isEmpty(table.checkTableUse) && router.pathname !== "/order/[TableID]" && table.checkTableUse.CusID === cusInfo.CusID)? router.push(`${process.env.BASE_URL}/order/${table.checkTableUse.TableID}`): ``}}
                              >
                                <div className="table-container" data-toggle="select-table">
                                  <div className="table-status" />
                                  <div className="table-name">
                                    {/* <div className="name">{table.TableID}</div> */}
                                    <div className="no">{table.TableName}</div>
                                    <div className="order">
                                      <span>{countOrder + (_.isNumber(countOrder)?` ${t('counter:txtOrder')}`:'')}</span><br/>
                                      <span>{ (!_.isEmpty(table.checkTableUse) && router.pathname !== "/order/[TableID]" && table.checkTableUse.CusID === cusInfo.CusID)? t('useTable') :'-' }</span>
                                    </div>
                                  </div>
                                  <div className="table-info-row">
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="far fa-user" />
                                        </span>
                                        { (!_.isEmpty(table.checkTableUse) && router.pathname !== "/order/[TableID]" && table.checkTableUse.CusID === cusInfo.CusID)? 
                                          <span className="text">{t('header:navbarUser.backTable')}</span>: 
                                          <span className="text">-</span>
                                        }
                                      </div>
                                    </div>
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="far fa-clock" />
                                        </span>
                                        <span className="text">
                                          {!_.isEmpty(table.checkTableUse) && !_.isEmpty(table.checkTableUse.PayID)?
                                          `${("0" + stopTime.hours()).slice(-2)}:${("0" + stopTime.minutes()).slice(-2)}:${("0" + stopTime.seconds()).slice(-2)}` :
                                          !_.isEmpty(table.checkTableUse)?
                                          <Timer initialTime={createdAt * 60000}>
                                            <Timer.Hours formatValue={value => `${(value > 10) ? `${value}:`: (value > 0)? `0${value}:`:''}`} />
                                            <Timer.Minutes formatValue={value => `${(value < 10 ? `0${value}` : value)}:`} />
                                            <Timer.Seconds formatValue={value => `${(value < 10 ? `0${value}` : value)}`} />
                                          </Timer>: '-' }
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="table-info-row">
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="fa fa-hand-point-up" />
                                        </span>
                                        <span className="text">-</span>
                                      </div>
                                    </div>
                                    <div className={`table-info-col ${!_.isEmpty(table.checkTableUse.PayID) && 'text-yellow'}`}>
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className={`fa ${!_.isEmpty(table.checkTableUse.PayID) && table.checkTableUse.ODStatus !== 'unpaid'? 'fa-check-circle' : 'fa-dollar-sign'}`} />
                                        </span>
                                        {/* <span className="text">-</span> */}
                                        <span className="text">{!_.isEmpty(table.checkTableUse.PayID) && table.checkTableUse.ODStatus !== 'unpaid'? t('customerOrder:order_sidebar.order_history_paid') : t('customerOrder:order_sidebar.order_history_unpaid')}</span>
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
                            )
                          })}

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        {/* end panel */}
      </div>
      {/* end #content */}
    </Layout>
  )
}

export const getStaticProps = async ({locale}) => {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common', 'header', 'customerTable', 'counter', 'customerOrder', 'customer']),
    },
  }
}


export default Table