import Document, { Html, Head, Main, NextScript } from "next/document"

class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charSet="utf-8" />
          <meta content="text/html; charset=UTF-8" name="Content-Type" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&family=Krub&family=Mali&display=swap" rel="stylesheet" />
        </Head>
        <body className="pace-top" style={{fontFamily: 'Krub, sans-serif'}}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
