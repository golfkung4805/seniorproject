import _ from 'lodash'
import { ConfigProvider, Spin  } from 'antd';
import enUS from 'antd/lib/locale/en_US';
import thTH from 'antd/lib/locale/th_TH';

import { useStore } from '../redux/store'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import { appWithTranslation, i18n } from 'next-i18next';


import 'antd/dist/antd.css';
import 'flag-icon-css/css/flag-icon.min.css'
import '../styles/app.min.css'
import '../styles/sr.min.css'
import '@sweetalert2/theme-bootstrap-4/bootstrap-4.css';
// import '@sweetalert2/theme-bootstrap-4/bootstrap-4.css';
// import '../public/plugins/ionicons/css/ionicons.min.css'

const MyApp = ({ Component, pageProps }) => {
    const store = useStore(pageProps.initialReduxState)

    const persistor = persistStore(store, {}, function () {
      persistor.persist()
    })

    return ( 
      <Provider store={store}>
        {/* <PersistGate loading={<Spin />} persistor={persistor}> */}
          <ConfigProvider locale={(_.isEmpty(i18n))? enUS: (i18n.language==='en')? enUS :thTH}>
            <Component {...pageProps} />
          </ConfigProvider>
        {/* </PersistGate> */}
      </Provider>
    )
}

export default appWithTranslation(MyApp)
