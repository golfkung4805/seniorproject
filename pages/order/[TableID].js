import qs from 'qs'
import _ from 'lodash'
import axios from 'axios'
import { 
  v4 as uuidv4, 
  version as uuidVersion, 
  validate as uuidValidate 
} from 'uuid'
import moment from 'moment';
import { io } from "socket.io-client"
import Swal from 'sweetalert2'

import Head from 'next/head'
import { useEffect, useState } from 'react';
import { useDispatch, useSelector} from 'react-redux';

import { useRouter } from 'next/router'
import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Form } from 'antd';

import { CUSTOMER_ANONYMOUS } from '../../redux/constants/customerConstants'

import { socketLocal } from '../../redux/actions/localActions'

import { listOrderCustomer } from '../../redux/actions/customerActions'

import Layout from '../../components/layout/main'

import PosMenu from '../../components/sectionPosMenu'
import PosContent from '../../components/sectionPosContent'
import PosSidebar from '../../components/sectionPosSidebar'

const CustomerOrder = () => {

  const router = useRouter()

  const { TableID } = router.query

  const dispatch = useDispatch()
  
  const { t } = useTranslation('customerOrder')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const [ form ] = Form.useForm();
  
  const customerSignIn = useSelector((status)=> status.customerSignIn)
  const { cusInfo } = customerSignIn

  const localSocket = useSelector((status)=> status.localSocket)
  const { isSocket: socket } = localSocket

  const cart = useSelector((state) => state.cart)
  const { cartItems } = cart

  const customerOrderList = useSelector((status)=> status.customerOrderList)
  const { loading: loadingOrderList, data: orderList, error: errorOrderList } = customerOrderList

  // const ISSERVER = typeof window === "undefined";

  const checkStatusTable = async () => {
    const { data: orderData } = await axios.get(`${process.env.API_URL}/v1/order`, { params: {
      filters: { Status: ['openOrder', 'paid', 'unpaid'], Date: moment().format('YYYY-MM-DD') },
      pagination: { current: 1, pageSize: 'all', total: 10 }
    }, paramsSerializer: params => qs.stringify(params) });
    
    const tableUse = orderData.data.find((x)=>x.TableID === TableID)
    if(!_.isEmpty(tableUse) && tableUse.CusID !== cusInfo.CusID){
      Swal.fire({
        icon: 'warning',
        title: t('common:alertWarning'),
        text: t('tableUse', { TableID }),
      }).then((result) => {
        if(result.isConfirmed || result.isDismissed){
          router.push(`/table`)
        }
      })
    }
  }

  useEffect(async()=>{
    const { data } = await axios.get(`${process.env.API_URL}/v1/table/${TableID}`)
    if(_.isEmpty(data)){
      Swal.fire({
        icon: 'warning',
        title: t('common:alertWarning'),
        text: t('tableScan', { TableID }),
      }).then((result) => {
        if(result.isConfirmed || result.isDismissed){
          router.push(`/table`)
        }
      })
    }
    if(!_.isEmpty(data) && data[0].TableStatus === 0){
      Swal.fire({
        icon: 'warning',
        title: t('common:alertWarning'),
        text: t('tableNotAvailable', { TableID }),
      }).then((result) => {
        if(result.isConfirmed || result.isDismissed){
          router.push(`/table`)
        }
      })
    }
  },[])

  useEffect(()=>{
    let getCusInfo = JSON.parse(localStorage.getItem("persist:authCus"));
    if(!_.isEmpty(cusInfo) && !_.isEmpty(cusInfo.CusID)){
      if(_.isEmpty(getCusInfo)){
        checkStatusTable()
      }
      dispatch(listOrderCustomer(cusInfo.CusID))
      const sk = io(process.env.API_URL)
      sk.emit('online', cusInfo)
      sk.on('connect', () => {
        if(!_.isEmpty(socket)) {
          checkStatusTable()
          socket.disconnect()
        }
      })
      dispatch(socketLocal(sk))
    }
    // if(_.isEmpty(cusInfo) || _.isEmpty(cusInfo.CusID) || !uuidValidateV4(cusInfo.CusID)){
    // if(_.isEmpty(cusInfo) || _.isEmpty(cusInfo.CusID)){
    //   dispatch({type: 'CUSTOMER_ANONYMOUS', payload: {CusID: uuidv4()}})
    // }
  },[cusInfo])

  useEffect(()=>{
    if(!_.isEmpty(orderList)){
      const lastOrder = orderList.find((x)=>x.ODStatus === "openOrder")
      if(!_.isEmpty(lastOrder) && TableID !== lastOrder.TableID){
        Swal.fire({
          icon: 'warning',
          title: t('common:alertWarning'),
          text: t('customerUseTable', { TableID: lastOrder.TableID }),
        }).then((result) => {
          if(result.isConfirmed || result.isDismissed){
            router.push(`/order/${lastOrder.TableID}`)
          }
        })
      }
    }
  }, [orderList])

  return (
    <Layout>
      <Head>
        <title>{t('common:titleCustomer', pageManage)}</title>
      </Head>
      {/* begin #content */}
      <div id="content" className="content">
        {/* begin panel */}
        <div className="panel">
          <div id="page-container" className="panel-body page-empty page-content-full-height show">
            <div id="content" className="content p-0">
              <div className="pos pos-customer" id="pos-customer">
                <PosMenu />
                <PosContent form={form} />
                <PosSidebar form={form} />
              </div>
              <a className="pos-mobile-sidebar-toggler" data-toggle-class="pos-mobile-sidebar-toggled" data-target="#pos-customer">
                <svg viewBox="0 0 16 16" className="img" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z" />
                  <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z" />
                </svg>
                <span className="badge">{cartItems.reduce((a, b)=> a + b.orderQty, 0)}</span>
              </a>
            </div>
          </div>
        </div>
        {/* end panel */}
      </div>
      {/* end #content */}
    </Layout>
  )
}

export const getStaticPaths =  async () => {
  let paths = []
  try {
    // const { data } = await axios.get(`http://localhost:4000/v1/table?pagination[pageSize]=all`)
    // console.log(data)
    // data.data.map((data)=>(
    //   paths = [ ...paths, 
    //             {params: { TableID: (data.TableID).toString() }, locale: 'en'}, 
    //             {params: { TableID: (data.TableID).toString() }, locale: 'th'} 
    //           ]
    // ))
  } catch (error) {
    console.log(error)
  } finally {
    return {
      paths, 
      fallback: 'blocking',
    }
  }
}

export const getStaticProps = async ({locale, params }) => {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common', 'header', 'customerOrder', 'customer']),
      params ,
    },
  }
}


export default CustomerOrder