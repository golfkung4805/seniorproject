import Head from 'next/head'

import { 
  version as uuidVersion, 
  validate as uuidValidate 
} from 'uuid'
import jwt from "jsonwebtoken";
import _ from 'lodash'
import axios from 'axios'
import Swal from 'sweetalert2'
import moment from 'moment'

import { useEffect, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Row, Col, Card, Form, Button, Typography, Pagination, Upload, message, Input, InputNumber, Select, DatePicker, Switch, Table, Space, Modal, notification } from 'antd'
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';

import Layout from '../components/layout/main'

import { 
  listVoucher,
} from '../redux/actions/voucherActions';

const Voucher = () => {

  const { t } = useTranslation('customerVoucher')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const dispatch = useDispatch();
  
  const { Meta } = Card;

  const { Paragraph } = Typography;

  const customerSignIn = useSelector((status)=> status.customerSignIn)
  const { cusInfo } = customerSignIn

  const voucherList = useSelector((state) => state.voucherList)
  const { loading, data: dataVoucher, pagination } = voucherList

  const [ dataVoucherHistory, setDataVoucherHistory ] = useState([])

  const uuidValidateV4 = (uuid) => {
    return uuidValidate(uuid) && uuidVersion(uuid) === 4;
  }

  const SwalFire = (icon, title, text) => {
    Swal.fire({
      icon,
      title,
      text: text
    })
  }

  const handleGetVoucher = async(VoucID, UsePoint) => {
    if(uuidValidateV4(cusInfo.CusID)){
      SwalFire('warning', t('alertLoginFirst'))
    }else{
      try {
        const decoded = jwt.verify(cusInfo.token, process.env.JWT_SECRET_KEY);
        const res = await axios.post(`${process.env.API_URL}/v1/customer/voucher/${decoded.CusID}`, {
          VoucID,
          UsePoint
        });
        if(res.status===201){
          SwalFire('success', res.data.msg)
          setDataVoucherHistory([...dataVoucherHistory, { VoucID }])
        }
        if(res.status===202 || res.status===404){
          SwalFire('warning', res.data.msg)
        }
      } catch (error) {
        console.error(error)
        SwalFire('error', 'เกิดข้อผิดพลาดในการแลกคูปอง')
      }
    }
  }

  useEffect(async()=>{
    try {
      if(!_.isEmpty(cusInfo)){
        dispatch(listVoucher({pagination: { current: 1, pageSize: 10, total: 10 }, filters: { VoucStatus: ['1'] } }))
        if(!_.isEmpty(cusInfo.token)){
          const decoded = jwt.verify(cusInfo.token, process.env.JWT_SECRET_KEY);
          const { data: getDataVoucherHistory } = await axios.get(`${process.env.API_URL}/v1/customer/voucher/${decoded.CusID}`);
          setDataVoucherHistory(getDataVoucherHistory)
        }
      }
    } catch (error) {
      console.error(error)
    }
  },[cusInfo])

  return (
    <Layout>
      <Head>
        <title>{t('common:titleCustomer', pageManage)}</title>
      </Head>
      {/* begin #content */}
      <div id="content" className="content">
        {/* begin panel */}
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('panel_title')}</h4>
            <div className="panel-heading-btn">
              {/* <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button> */}
              {/* <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button> */}
            </div>
          </div>
          <div className="panel-body">
            <Row gutter={24} justify="center">
              {
                dataVoucher.map((item, index)=>{
                  return (
                    <Col xs={24} md={8} lg={6} xl={4} key={index}>
                      <Card
                        className="mt-2 mb-2"
                        style={{ width: '100%' }}
                        cover={
                          <img
                            alt="example"
                            src={(item.VoucType === "฿"? 
                            `https://static.thenounproject.com/png/113961-200.png`
                            : `http://pngimg.com/uploads/percent/percent_PNG24.png`)}
                            style={{maxHeight: 200, objectFit: 'scale-down'}}
                          />
                        }
                        actions={[
                          (dataVoucherHistory.find((x)=>x.VoucID === item.VoucID)? 
                            <Button disabled style={{width: '100%', border: '0px', boxShadow: 'none'}}>{t('couponRedeemed')}</Button>
                            : (item.VoucUse >= item.VoucQuota)? 
                              <Button disabled style={{width: '100%', border: '0px', boxShadow: 'none'}}>{t('couponSoldOut')}</Button>
                              : <Button onClick={()=> handleGetVoucher(item.VoucID, item.VoucUsePoint)} style={{width: '100%', border: '0px', boxShadow: 'none'}}>{t('btnRedeemCoupon', { VoucUsePoint:item.VoucUsePoint  })}</Button>)
                        ]}
                      >
                        <Meta
                          title={item.VoucName}
                          description={
                            <Paragraph 
                              className="mb-0"
                              ellipsis={{ rows: 2, tooltip: t('couponDescription', { VoucStart: moment(item.VoucStart).format('DD/MM/YYYY HH:mm'), VoucEnd: moment(item.VoucEnd).format('DD/MM/YYYY HH:mm'), VoucDiscount: item.VoucDiscount, VoucType: item.VoucType, VoucCondition: item.VoucCondition, VoucQuota: item.VoucQuota }) }} 
                            >
                              {t('couponDescription', { VoucStart: moment(item.VoucStart).format('DD/MM/YYYY HH:mm'), VoucEnd: moment(item.VoucEnd).format('DD/MM/YYYY HH:mm'), VoucDiscount: item.VoucDiscount, VoucType: item.VoucType, VoucCondition: item.VoucCondition, VoucQuota: item.VoucQuota })}
                            </Paragraph>
                          }
                        />
                      </Card>
                    </Col>
                  )
                })
              }
            </Row>
          </div>
          <div className="panel-footer mx-auto">
            <Row gutter={24} justify="center">
              <Col>
                <Pagination 
                  defaultCurrent={1} 
                  responsive={true} 
                  pageSize={pagination.pageSize} 
                  total={pagination.total} 
                  onChange={(page, pageSize)=> {
                    dispatch(listVoucher({pagination: { current: page, pageSize }, filters: { VoucStatus: ['1'] } }))
                  }}/>
              </Col>
            </Row>
          </div>
        </div>
        {/* end panel */}
      </div>
      {/* end #content */}
    </Layout>
  )
}

export const getStaticProps = async ({locale}) => {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common', 'header', 'customerVoucher', 'customer']),
    },
  }
}


export default Voucher