import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import axios from 'axios'
import Swal from 'sweetalert2'
import moment from 'moment'

import { useEffect, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { PlusOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, InputNumber, Select, DatePicker, Switch, Table, Space, Modal, notification } from 'antd'

import Layout from '../../components/layout/manage/mainManage'

const VoucherManage = () => {

  const { t } = useTranslation('setting')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const [ form ] = Form.useForm();
  
  const { RangePicker } = DatePicker;

  const dispatch = useDispatch();

  const SwalFire = (icon, title, text) => {
    Swal.fire({
      icon,
      title,
      text: text
    })
  }

  const handleBtnUpdate = () => {
    form.submit();
  }

  const onFormFinish = (values) => {
    Swal.fire({
      title: t('confirmTitle'),
      text: t('confirmText'),
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: t('confirmBtnText')
    }).then(async(result) => {
      if (result.isConfirmed) {
        try {
          const res = await axios.put(`${process.env.API_URL}/v1/setting/SYS-0001`, values)
          if(res.status===201){
            SwalFire('success', t('common:alertWarning'), t('updateSuccess'))
          }
        } catch (error) {
          SwalFire('error', t('common:alertError'), t('updateError'))
        }
        // console.log(values)
      }
    })
    
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.error('Failed:', errorInfo);
  };

  useMemo(async()=>{
    const { data } = await axios.get(`${process.env.API_URL}/v1/setting/SYS-0001`);
    let dataSetting = data[0]
    const moneyPerPoint = dataSetting.SysPointCondition.split('/')
    dataSetting.SysPointCondition = {...dataSetting.SysPointCondition, Money: moneyPerPoint[0], Point: moneyPerPoint[1]}
    dataSetting.SysOpenClose = [moment(dataSetting.SysOpen, 'HH:mm'), moment(dataSetting.SysClose, 'HH:mm')]
    dataSetting.SysKitchenOpenClose = [moment(dataSetting.SysKitchenOpen, 'HH:mm'), moment(dataSetting.SysKitchenClose, 'HH:mm')]
    form.setFieldsValue(dataSetting)
  },[])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              {/* <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button> */}
              {/* <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button> */}
            </div>
          </div>
          <div className="panel-body">
            <Form
              layout="vertical"
              form={form}
              onFinish={onFormFinish}
              onFinishFailed={onFormFinishFailed}
            >
              <Row gutter={24} justify="center">
                <Col xs={24} lg={24} xl={3}>
                  <Form.Item 
                    label={t("ID")}
                    name="SysID"
                  >
                    <Input readOnly={true}  />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={4}>
                  <Form.Item 
                    label={t("storeName")}
                    name="SysName"
                    rules={[{ required: true, whitespace: true }]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={4} className="site-input-group-wrapper">
                  <Form.Item label={t("pointCondition")} tooltip={{ title: t("pointConditionInfo") }}>
                    <Input.Group compact>
                      <Form.Item 
                        name={['SysPointCondition', 'Money']}
                        noStyle
                        rules={[{ required: true }]}
                      >
                        <InputNumber style={{ width: '40%' }} placeholder="Bath" />
                      </Form.Item>
                      <Input
                        className="site-input-split"
                        style={{ width: '20%', borderLeft: 0, borderRight: 0, pointerEvents: 'none', textAlign: 'center' }}
                        placeholder="/"
                        disabled
                      />
                      <Form.Item 
                        name={['SysPointCondition', 'Point']}
                        noStyle
                        rules={[{ required: true }]}
                      >
                        <InputNumber className="site-input-right" style={{ width: '40%', textAlign: 'center' }} placeholder="Point" />
                      </Form.Item>
                    </Input.Group>
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={4}>
                  <Form.Item 
                    className="storeOpenClose"
                    label={t("storeOpenClose")}
                    name="SysOpenClose"
                    // rules={[{ type:'number', required: true, min: 0 }]}
                  >
                    <RangePicker picker="time" format={['HH:mm', 'HH:mm']} style={{ width: "100%" }} />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={4}>
                  <Form.Item 
                    label={t("kitchenOpenClose")}
                    name="SysKitchenOpenClose"
                    className="kitchenOpenClose"
                    // rules={[{ required: true }]}
                  >
                    <RangePicker picker="time" format={['HH:mm', 'HH:mm']} style={{ width: "100%" }} />
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
          <div className="panel-footer d-flex justify-content-end">
            <Button className="btn btn-primary btn-insert" onClick={()=> handleBtnUpdate()}>{t('btnUpdate')}</Button>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'setting', 'common', 'employee']),
  },
})

export default VoucherManage