import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import Swal from 'sweetalert2'
import { ExcelRenderer } from "react-excel-renderer";

import { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, Select, Switch, Table, Space, Modal, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  EMPLOYEE_IMPORT_RESET,
  EMPLOYEE_CREATE_RESET,
  EMPLOYEE_UPDATE_RESET,
  EMPLOYEE_DELETE_RESET,
  EMPLOYEE_CHANGE_RESET
} from '../../redux/constants/employeeConstants'

import { 
  listEmployee,
  importEmployee,
  createEmployee,
  updateEmployee,
  changeStatusEmployee,
  deleteEmployee
} from '../../redux/actions/employeeActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const EmployeeManage = () => {

  const { t } = useTranslation('employee')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Dragger } = Upload;

  const { Option } = Select;

  const [ form ] = Form.useForm();
  
  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const employeeList = useSelector((state) => state.employeeList)
  const { loading, data, pagination } = employeeList

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  
  const employeeImport = useSelector((state) => state.employeeImport)
  const { loading: loadingImport, success: successImport, employee: ImportdEmployee, error: errorImport } = employeeImport
  
  const employeeCreate = useSelector((state) => state.employeeCreate)
  const { loading: loadingCreate, success: successCreate, employee: createdEmployee, error: errorCreate } = employeeCreate

  const employeeUpdate = useSelector((state) => state.employeeUpdate)
  const { loading: loadingUpdate, success: successUpdate, employee: updatedEmployee, error: errorUpdate } = employeeUpdate
  
  const employeeChangeStatus = useSelector((state) => state.employeeChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, employee: changingStatusEmployee, error: errorChangeStatus } = employeeChangeStatus

  const employeeDelete = useSelector((state) => state.employeeDelete)
  const { loading: loadingDelete, success: successDelete, employee: deletedEmployee, error: errorDelete } = employeeDelete

  // * START props MODAL & FORM & UPLOAD EMPLOYEE
  // ?  UPLOAD
  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['EmpUsername',	'EmpPasswd',	'EmpGender',	'EmpFirstname',	'EmpLastname',	'EmpNickname',	'EmpTel',	'EmpEmail']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const onChangeUpload = ({fileList}) => {
    dispatch(uploadFileLocal(fileList))
  }

  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false)); 
    form.resetFields();
  }

  const handleModalOpenUpdate = (EmpData = {}, EmpID = "") => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(EmpData);
  }

  const handleOpenModalDelete = (EmpID = "") => {
    // console.log(EmpID)
    Modal.confirm({
      title: t('common:modal.txt_delete', { page_manage: pageManage.page_manage, ID: EmpID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteEmployee(EmpID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          let EmpData = [];
          resp.rows.slice(1).map((row, index) => {
            if (row && !_.isEmpty(row)) {
              EmpData.push({
                Username: row[0],
                Passwd: row[1],
                Gender: row[2],
                Firstname: row[3],
                Lastname: row[4],
                Nickname: row[5],
                Tel: row[6],
                Email: row[7]
              });
            }
          });
          if (EmpData.length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importEmployee(EmpData))
          }
        }
      });
    }else{
      form.submit();
    }
  };

  const handleModalBtnCancel = () => {
    if(isUpload) dispatch({ type: LOCAL_UPLOAD_RESET })
    dispatch(modalCloseLocal())
  };

  const onFormFinish = (values) => {
    if(isUpdate){
      dispatch(updateEmployee(values))
    }else{
      dispatch(createEmployee(values))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
  // * END props MODAL & FORM & UPLOAD EMPLOYEE

  // * START props TABLE EMPLOYEE
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listEmployee({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (EmpID) => {
    dispatch(changeStatusEmployee(EmpID))
  }
  const role = {
    'counter': t('table.roleCounter'),
    'kitchen': t('table.roleKitchen'),
  };
  const columns = [
    {
      title: t('table.id'),
      dataIndex: 'EmpID',
      key: 'EmpID',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.id')),
    },
    {
      title: t('table.username'),
      dataIndex: 'EmpUsername',
      key: 'EmpUsername',
      ...getColumnSearchProps(t, t('table.username')),
    },
    {
      title: t('table.fullname'),
      dataIndex: 'EmpName',
      key: 'EmpName',
      ...getColumnSearchProps(t, t('table.fullname')),
    },
    {
      title: t('table.nickname'),
      dataIndex: 'EmpNickname',
      key: 'EmpNickname',
      ...getColumnSearchProps(t, t('table.nickname')),
    },
    {
      title: t('table.tell'),
      dataIndex: 'EmpTel',
      key: 'EmpTel',
      ...getColumnSearchProps(t, t('table.tell')),
    },
    {
      title: t('table.email'),
      dataIndex: 'EmpEmail',
      key: 'EmpEmail',
      ...getColumnSearchProps(t, t('table.email')),
    },
    {
      title: t('table.role'),
      dataIndex: 'EmpRole',
      key: 'EmpRole',
      align: 'center',
      filters: [
        { text: t('table.roleCounter'), value: 'counter' },
        { text: t('table.roleKitchen'), value: 'kitchen' },
      ],
      render: (text, record) => (<>{role[record.EmpRole]}</>)
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'EmpStatus',
      key: 'EmpStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( <Switch defaultChecked={record.EmpStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.EmpID)} /> )
    },
    {
      title: t('common:table.action.txt_column'),
      key: 'Action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(record)}>{t('common:table.action.btn_edit')}</Button>
          <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.EmpID)}>{t('common:table.action.btn_delete')}</Button>
        </Space>
      ),
    }
  ]
  // * END props TABLE EMPLOYEE

  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        EMPLOYEE_LIST()
      }
    })
  }

  const EMPLOYEE_LIST = () => {
    dispatch(listEmployee())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      EMPLOYEE_LIST()
    }
  },[])
  
  useEffect(()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), ImportdEmployee)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: EMPLOYEE_IMPORT_RESET })
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdEmployee)
      dispatch({ type: EMPLOYEE_CREATE_RESET })
    }
    if(successUpdate){
      SwalFire('success', t('common:alertSuccess'), updatedEmployee)
      dispatch({ type: EMPLOYEE_UPDATE_RESET })
    }
    if(successDelete){
      SwalFire('success', t('common:alertSuccess'), deletedEmployee)
      dispatch({ type: EMPLOYEE_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: EMPLOYEE_IMPORT_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: EMPLOYEE_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: EMPLOYEE_UPDATE_RESET })
    }
    if(successDelete === false){
      SwalFire('error', t('common:alertError'), errorDelete)
      dispatch({ type: EMPLOYEE_DELETE_RESET })
    }

    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusEmployee.msg: errorChangeStatus.msg,
      });
      dispatch({ type: EMPLOYEE_CHANGE_RESET })
    }
  },[
    successImport,
    successCreate,
    successUpdate,
    successDelete,
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button>
              <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button>
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns} 
              rowKey={record => record.EmpID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            <Row gutter={24}>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.id')} 
                  name="EmpID"
                >
                  <Input disabled={true}  />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.username')} 
                  name="EmpUsername"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.password')} 
                  name="EmpPasswd"
                >
                  <Input.Password />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={8}>
                <Form.Item 
                  label={t('table.email')} 
                  name="EmpEmail"
                  rules={[{  type: 'email', required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.gender')}  
                  name="EmpGender"
                  rules={[{ type:'enum', required: true, whitespace: true, enum: ['นาย', 'นาง', 'นางสาว'] }]}
                >
                  <Select
                    showSearch
                    allowClear
                  >
                    <Option value="นาย">นาย</Option>
                    <Option value="นาง">นาง</Option>
                    <Option value="นางสาว">นางสาว</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.firstname')} 
                  name="EmpFirstname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.lastname')} 
                  name="EmpLastname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.nickname')} 
                  name="EmpNickname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.tell')} 
                  name="EmpTel"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.role')}  
                  name="EmpRole"
                  rules={[{ type:'enum', required: true, whitespace: true, enum: ['counter', 'kitchen'] }]}
                >
                  <Select
                    showSearch
                    allowClear
                  >
                    <Option value="counter">{t('table.roleCounter')}</Option>
                    <Option value="kitchen">{t('table.roleKitchen')}</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'employee', 'common']),
  },
})

export default EmployeeManage