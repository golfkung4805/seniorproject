import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import Swal from 'sweetalert2'
import { ExcelRenderer } from "react-excel-renderer";

import { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, Select, Switch, Table, Space, Modal, Image, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  TABLE_IMPORT_RESET,
  TABLE_CREATE_RESET,
  TABLE_UPDATE_RESET,
  TABLE_DELETE_RESET,
  TABLE_CHANGE_RESET
} from '../../redux/constants/tableConstants'

import { 
  listTable,
  importTable,
  createTable,
  updateTable,
  changeStatusTable,
  deleteTable
} from '../../redux/actions/tableActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const TableManage = () => {

  const { t } = useTranslation('table')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Dragger } = Upload;

  const { Option } = Select;

  const [ form ] = Form.useForm();
  
  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo } = employeeSignIn

  const tableList = useSelector((state) => state.tableList)
  const { loading, data, pagination } = tableList

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  
  const tableImport = useSelector((state) => state.tableImport)
  const { loading: loadingImport, success: successImport, table: importdTable, error: errorImport } = tableImport
  
  const tableCreate = useSelector((state) => state.tableCreate)
  const { loading: loadingCreate, success: successCreate, table: createdTable, error: errorCreate } = tableCreate

  const tableUpdate = useSelector((state) => state.tableUpdate)
  const { loading: loadingUpdate, success: successUpdate, table: updatedTable, error: errorUpdate } = tableUpdate
  
  const tableChangeStatus = useSelector((state) => state.tableChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, table: changingStatusTable, error: errorChangeStatus } = tableChangeStatus

  const tableDelete = useSelector((state) => state.tableDelete)
  const { loading: loadingDelete, success: successDelete, table: deletedTable, error: errorDelete } = tableDelete

  // * START props MODAL & FORM & UPLOAD TABLE
  // ?  UPLOAD
  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['TableName']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const onChangeUpload = ({fileList}) => {
    dispatch(uploadFileLocal(fileList))
  }

  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false)); 
    form.resetFields();
  }

  const handleModalOpenUpdate = (TableData = {}, TableID = "") => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(TableData);
  }

  const handleOpenModalDelete = (TableID = "") => {
    Modal.confirm({
      title: t('common:modal.txt_delete', { pageManage, ID: TableID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteTable(TableID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          let TableData = [];
          resp.rows.slice(1).map((row, index) => {
            if (row && !_.isEmpty(row)) {
              TableData.push({
                TName: row[0],
              });
            }
          });
          if (TableData.length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importTable(TableData))
          }
        }
      });
    }else{
      form.submit();
    }
  };

  const handleModalBtnCancel = () => {
    if(isUpload) dispatch({ type: LOCAL_UPLOAD_RESET })
    dispatch(modalCloseLocal())
  };

  const onFormFinish = (values) => {
    if(isUpdate){
      dispatch(updateTable(values))
    }else{
      dispatch(createTable(values))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
  // * END props MODAL & FORM & UPLOAD TABLE

  // * START props TABLE TABLE
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listTable({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (TableID) => {
    dispatch(changeStatusTable(TableID))
  }

  const columns = () =>{
    let columnsRole = [{
      title: t('table.id'),
      dataIndex: 'TableID',
      key: 'TableID',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.id')),
    },
    {
      title: t('table.name'),
      dataIndex: 'TableName',
      key: 'TableName',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.name')),
    },
    {
      title: t('table.qr'),
      dataIndex: 'TableQR',
      key: 'TableQR',
      align: 'center',
      render: (text, render) => (
        <>
          <Image
            width={100}
            src={`${process.env.API_URL}/v1/table/image/${render.TableQR}`}
            fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
          />
        </>
      )
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'TableStatus',
      key: 'TableStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( <Switch defaultChecked={record.TableStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.TableID)} /> )
    }]
    if(empInfo.isRole === 'admin'){
      columnsRole =  [...columnsRole, {
        title: t('common:table.action.txt_column'),
        key: 'Action',
        align: 'center',
        render: (text, record) => (
          <Space size="middle">
            <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(record)}>{t('common:table.action.btn_edit')}</Button>
            <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.TableID)}>{t('common:table.action.btn_delete')}</Button>
          </Space>
        ),
      }]
    }
    return columnsRole
  }
  // * END props TABLE TABLE

  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        TABLE_LIST()
      }
    })
  }

  const TABLE_LIST = () => {
    dispatch(listTable())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      TABLE_LIST()
    }
  },[])
  
  useEffect(()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), importdTable)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: TABLE_IMPORT_RESET })
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdTable)
      dispatch({ type: TABLE_CREATE_RESET })
    }
    if(successUpdate){
      SwalFire('success', t('common:alertSuccess'), updatedTable)
      dispatch({ type: TABLE_UPDATE_RESET })
    }
    if(successDelete){
      SwalFire('success', t('common:alertSuccess'), deletedTable)
      dispatch({ type: TABLE_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: TABLE_IMPORT_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: TABLE_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: TABLE_UPDATE_RESET })
    }
    if(successDelete === false){
      SwalFire('error', t('common:alertError'), errorDelete)
      dispatch({ type: TABLE_DELETE_RESET })
    }

    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusTable.msg: errorChangeStatus.msg,
      });
      dispatch({ type: TABLE_CHANGE_RESET })
    }
  },[
    successImport,
    successCreate,
    successUpdate,
    successDelete,
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              {empInfo.isRole === 'admin' && (<>
                <Button className="btn btn-primary btn-insert mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button>
                <Button className="btn btn-success btn-insert" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button>
              </>)}
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns()} 
              rowKey={record => record.TableID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            <Row gutter={24} justify="center">
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.id')} 
                  name="TableID"
                >
                  <Input disabled={true}  />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.name')} 
                  name="TableName"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'table', 'common', 'employee']),
  },
})

export default TableManage