import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'

import { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Switch, Table, Image, notification } from 'antd'

import { 
  PAYMENT_METHOD_CHANGE_RESET
} from '../../redux/constants/paymentMethodConstants'

import { 
  listPaymentMethod,
  changeStatusPaymentMethod,
} from '../../redux/actions/paymentMethodActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import Layout from '../../components/layout/manage/mainManage'

const TableManage = () => {

  const { t } = useTranslation('payment')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const dispatch = useDispatch();
  
  const paymentMethodList = useSelector((state) => state.paymentMethodList)
  const { loading, data, pagination } = paymentMethodList

  const paymentMethodChangeStatus = useSelector((state) => state.paymentMethodChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, paymentMethod: changingStatusTable, error: errorChangeStatus } = paymentMethodChangeStatus

  // * START props TABLE TABLE
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listPaymentMethod({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (PayID) => {
    dispatch(changeStatusPaymentMethod(PayID))
  }

  const columns = [
    {
      title: t('table.name'),
      dataIndex: 'PayName',
      key: 'PayName',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.name')),
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'PayStatus',
      key: 'PayStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( record.PayID !== "checkout_counter" &&<Switch defaultChecked={record.PayStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.PayID)} /> )
    }
  ]
  // * END props TABLE TABLE

  const PAYMENT_METHOD_LIST = () => {
    dispatch(listPaymentMethod())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      PAYMENT_METHOD_LIST()
    }
  },[])
  
  useEffect(()=>{
    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusTable.msg: errorChangeStatus.msg,
      });
      dispatch({ type: PAYMENT_METHOD_CHANGE_RESET })
    }
  },[
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              // scroll={{ x: '20vh' }}
              columns={columns} 
              rowKey={record => record.PayID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'payment', 'common', 'employee']),
  },
})

export default TableManage