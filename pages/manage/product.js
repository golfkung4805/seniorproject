import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import Swal from 'sweetalert2'

import { ExcelRenderer } from "react-excel-renderer";

import { useEffect, useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined, PlusOutlined } from "@ant-design/icons"
import { Row, Col, Image, Form, Button, Upload, message, Input, InputNumber, Select, Switch, Table, Space, Modal, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  PRODUCT_IMPORT_RESET,
  PRODUCT_CREATE_RESET,
  PRODUCT_UPDATE_RESET,
  PRODUCT_DELETE_RESET,
  PRODUCT_DELETE_IMAGE_RESET,
  PRODUCT_CHANGE_RESET
} from '../../redux/constants/productConstants'

import { 
  listProduct,
  importProduct,
  createProduct,
  updateProduct,
  changeStatusProduct,
  deleteProduct,
  deleteImageProduct
} from '../../redux/actions/productActions';

import { 
  listCategory
} from '../../redux/actions/categoryActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal,
  uploadShowUpdateFileLocal,
  uploadDragableFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport,
  DragableUploadListItem
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const ProductManage = () => {

  const { t } = useTranslation('product')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Dragger } = Upload;

  const { Option } = Select;

  const { TextArea } = Input;

  const [ form ] = Form.useForm();
  
  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo } = employeeSignIn

  const productList = useSelector((state) => state.productList)
  const { loading, data, pagination } = productList

  const categoryList = useSelector((state) => state.categoryList)
  const { data: categoryData } = categoryList

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  
  const productImport = useSelector((state) => state.productImport)
  const { loading: loadingImport, success: successImport, product: ImportdProduct, error: errorImport } = productImport
  
  const productCreate = useSelector((state) => state.productCreate)
  const { loading: loadingCreate, success: successCreate, product: createdProduct, error: errorCreate } = productCreate

  const productUpdate = useSelector((state) => state.productUpdate)
  const { loading: loadingUpdate, success: successUpdate, product: updatedProduct, error: errorUpdate } = productUpdate
  
  const productChangeStatus = useSelector((state) => state.productChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, product: changingStatusProduct, error: errorChangeStatus } = productChangeStatus

  const productDelete = useSelector((state) => state.productDelete)
  const { loading: loadingDelete, success: successDelete, product: deletedProduct, error: errorDelete } = productDelete

  const productDeleteImage = useSelector((state) => state.productDeleteImage)
  const { loading: loadingDeleteImage, success: successDeleteImage, product: deletedImageProduct, error: errorDeleteImage } = productDeleteImage

  const options = categoryData.map((value)=>{
    return {label: `${value.CID} - ${value.CName}`, text: `${value.CID} - ${value.CName}`, value: value.CID}
  })

  // * START props MODAL & FORM & UPLOAD PRODUCT
  // ?  UPLOAD
  const moveRow = useCallback(
    (dragIndex, hoverIndex) => {
      const dragRow = fileList[dragIndex];
      dispatch(uploadDragableFileLocal({dragIndex, hoverIndex, dragRow}))
    },
    [fileList],
  )

  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['PDName', 'PDDesc', 'PDPrice', 'CID']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const beforeUploadImage = async (fileObj, fileListObj) => {
    return await checkColumnFileImport(t, 5, [], fileList, fileObj, fileListObj)
  };
  
  const onChangeUpload = ({ fileList }) => {
    dispatch(uploadFileLocal(fileList))
  }
  
  const onRemoveImage = (fileObj) => {
    if(fileObj.response.status==='success') dispatch(deleteImageProduct(fileObj.uid))
  }
  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false)); 
    form.resetFields();
  }

  const handleModalOpenUpdate = (ProductData = {}, PDID = "") => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(uploadShowUpdateFileLocal(ProductData.PDID))
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(ProductData);
  }

  const handleOpenModalDelete = (PDID = "") => {
    Modal.confirm({
      title: t('common:modal.txt_delete', { page_manage: pageManage.page_manage, ID: PDID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteProduct(PDID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          // console.log(resp)
          let ProductData = [];
          resp.rows.slice(1).map((row, index) => {
            if (row && !_.isEmpty(row)) {
              ProductData.push({
                PDName: row[0],
                PDDesc: row[1],
                PDPrice: parseFloat(row[2]),
                CID: row[3]
              });
            }
          });
          if (ProductData.length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importProduct(ProductData))
          }
        }
      });
    }else{
      form.submit();
    }
  };

  const handleModalBtnCancel = () => {
    dispatch(modalCloseLocal())
    dispatch({ type: LOCAL_UPLOAD_RESET })
  };

  const onFormFinish = (values) => {
    if(fileList.length > 0){
      const formData = new FormData();
      formData.append('PDID', values.PDID || '');
      formData.append('PDName', values.PDName || '');
      formData.append('PDPrice', parseFloat(values.PDPrice));
      formData.append('CID', values.CID || '');
      formData.append('PDDesc', values.PDDesc || '');
      fileList.forEach((file, index) => {
        if(!_.isEmpty(file.originFileObj)){
          formData.append('image', file.originFileObj)
        }
        formData.append('imageList', JSON.stringify({ uid: (!_.isEmpty(file.originFileObj))?"" :file.uid , ...{index, name: file.name} }));
      });
      if(isUpdate){
        dispatch(updateProduct(formData, values.PDID))
      }else{
        dispatch(createProduct(formData))
      }
    }else{
      message.error(t('common:upload.no_image'))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
  // * END props MODAL & FORM & UPLOAD PRODUCT

  // * START props TABLE PRODUCT
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listProduct({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (PDID) => {
    dispatch(changeStatusProduct(PDID))
  }

  const columns = () => {
    let columnsRole = [{
      title: t('table.id'),
      dataIndex: 'PDID',
      key: 'PDID',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.id')),
    },
    {
      title: t('table.name'),
      dataIndex: 'PDName',
      key: 'PDName',
      ...getColumnSearchProps(t, t('table.name')),
    },
    // {
    //   title: t('table.desc'),
    //   dataIndex: 'PDDesc',
    //   key: 'PDDesc',
    //   ...getColumnSearchProps(t, t('table.desc')),
    // },
    {
      title: t('table.price'),
      dataIndex: 'PDPrice',
      key: 'PDPrice',
      ...getColumnSearchProps(t, t('table.price')),
    },
    {
      title: t('table.category'),
      dataIndex: 'CName',
      key: 'CName',
      // ...getColumnSearchProps(t, t('table.category')),
      filters: _.map(options),
    },
    {
      title: t('table.rating'),
      dataIndex: 'PDRating',
      key: 'PDRating',
    },
    {
      title: t('table.image'),
      dataIndex: 'PDImage',
      key: 'PDImage',
      align: 'center',
      render: (text, render) => (
        <>
        <Image
          width={100}
          src={`${process.env.API_URL}/v1/product/image/${render.PDImage}`}
          fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
        />
        </>
      )
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'PDStatus',
      key: 'PDStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( <Switch defaultChecked={record.PDStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.PDID)} /> )
    }
    ]
    if(empInfo.isRole === 'admin'){
      columnsRole =  [...columnsRole, {
        title: t('common:table.action.txt_column'),
        key: 'Action',
        align: 'center',
        render: (text, record) => (
          <Space size="middle">
            <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(record)}>{t('common:table.action.btn_edit')}</Button>
            <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.PDID)}>{t('common:table.action.btn_delete')}</Button>
          </Space>
        ),
      }]
    }
    return columnsRole
  }
  // * END props TABLE PRODUCT

  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        PRODUCT_LIST()
      }
    })
  }

  const PRODUCT_LIST = async () => {
    dispatch(listProduct())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      PRODUCT_LIST()
      dispatch(listCategory())
    }
  },[])
  
  useEffect(()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), ImportdProduct)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: PRODUCT_IMPORT_RESET })
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdProduct)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: PRODUCT_CREATE_RESET })
    }
    if(successUpdate){
      SwalFire('success', t('common:alertSuccess'), updatedProduct)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: PRODUCT_UPDATE_RESET })
    }
    if(successDelete){
      SwalFire('success', t('common:alertSuccess'), deletedProduct)
      dispatch({ type: PRODUCT_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: PRODUCT_IMPORT_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: PRODUCT_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: PRODUCT_UPDATE_RESET })
    }
    if(successDelete === false){
      SwalFire('error', t('common:alertError'), errorDelete)
      dispatch({ type: PRODUCT_DELETE_RESET })
    }

    if(successDeleteImage){
      notification[(successDeleteImage)?'success':'error']({
        message: (successDeleteImage)? deletedImageProduct.msg: errorDeleteImage.msg,
      });
      dispatch({ type: PRODUCT_DELETE_IMAGE_RESET })
    }
    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusProduct.msg: errorChangeStatus.msg,
      });
      dispatch({ type: PRODUCT_CHANGE_RESET })
    }
  },[
    successImport,
    successCreate,
    successUpdate,
    successDelete,
    successDeleteImage,
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              {empInfo.isRole === 'admin' && (<>
                <Button className="btn btn-primary btn-insert mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button>
                <Button className="btn btn-success btn-insert" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button>
              </>)}
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns()} 
              rowKey={record => record.PDID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            maxCount={1}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            <Row gutter={24}>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.id')} 
                  name="PDID"
                >
                  <Input disabled={true}  />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={7}>
                <Form.Item 
                  label={t('table.name')} 
                  name="PDName"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item  
                  label={t('table.price')} 
                  name="PDPrice"
                  rules={[{ required: true }]}
                >
                  <InputNumber style={{ width: "100%" }} min={1} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={7}>
                <Form.Item 
                  label={t('table.category')}  
                  name="CID"
                  rules={[{ type:'enum', required: true, whitespace: true, enum: _.map(options, 'value') }]}
                >
                  <Select
                    showSearch
                    allowClear
                    options={options}
                  >
                    
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={24}>
                <Form.Item 
                  label={t('table.desc')} 
                  name="PDDesc"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <TextArea rows={3} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={24}>
                <Form.Item 
                  label={t('common:upload.upload_image')} 
                >
                  <DndProvider backend={HTML5Backend}>
                    <Upload 
                      listType="picture-card"
                      // maxCount={1}
                      multiple={true}
                      fileList={fileList}
                      beforeUpload={beforeUploadImage}
                      onChange={onChangeUpload}
                      onRemove={onRemoveImage}
                      itemRender={(originNode, file, currFileList) => (
                        <DragableUploadListItem
                          originNode={originNode}
                          file={file}
                          fileList={currFileList}
                          moveRow={moveRow}
                        />
                      )}
                      
                    >
                      {fileList.length >= 5 ? null : 
                        <div>
                          <PlusOutlined />
                          <div style={{ marginTop: 8 }}>Upload</div>
                        </div>
                      }
                    </Upload>
                  </DndProvider>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'product', 'common', 'employee']),
  },
})

export default ProductManage