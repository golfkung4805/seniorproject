import Link from 'next/link'
import Head from 'next/head'
import dynamic from 'next/dynamic'

import _ from 'lodash'
import axios from 'axios'
import { io } from "socket.io-client"
import jwt from "jsonwebtoken";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import moment from 'moment'
import 'moment/locale/th'

import { useState, useEffect, useMemo } from 'react'

import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Button, DatePicker, Select } from 'antd';

// import { Line } from 'react-chartjs-2';
const MyChart = dynamic(() => import('../../components/index/DashboardChart'),{ ssr: false })
const MyProductChart = dynamic(() => import('../../components/index/ProductChart'),{ ssr: false })
const MyCategoryChart = dynamic(() => import('../../components/index/CategoryChart'),{ ssr: false })

import Layout from '../../components/layout/manage/mainManage'

const HomeManage = () => {

  moment.locale(i18n.language)

  let addDataDashboard = []

  const typeFormat = {
    'date_query': 'YYYY-MM-DD',
    'week_query': 'YYYY-MM-DD',
    'month_query': 'YYYY-MM',
    'year_query': 'YYYY',
  
    'date': 'HH:mm',
    'week': 'YYYY-MM-DD',
    'month': 'YYYY-MM-DD',
    'year': 'YYYY-MM',
  };

  const { t } = useTranslation('home')

  const { Option } = Select;

  const formatPicker = {
    'date': 'DD MMM YYYY',
    'week': 'W-YYYY',
    'month': 'MMM YYYY',
    'year': 'YYYY'
  }

  const [ socket, setSocket ] = useState({})
  const [ dataDashboard, setDataDashboard ] = useState({ overview: [], popularProduct: [], popularCategory: [] })

  const queryDataDashboard = async(typePicker, datePicker) => {
    const { data } = await axios.get(`${process.env.API_URL}/v1/dashboard`, { 
      params: {
        typePicker,
        datePicker,
      }
    })
    addDataDashboard = data
    setDataDashboard({overview: addDataDashboard.overview , popularProduct: addDataDashboard.popularProduct, popularCategory: addDataDashboard.popularCategory })
  }

  const exportReport = () => {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    var ws = XLSX.utils.json_to_sheet([
      { DateTime: "ตั้งแต่วันที่", subTotal: `${dataDashboard.overview.at(0).DateTime}-${dataDashboard.overview.at(-1).DateTime}` },
      { DateTime: "ยอดขายทั้งหมด (THB)", subTotal: _.sumBy(dataDashboard.overview, 'orderTotal') },
      { DateTime: "คำสั่งซื้อทั้งหมด", subTotal: _.sumBy(dataDashboard.overview, 'subTotal') },
      { DateTime: "" },
      { DateTime: "" },
      { DateTime: "วันที่", subTotal: "ยอดขายทั้งหมด (THB)", orderTotal: "คำสั่งซื้อทั้งหมด" },
      ...dataDashboard.overview
    ], { header: ["DateTime","subTotal","orderTotal"], skipHeader: true });
    const wb = { Sheets: { 'Report': ws }, SheetNames: ['Report'] };
    // console.log('ws=>',ws)
    // console.log('wb=>',wb)
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data = new Blob([excelBuffer], {type: fileType});
    FileSaver.saveAs(data, `report_${dataDashboard.overview.at(0).DateTime}-${dataDashboard.overview.at(-1).DateTime}.xlsx`);
  }

  useMemo(()=>{
    queryDataDashboard('date', moment().format('YYYY-MM-DD'))
  },[])

  useEffect(()=>{
    try {
      if(_.isEmpty(socket)){
        let GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo).token
        if(!_.isEmpty(GetEmpInfo)){
          GetEmpInfo = jwt.verify(GetEmpInfo, process.env.JWT_SECRET_KEY);
        }

        const sk = io(process.env.API_URL)
        sk.emit('online', {
          CusID: GetEmpInfo._id,
          isLogin: true,
        })
        setSocket(sk)
        sk.on('confirmPayment', data => {
          // console.log(data)
        })
      }
    } catch (error) {
      console.error(error)
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  const [ typePicker, setTypePicker ] = useState('date');
  const [ datePicker, setDatePicker] = useState(moment())

  const onChangeDate = (value) => {
    setDatePicker(value)
    queryDataDashboard(typePicker, value.format('YYYY-MM-DD'))
  }
  const onChangeTypePicker = (value) => {
    setTypePicker(value)
    setDatePicker(moment())
    queryDataDashboard(value, moment().format('YYYY-MM-DD'))
  }

  return (
    <Layout>
      <Head>
        <title>{t('title')}</title>
      </Head>
      {/* begin #content */}
      <div id="content" className="content">
        {/* begin breadcrumb */}
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('breadcrumb.1')}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('breadcrumb.active')}</li>
        </ol>
        {/* end breadcrumb */}
        {/* begin page-header */}
        <h1 className="page-header">{t('breadcrumb.active')}</h1>
        {/* end page-header */}
        {/* begin panel */}
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('order:panel_title')} 
              <DatePicker 
                className="ml-3" 
                allowClear={false}
                style={{ width: 200 }} 
                picker={typePicker}
                value={datePicker}
                format={formatPicker[typePicker]}
                onChange={onChangeDate} 
              />
              <Select className="ml-3" style={{ width: 125 }} value={typePicker} onChange={onChangeTypePicker}>
                <Option value="date">{t('order:typePicker.date')}</Option>
                <Option value="week">{t('order:typePicker.week')}</Option>
                <Option value="month">{t('order:typePicker.month')}</Option>
                <Option value="year">{t('order:typePicker.year')}</Option>
              </Select>
            </h4>
            <div className="panel-heading-btn">
              <Button onClick={() => exportReport()}>{t('order:exportData')}</Button>
            </div>
          </div>
          <div className="panel-body w-100 pb-5" style={{ height: 500 }}>
            {t('totalSales')}: {_.sumBy(dataDashboard.overview, 'subTotal')}<br/>
            {t('totalOrders')}: {_.sumBy(dataDashboard.overview, 'orderTotal')}
            {/* <Line 
              data={data} 
              options={options} 
              width={500}
            /> */}
            <MyChart dataDashboard={dataDashboard.overview} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 col-md-7">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">{t('productRanking')}</h4>
                <div className="panel-heading-btn">
                  {/* <Button onClick={()=> {console.log(datePicker)}}>ดาวน์โหลดข้อมูล</Button> */}
                </div>
              </div>
              <div className="panel-body w-100" style={{ height: 400 }}>
                <MyProductChart dataDashboard={dataDashboard.popularProduct} />
              </div>
            </div>
          </div>

          <div className="col-xs-12 col-md-5">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">{t('categoryRanking')}</h4>
                <div className="panel-heading-btn">
                  {/* <Button onClick={()=> {console.log(datePicker)}}>ดาวน์โหลดข้อมูล</Button> */}
                </div>
              </div>
              <div className="panel-body w-100" style={{ height: 400 }}>
                <MyCategoryChart dataDashboard={dataDashboard.popularCategory} />
              </div>
            </div>
          </div>
        </div>
        {/* end panel */}
      </div>
      {/* end #content */}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'home', 'common', 'order', 'employee']),
  },
})

export default HomeManage