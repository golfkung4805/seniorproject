import Link from 'next/link'
import Head from 'next/head'

import qs from 'qs'
import _ from 'lodash'
import axios from 'axios'
import Swal from 'sweetalert2'
import moment from 'moment'
import 'moment/locale/th'

import Timer from 'react-compound-timer'
import { io } from "socket.io-client"
import jwt from "jsonwebtoken";

import { useState, useEffect, useMemo } from 'react'

import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Button } from 'antd';

import Layout from '../../components/layout/manage/mainManage'

const kitchenManage = () => {
  moment.locale(i18n.language)
  
  let allKitchenOrder = [], allOrderOpenList = []

  const { t } = useTranslation('kitchen')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const [ socket, setSocket ] = useState({})
  const [ orderOpenList, setOrderOpenList ] = useState([])
  const [ kitchenList, setKitchenList ] = useState({kitchenOrder: [], kitchenOrderComplete: []})

  const changeStatusOrder = (indexOrder, _ID, status) => {
    Swal.fire({
      title: t('confirmTitle'),
      text: t('confirmText'),
      icon: 'warning',
      showCancelButton: true,
    }).then(async(result) => {
      if (result.isConfirmed) {
        let newObject = kitchenList.kitchenOrder, orderComplete = kitchenList.kitchenOrderComplete, tempObject = []
        let orderUpdate = newObject[indexOrder].order.find((item)=> item._ID === _ID)
        try {
          newObject[indexOrder].countComplete += 1
          newObject[indexOrder].order.map((item)=>item._ID === _ID? item.ODStatus = status: item)
          // newObject[indexOrder].order.map((item)=>item._ID === _ID? item.updatedAt = moment(): item)

          // if countComplete === countOrder then move object to end object
          if(newObject[indexOrder].countComplete === newObject[indexOrder].countOrder){
            tempObject = newObject[indexOrder] // object to move
            newObject = newObject.filter((item)=> item !== tempObject) // filter object to move out
            orderComplete = [...kitchenList.kitchenOrderComplete, tempObject]

            if(!_.isEmpty(newObject)){
              socket.emit('kitchenNextQueueOrder', {
                CusID: orderOpenList.find((x)=> x.ODID === newObject.at(0).order.at(0).ODID).CusID,
                isSee: false,
                Type: 'NextQueue',
                Order: newObject.at(0).order.at(0)
              })
            }
          }

          await axios.put(`${process.env.API_URL}/v1/order/detail/${_ID}/status`, { status })
        
        } catch (error) {
          console.error(error)
        } finally {
          socket.emit('kitchenChangeOrder', {
            CusID: orderOpenList.find((x)=> x.ODID === orderUpdate.ODID).CusID,
            isSee: false,
            Type: 'ChangeOrder',
            indexOrder,
            Order: orderUpdate
          })
          // allKitchenOrder = { kitchenOrder: newObject, kitchenOrderComplete: _.orderBy(orderComplete, (item) => {return item.order[0]._ID}, ['desc']) }
          setKitchenList({ kitchenOrder: newObject, kitchenOrderComplete: _.orderBy(orderComplete, (item) => {return item.order[0]._ID}, ['desc']) })
        }
      }
    })
  }

  const handleOrderComplete = async (indexOrder, _ID) => {
    changeStatusOrder(indexOrder, _ID, 'complete')
  }

  const handleOrderCancel = (indexOrder, _ID) => {
    changeStatusOrder(indexOrder, _ID, 'cancel')
  }

  useMemo(async()=>{
    try {
      const { data: orderData } = await axios.get(`${process.env.API_URL}/v1/order`, { params: {
        filters: { Status: ['openOrder', 'paid', 'unpaid'], Date: moment().format('YYYY-MM-DD') },
        pagination: { current: 1, pageSize: 'all', total: 10 }
      }, paramsSerializer: params => qs.stringify(params) });
      
      allOrderOpenList = orderData.data
      setOrderOpenList(allOrderOpenList)
      const { data } = await axios.get(`${process.env.API_URL}/v1/order/kitchen`);
      allKitchenOrder = data
      setKitchenList(allKitchenOrder)
    } catch (error) {
      console.error(error)
    }
  },[])

  useEffect(()=>{
    try {
      if(_.isEmpty(socket)){
        let GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo).token
        if(!_.isEmpty(GetEmpInfo)){
          GetEmpInfo = jwt.verify(GetEmpInfo, process.env.JWT_SECRET_KEY);
        }

        const sk = io(process.env.API_URL)
        sk.emit('online', {
          CusID: GetEmpInfo._id,
          isLogin: true,
        })
        setSocket(sk)
        sk.on('newOpenOrder', newOpenOrder => {
          allOrderOpenList.push(newOpenOrder)
          // console.log(newOpenOrder)
          setOrderOpenList(allOrderOpenList)
        })
        sk.on('newOrder', newOrders => {
          // console.log(newOrders)
          allKitchenOrder.kitchenOrder = [...allKitchenOrder.kitchenOrder, {countComplete: 0, countOrder: newOrders.length, order: newOrders}]
          allKitchenOrder.kitchenOrder.map((item)=>{
            if(item.countComplete === item.countOrder){
              allKitchenOrder.kitchenOrderComplete = [...allKitchenOrder.kitchenOrderComplete, item]
            }
          })
          allKitchenOrder.kitchenOrder = allKitchenOrder.kitchenOrder.filter((x)=>x.countComplete !== x.countOrder)
          setKitchenList({ kitchenOrder: allKitchenOrder.kitchenOrder, kitchenOrderComplete: allKitchenOrder.kitchenOrderComplete })
        })
        sk.on('confirmPayment', data => {
          allOrderOpenList = allOrderOpenList.filter((x)=>x.TableID !== data.TableID)
          allKitchenOrder.kitchenOrder = allKitchenOrder.kitchenOrder.filter((x)=>x.order.at(0).ODID !== data.ODID)
          allKitchenOrder.kitchenOrderComplete = allKitchenOrder.kitchenOrderComplete.filter((x)=>x.order.at(0).ODID !== data.ODID)
          setOrderOpenList(allOrderOpenList)
          setKitchenList({ kitchenOrder: allKitchenOrder.kitchenOrder, kitchenOrderComplete: allKitchenOrder.kitchenOrderComplete })
        })
      }
    } catch (error) {
      console.error(error)
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default" style={{background: 'linear-gradient(0deg, rgba(222,226,230,1) 70%, rgba(255,255,255,1) 100%)'}}>
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              <a href='#' onClick={e => e.preventDefault()} className="btn btn-xs btn-icon btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
              <a href='#' onClick={e => e.preventDefault()} className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a>
            </div>
          </div>
          <div className="panel-body">
            
            <div id="page-container" className="page-empty bg-white page-content-full-height mt-0 p-0">
              <div id="content" className="content p-0 ml-0">
                <div className="pos pos-kitchen pt-0" id="pos-kitchen" style={{background: '#dee2e6'}}>
                  <div className="pos-kitchen-body">
                    { !_.isEmpty(orderOpenList) && [...kitchenList.kitchenOrder, ...kitchenList.kitchenOrderComplete].map((task, index)=>{
                      const indexOrder = index
                      const thisOrder = orderOpenList.find((x)=>x.ODID===task.order.at(0).ODID)
                      if(_.isEmpty(thisOrder)){
                        return
                      }
                      return(
                        <div className="pos-task-row" key={index}>
                          <div className="pos-task">
                            <div className="pos-task-info">
                              <div className="table-no">
                              {thisOrder.TableID}
                              </div>
                              <div className="order-no">
                                {t('txtRef')}: #{task.order.at().Ref} <br/>
                                {t('txtNo')}: #{thisOrder.ODID}
                              </div>
                              <div className="order-type">
                                <span className={`label ${task.countComplete === task.countOrder?'bg-gray-500' :'label-success'} `}>{t('dineIn')}</span>
                              </div>
                              <div className="time-pass" data-start-time={3}>
                                {t('orderTime')} {moment(task.order.at().createdAt).format('HH:mm:ss')} {t('header:notification.minute')}<br/>
                                {task.countComplete === task.countOrder
                                  ?<>{t('allDishesServed')}<br />{/* moment(task.order.at().createdAt).from(moment(task.order.at().updatedAt)) */}</>
                                  :<Timer initialTime={moment().diff(moment(task.order.at(0).createdAt), 'minutes', true) * 60000}>
                                    <Timer.Hours formatValue={value => `${(value > 0)? `${value} ${t('header:notification.hour')} `:''}`} />
                                    <Timer.Minutes formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('header:notification.minute')} `} />
                                    <Timer.Seconds formatValue={value => `${(value < 10 ? `0${value}` : value)} ${t('header:notification.seconds')}`} />
                                  </Timer>
                                }
                                <br/>
                              </div>
                            </div>
                            <div className="pos-task-body">
                              <div className="pos-task-completed">
                                {t('status.complete')}: <b>({task.countComplete}/{task.countOrder})</b>
                              </div>
                              <div className="pos-task-product-row">
                                { task.order.map((item, index)=>{
                                  const itemStatus = (item.ODStatus === 'complete' || item.ODStatus === 'cancel')?true :false
                                  return(
                                    <div className={`pos-task-product ${itemStatus? 'completed': ''}`} key={index}>
                                      <div className="pos-task-product-img">
                                        <div className="cover" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/${item.PDImage})`}} />
                                        {itemStatus
                                          ?<div className="caption text-center">
                                            <div>
                                              {item.ODStatus === 'complete' ?t('status.complete'): t('status.cancel')}<br/>
                                            </div>
                                          </div>
                                          : ''
                                        }
                                      </div>
                                      <div className="pos-task-product-info">
                                        <div className="info">
                                          <div className="title">{item.PDName}</div>
                                          <div className="desc">
                                            {JSON.parse(item.ODSizeListDisplay).map((option, index)=>{
                                              return <div key={index}>{option}<br/></div>
                                            })}
                                            {item.ODDesc}
                                          </div>
                                        </div>
                                        <div className="qty">
                                          x{item.ODQty}
                                        </div>
                                      </div>
                                      <div className="pos-task-product-action">
                                        <Button className="btn btn-success btn-block" disabled={itemStatus} onClick={()=> handleOrderComplete(indexOrder, item._ID)}>{t('status.complete')}</Button>
                                        <Button className="btn btn-outline-danger btn-block" disabled={itemStatus} onClick={()=> handleOrderCancel(indexOrder, item._ID)}>{t('status.cancel')}</Button>
                                      </div>
                                    </div>
                                  )
                                }) }
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    }) }
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'kitchen', 'common', 'employee']),
  },
})

export default kitchenManage