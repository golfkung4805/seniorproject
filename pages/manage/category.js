import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import axios from 'axios'
import Swal from 'sweetalert2'
import { ExcelRenderer } from "react-excel-renderer";

import { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined, PlusCircleTwoTone, MinusCircleTwoTone,  MinusCircleOutlined, PlusOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, InputNumber, Switch, Table, Select, Space, Modal, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  CATEGORY_IMPORT_RESET,
  CATEGORY_CREATE_RESET,
  CATEGORY_UPDATE_RESET,
  CATEGORY_DELETE_RESET,
  CATEGORY_CHANGE_RESET,
  INGREDIENT_DELETE_RESET,
  OPTION_CREATE_RESET ,
  OPTION_UPDATE_RESET, 
  OPTION_DELETE_RESET,
} from '../../redux/constants/categoryConstants'

import { 
  listCategory,
  importCategory,
  createCategory,
  updateCategory,
  changeStatusCategory,
  deleteCategory,
  deleteCategoryIngredient,
  createCategoryOption,
  updateCategoryOption,
  deleteCategoryOption
} from '../../redux/actions/categoryActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const CategoryManage = () => {

  const { t } = useTranslation('category')

  const pageManage = {
    page_manage: t('page_manage'),
    sub_text: t('table_expand_option.name'),
  }

  const { Dragger } = Upload;

  const [ form ] = Form.useForm();
  
  const { Option } = Select;

  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  // TODO state category
  const categoryList = useSelector((state) => state.categoryList)
  const { loading, data, pagination } = categoryList

  const categoryImport = useSelector((state) => state.categoryImport)
  const { loading: loadingImport, success: successImport, category: importdCategory, error: errorImport } = categoryImport
  
  const categoryCreate = useSelector((state) => state.categoryCreate)
  const { loading: loadingCreateCategory, success: successCreateCategory, category: createdCategory, error: errorCreateCategory } = categoryCreate

  const categoryUpdate = useSelector((state) => state.categoryUpdate)
  const { loading: loadingUpdateCategory, success: successUpdateCategory, category: updatedCategory, error: errorUpdateCategory } = categoryUpdate
  
  const categoryChangeStatus = useSelector((state) => state.categoryChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, category: changingStatusCategory, error: errorChangeStatus } = categoryChangeStatus

  const categoryDelete = useSelector((state) => state.categoryDelete)
  const { loading: loadingDeleteCategory, success: successDeleteCategory, category: deletedCategory, error: errorDeleteCategory } = categoryDelete
  // TODO state category Ingredient
  const categoryIngredientDelete = useSelector((state) => state.categoryIngredientDelete)
  const { loading: loadingDeleteIngredient, success: successDeleteIngredient, ingredient: deletedIngredient, error: errorDeleteIngredient } = categoryIngredientDelete
  // TODO state category Option
  const categoryOptionCreate = useSelector((state) => state.categoryOptionCreate)
  const { loading: loadingCreateOption, success: successCreateOption, option: createdOption, error: errorCreateOption } = categoryOptionCreate

  const categoryOptionUpdate = useSelector((state) => state.categoryOptionUpdate)
  const { loading: loadingUpdateOption, success: successUpdateOption, option: updatedOption, error: errorUpdateOption } = categoryOptionUpdate

  const categoryOptionDelete = useSelector((state) => state.categoryOptionDelete)
  const { loading: loadingDeleteOption, success: successDeleteOption, option: deletedOption, error: errorDeleteOption } = categoryOptionDelete

  const showIcon = (icon) => {
    return (<i className={`fas ${icon} fa-lg`} />)
  }

  const optionsIcon = [
    { label: (showIcon('fa-concierge-bell')), value: 'fa-concierge-bell' },
    { label: "Food", disabled: true },
    { label: (showIcon('fa-apple-alt')), value: 'fa-apple-alt' },
    { label: (showIcon('fa-bacon')), value: 'fa-bacon' },
    { label: (showIcon('fa-bread-slice')), value: 'fa-bread-slice' },
    { label: (showIcon('fa-candy-cane')), value: 'fa-candy-cane' },
    { label: (showIcon('fa-cheese')), value: 'fa-cheese' },
    { label: (showIcon('fa-cookie')), value: 'fa-cookie' },
    { label: (showIcon('fa-drumstick-bite')), value: 'fa-drumstick-bite' },
    { label: (showIcon('fa-hamburger')), value: 'fa-hamburger' },
    { label: (showIcon('fa-hotdog')), value: 'fa-hotdog' },
    { label: (showIcon('fa-ice-cream')), value: 'fa-ice-cream' },
    { label: (showIcon('fa-pizza-slice')), value: 'fa-pizza-slice' },
    { label: (showIcon('fa-stroopwafel')), value: 'fa-stroopwafel' },
    { label: "Beverage", disabled: true },
    { label: (showIcon('fa-beer')), value: 'fa-beer' },
    { label: (showIcon('fa-blender')), value: 'fa-blender' },
    { label: (showIcon('fa-cocktail')), value: 'fa-cocktail' },
    { label: (showIcon('fa-coffee')), value: 'fa-coffee' },
    { label: (showIcon('fa-glass-cheers')), value: 'fa-glass-cheers' },
    { label: (showIcon('fa-glass-martini-alt')), value: 'fa-glass-martini-alt' },
    { label: (showIcon('fa-glass-whiskey')), value: 'fa-glass-whiskey' },
    { label: (showIcon('fa-mug-hot')), value: 'fa-mug-hot' },
    { label: (showIcon('fa-wine-glass-alt')), value: 'fa-wine-glass-alt' },
  ]
  const [ categoryManage, setCategoryManage ] = useState({
    updateCategoryIngredientOption: false,
    newCategoryIngredient: [],
    newCategoryOption: [],
    expandedRowCategory: [],
    expandedRowCategoryIngredient: [],
    dataCategoryIngredient: [],
    dataCategoryIngredientOption: []
  })
  const { updateCategoryIngredientOption, expandedRowCategory, expandedRowCategoryIngredient, dataCategoryIngredient, dataCategoryIngredientOption } = categoryManage

  // * GET DATA
  const getDataCategoryIngredient = (CID) => {
    try {
      return axios.get(`${process.env.API_URL}/v1/category/ingredient/${CID}`)
    } catch (error) {
      console.error(error)
    }
  }

  const getDataCategoryIngredientOption = (CIID) => {
    try {
      return axios.get(`${process.env.API_URL}/v1/category/option/${CIID}`)
    } catch (error) {
      console.error(error)
    }
  }

  // * START props MODAL & FORM & UPLOAD Category
  // ?  UPLOAD
  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['CName',	'CIName',	'CIOName', 'CIOPrice']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const onChangeUpload = ({fileList}) => {
    dispatch(uploadFileLocal(fileList))
  }

  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = (check = false, record = {}) => {
    setCategoryManage({ ...categoryManage, updateCategoryIngredientOption: check})
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false));
    form.resetFields();
    form.setFieldsValue(record);
  }

  const handleModalOpenUpdate = async (check = false, CategoryData = {}) => {
    let subData = []
    // ? check show form Material or Option in modal
    if(check){
      const { data } = await getDataCategoryIngredientOption(CategoryData.CIID)
      subData = {categoryIngredientsOption: data}
      // TODO open expand table when select update
      // ? params => ( true = expand, CategoryData = data in row, check = check show form Material or Option in modal )
      handleOnExpandCategoryIngredient(true, CategoryData, check)
    }else{
      const { data } = await getDataCategoryIngredient(CategoryData.CID)
      subData = {categoryIngredient: data}
      // TODO open expand table when select update
      // ? params => ( true = expand, CategoryData = data in row, check = check show form Material or Option in modal )
      handleOnExpandCategory(true, CategoryData, check)
    }
    // ! create object newCategoryData and push CategoryData and subData in newCategoryData to be show data in form
    const newCategoryData = {...CategoryData, ...subData}
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(newCategoryData);
  }
  
  const handleOpenModalDelete = (CategoryID = "") => {
    Modal.confirm({
      title: t('common:modal.txt_delete', { page_manage: pageManage.page_manage, ID: CategoryID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteCategory(CategoryID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          if (resp.rows.slice(1).length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importCategory({CategoryData: resp.rows}))
          }
        }
      });
    }else{
      form.submit();
    }
  }

  const handleModalBtnCancel = () => {
    if(isUpload) dispatch({ type: LOCAL_UPLOAD_RESET })
    dispatch(modalCloseLocal())
  }

  const onFormFinish = (values) => {
    if(typeof values.categoryIngredientsOption === 'undefined'){
      (isUpdate)? dispatch(updateCategory(values)): dispatch(createCategory(values))
    }else{
      (isUpdate)? dispatch(updateCategoryOption(values, expandedRowCategory)): dispatch(createCategoryOption(values, expandedRowCategory))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  }
  // * END props MODAL & FORM & UPLOAD Category

  // * START props Table Category
  const handleCategoryChange = (pagination, filters, sorter) => {
    dispatch(listCategory({pagination, filters, sorter}));
  }

  const handleCategoryChangeStatus = (CategoryID) => {
    dispatch(changeStatusCategory(CategoryID))
  }

  const columns = [
    {
      title: t('table.id'),
      dataIndex: 'CID',
      key: 'CID',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.id')),
    },
    {
      title: t('table.name'),
      dataIndex: 'CName',
      key: 'CName',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.name')),
    },
    {
      title: t('table.icon'),
      dataIndex: 'CIcon',
      key: 'CIcon',
      align: 'center',
      render: (text, record) => ( <i className={`fas ${record.CIcon} fa-2x`} /> )
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'CStatus',
      key: 'CStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( <Switch defaultChecked={record.CStatus} disabled={loadingChangeStatus} onChange={()=> handleCategoryChangeStatus(record.CID)} /> )
    },
    {
      title: t('common:table.action.txt_column'),
      key: 'Action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(false, record)}>{t('common:table.action.btn_edit')}</Button>
          <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.CID)}>{t('common:table.action.btn_delete')}</Button>
        </Space>
      ),
    }
  ]
  // * END props Table Category

  // * START props Expand Table
  const handleOnExpandCategoryIngredient = async (expanded, record, check = false) => {
    if(expanded){
      const { data } = await getDataCategoryIngredientOption(record.CIID)
      setCategoryManage({ ...categoryManage, updateCategoryIngredientOption: check, expandedRowCategoryIngredient: [record.CIID], dataCategoryIngredientOption: data})
    }else{
      setCategoryManage({ ...categoryManage, updateCategoryIngredientOption: check, expandedRowCategoryIngredient: []})
    }
  }

  const handleOnExpandCategory = async (expanded, record, check = false) => {
    try {
      if(expanded){
        const { data } = await getDataCategoryIngredient(record.CID)
        setCategoryManage({ ...categoryManage, updateCategoryIngredientOption: check, expandedRowCategory: [record.CID], dataCategoryIngredient: data})
      }else{
        setCategoryManage({ ...categoryManage, updateCategoryIngredientOption: check, expandedRowCategory: []})
      }
    } catch (error) {
      console.error(error)
    }
  }

  const expandIconCustom = ({ expanded, onExpand, record }) => {
    return (
      expanded ? (
        <MinusCircleTwoTone onClick={e => onExpand(record, e)} />
      ) : (
        <PlusCircleTwoTone onClick={e => onExpand(record, e)} />
      )
    )
  }

  const expandIconBtnCustom = ({ expanded, onExpand, record }) => {
    return (
      expanded ? (
        <Button className="btn btn-info"  onClick={e => onExpand(record, e)}><MinusCircleTwoTone /> {t('common:table.close_expand')}</Button>
      ) : (
        <Button className="btn btn-info"  onClick={e => onExpand(record, e)}><PlusCircleTwoTone /> {t('common:table.open_expand')}</Button>
      )
    )
  }

  const expandedCategoryIngredientOption = () => {
    const columns = [
      { title: '#CIOID', dataIndex: 'CIOID', key: 'CIOID' },
      { title: t('table_expand_option.name'), dataIndex: 'CIOName', key: 'CIOName' },
      { title: t('table_expand_option.price'), dataIndex: 'CIOPrice', key: 'CIOPrice' },
    ];

    return <Table 
              size="small"
              rowKey={record => record.CIOID} 
              columns={columns} 
              dataSource={dataCategoryIngredientOption} 
              pagination={false} 
            />;
  }

  const expandedCategoryIngredient = () => {
    const columns = [
      { title: t('table_expand_material.id'), dataIndex: 'CIID', key: 'CIID' },
      { title: t('table_expand_material.name'), dataIndex: 'CIName', key: 'CIName' },
      {
        title: t('common:table.action.txt_column'),
        key: 'Action',
        align: 'center',
        render: (text, record) => (
          <Space size="middle">
            {(record.isEmpty <= 0)
            ?<Button className="btn btn-success" onClick={()=> handleModalOpenCreate(true, record)}>{t('common:modal.txt_insert', {page_manage: pageManage.sub_text})}</Button>
            :<Space size="middle">
            <Button className="btn btn-info" onClick={()=> handleOnExpandCategoryIngredient(true, record)}>{t('common:table.sub_open_expand', {sub_text: pageManage.sub_text})}</Button>
            <Button className="btn btn-yellow" onClick={()=> handleModalOpenUpdate(true, record)}>{t('common:modal.txt_update', {page_manage: pageManage.sub_text})}</Button>
            </Space>
            }
          </Space>
        ),
      }
    ];

    return <Table 
              rowKey={record => record.CIID} 
              columns={columns} 
              dataSource={dataCategoryIngredient} 
              pagination={false} 
              expandedRowKeys={expandedRowCategoryIngredient}
              expandable={{ expandedRowRender: expandedCategoryIngredientOption, expandIcon: expandIconCustom }} 
              onExpand={handleOnExpandCategoryIngredient}
            />;
  }
  // * END props Expand Table
  const handleRemoveCategoryIngredient = (remove, name) => {
    const formData = form.getFieldValue();
    if(!_.isEmpty(formData.categoryIngredient[name])){
      if(Object.keys(formData.categoryIngredient[name]).length === 5){
        const CIID = formData.categoryIngredient[name].CIID
        Modal.confirm({
          title: t('common:modal.txt_delete', { page_manage: t('table_expand_material.name'), ID: CIID }),
          icon: <ExclamationCircleOutlined />,
          okType: 'danger',
          zIndex: 1050,
          onOk() {
            dispatch(deleteCategoryIngredient(CIID))
            remove(name)
          },
        });
      }else{
        remove(name)
      }
    }else{
      remove(name)
    }
  }

  const handleRemoveCategoryIngredientOption = (remove, name) => {
    const formData = form.getFieldValue();
    if(!_.isEmpty(formData.categoryIngredientsOption[name])){
      if(Object.keys(formData.categoryIngredientsOption[name]).length === 4){
        const CIOID = formData.categoryIngredientsOption[name].CIOID
        Modal.confirm({
          title: t('common:modal.txt_delete', { page_manage: t('table_expand_option.name'), ID: CIOID }),
          icon: <ExclamationCircleOutlined />,
          okType: 'danger',
          zIndex: 1050,
          onOk() {
            dispatch(deleteCategoryOption(CIOID))
            remove(name)
          },
        });
      }else{
        remove(name)
      }
    }else{
      remove(name)
    }
  }
  // * START handle Form.List

  // * END handle Form.List

  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        CATEGORY_LIST()
      }
    })
  }

  const CATEGORY_LIST = () => {
    dispatch(listCategory())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      CATEGORY_LIST()
    }
  },[])

  useEffect(async()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), importdCategory)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: CATEGORY_IMPORT_RESET })
    }
    if(successCreateCategory){
      SwalFire('success', t('common:alertSuccess'), createdCategory)
      handleOnExpandCategory(true, {CID: createdCategory.CID}) // TODO: if success open Expand table 
      dispatch({ type: CATEGORY_CREATE_RESET })
    }
    if(successUpdateCategory){
      SwalFire('success', t('common:alertSuccess'), updatedCategory)
      handleOnExpandCategory(true, {CID: updatedCategory.CID}) // TODO: if success reload data in Expand table 
      dispatch({ type: CATEGORY_UPDATE_RESET })
    }
    if(successDeleteCategory){
      SwalFire('success', t('common:alertSuccess'), deletedCategory)
      dispatch({ type: CATEGORY_DELETE_RESET })
    }
    if(successDeleteIngredient){
      setCategoryManage({...categoryManage, dataCategoryIngredient: [ ...dataCategoryIngredient.filter((x)=> x.CIID !== deletedIngredient.CIID) ] })
      dispatch({ type: INGREDIENT_DELETE_RESET })
    }
    if(successCreateOption){
      SwalFire('success', t('common:alertSuccess'), createdOption)
      handleOnExpandCategoryIngredient(true, {CIID: createdOption.CIID}, true) // TODO: if success open Expand table 
      dispatch({ type: OPTION_CREATE_RESET })
      // TODO: update isEmpty == 1 in added data for change btn in column Action on table
      const index = _.findIndex(categoryManage.dataCategoryIngredient, {'CIID': createdOption.CIID})
      dataCategoryIngredient[index].isEmpty = 1
      setCategoryManage({...categoryManage, dataCategoryIngredient: [ ...dataCategoryIngredient ] })
    }
    if(successUpdateOption){
      SwalFire('success', t('common:alertSuccess'), updatedOption)
      handleOnExpandCategoryIngredient(true, {CIID: updatedOption.CIID}, true) // TODO: if success reload data in Expand table 
      dispatch({ type: OPTION_UPDATE_RESET })
    }
    if(successDeleteOption){
      setCategoryManage({...categoryManage, dataCategoryIngredientOption: [ ...dataCategoryIngredientOption.filter((x)=> x.CIOID !== deletedOption.CIOID) ] })
      dispatch({ type: OPTION_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: CATEGORY_IMPORT_RESET })
    }
    if(successCreateCategory === false){
      SwalFire('error', t('common:alertError'), errorCreateCategory)
      dispatch({ type: CATEGORY_CREATE_RESET })
    }
    if(successUpdateCategory === false){
      SwalFire('error', t('common:alertError'), errorUpdateCategory)
      dispatch({ type: CATEGORY_UPDATE_RESET })
    }
    if(successDeleteCategory === false){
      SwalFire('error', t('common:alertError'), errorDeleteCategory)
      dispatch({ type: CATEGORY_DELETE_RESET })
    }
    if(successDeleteIngredient === false){
      SwalFire('error', t('common:alertError'), errorDeleteIngredient)
      dispatch({ type: INGREDIENT_DELETE_RESET })
    }
    if(successCreateOption === false){
      SwalFire('error', t('common:alertError'), errorCreateOption)
      dispatch({ type: OPTION_CREATE_RESET })
    }
    if(successUpdateOption === false){
      SwalFire('error', t('common:alertError'), errorUpdateOption)
      dispatch({ type: OPTION_UPDATE_RESET })
    }
    if(successDeleteOption === false){
      SwalFire('error', t('common:alertError'), errorDeleteOption)
      dispatch({ type: OPTION_DELETE_RESET })
    }

    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusCategory.msg: errorChangeStatus.msg,
      });
      dispatch({ type: CATEGORY_CHANGE_RESET })
    }
  },[
    successImport,
    successCreateCategory,
    successUpdateCategory,
    successDeleteCategory,
    successChangeStatus,
    successDeleteIngredient,
    successCreateOption,
    successUpdateOption,
    successDeleteOption
  ])
  const rulesDynamicField = [{
    validator: async (_, categoryIngredient) => {
      if (!categoryIngredient || categoryIngredient.length < 1) {
        return Promise.reject(new Error('At least 1 field'));
      }
    },
  }]
  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={()=> handleModalOpenCreate(false)}>{t('common:modal.txt_insert', pageManage)}</Button>
              <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button>
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns} 
              rowKey={record => record.CID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              expandedRowKeys={expandedRowCategory}
              expandable={{ expandedRowRender: expandedCategoryIngredient, expandIcon: expandIconBtnCustom }}
              onExpand={handleOnExpandCategory}
              onChange={handleCategoryChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreateCategory || loadingUpdateCategory|| loadingCreateOption || loadingUpdateOption)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            initialValues={{categoryIngredient: [{CIName: ''}], categoryIngredientsOption: [{CIOName:'', CIOPrice: ''}]}}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            {(!updateCategoryIngredientOption)?
              <Row gutter={24}>
                <Col xs={24} lg={24} xl={5}>
                  <Form.Item 
                    label={t('table.id')} 
                    name="CID"
                  >
                    <Input disabled={true}  />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={6}>
                  <Form.Item 
                    label={t('table.name')} 
                    name="CName"
                    rules={[{ required: true, whitespace: true }]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={4}>
                  <Form.Item 
                    label={t('table.icon')} 
                    name="CIcon"
                    rules={[{ required: true, whitespace: true }]}
                  >
                    <Select allowClear autoFocus style={{ width: '100%' }} className="ml-3" onChange={()=>{}} options={optionsIcon}>

                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={24}>
                  <Form.List name="categoryIngredient" rules={rulesDynamicField}>
                    {(fields, { add, remove }, { errors }) => (
                      <>
                        {fields.map(({ key, name, fieldKey, ...restField }) => (
                          <Row key={key} gutter={24} justify="space-around" align="middle">
                            <Col xs={8} lg={9} xl={9}>
                              <Form.Item
                                {...restField}
                                label={t('table_expand_material.id')}  
                                name={[name, 'CIID']}
                                fieldKey={[fieldKey, 'CIID']}
                              >
                                <Input disabled={true}/>
                              </Form.Item>
                            </Col>
                            <Col xs={8} lg={9} xl={9}>
                              <Form.Item
                                {...restField}
                                label={t('table_expand_material.name')}  
                                name={[name, 'CIName']}
                                fieldKey={[fieldKey, 'CIName']}
                                rules={[{ required: true, whitespace: true }]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col xs={4} lg={4} xl={4} className="text-center">
                              <Form.Item
                                {...restField}
                                label={t('table_expand_material.multiple')} 
                                tooltip={t('table_expand_material.multipleTooltip')} 
                                name={[name, 'CIMultiple']}
                                fieldKey={[fieldKey, 'CIMultiple']}
                                valuePropName="checked"
                              >
                                <Switch checkedChildren={t('table_expand_material.multipleOn')}  unCheckedChildren={t('table_expand_material.multipleOff')}  />
                              </Form.Item>
                            </Col>
                            <Col xs={4} lg={2} xl={2}>
                              {fields.length > 1 ? (<MinusCircleOutlined onClick={() => handleRemoveCategoryIngredient(remove, name)} />): null}
                            </Col>
                          </Row>
                        ))}
                        <Form.Item>
                          <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                            {t('common:add_field')}
                          </Button>
                          <Form.ErrorList errors={errors} />
                        </Form.Item>
                      </>
                    )}
                  </Form.List>
                </Col>
              </Row>:
              <Row gutter={24}>
                <Col xs={0} lg={0} xl={0}>
                  <Form.Item 
                    name="CIID"
                  >
                    <Input disabled={true}  />
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24} xl={24}>
                  <Form.List name="categoryIngredientsOption" rules={rulesDynamicField}>
                    {(fields, { add, remove }, { errors }) => (
                      <>
                        {fields.map(({ key, name, fieldKey, ...restField }) => (
                          <Row key={key} gutter={24} align="middle">
                            <Col xs={20} lg={6} xl={6}>
                              <Form.Item
                                {...restField}
                                label={t('table_expand_option.id')} 
                                name={[name, 'CIOID']}
                                fieldKey={[fieldKey, 'CIOID']}
                              >
                                <Input disabled={true}/>
                              </Form.Item>
                            </Col>
                            <Col xs={10} lg={8} xl={8}>
                              <Form.Item
                                {...restField}
                                label={t('table_expand_option.name')} 
                                name={[name, 'CIOName']}
                                fieldKey={[fieldKey, 'CIOName']}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col xs={10} lg={8} xl={8}>
                              <Form.Item
                                {...restField}
                                label={t('table_expand_option.price')} 
                                name={[name, 'CIOPrice']}
                                fieldKey={[fieldKey, 'CIOPrice']}
                                // rules={[{ required: true }]}
                              >
                                <InputNumber style={{ width: "100%" }} min={0} />
                              </Form.Item>
                            </Col>
                            <Col xs={4} lg={2} xl={2}>
                              {fields.length > 1 ? (<MinusCircleOutlined onClick={() => handleRemoveCategoryIngredientOption(remove, name)} />): null}
                            </Col>
                          </Row>
                        ))}
                        <Form.Item>
                          <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                            {t('common:add_field')}
                          </Button>
                          <Form.ErrorList errors={errors} />
                        </Form.Item>
                      </>
                    )}
                  </Form.List>
                </Col>
              </Row>
            }
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'category', 'common', 'employee']),
  },
})

export default CategoryManage