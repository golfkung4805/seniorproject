import _ from 'lodash'
import Swal from 'sweetalert2'
import jwt from "jsonwebtoken";

import Head from 'next/head'

import { useEffect } from 'react';
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { Row, Col, Form, Button, Upload, message, Input, Select, Switch, Table, Space, Modal, notification } from 'antd'


import { EMPLOYEE_SIGNIN_RESET, EMPLOYEE_SIGNOUT } from '../../redux/constants/employeeConstants'

import { signin } from '../../redux/actions/employeeActions'

const LoginManage = () => {

  const router = useRouter()

  const { t } = useTranslation('header')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const dispatch = useDispatch();

  const [ form ] = Form.useForm();

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo, loading, error, success } = employeeSignIn

  const SwalFire = (icon = "", title = "", text = "") => {
    if(icon === 'success') {
      // dispatch(modalCloseLocal())
    }
    Swal.fire({
      icon,
      title,
      text: text.msg || text
    })
  }

  const handleBtnLogin = () => {
    form.submit();
  }
  
  const onFormFinish = async (values) => {
    // console.log(values)
    dispatch(signin(values))
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };

  useEffect(()=>{
    try {
      let GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo)
      const decoded = jwt.verify(GetEmpInfo.token, process.env.JWT_SECRET_KEY);
      if(decoded){
        if(decoded.EmpRole === 'admin'){
          location.replace(`${process.env.BASE_URL}/manage`)
        }else{
          location.replace(`${process.env.BASE_URL}/manage/${decoded.EmpRole}`)
        } 
      }
    } catch (error) {
      console.error(error);
      dispatch({type: EMPLOYEE_SIGNOUT})
    }
  },[])

  useEffect(()=>{
    if(success){
      SwalFire('success', t('common:alertSuccess'), t('loginSuccess'))
      if(empInfo.isRole === 'admin'){
        router.push(`/manage`)
      }else{
        router.push(`/manage/${empInfo.isRole}`)
      } 
      dispatch({ type: EMPLOYEE_SIGNIN_RESET })
    }
    if(success === false){
      SwalFire('error', t('common:alertError'), error)
      dispatch({ type: EMPLOYEE_SIGNIN_RESET })
    }
  },[success])

  return(
    <>
      <Head>
        <title>Login | SR RestaurantManage</title>
      </Head>
      <div id="page-container">
        <div className="login login-with-news-feed">
          <div className="news-feed">
            <div className="news-image" style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-11.jpg)'}} />
            <div className="news-caption">
              {/* <h4 className="caption-title"><b>Color</b> Admin App</h4>
              <p>
                Download the Color Admin app for iPhone®, iPad®, and Android™. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p> */}
            </div>
          </div>
          <div className="right-content">
            <div className="login-header">
              <div className="brand">
                <span className="logo" /> <b className="pr-1">SR</b> RestaurantManage
                {/* <small>responsive bootstrap 4 admin template</small> */}
              </div>
              <div className="icon">
                <i className="fa fa-sign-in-alt" />
              </div>
            </div>
            <div className="login-content">
              <Form
                layout="vertical"
                form={form}
                onFinish={onFormFinish}
                onFinishFailed={onFormFinishFailed}
              >
                <Row gutter={24}>
                  <Col sm={24} className="w-100">
                    <Form.Item 
                      label={t('navbarUser.txtUsername')}
                      name="Username"
                      rules={[{ required: true, whitespace: true }]}
                    >
                      <Input className="form-control form-control-lg" />
                    </Form.Item>
                  </Col>
                  <Col sm={24} className="w-100">
                    <Form.Item 
                      label={t('navbarUser.txtPassword')}
                      name="Password"
                      rules={[{ required: true, whitespace: true }]}
                    >
                      <Input className="form-control form-control-lg" type="password" />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
                {/* <div className="checkbox checkbox-css m-b-30">
                  <input type="checkbox" id="remember_me_checkbox" defaultValue />
                  <label htmlFor="remember_me_checkbox">
                    Remember Me
                  </label>
                </div> */}
                <div className="login-buttons">
                  <Button type="submit" onClick={handleBtnLogin} className="btn btn-success btn-block" loading={loading} size="large">{t('navbarUser.logIn')}</Button>
                </div>
                <hr />
                <p className="text-center text-grey-darker mb-0">
                  © Color Admin All Right Reserved 2020
                </p>
            </div>
          </div>
        </div>
      </div>

      <script src="/js/app.min.js"></script>
      <script src="/js/default.min.js"></script>
    </>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'common']),
  },
})

export default LoginManage