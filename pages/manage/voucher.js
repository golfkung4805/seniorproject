import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import Swal from 'sweetalert2'
import moment from 'moment';

import { ExcelRenderer } from "react-excel-renderer";
import randomstring from "randomstring";
import { useEffect, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, CodeSandboxOutlined, ExclamationCircleOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, InputNumber, Select, DatePicker, Switch, Table, Space, Modal, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  VOUCHER_IMPORT_RESET,
  VOUCHER_CREATE_RESET,
  VOUCHER_UPDATE_RESET,
  VOUCHER_DELETE_RESET,
  VOUCHER_CHANGE_RESET
} from '../../redux/constants/voucherConstants'

import { 
  listVoucher,
  importVoucher,
  createVoucher,
  updateVoucher,
  changeStatusVoucher,
  deleteVoucher
} from '../../redux/actions/voucherActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps,
  getColumnSearchDateProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const VoucherManage = () => {

  const { t } = useTranslation('voucher')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Dragger } = Upload;

  const { Option } = Select;

  const { TextArea } = Input;

  const { RangePicker } = DatePicker;

  const [ form ] = Form.useForm();
  
  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const employeeSignIn = useSelector((state) => state.employeeSignIn)
  const { empInfo } = employeeSignIn

  const voucherList = useSelector((state) => state.voucherList)
  const { loading, data, pagination } = voucherList

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  
  const voucherImport = useSelector((state) => state.voucherImport)
  const { loading: loadingImport, success: successImport, voucher: ImportdVoucher, error: errorImport } = voucherImport
  
  const voucherCreate = useSelector((state) => state.voucherCreate)
  const { loading: loadingCreate, success: successCreate, voucher: createdVoucher, error: errorCreate } = voucherCreate

  const voucherUpdate = useSelector((state) => state.voucherUpdate)
  const { loading: loadingUpdate, success: successUpdate, voucher: updatedVoucher, error: errorUpdate } = voucherUpdate
  
  const voucherChangeStatus = useSelector((state) => state.voucherChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, voucher: changingStatusVoucher, error: errorChangeStatus } = voucherChangeStatus

  const voucherDelete = useSelector((state) => state.voucherDelete)
  const { loading: loadingDelete, success: successDelete, voucher: deletedVoucher, error: errorDelete } = voucherDelete

  const [ getVoucType, setGetVoucType ] = useState('')

  // * START props MODAL & FORM & UPLOAD VOUCHER
  // ?  UPLOAD
  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['VoucCode', 'VoucName', 'VoucUsePoint', 'VoucDiscount', 'VoucType', 'VoucCondition', 'VoucStart', 'VoucEnd', 'VoucQuota']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const onChangeUpload = ({fileList}) => {
    dispatch(uploadFileLocal(fileList))
  }

  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false)); 
    form.resetFields();
  }

  const handleModalOpenUpdate = (VouData = {}, VoucID = "") => {
    const newCategoryData = {...VouData, ...{ VoucRangeDate: [moment(VouData.VoucStart), moment(VouData.VoucEnd)] }}
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(newCategoryData);
  }

  const handleOpenModalDelete = (VoucID = "") => {
    // console.log(VoucID)
    Modal.confirm({
      title: t('common:modal.txt_delete', { page_manage: pageManage.page_manage, ID: VoucID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteVoucher(VoucID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          let VouData = [];
          resp.rows.slice(1).map((row, index) => {
            if (row && !_.isEmpty(row)) {
              VouData.push({
                VoucCode: row[0],
                VoucName: row[1],
                VoucUsePoint: row[2],
                VoucDiscount: row[3],
                VoucType: row[4],
                VoucCondition: row[5],
                VoucStart: row[6],
                VoucEnd: row[7],
                VoucQuota: row[8]
              });
            }
          });
          if (VouData.length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importVoucher(VouData))
          }
        }
      });
    }else{
      form.submit();
    }
  };

  const handleModalBtnCancel = () => {
    if(isUpload) dispatch({ type: LOCAL_UPLOAD_RESET })
    dispatch(modalCloseLocal())
  };

  const onFormFinish = (values) => {
    if(isUpdate){
      dispatch(updateVoucher(values))
    }else{
      dispatch(createVoucher(values))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
  // * END props MODAL & FORM & UPLOAD VOUCHER

  // * START props TABLE VOUCHER
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listVoucher({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (VoucID) => {
    dispatch(changeStatusVoucher(VoucID))
  }

  const columns = () => {
    let columnsRole = [
      {
        title: t('table.id'),
        dataIndex: 'VoucID',
        key: 'VoucID',
        sorter: true,
        sortDirections: ['descend', 'ascend'],
        ...getColumnSearchProps(t, t('table.id')),
      },
      {
        title: t('table.name'),
        dataIndex: 'VoucName',
        key: 'VoucName',
        ...getColumnSearchProps(t, t('table.name')),
      },
      {
        title: t('table.code'),
        dataIndex: 'VoucCode',
        key: 'VoucCode',
        // ...getColumnSearchProps(t, t('table.code')),
      },
      {
        title: t('table.use_point'),
        dataIndex: 'VoucUsePoint',
        key: 'VoucUsePoint',
        sorter: true,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: t('table.date'),
        dataIndex: 'VoucRangeDate',
        key: 'VoucRangeDate',
        render: (text, record) => (`${moment(record.VoucStart).format('DD/MM/YYYY')} - ${moment(record.VoucEnd).format('DD/MM/YYYY')}`),
        ...getColumnSearchDateProps(t, t('table.date')),
      },
      {
        title: t('table.quota'),
        dataIndex: 'VoucQuota',
        key: 'VoucQuota',
        sorter: true,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: t('common:table.status.txt_column'),
        dataIndex: 'VoucStatus',
        key: 'VoucStatus',
        align: 'center',
        filters: [
          { text: t('common:table.status.approved'), value: '1' },
          { text: t('common:table.status.disapproved'), value: '0' },
        ],
        render: (text, record) => ( <Switch defaultChecked={record.VoucStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.VoucID)} /> )
      }
    ]
    if(empInfo.isRole === 'admin'){
      columnsRole =  [...columnsRole, {
        title: t('common:table.action.txt_column'),
        key: 'Action',
        align: 'center',
        render: (text, record) => (
          <Space size="middle">
            <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(record)}>{t('common:table.action.btn_edit')}</Button>
            <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.VoucID)}>{t('common:table.action.btn_delete')}</Button>
          </Space>
        ),
      }]
    }
    return columnsRole
  }
  // * END props TABLE VOUCHER

  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        VOUCHER_LIST()
      }
    })
  }

  const VOUCHER_LIST = () => {
    dispatch(listVoucher())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      VOUCHER_LIST()
    }
  },[])
  
  useEffect(()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), ImportdVoucher)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: VOUCHER_IMPORT_RESET })
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdVoucher)
      dispatch({ type: VOUCHER_CREATE_RESET })
    }
    if(successUpdate){
      SwalFire('success', t('common:alertSuccess'), updatedVoucher)
      dispatch({ type: VOUCHER_UPDATE_RESET })
    }
    if(successDelete){
      SwalFire('success', t('common:alertSuccess'), deletedVoucher)
      dispatch({ type: VOUCHER_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: VOUCHER_IMPORT_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: VOUCHER_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: VOUCHER_UPDATE_RESET })
    }
    if(successDelete === false){
      SwalFire('error', t('common:alertError'), errorDelete)
      dispatch({ type: VOUCHER_DELETE_RESET })
    }

    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusVoucher.msg: errorChangeStatus.msg,
      });
      dispatch({ type: VOUCHER_CHANGE_RESET })
    }
  },[
    successImport,
    successCreate,
    successUpdate,
    successDelete,
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              {empInfo.isRole === 'admin' && (<Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button>)}
              {/* <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button> */}
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns()} 
              rowKey={record => record.VoucID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            <Row gutter={24}>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.id')} 
                  name="VoucID"
                >
                  <Input disabled={true}  />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={14}>
                <Form.Item 
                  label={t('table.name')} 
                  name="VoucName"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.code')} 
                  name="VoucCode"
                  rules={[{ required: true, whitespace: true, min: 5, max: 10 }]}
                >
                  <Input.Search enterButton={<CodeSandboxOutlined style={{ fontSize: '22px' }}/>} onSearch={()=> form.setFieldsValue({VoucCode: randomstring.generate({ length: 10, charset: 'alphanumeric' })})} />
                  {/* <Input /> */}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.use_point')}  
                  name="VoucUsePoint"
                  rules={[{ type:'number', required: true, min: 0 }]}
                >
                  <InputNumber style={{ width: "100%" }} min={0} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.type')} 
                  name="VoucType"
                  rules={[{ type:'enum', required: true, whitespace: true, enum: ['%', '฿'] }]}
                >
                  <Select
                    showSearch
                    allowClear
                    onChange={(value)=> setGetVoucType(value)}
                  >
                    <Option value="฿">ส่วนลดเป็น ฿</Option>
                    <Option value="%">ส่วนลดเป็น %</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.discount')} 
                  name="VoucDiscount"
                  rules={[{ type:'number', required: true, min: 1, max: (getVoucType === '%')? 100: Number.MAX_SAFE_INTEGER }]}
                >
                  <InputNumber style={{ width: "100%" }} min={1} max={(getVoucType === '%')? 100: Number.MAX_SAFE_INTEGER} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.condition')} 
                  name="VoucCondition"
                  rules={[{ type:'number', required: true, min: 0 }]}
                >
                  <InputNumber style={{ width: "100%" }} min={0} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.quota')} 
                  name="VoucQuota"
                  rules={[{ type:'number', required: true, min: 1 }]}
                >
                  <InputNumber style={{ width: "100%" }} min={1} />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={9}>
                <Form.Item 
                  label={t('table.date')} 
                  name="VoucRangeDate"
                  rules={[{ required: true }]}
                >
                  <RangePicker format={['DD/MM/YYYY', 'DD/MM/YYYY']} style={{ width: "100%" }} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'voucher', 'common', 'employee']),
  },
})

export default VoucherManage