import Link from 'next/link'
import Head from 'next/head'

import qs from 'qs'
import jwt from "jsonwebtoken";
import _ from 'lodash'
import axios from 'axios'
import Swal from 'sweetalert2'
import { io } from "socket.io-client"
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import moment from 'moment'
import 'moment/locale/th'

import { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, DatePicker, message, Tag, Typography, Input, Select, Switch, Table, Space, Modal, notification } from 'antd'

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import Layout from '../../components/layout/manage/mainManage'

const orderManage = () => {

  const { t } = useTranslation('order')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Option } = Select;

  const { Text } = Typography;

  const [ form ] = Form.useForm();
  
  const [ socket, setSocket ] = useState({})
  const [ typePicker, setTypePicker ] = useState('date');
  const [ exportFilterStatus, setExportFilterStatus ] = useState(['openOrder', 'paid', 'unpaid', 'successful', 'failed', 'overdue']);
  const [ datePicker, setDatePicker] = useState(moment())
  const [ dataOrder, setDataOrder ] = useState({ loading: false, data: [] })
  const [ dataOrderDetail, setDataOrderDetail ] = useState({ expandedRowKeys: [], data: [] })

  const formatPicker = {
    'date': 'DD MMM YYYY',
    'week': 'W-YYYY',
    'month': 'MMM YYYY',
    'year': 'YYYY'
  }

  const tagStatusOrder = {
    'openOrder': 'blue',
    'paid': 'gold',
    'unpaid': 'volcano',
    'successful': 'green',
    'failed': 'red',
    'overdue': '',
  }

  const tagStatusOrderDetail = {
    'waitQueue': 'orange',
    'complete': 'green',
    'cancel': 'red',
  }

  const payName = {
    'checkout_counter': 'Checkout Counter',
    'credit_card': 'Credit Card',
    'internet_banking': 'Internet Banking',
    'rabbit_linepay': 'Rabbit Linepay',
    'truemoney': 'Truemoney'
  }

  const queryDataOrder = async(params = { 
    pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }
  }) => {
    try {
      setDataOrder({ ...dataOrder, loading: true})
      setDataOrderDetail({ expandedRowKeys: [], data: [] }) 
      const { data } = await axios.get(`${process.env.API_URL}/v1/order`, { params , paramsSerializer: params => qs.stringify(params) })  
      setDataOrder({ loading: false, data: data})
    } catch (error) {
      setDataOrder({ loading: false, data: []})
    }
  }

  const handleChangStatusOrder = async(statusDetail = false, getDataOrder = {}) => {
    let statusOptions = {}
    if(statusDetail){
      statusOptions = {
        waitQueue: t('kitchen:status.waitQueue'),
        complete: t('kitchen:status.complete'),
        cancel: t('kitchen:status.cancel')
      }
    }else{
      statusOptions = {
        openOrder: t('customerOrder:order_sidebar.order_history_open_order'),
        successful: t('customerOrder:order_sidebar.order_history_paid_successful'),
        failed: t('customerOrder:order_sidebar.order_history_paid_failed'),
        unpaid: t('customerOrder:order_sidebar.order_history_unpaid'),
        overdue: t('customerOrder:order_sidebar.order_history_paid_overdue'),
      }
    }
    const { value: orderStatus } = await Swal.fire({
      icon: 'question',
      iconHtml: '🍽',
      title: (statusDetail)?t('titleChangeOrderDetail'): t('titleChangeOrder'),
      text: getDataOrder.ODID,
      input: 'select',
      inputOptions: statusOptions,
      inputPlaceholder: t('inputChangePlaceholder'),
      inputValue: getDataOrder.ODStatus,
      showCancelButton: true,
      inputValidator: (value) => {
        return new Promise((resolve) => {
          (value === '')? resolve(t('changeOrderResolve')): resolve()
        })
      }
    })
    
    if(!_.isEmpty(orderStatus)){
      try {
        let res = [], newData = []
        if (statusDetail) {
          res = await axios.put(`${process.env.API_URL}/v1/order/detail/${getDataOrder._ID}/status`, {
            status: orderStatus
          })
        }else {
          res = await axios.put(`${process.env.API_URL}/v1/order/${getDataOrder.ODID}/status`, {
            status: orderStatus
          })
        }
        if(res.status === 200){
          notification['success']({
            message: res.data.msg
          });
          if (statusDetail) {
            newData = dataOrderDetail.data.map((x)=>x._ID===getDataOrder._ID? {...x, ODStatus: orderStatus}: x)
            setDataOrderDetail({...dataOrderDetail, data: newData})
          }else{
            newData = dataOrder.data.data.map((x)=>x.ODID===getDataOrder.ODID? {...x, ODStatus: orderStatus}: x)
            setDataOrder({...dataOrder, data: {...dataOrder.data, data: newData}})
          }
        }
      } catch (error) {
        console.error(error)
        notification['error']({
          message: `เกิดความผิดพลาดบางอย่าง`
        });
      }
    }
  }
  
  const handleExportReport = async() => {
    const { data } = await axios.get(`${process.env.API_URL}/v1/order`, { params: { 
      filters: { 
        Status: exportFilterStatus, 
        typePicker,
        datePicker: datePicker.format('YYYY-MM-DD')
      },
      pagination: { current: 1, pageSize: 'all', total: 10 },
    }, paramsSerializer: params => qs.stringify(params) })  

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    var ws = XLSX.utils.json_to_sheet([
      { ODID: "ตั้งแต่วันที่", EmpID: `${moment(data.data.at(0).createdAt).format('DD/MM/YYYY HH:mm:ss')}-${moment(data.data.at(-1).createdAt).format('DD/MM/YYYY HH:mm:ss')}` },
      { ODID: "ยอดขายทั้งหมด (THB)", EmpID: _.sumBy(data.data, 'ODTotal') },
      { ODID: "คำสั่งซื้อทั้งหมด", EmpID: _.sumBy(data.data, 'countOrder') },
      { ODID: "" },
      { ODID: "" },
      { ODID: "รหัสออเดอร์", EmpID: "รหัสพนักงาน", CusID: "รหัสสมาชิก", TableID: "รหัสโต๊ะ", ODTotal: "ยอดรวม", VoucDiscount: "ส่วนลด", ODTotalPoint: "พอยท์ที่ได้รับ", PayID: "รหัสชำระเงิน", ODPayDetail: "รายละเอียดชำระเงิน", countOrder: "จำนวนคำสั่งซื้อ", ODStatus: "สถานะ", createdAt: "เวลาสร้าง", updatedAt: "เวลาอัปเดจล่าสุด" },
      ...data.data
    ], { header: ["ODID", "EmpID", "CusID", "TableID", "ODTotal", "VoucDiscount", "ODTotalPoint", "PayID", "ODPayDetail", "countOrder", "ODStatus", "createdAt", "updatedAt"], skipHeader: true });
    const wb = { Sheets: { 'Report': ws }, SheetNames: ['Report'] };

    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const dataExport = new Blob([excelBuffer], {type: fileType});
    FileSaver.saveAs(dataExport, `report_${moment(data.data.at(0).createdAt).format('DD/MM/YYYY')}-${moment(data.data.at(-1).createdAt).format('DD/MM/YYYY')}.xlsx`);
  }

  const onChangeDate = (value) => {
    setDatePicker(value)
    queryDataOrder({
      filters: { typePicker, datePicker: value.format('YYYY-MM-DD') },
      pagination: {
        current: 1,
        pageSize: 10,
        total: 10,
      }
    })
  }

  const onChangeTypePicker = (value) => {
    setTypePicker(value)
    setDatePicker(moment())
    queryDataOrder({
      filters: { typePicker: value, datePicker: moment().format('YYYY-MM-DD') },
      pagination: {
        current: 1,
        pageSize: 10,
        total: 10,
      }
    })
  }
  // * START props TABLE ORDER
  const handleTableChange = (pagination, filters, sorter) => {
    if(!_.isEmpty(filters.Status)){
      setExportFilterStatus(filters.Status)
    }else{
      setExportFilterStatus(['openOrder', 'paid', 'unpaid', 'successful', 'failed', 'overdue'])
    }
    filters = {...filters, typePicker, datePicker: datePicker.format('YYYY-MM-DD') }
    queryDataOrder({pagination, filters, sorter});
  }

  const ODStatusList = {
    'openOrder': t('customerOrder:order_sidebar.order_history_open_order'),
    'paid': t('customerOrder:order_sidebar.order_history_paid'),
    'unpaid': t('customerOrder:order_sidebar.order_history_not_paid'),
    'successful': t('customerOrder:order_sidebar.order_history_paid_successful'),
    'failed': t('customerOrder:order_sidebar.order_history_paid_failed'),
    'overdue': t('customerOrder:order_sidebar.order_history_paid_overdue'),
  }
  const columns = [
    {
      title: t('table.ODID'),
      dataIndex: 'ODID',
      key: 'ODID',
      sortDirections: ['descend', 'ascend'],
      ...getColumnSearchProps(t, t('table.ODID')),
    },
    {
      title: t('table.TableID'),
      dataIndex: 'TableID',
      key: 'TableID',
      ...getColumnSearchProps(t, t('table.TableID')),
    },
    {
      title: t('table.CusID'),
      dataIndex: 'CusID',
      key: 'CusID',
      ...getColumnSearchProps(t, t('table.CusID')),
    },
    {
      title: t('table.ODTotal'),
      dataIndex: 'ODTotal',
      key: 'ODTotal',
      sorter: true,
      render: (text, record) => (
        <>{record.ODTotal - record.VoucDiscount}</>
      ),
    },
    {
      title: t('table.ODStatus'),
      align: 'center',
      dataIndex: 'ODStatus',
      key: 'Status',
      render: (ODStatus) => (
        <Tag color={tagStatusOrder[ODStatus]} style={{fontSize: 14}}>
          {ODStatusList[ODStatus]}
        </Tag>
      ),
      filters: [
        { text: t('customerOrder:order_sidebar.order_history_open_order'), value: 'openOrder' },
        { text: t('customerOrder:order_sidebar.order_history_paid'), value: 'paid' },
        { text: t('customerOrder:order_sidebar.order_history_not_paid'), value: 'unpaid' },
        { text: t('customerOrder:order_sidebar.order_history_paid_successful'), value: 'successful' },
        { text: t('customerOrder:order_sidebar.order_history_paid_failed'), value: 'failed' },
        { text: t('customerOrder:order_sidebar.order_history_paid_overdue'), value: 'overdue' },
      ],
    },
    {
      title: t('common:table.action.txt_column'),
      key: 'Action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Button className="btn btn-info" onClick={()=> handleChangStatusOrder(false, record)}>{t('btnChangStatus')}</Button>
        </Space>
      ),
    }
  ]

  const handleOnExpandOrder = async(expanded, record) => {
    try {
      if(expanded){
        const { data } = await axios.get(`${process.env.API_URL}/v1/order/detail/${record.ODID}`)  
        setDataOrderDetail({ expandedRowKeys: [record.ODID], data: data }) 
      }else{
        setDataOrderDetail({ expandedRowKeys: [], data: [] }) 
      }
    } catch (error) {
      setDataOrderDetail({ expandedRowKeys: [], data: [] }) 
    }
  }

  const expandedRowOrder = () => {
    const thisOrder = (!_.isEmpty(dataOrderDetail.expandedRowKeys) && !_.isEmpty(dataOrderDetail.data))? dataOrder.data.data.find((x)=>x.ODID === dataOrderDetail.data.at(0).ODID) : []
    
    const columns = [
      { title: 'ID', dataIndex: '_ID', key: 'ID' },
      { title: 'PDName', dataIndex: 'PDName', key: 'PDName' },
      { title: 'PDPrice', dataIndex: 'PDPrice', key: 'PDPrice' },
      { title: 'ODSizeListDisplay', dataIndex: 'ODSizeListDisplay', key: 'ODSizeListDisplay', 
        render: (ODSizeListDisplay) => (
          <>
            {JSON.parse(ODSizeListDisplay).map((item, index)=>{
              return ( <Tag key={index} color="geekblue"> { item } </Tag> )
            })}
          </>
        ) 
      },
      { title: 'ODQty', align: 'center', dataIndex: 'ODQty', key: 'ODQty' },
      { title: 'ODTotalPrice', align: 'center', dataIndex: 'ODTotalPrice', key: 'ODTotalPrice' },
      { title: 'ODStatus', align: 'center', dataIndex: 'ODStatus', key: 'ODStatus', render: (ODStatus) => ( <Tag color={tagStatusOrderDetail[ODStatus]} style={{fontSize: 14}}> {ODStatus} </Tag> ), },
      { title: 'Ref.', dataIndex: 'Ref', key: 'Ref' },
      {
        title: t('common:table.action.txt_column'),
        key: 'Action',
        align: 'center',
        render: (text, record) => (
          <Space size="middle">
            <Button className="btn btn-info" onClick={()=> handleChangStatusOrder(true, record)}>{t('btnChangStatus')}</Button>
          </Space>
        ),
      }
    ];

    return <Table 
            size="small" 
            rowKey={record => record._ID}
            columns={columns} 
            dataSource={dataOrderDetail.data} 
            pagination={false} 
            // bordered
            summary={pageData => {
              let totalOrderPrice = 0;
              
              pageData.forEach(({ ODTotalPrice }) => {
                totalOrderPrice += ODTotalPrice;
              });
      
              return (
                <>
                  <Table.Summary.Row>
                  <Table.Summary.Cell colSpan={9} className="p-0"></Table.Summary.Cell>
                  </Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right">{t('customerOrder:order_sidebar.model_order_detail.order_subtotal')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1} className="text-center">
                      <Text type="danger">{totalOrderPrice}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3}></Table.Summary.Cell>
                  </Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right">{t('customerOrder:order_sidebar.model_order_detail.voucher')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1} className="text-center">
                      <Text type="danger">{thisOrder.VoucDiscount || 0}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3}></Table.Summary.Cell>
                  </Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right">{t('customerOrder:order_sidebar.model_order_detail.order_total')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1} className="text-center">
                      <Text type="danger">{totalOrderPrice - thisOrder.VoucDiscount || 0}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3}></Table.Summary.Cell>
                  </Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={5} className="text-right">{t('customerOrder:order_sidebar.model_order_detail.payment_method')}</Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={1} className="text-center">
                      <Text type="danger">{payName[thisOrder.PayID] || `-`}</Text>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell colSpan={3}></Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
          />;
  } 
  // * END props TABLE ORDER

  useEffect(()=>{
    queryDataOrder({ 
      filters: { 
        Status: ['openOrder', 'paid', 'unpaid', 'successful', 'failed', 'overdue'], 
        typePicker,
        datePicker: datePicker.format('YYYY-MM-DD') 
      },
      pagination: { current: 1, pageSize: 10, total: 10 },
    })
  },[])

  useEffect(()=>{
    try {
      if(_.isEmpty(socket)){
        let GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo).token
        if(!_.isEmpty(GetEmpInfo)){
          GetEmpInfo = jwt.verify(GetEmpInfo, process.env.JWT_SECRET_KEY);
        }

        const sk = io(process.env.API_URL)
        sk.emit('online', {
          CusID: GetEmpInfo._id,
          isLogin: true,
        })
        setSocket(sk)
        
      }
    } catch (error) {
      console.error(error)
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('panel_title')} 
              <DatePicker 
                className="ml-3" 
                allowClear={false}
                style={{ width: 200 }} 
                picker={typePicker}
                value={datePicker}
                format={formatPicker[typePicker]}
                onChange={onChangeDate} 
              />
              <Select className="ml-3" style={{ width: 125 }} value={typePicker} onChange={onChangeTypePicker}>
                <Option value="date">{t('typePicker.date')}</Option>
                <Option value="week">{t('typePicker.week')}</Option>
                <Option value="month">{t('typePicker.month')}</Option>
                <Option value="year">{t('typePicker.year')}</Option>
              </Select>
            </h4>
            <div className="panel-heading-btn">
              <Button onClick={() => handleExportReport()}>{t('exportData')}</Button>
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns} 
              rowKey={record => record.ODID}
              dataSource={dataOrder.data.data} 
              loading={dataOrder.loading} 
              pagination={{
                current: dataOrder.data.current,
                pageSize: dataOrder.data.pageSize,
                total: dataOrder.data.total
              }} 
              expandedRowKeys={dataOrderDetail.expandedRowKeys}
              expandable={{ expandedRowRender: expandedRowOrder }}
              onExpand={handleOnExpandOrder}
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'order', 'customerOrder', 'kitchen', 'common', 'employee']),
  },
})

export default orderManage