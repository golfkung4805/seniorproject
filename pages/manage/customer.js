import Link from 'next/link'
import Head from 'next/head'

import _ from 'lodash'
import Swal from 'sweetalert2'
import { ExcelRenderer } from "react-excel-renderer";

import { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { InboxOutlined, ExclamationCircleOutlined } from "@ant-design/icons"
import { Row, Col, Form, Button, Upload, message, Input, Select, Switch, Table, Space, Modal, notification } from 'antd'

import { 
  LOCAL_UPLOAD_RESET
} from '../../redux/constants/localConstants'

import { 
  CUSTOMER_IMPORT_RESET,
  CUSTOMER_CREATE_RESET,
  CUSTOMER_UPDATE_RESET,
  CUSTOMER_DELETE_RESET,
  CUSTOMER_CHANGE_RESET
} from '../../redux/constants/customerConstants'

import { 
  listCustomer,
  importCustomer,
  createCustomer,
  updateCustomer,
  changeStatusCustomer,
  deleteCustomer
} from '../../redux/actions/customerActions';

import { 
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal
} from '../../redux/actions/localActions';

import {
  getColumnSearchProps
} from '../../components/layout/manage/tableColumnSearchComponents'

import {
  checkColumnFileImport
} from '../../components/layout/manage/propsUploadComponents'

import Layout from '../../components/layout/manage/mainManage'

const CustomerManage = () => {

  const { t } = useTranslation('customer')

  const pageManage = {
    page_manage: t('page_manage')
  }

  const { Dragger } = Upload;

  const { Option } = Select;

  const [ form ] = Form.useForm();
  
  const dispatch = useDispatch();
  
  const localModal = useSelector((state) => state.localModal)
  const { isUpload, isUpdate, isVisible } = localModal

  const customerList = useSelector((state) => state.customerList)
  const { loading, data, pagination } = customerList

  const localUploadFile = useSelector((state) => state.localUploadFile)
  const { fileList } = localUploadFile
  
  const customerImport = useSelector((state) => state.customerImport)
  const { loading: loadingImport, success: successImport, customer: ImportdCustomer, error: errorImport } = customerImport
  
  const customerCreate = useSelector((state) => state.customerCreate)
  const { loading: loadingCreate, success: successCreate, customer: createdCustomer, error: errorCreate } = customerCreate

  const customerUpdate = useSelector((state) => state.customerUpdate)
  const { loading: loadingUpdate, success: successUpdate, customer: updatedCustomer, error: errorUpdate } = customerUpdate
  
  const customerChangeStatus = useSelector((state) => state.customerChangeStatus)
  const { loading: loadingChangeStatus, success: successChangeStatus, customer: changingStatusCustomer, error: errorChangeStatus } = customerChangeStatus

  const customerDelete = useSelector((state) => state.customerDelete)
  const { loading: loadingDelete, success: successDelete, customer: deletedCustomer, error: errorDelete } = customerDelete

  // * START props MODAL & FORM & UPLOAD CUSTOMER
  // ?  UPLOAD
  const beforeUploadImportFile = async (fileObj, fileListObj) => {
    const column = ['CusUsername',	'CusPasswd',	'CusGender',	'CusFirstname',	'CusLastname',	'CusNickname',	'CusTel',	'CusEmail']
    return await checkColumnFileImport(t, 1, column, fileList, fileObj, fileListObj)
  }

  const onChangeUpload = ({fileList}) => {
    dispatch(uploadFileLocal(fileList))
  }

  // ?  MODAL & FORM
  const handleModalOpenUpload = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(true, false)); 
  }

  const handleModalOpenCreate = () => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, false)); 
    form.resetFields();
  }

  const handleModalOpenUpdate = (CusData = {}, CusID = "") => {
    // TODO params modalOpenLocal(check upload, check editing)
    dispatch(modalOpenLocal(false, true))
    form.resetFields();
    form.setFieldsValue(CusData);
  }

  const handleOpenModalDelete = (CusID = "") => {
    Modal.confirm({
      title: t('common:modal.txt_delete', { page_manage: pageManage.page_manage, ID: CusID }),
      icon: <ExclamationCircleOutlined />,
      okType: 'danger',
      zIndex: 1050,
      onOk() {
        dispatch(deleteCustomer(CusID))
      },
    });
  }
  
  const handleModalBtnConfirm = () => {
    if(isUpload){
      // const formData = new FormData();
      // fileList.forEach(file => {
      //   formData.append('file', file);
      // });
      // dispatch(importCustomer(formData))
      ExcelRenderer(fileList[0].originFileObj, (err, resp) => {
        if (err) {
          console.error(err);
        } else {
          let CusData = [];
          resp.rows.slice(1).map((row, index) => {
            if (row && !_.isEmpty(row)) {
              CusData.push({
                Username: row[0],
                Passwd: row[1],
                Gender: row[2],
                Firstname: row[3],
                Lastname: row[4],
                Nickname: row[5],
                Tel: row[6],
                Email: row[7]
              });
            }
          });
          if (CusData.length === 0) {
            message.error(t('common:upload.no_data'))
            return false;
          } else {
            dispatch(importCustomer(CusData))
          }
        }
      });
    }else{
      form.submit();
    }
  };

  const handleModalBtnCancel = () => {
    if(isUpload) dispatch({ type: LOCAL_UPLOAD_RESET })
    dispatch(modalCloseLocal())
  };

  const onFormFinish = (values) => {
    if(isUpdate){
      dispatch(updateCustomer(values))
    }else{
      dispatch(createCustomer(values))
    }
  };

  const onFormFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
  // * END props MODAL & FORM & UPLOAD CUSTOMER

  // * START props TABLE CUSTOMER
  const handleTableChange = (pagination, filters, sorter) => {
    dispatch(listCustomer({pagination, filters, sorter}));
  }

  const handleTableChangeStatus = (CusID) => {
    dispatch(changeStatusCustomer(CusID))
  }

  const columns = [
    {
      title: t('table.id'),
      dataIndex: 'CusID',
      key: 'CusID',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
      // sorter: (a, b) => a.CusID.localeCompare(b.CusID),
      ...getColumnSearchProps(t, t('table.id')),
    },
    {
      title: t('table.username'),
      dataIndex: 'CusUsername',
      key: 'CusUsername',
      // sorter: true,
      ...getColumnSearchProps(t, t('table.username')),
    },
    {
      title: t('table.fullname'),
      dataIndex: 'CusName',
      key: 'CusName',
      // sorter: true,
      ...getColumnSearchProps(t, t('table.fullname')),
    },
    {
      title: t('table.nickname'),
      dataIndex: 'CusNickname',
      key: 'CusNickname',
      // sorter: true,
      ...getColumnSearchProps(t, t('table.nickname')),
    },
    {
      title: t('table.tell'),
      dataIndex: 'CusTel',
      key: 'CusTel',
      ...getColumnSearchProps(t, t('table.tell')),
    },
    {
      title: t('table.email'),
      dataIndex: 'CusEmail',
      key: 'CusEmail',
      // sorter: true,
      ...getColumnSearchProps(t, t('table.email')),
    },
    {
      title: t('table.point'),
      dataIndex: 'CusPoint',
      key: 'CusPoint',
      sorter: true,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: t('common:table.status.txt_column'),
      dataIndex: 'CusStatus',
      key: 'CusStatus',
      align: 'center',
      filters: [
        { text: t('common:table.status.approved'), value: '1' },
        { text: t('common:table.status.disapproved'), value: '0' },
      ],
      render: (text, record) => ( <Switch defaultChecked={record.CusStatus} disabled={loadingChangeStatus} onChange={()=> handleTableChangeStatus(record.CusID)} /> )
    },
    {
      title: t('common:table.action.txt_column'),
      key: 'Action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Button className="btn btn-warning" onClick={()=> handleModalOpenUpdate(record)}>{t('common:table.action.btn_edit')}</Button>
          <Button className="btn btn-danger" onClick={()=> handleOpenModalDelete(record.CusID)}>{t('common:table.action.btn_delete')}</Button>
        </Space>
      ),
    }
  ]
  // * END props TABLE CUSTOMER
  
  const SwalFire = (icon, title, text) => {
    if(icon === 'success') dispatch(modalCloseLocal())
    Swal.fire({
      icon,
      title,
      text: text.msg
    }).then((result) => {
      if (result.isConfirmed || result.isDismissed) {
        CUSTOMER_LIST()
      }
    })
  }

  const CUSTOMER_LIST = () => {
    dispatch(listCustomer())
  }

  useMemo(()=>{
    if (_.isEmpty(data)) {
      CUSTOMER_LIST()
    }
  },[])
  
  useEffect(()=>{
    if(successImport){
      SwalFire('success', t('common:alertSuccess'), ImportdCustomer)
      dispatch({ type: LOCAL_UPLOAD_RESET })
      dispatch({ type: CUSTOMER_IMPORT_RESET })
    }
    if(successCreate){
      SwalFire('success', t('common:alertSuccess'), createdCustomer)
      dispatch({ type: CUSTOMER_CREATE_RESET })
    }
    if(successUpdate){
      SwalFire('success', t('common:alertSuccess'), updatedCustomer)
      dispatch({ type: CUSTOMER_UPDATE_RESET })
    }
    if(successDelete){
      SwalFire('success', t('common:alertSuccess'), deletedCustomer)
      dispatch({ type: CUSTOMER_DELETE_RESET })
    }

    if(successImport === false){
      SwalFire('error', t('common:alertError'), errorImport)
      dispatch({ type: CUSTOMER_IMPORT_RESET })
    }
    if(successCreate === false){
      SwalFire('error', t('common:alertError'), errorCreate)
      dispatch({ type: CUSTOMER_CREATE_RESET })
    }
    if(successUpdate === false){
      SwalFire('error', t('common:alertError'), errorUpdate)
      dispatch({ type: CUSTOMER_UPDATE_RESET })
    }
    if(successDelete === false){
      SwalFire('error', t('common:alertError'), errorDelete)
      dispatch({ type: CUSTOMER_DELETE_RESET })
    }

    if(successChangeStatus){
      notification[(successChangeStatus)?'success':'error']({
        message: (successChangeStatus)? changingStatusCustomer.msg: errorChangeStatus.msg,
        // description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
      });
      dispatch({ type: CUSTOMER_CHANGE_RESET })
    }
  },[
    successImport,
    successCreate,
    successUpdate,
    successDelete,
    successChangeStatus
  ])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              <Button className="btn btn-primary btn-insert ml-3 mr-3" onClick={handleModalOpenCreate}>{t('common:modal.txt_insert', pageManage)}</Button>
              <Button className="btn btn-success btn-insert mr-3" onClick={handleModalOpenUpload}>{t('common:modal.txt_import', pageManage)}</Button>
              {/* <a href='#' className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a> */}
            </div>
          </div>
          <div className="panel-body">
            <Table 
              scroll={{ x: '100vh' }}
              columns={columns} 
              rowKey={record => record.CusID}
              dataSource={data} 
              loading={loading} 
              pagination={pagination} 
              onChange={handleTableChange} 
            />
          </div>
        </div>
      </div>
      <Modal
        title={isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
        visible={isVisible}
        onCancel={handleModalBtnCancel}
        className="modal-lg"
        maskStyle={{zIndex:1049}}
        zIndex={1050}
        width={800}
        footer={[
          <Button 
            key="submit" 
            className={`btn btn-${isUpload? "success": isUpdate? "warning": "primary"}`} 
            loading={ (loadingImport || loadingCreate || loadingUpdate)? true: false } 
            disabled={isUpload && fileList.length === 0}
            onClick={handleModalBtnConfirm}
          >
            {isUpload? t('common:modal.txt_import', pageManage): isUpdate? t('common:modal.txt_update', pageManage): t('common:modal.txt_insert', pageManage)}
          </Button>,
          <Button key="back" className="btn btn-white" onClick={handleModalBtnCancel}>{t('common:modal.btn_close')}</Button>
        ]}
      >
        {
          (isUpload)?
          <Dragger 
            listType="picture"
            beforeUpload={beforeUploadImportFile}
            onChange={onChangeUpload}
            fileList={fileList}
          >
            <p className="ant-upload-drag-icon"><InboxOutlined /></p>
            <p className="ant-upload-text">{t('common:modal.txt_upload')}</p>
            <p className="ant-upload-hint">{t('common:modal.txt_upload_desc')}</p>
          </Dragger>:
          <Form
            layout="vertical"
            form={form}
            onFinish={onFormFinish}
            onFinishFailed={onFormFinishFailed}
          >
            <Row gutter={24}>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.id')} 
                  name="CusID"
                >
                  <Input disabled={true}  />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.username')} 
                  name="CusUsername"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={6}>
                <Form.Item 
                  label={t('table.password')} 
                  name="CusPasswd"
                  // rules={[{ required: true, whitespace: true }]}
                >
                  <Input.Password />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={8}>
                <Form.Item 
                  label={t('table.email')} 
                  name="CusEmail"
                  rules={[{  type: 'email', required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={4}>
                <Form.Item 
                  label={t('table.gender')}  
                  name="CusGender"
                  rules={[{ type:'enum', required: true, whitespace: true, enum: ['นาย', 'นาง', 'นางสาว'] }]}
                >
                  <Select
                    showSearch
                    allowClear
                  >
                    <Option value="นาย">นาย</Option>
                    <Option value="นาง">นาง</Option>
                    <Option value="นางสาว">นางสาว</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.firstname')} 
                  name="CusFirstname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.lastname')} 
                  name="CusLastname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.nickname')} 
                  name="CusNickname"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} lg={24} xl={5}>
                <Form.Item 
                  label={t('table.tell')} 
                  name="CusTel"
                  rules={[{ required: true, whitespace: true }]}
                >
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        }
      </Modal>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'customer', 'common', 'employee']),
  },
})

export default CustomerManage