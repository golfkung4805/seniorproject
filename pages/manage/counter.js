import Link from 'next/link'
import Head from 'next/head'
import Script from 'next/script'

import qs from 'qs'
import _ from 'lodash'
import axios from 'axios'
import moment from 'moment';
import { io } from "socket.io-client"
import Timer from 'react-compound-timer'
import jwt from "jsonwebtoken";
import Swal from 'sweetalert2'

import { useState, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, i18n } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { ToCurrency } from '../../components/converterCurrency'

import { listTable } from '../../redux/actions/tableActions'

import Layout from '../../components/layout/manage/mainManage'

let allTableList = [], allOrderDetail = [], GetEmpInfo = {}

const counterManage = () => {

  // moment.locale(i18n.language==='en'? 'en-US': 'th-TH')

  const { t } = useTranslation('counter')

  const pageManage = {
    page_manage: t('page_manage')
  }
  
  const dispatch = useDispatch();

  const tableData = useSelector((state) => state.tableList)
  const { data: tableList } = tableData

  const [ socket, setSocket ] = useState({})
  const [ tableOrderList, setTableOrderList ] = useState([])
  const [ tableOrderDetailList, setTableOrderDetailList ] = useState([])

  const handleSelectTableOrder = async(tableOrderList) => {
    try {
      if(!_.isEmpty(tableOrderList.checkTableUse)){
        if(_.isEmpty(tableOrderDetailList) || tableOrderList.checkTableUse.ODID !== tableOrderDetailList.checkTableUse.ODID){
          const { data: orderDetail } = await axios.get(`${process.env.API_URL}/v1/order/detail/${tableOrderList.checkTableUse.ODID}`)
          allOrderDetail = {...tableOrderList, orderDetail: orderDetail}
          // console.log(allOrderDetail)
          setTableOrderDetailList(allOrderDetail)
          return
        }
      }
      setTableOrderDetailList([])
    } catch (error) {
      console.error(error)
    }
  }

  const paymentAlert = (thisOrder, icon, title, html, chiIcon, chiText, chiHtml) => {
    Swal.fire({
      title,
      html,
      icon,
      showCancelButton: true,
    }).then(async(result) => {
      if (result.isConfirmed) {
        try {
          const data = await axios.patch(`${process.env.API_URL}/v1/order/${thisOrder.checkTableUse.ODID}/confirmPayment`, {
            TableID: thisOrder.TableID,
            EmpID: GetEmpInfo._id,
            Detail: thisOrder.checkTableUse
          })
          if(data.status === 200){
            socket.emit('confirmPayment', {
              TableID: thisOrder.TableID,
              ODID: thisOrder.checkTableUse.ODID,
              Detail: thisOrder
            })

            
            allTableList = allTableList.map((x)=>x.TableID === thisOrder.TableID? {...x, checkTableUse: {}}: x)
            setTableOrderList(allTableList)
            setTableOrderDetailList([])
          }
          Swal.fire({
            title: chiText,
            html: chiHtml,
            icon: chiIcon
          })
        } catch (error) {
          console.error(error)
        }
      }
    })
  }

  const handleConfirmPayment = (thisOrder) => {
    const paymentMethod = {
      "credit_card": "Credit Card",
      "internet_banking": "Internet Banking",
      "rabbit_linepay": "Rabbit Linepay",
      "truemoney": "Truemoney"
    }
    paymentAlert(
      thisOrder,
      'warning', 
      t('paymentAlert'), 
      t('paymentAlert_1', { ODID: thisOrder.checkTableUse.ODID, ODTotal: ToCurrency(thisOrder.checkTableUse.ODTotal-thisOrder.checkTableUse.VoucDiscount), paymentMethod: paymentMethod[thisOrder.checkTableUse.PayID] }),
      'success', 
      t('warningPaymentAlert'), 
      t('paymentAlert_2', { ODID: thisOrder.checkTableUse.ODID, ODTotal: ToCurrency(thisOrder.checkTableUse.ODTotal-thisOrder.checkTableUse.VoucDiscount), paymentMethod: paymentMethod[thisOrder.checkTableUse.PayID] }),
    )
  }
  
  const handlePayByCash = (thisOrder) => {
    if(!_.isEmpty(thisOrder.orderDetail.filter((x)=>x.ODStatus==="waitQueue"))){
      return Swal.fire({
        icon: 'warning',
        title: t('common:alertWarning'),
        text: t('warningPayCash')
      })
    }
    paymentAlert(
      thisOrder,
      'warning', 
      t('payByCashAlert'), 
      t('payByCashAlert_1', { ODID: thisOrder.checkTableUse.ODID, ODTotal: ToCurrency(thisOrder.checkTableUse.ODTotal-thisOrder.checkTableUse.VoucDiscount) }),
      'success', 
      t('warningPaymentAlert'), 
      t('payByCashAlert_2', { ODID: thisOrder.checkTableUse.ODID, ODTotal: ToCurrency(thisOrder.checkTableUse.ODTotal-thisOrder.checkTableUse.VoucDiscount) }),
    )
  }

  useMemo(()=>{
    dispatch(listTable({ filters: { TableStatus: ['1'] } }))
  },[])

  useEffect(async()=>{
    if(!_.isEmpty(tableList)){
      try {
        const { data: orderData } = await axios.get(`${process.env.API_URL}/v1/order`, { params: {
          filters: { Status: ['openOrder', 'paid', 'unpaid'], Date: moment().format('YYYY-MM-DD') }, 
          pagination: { current: 1, pageSize: 'all', total: 10 }
        }, paramsSerializer: params => qs.stringify(params) });
        
        tableList.map((table, index)=>{
          const checkTableUse = orderData.data.find((x)=>x.TableID === table.TableID) || {}
          tableList[index] = {...table, checkTableUse}
        })
        allTableList = tableList
        setTableOrderList(allTableList)
        // console.log(tableList)
      } catch (error) {
        console.error(error)
      }
    }
  },[tableList])
  
  useEffect(()=>{
    try {
      if(_.isEmpty(socket)){
        GetEmpInfo = JSON.parse(JSON.parse(localStorage.getItem("persist:authEmp")).empInfo).token
        if(!_.isEmpty(GetEmpInfo)){
          GetEmpInfo = jwt.verify(GetEmpInfo, process.env.JWT_SECRET_KEY);
        }

        const sk = io(process.env.API_URL)
        sk.emit('online', {
          CusID: GetEmpInfo._id,
          isLogin: true,
        })
        setSocket(sk)
        sk.on('newOpenOrder', newOpenOrder => {
          // console.log(newOpenOrder)
          allTableList = allTableList.map((x)=>x.TableID === newOpenOrder.TableID? {...x, checkTableUse: newOpenOrder}: x)
          setTableOrderList(allTableList)
        })
        sk.on('newOrder', newOrders => {
          // console.log(newOrders)
          let changeDataTable = allTableList.find((x)=>x.checkTableUse.ODID === newOrders.at(0).ODID)
          changeDataTable.checkTableUse.ODTotal += _.sumBy(newOrders, 'ODTotalPrice')
          changeDataTable.checkTableUse.countOrder = changeDataTable.checkTableUse.countOrder || 0
          changeDataTable.checkTableUse.countOrder += newOrders.length
          allTableList = allTableList.map((x)=>x.checkTableUse.ODID === newOrders.at(0).ODID? changeDataTable: x)
          setTableOrderList(allTableList)
          
          if(!_.isEmpty(allOrderDetail) && allOrderDetail.orderDetail.at(0).ODID === newOrders.at(0).ODID){
            allOrderDetail = {...allOrderDetail, orderDetail: [...newOrders, ...allOrderDetail.orderDetail]}
            setTableOrderDetailList(allOrderDetail)
          }
        })
        sk.on('newOrderPayment', newPayment => {
          newPayment = _.isEmpty(newPayment.data.metadata)? newPayment.data: newPayment.data.metadata
          // console.log(newPayment)
          const ODStatus = newPayment.paymentMethod === 'checkout_counter'? 'unpaid': 'paid'
          allTableList = allTableList.map((x)=>x.checkTableUse.ODID===newPayment.ODID? {...x, checkTableUse:{...x.checkTableUse,
            PayID: newPayment.paymentMethod,
            ODPayDetail: newPayment.data || '{}',
            ODStatus,
            VoucDiscount: newPayment.VoucDiscount,
            updatedAt: moment().format()
          }}: x)
          
          setTableOrderList(allTableList)
          if(!_.isEmpty(allOrderDetail) && allOrderDetail.TableID === newPayment.TableID){
            allOrderDetail = { ...allOrderDetail, checkTableUse: {...allOrderDetail.checkTableUse, VoucDiscount: newPayment.VoucDiscount} }
            setTableOrderDetailList(allOrderDetail)
          }
        })
      }
    } catch (error) {
      console.error(error)
    }
    return ()=>{
      if(!_.isEmpty(socket)){
        socket.disconnect()
      }
    }
  }, [socket])

  return (
    <Layout>
      <Head>
        <title>{t('common:title', pageManage)}</title>
      </Head>
      <Script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        onLoad={() => {
          var handleSelectTable = function() {
            "use strict";

            $(document).on('click', '[data-toggle="select-table"]', function(e) {
                e.preventDefault();

                var targetTable = $(this).closest('.table');

                if ($(targetTable).hasClass('in-use')) {
                    $('[data-toggle="select-table"]').not(this).closest('.table').removeClass('selected');
                    $(targetTable).toggleClass('selected');
                    $('#pos-counter').toggleClass('pos-mobile-sidebar-toggled');
                }
            });
          };

          $(document).ready(function() {
            handleSelectTable();
          });
        }}
      />
      <div id="content" className="content">
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link href="/manage/">
              {t('common:breadcrumb.1', pageManage)}
            </Link>
          </li>
          <li className="breadcrumb-item active">{t('common:breadcrumb.active', pageManage)}</li>
        </ol>
        <h1 className="page-header">{t('common:breadcrumb.active', pageManage)}</h1>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">{t('common:panel_title', pageManage)}</h4>
            <div className="panel-heading-btn">
              <a href='#' onClick={e => e.preventDefault()} className="btn btn-xs btn-icon btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
              <a href='#' onClick={e => e.preventDefault()} className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-redo" /></a>
            </div>
          </div>
          <div className="panel-body">

            <div id="page-container" className="page-empty bg-white page-content-full-height mt-0 p-0">
              <div id="content" className="content m-0 p-0">
                <div className="pos pos-counter pt-0" id="pos-counter">
                  <div className="pos-counter-body">
                    <div className="pos-counter-content">
                      <div className="pos-counter-content-container" data-scrollbar="true" data-height="100%" data-skip-mobile="true">
                        <div className="table-row">
                          
                          {tableOrderList.map((table)=>{
                            // console.log('table=>',table)
                            const updatedAt = (table.checkTableUse.ODStatus === 'unpaid' || table.checkTableUse.ODStatus === 'paid')? moment(table.checkTableUse.updatedAt): moment()
                            const createdAt = updatedAt.diff(moment(table.checkTableUse.createdAt), 'minutes', true)
                            const stopTime = moment.duration(createdAt, 'minutes')
                            const ODTotal = ToCurrency(table.checkTableUse.ODTotal-table.checkTableUse.VoucDiscount) || '-'
                            const countOrder = table.checkTableUse.countOrder || t('txtEmpty')

                            return(
                              <div key={table.TableID} className={`table ${_.isEmpty(table.checkTableUse)?'available' :'in-use'}`} onClick={()=> handleSelectTableOrder(table)} style={{cursor: 'pointer'}}>
                                <div className="table-container" data-toggle="select-table">
                                  <div className="table-status" />
                                  <div className="table-name">
                                    <div className="name">{table.TableID}</div>
                                    <div className="no">{table.TableName}</div>
                                    <div className="order"><span>{countOrder + (_.isNumber(countOrder)?` ${t('txtOrder')}`:'')}</span></div>
                                  </div>
                                  <div className="table-info-row">
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="far fa-user" />
                                        </span>
                                        <span className="text">0 / 6</span>
                                      </div>
                                    </div>
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="far fa-clock" />
                                        </span>
                                        <span className="text">
                                          {!_.isEmpty(table.checkTableUse) && !_.isEmpty(table.checkTableUse.PayID)?
                                          `${("0" + stopTime.hours()).slice(-2)}:${("0" + stopTime.minutes()).slice(-2)}:${("0" + stopTime.seconds()).slice(-2)}` :
                                          !_.isEmpty(table.checkTableUse)?
                                          <Timer initialTime={createdAt * 60000}>
                                            <Timer.Hours formatValue={value => `${(value > 10) ? `${value}:`: (value > 0)? `0${value}:`:''}`} />
                                            <Timer.Minutes formatValue={value => `${(value < 10 ? `0${value}` : value)}:`} />
                                            <Timer.Seconds formatValue={value => `${(value < 10 ? `0${value}` : value)}`} />
                                          </Timer>: '-' }
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="table-info-row">
                                    <div className="table-info-col">
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className="fa fa-hand-point-up" />
                                        </span>
                                        <span className="text">{ODTotal}</span>
                                      </div>
                                    </div>
                                    <div className={`table-info-col ${!_.isEmpty(table.checkTableUse.PayID) && 'text-yellow'}`}>
                                      <div className="table-info-container">
                                        <span className="icon">
                                          <i className={`fa ${!_.isEmpty(table.checkTableUse.PayID) && table.checkTableUse.ODStatus !== 'unpaid'? 'fa-check-circle' : 'fa-dollar-sign'}`} />
                                        </span>
                                        {/* <span className="text">-</span> */}
                                        <span className="text">{!_.isEmpty(table.checkTableUse.PayID) && table.checkTableUse.ODStatus !== 'unpaid'? t('customerOrder:order_sidebar.order_history_paid') : t('customerOrder:order_sidebar.order_history_unpaid')}</span>
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
                            )
                          })}

                        </div>
                      </div>
                    </div>
                    <div className="pos-counter-sidebar" id="pos-counter-sidebar">
                      <div className="pos-sidebar-header">
                        <div className="back-btn">
                          <button type="button" data-dismiss-class="pos-mobile-sidebar-toggled" data-target="#pos-counter" className="btn" onClick={()=> handleSelectTableOrder(allOrderDetail)}>
                            <svg viewBox="0 0 16 16" className="bi bi-chevron-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fillRule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            </svg>
                          </button>
                        </div>
                        <div className="icon"><img src="../assets/img/pos/icon-table.svg" /></div>
                        <div className="title">{_.isEmpty(tableOrderDetailList)? t('txtTable'): tableOrderDetailList.TableID}</div>
                        <div className="order"><b>#{_.isEmpty(tableOrderDetailList)? t('customerOrder:order_sidebar.lastOrderStatus'): tableOrderDetailList.checkTableUse.ODID}</b></div>
                      </div>
                      <div className="pos-sidebar-body">
                        {!_.isEmpty(tableOrderDetailList)?
                          <div className="pos-table" data-id="pos-table-info">
                            {tableOrderDetailList.orderDetail.map((item, index)=>{
                              return(
                                <div className="row pos-table-row" key={item._ID}>
                                  <div className="col-6">
                                    <div className="pos-product-thumb">
                                      <div className="img" style={{backgroundImage: `url(${process.env.API_URL}/v1/product/image/${item.PDImage})`}} />
                                      <div className="info">
                                        <div className="title">{item.PDName}</div>
                                        <div className="desc">
                                          {JSON.parse(item.ODSizeListDisplay).map((option, index) => {
                                            return (
                                              <div key={index}>{option}<br/></div>
                                            )
                                          })}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  {/* <div className="col-2 total-price text-left">{ToCurrency(item.PDPrice)}</div> */}
                                  <div className="col-2 total-qty text-center d-flex align-items-center justify-content-center">x{item.ODQty}</div>
                                  <div className="col-2 total-price d-flex align-items-center justify-content-center">{ToCurrency(item.ODTotalPrice)}.-</div>
                                  <div className="col-2 total-price d-flex align-items-center justify-content-center">{item.ODStatus === 'complete'? t('customerOrder:order_sidebar.table_order_detail.status.completed') :item.ODStatus === 'cancel'? t('customerOrder:order_sidebar.table_order_detail.status.cancelled') : t('customerOrder:order_sidebar.table_order_detail.status.waitQueue')}</div>
                                </div>
                              )
                            })}
                          </div> 
                          :<div className="h-100 d-flex align-items-center justify-content-center text-center p-20" data-id="pos-table-empty">
                            <div>
                              <div className="mb-3">
                                <svg width="6em" height="6em" viewBox="0 0 16 16" className="text-gray-300" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fillRule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z" />
                                  <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z" />
                                </svg>
                              </div>
                              <h4>{t('notSelected')}</h4>
                            </div>
                          </div>
                        }
                      </div>

                      <div className="pos-sidebar-footer">
                        {/* <div className="subtotal">
                          <div className="text">Subtotal</div>
                          <div className="price" data-id="price-subtotal">
                          {!_.isEmpty(tableOrderDetailList)? ToCurrency(tableOrderDetailList.checkTableUse.ODTotal) : 0.00} .-
                          </div>
                        </div> */}
                        {/* <div className="taxes">
                          <div className="text">Taxes (6%)</div>
                          <div className="price" data-id="price-subtotal">3.90.-</div>
                        </div> */}
                        <div className="taxes">
                          <div className="text">{t('customerOrder:order_sidebar.model_order_detail.voucher')}</div>
                          <div className="price" data-id="price-subtotal">{!_.isEmpty(tableOrderDetailList)? ToCurrency(tableOrderDetailList.checkTableUse.VoucDiscount) : ToCurrency(0.00)} ฿</div>
                        </div>
                        <div className="total">
                          <div className="text">{t('customerOrder:order_sidebar.order_total')}</div>
                          <div className="price" data-id="price-subtotal">
                            {!_.isEmpty(tableOrderDetailList)? ToCurrency((tableOrderDetailList.checkTableUse.ODTotal-tableOrderDetailList.checkTableUse.VoucDiscount) > 0? tableOrderDetailList.checkTableUse.ODTotal-tableOrderDetailList.checkTableUse.VoucDiscount: 0) : ToCurrency(0.00)} ฿
                          </div>
                        </div>
                        <div className="btn-row">
                          {/* <a href='#' onClick={e => e.preventDefault()} className="btn btn-default width-150"><i className="fa fa-qrcode fa-fw fa-lg" /> Digital Wallet</a> */}
                          <button onClick={()=> handleConfirmPayment(tableOrderDetailList)} disabled={_.isEmpty(tableOrderDetailList) || (!_.isEmpty(tableOrderDetailList) && tableOrderDetailList.checkTableUse.ODStatus !== 'paid')} className="btn btn-default width-350"><i className="fab fa-cc-visa fa-fw fa-lg" /> {t('btnConfirm')}</button>
                          <button onClick={()=> handlePayByCash(tableOrderDetailList)} disabled={_.isEmpty(tableOrderDetailList) || (!_.isEmpty(tableOrderDetailList) && tableOrderDetailList.checkTableUse.ODStatus === 'paid')} className="btn btn-success"><i className="fa fa-cash-register fa-fw fa-lg" /> {t('btnPayCash')}</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['header', 'sidebar', 'counter', 'common', 'customerOrder', 'employee']),
  },
})

export default counterManage