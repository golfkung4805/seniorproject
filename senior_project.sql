-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2021 at 08:01 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `senior_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `CID` varchar(7) NOT NULL,
  `CName` varchar(50) NOT NULL,
  `CStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_category_ingredients`
--

CREATE TABLE `tb_category_ingredients` (
  `CIID` varchar(8) NOT NULL,
  `CID` varchar(7) NOT NULL,
  `CIName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_category_ingredients_option`
--

CREATE TABLE `tb_category_ingredients_option` (
  `CIOID` varchar(9) NOT NULL,
  `CIID` varchar(8) NOT NULL,
  `CIOName` varchar(50) NOT NULL,
  `CIOPrice` float(13,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `CusID` varchar(9) NOT NULL,
  `CusUsername` varchar(100) NOT NULL,
  `CusPasswd` varchar(100) NOT NULL,
  `CusGender` varchar(50) NOT NULL,
  `CusFirstname` varchar(100) NOT NULL,
  `CusLastname` varchar(100) NOT NULL,
  `CusNickname` varchar(50) NOT NULL,
  `CusTel` varchar(20) NOT NULL,
  `CusEmail` varchar(100) NOT NULL,
  `CusPoint` float(13,2) NOT NULL DEFAULT 0.00,
  `CusStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employees`
--

CREATE TABLE `tb_employees` (
  `EmpID` varchar(9) NOT NULL,
  `EmpUsername` varchar(100) NOT NULL,
  `EmpPasswd` varchar(100) NOT NULL,
  `EmpGender` varchar(50) NOT NULL,
  `EmpFirstname` varchar(100) NOT NULL,
  `EmpLastname` varchar(100) NOT NULL,
  `EmpNickname` varchar(50) NOT NULL,
  `EmpTel` varchar(20) NOT NULL,
  `EmpEmail` varchar(100) NOT NULL,
  `EmpIsAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `EmpStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `ODID` varchar(100) NOT NULL,
  `TableID` varchar(12) NOT NULL,
  `CusID` varchar(255) NOT NULL,
  `EmpID` varchar(9) DEFAULT NULL,
  `VoucCode` varchar(255) DEFAULT NULL,
  `PayID` varchar(25) DEFAULT NULL,
  `ODPayDetail` text NOT NULL COMMENT 'Object from Webhooks',
  `ODStatus` enum('openOrder','successful','failed','overdue','') NOT NULL DEFAULT 'openOrder',
  `ODTotal` float(13,2) NOT NULL DEFAULT 0.00,
  `ODTax` float(13,2) NOT NULL,
  `VoucDiscount` float(13,2) NOT NULL DEFAULT 0.00,
  `ODTotalPoint` float(13,2) NOT NULL DEFAULT 0.00,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `_ID` int(11) NOT NULL,
  `ODID` varchar(100) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `PDName` varchar(50) NOT NULL,
  `ODSizeList` text NOT NULL COMMENT '[พิเศษ,หมู,ไข่ดาว]',
  `ODSizeListDisplay` text NOT NULL,
  `ODPriceList` varchar(255) NOT NULL COMMENT '[5,5,7]',
  `PDPrice` float(13,2) NOT NULL,
  `ODQty` int(11) NOT NULL,
  `ODTotalPrice` float(13,2) NOT NULL COMMENT '(PDPrice + ODPriceList)*ODQty',
  `ODTotalTax` float(13,2) NOT NULL,
  `ODDesc` text NOT NULL,
  `ODStatus` decimal(1,0) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp(),
  `Ref` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_method`
--

CREATE TABLE `tb_payment_method` (
  `PayID` varchar(25) NOT NULL,
  `PayName` varchar(50) NOT NULL,
  `PayStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `PDID` varchar(8) NOT NULL,
  `PDName` varchar(50) NOT NULL,
  `PDDesc` text NOT NULL,
  `PDPrice` float(13,2) NOT NULL,
  `PDRating` decimal(3,2) NOT NULL DEFAULT 0.00,
  `PDStatus` tinyint(1) NOT NULL DEFAULT 1,
  `CID` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_image`
--

CREATE TABLE `tb_product_image` (
  `_ID` int(7) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `PDImage` varchar(100) NOT NULL,
  `PDImageIndex` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_price`
--

CREATE TABLE `tb_product_price` (
  `_ID` int(7) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `PDSize` varchar(100) NOT NULL,
  `PDPrice` float NOT NULL,
  `PDQty` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_rating`
--

CREATE TABLE `tb_product_rating` (
  `_ID` int(11) NOT NULL,
  `PDID` varchar(8) NOT NULL,
  `RevRating` decimal(3,2) NOT NULL,
  `RevComment` text NOT NULL,
  `CusID` varchar(8) NOT NULL,
  `ODD_ID` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sequence`
--

CREATE TABLE `tb_sequence` (
  `_ID` int(7) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `LastID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sysconfig`
--

CREATE TABLE `tb_sysconfig` (
  `_ID` int(11) NOT NULL,
  `SysID` varchar(9) NOT NULL,
  `SysName` varchar(100) NOT NULL,
  `SysLogo` varchar(100) NOT NULL,
  `SysOpen` time NOT NULL,
  `SysClose` time NOT NULL,
  `SysPointType` varchar(50) NOT NULL,
  `SysPointCondition` varchar(100) NOT NULL COMMENT '100/1 ทุกๆ100บาทจะได้รับ1Point',
  `SysKitchenOpen` time NOT NULL,
  `SysKitchenClose` time NOT NULL,
  `SysStatus` decimal(1,0) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_table`
--

CREATE TABLE `tb_table` (
  `TableID` varchar(12) NOT NULL,
  `TableName` varchar(50) NOT NULL,
  `TableQR` varchar(100) NOT NULL,
  `TableStatus` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_voucher`
--

CREATE TABLE `tb_voucher` (
  `VoucID` varchar(7) NOT NULL,
  `VoucCode` varchar(255) NOT NULL,
  `VoucName` text NOT NULL,
  `VoucUsePoint` float(13,2) NOT NULL DEFAULT 0.00,
  `VoucDiscount` float(13,2) NOT NULL,
  `VoucType` varchar(11) NOT NULL,
  `VoucCondition` float(13,2) NOT NULL DEFAULT 0.00,
  `VoucStart` datetime NOT NULL,
  `VoucEnd` datetime NOT NULL,
  `VoucQuota` int(11) NOT NULL,
  `VoucUse` int(11) NOT NULL DEFAULT 0,
  `VoucStatus` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `tb_category_ingredients`
--
ALTER TABLE `tb_category_ingredients`
  ADD PRIMARY KEY (`CIID`),
  ADD KEY `TbCategoryIngredients_CID_FK` (`CID`);

--
-- Indexes for table `tb_category_ingredients_option`
--
ALTER TABLE `tb_category_ingredients_option`
  ADD PRIMARY KEY (`CIOID`),
  ADD KEY `TbCategoryIngredientsDetail_CIID_FK` (`CIID`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`CusID`),
  ADD UNIQUE KEY `EmpUsername` (`CusUsername`);

--
-- Indexes for table `tb_employees`
--
ALTER TABLE `tb_employees`
  ADD PRIMARY KEY (`EmpID`),
  ADD UNIQUE KEY `EmpUsername` (`EmpUsername`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`ODID`),
  ADD KEY `TbOder_TableID_FK` (`TableID`),
  ADD KEY `TbOder_EmpID_FK` (`EmpID`),
  ADD KEY `TbOder_PayID_FK` (`PayID`),
  ADD KEY `TbOder_VoucCode_FK` (`VoucCode`);

--
-- Indexes for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbOderDetail_ODID_FK` (`ODID`),
  ADD KEY `TbOderDetail_PDID_FK` (`PDID`);

--
-- Indexes for table `tb_payment_method`
--
ALTER TABLE `tb_payment_method`
  ADD PRIMARY KEY (`PayID`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`PDID`),
  ADD KEY `TbProduct_CID_FK` (`CID`);

--
-- Indexes for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbProductImage_PDID_FK` (`PDID`);

--
-- Indexes for table `tb_product_price`
--
ALTER TABLE `tb_product_price`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbProductPrice_PDID_FK` (`PDID`);

--
-- Indexes for table `tb_product_rating`
--
ALTER TABLE `tb_product_rating`
  ADD PRIMARY KEY (`_ID`),
  ADD KEY `TbProductRating_PDID_FK` (`PDID`),
  ADD KEY `TbProductRating_CusID_FK` (`CusID`),
  ADD KEY `TbProductRating_ODD_ID_FK` (`ODD_ID`);

--
-- Indexes for table `tb_sequence`
--
ALTER TABLE `tb_sequence`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `tb_sysconfig`
--
ALTER TABLE `tb_sysconfig`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `tb_table`
--
ALTER TABLE `tb_table`
  ADD PRIMARY KEY (`TableID`);

--
-- Indexes for table `tb_voucher`
--
ALTER TABLE `tb_voucher`
  ADD PRIMARY KEY (`VoucID`),
  ADD UNIQUE KEY `VoucCode` (`VoucCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  MODIFY `_ID` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_product_price`
--
ALTER TABLE `tb_product_price`
  MODIFY `_ID` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_product_rating`
--
ALTER TABLE `tb_product_rating`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_sequence`
--
ALTER TABLE `tb_sequence`
  MODIFY `_ID` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_sysconfig`
--
ALTER TABLE `tb_sysconfig`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_category_ingredients`
--
ALTER TABLE `tb_category_ingredients`
  ADD CONSTRAINT `TbCategoryIngredients_CID_FK` FOREIGN KEY (`CID`) REFERENCES `tb_category` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_category_ingredients_option`
--
ALTER TABLE `tb_category_ingredients_option`
  ADD CONSTRAINT `TbCategoryIngredientsDetail_CIID_FK` FOREIGN KEY (`CIID`) REFERENCES `tb_category_ingredients` (`CIID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD CONSTRAINT `TbOder_EmpID_FK` FOREIGN KEY (`EmpID`) REFERENCES `tb_employees` (`EmpID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_PayID_FK` FOREIGN KEY (`PayID`) REFERENCES `tb_payment_method` (`PayID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_TableID_FK` FOREIGN KEY (`TableID`) REFERENCES `tb_table` (`TableID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbOder_VoucCode_FK` FOREIGN KEY (`VoucCode`) REFERENCES `tb_voucher` (`VoucCode`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD CONSTRAINT `TbOderDetail_ODID_FK` FOREIGN KEY (`ODID`) REFERENCES `tb_order` (`ODID`),
  ADD CONSTRAINT `TbOderDetail_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD CONSTRAINT `TbProduct_CID_FK` FOREIGN KEY (`CID`) REFERENCES `tb_category` (`CID`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD CONSTRAINT `TbProductImage_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_product_price`
--
ALTER TABLE `tb_product_price`
  ADD CONSTRAINT `TbProductPrice_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_product_rating`
--
ALTER TABLE `tb_product_rating`
  ADD CONSTRAINT `TbProductRating_CusID_FK` FOREIGN KEY (`CusID`) REFERENCES `tb_customer` (`CusID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TbProductRating_ODD_ID_FK` FOREIGN KEY (`ODD_ID`) REFERENCES `tb_order_detail` (`_ID`),
  ADD CONSTRAINT `TbProductRating_PDID_FK` FOREIGN KEY (`PDID`) REFERENCES `tb_product` (`PDID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
