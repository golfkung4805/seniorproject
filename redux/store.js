import { useMemo } from 'react'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
// import storage  from 'redux-persist/lib/storage'
import createWebStorage  from 'redux-persist/lib/storage/createWebStorage'


import { cartReducers, modalProductDetailReducers } from './reducers/cartReducers'

import { 
  localSocketReducer,
  localModalReducer,  
  localSelectManageReducer,
  localUploadFileReducer
} from './reducers/localReducers'

import { 
  customerSignInReducer,
  customerListReducer, 
  customerOrderListReducer,
  customerImportReducer,
  customerCreateReducer, 
  customerUpdateReducer, 
  customerChangeStatusReducer,
  customerDeleteReducer 
} from './reducers/customerReducers'

import { 
  employeeListReducer,
  employeeSignInReducer, 
  employeeImportReducer,
  employeeCreateReducer, 
  employeeUpdateReducer, 
  employeeChangeStatusReducer,
  employeeDeleteReducer 
} from './reducers/employeeReducers'

import { 
  tableListReducer, 
  tableImportReducer,
  tableCreateReducer, 
  tableUpdateReducer, 
  tableChangeStatusReducer,
  tableDeleteReducer 
} from './reducers/tableReducers'

import { 
  paymentMethodListReducer,
  paymentMethodChangeStatusReducer,
} from './reducers/paymentMethodReducers'

import { 
  categoryListReducer, 
  categoryImportReducer,
  categoryCreateReducer, 
  categoryUpdateReducer, 
  categoryChangeStatusReducer,
  categoryDeleteReducer,
  categoryIngredientDeleteReducer,
  categoryOptionCreateReducer,
  categoryOptionUpdateReducer,
  categoryOptionDeleteReducer
} from './reducers/categoryReducers'

import { 
  productListReducer, 
  productImportReducer,
  productCreateReducer, 
  productUpdateReducer, 
  productChangeStatusReducer,
  productDeleteReducer ,
  productDeleteImageReducer
} from './reducers/productReducers'

import { 
  voucherListReducer, 
  voucherImportReducer,
  voucherCreateReducer, 
  voucherUpdateReducer, 
  voucherChangeStatusReducer,
  voucherDeleteReducer 
} from './reducers/voucherReducers'

import { orderCreateReducer } from './reducers/orderReducers'

let store

const createNoopStorage = () => {
  return {
    getItem(_key) {
      return Promise.resolve(null);
    },
    setItem(_key, value) {
      return Promise.resolve(value);
    },
    removeItem(_key) {
      return Promise.resolve();
    },
  };
};

const storage = typeof window !== "undefined" ? createWebStorage("local") : createNoopStorage();

const composeEnhancer = typeof window != 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const exampleInitialState = {
  cart: { cartItems: [] },
}

const persistConfig = {
  key: 'cart',
  storage,
  whitelist: ['cart'], // place to select which state you want to persist
}

const authCusPersistConfig = {
  key: 'authCus',
  storage,
  blacklist: ['customerSignIn'], 
}

const authEmpPersistConfig = {
  key: 'authEmp',
  storage,
  blacklist: ['employeeSignIn'], 
}

const reducer = combineReducers({
  // TODO Cart Frontend 
  cart: cartReducers,
  modalProductDetail: modalProductDetailReducers,
  // TODO local
  localSocket: localSocketReducer,
  localModal: localModalReducer,
  localSelectManage: localSelectManageReducer,
  localUploadFile: localUploadFileReducer,
  // TODO customer
  customerList: customerListReducer,
  customerOrderList: customerOrderListReducer,
  customerSignIn: persistReducer(authCusPersistConfig, customerSignInReducer),
  customerImport: customerImportReducer,
  customerCreate: customerCreateReducer,
  customerUpdate: customerUpdateReducer,
  customerChangeStatus: customerChangeStatusReducer,
  customerDelete: customerDeleteReducer,
  // TODO employee
  employeeSignIn: persistReducer(authEmpPersistConfig, employeeSignInReducer),
  employeeList: employeeListReducer,
  employeeImport: employeeImportReducer,
  employeeCreate: employeeCreateReducer,
  employeeUpdate: employeeUpdateReducer,
  employeeChangeStatus: employeeChangeStatusReducer,
  employeeDelete: employeeDeleteReducer,
  // TODO table
  tableList: tableListReducer,
  tableImport: tableImportReducer,
  tableCreate: tableCreateReducer,
  tableUpdate: tableUpdateReducer,
  tableChangeStatus: tableChangeStatusReducer,
  tableDelete: tableDeleteReducer,
  // TODO payment method
  paymentMethodList: paymentMethodListReducer,
  paymentMethodChangeStatus: paymentMethodChangeStatusReducer,
  // TODO category
  categoryList: categoryListReducer,
  categoryImport: categoryImportReducer,
  categoryCreate: categoryCreateReducer,
  categoryUpdate: categoryUpdateReducer,
  categoryChangeStatus: categoryChangeStatusReducer,
  categoryDelete: categoryDeleteReducer,
  categoryIngredientDelete: categoryIngredientDeleteReducer,
  categoryOptionCreate: categoryOptionCreateReducer,
  categoryOptionUpdate: categoryOptionUpdateReducer,
  categoryOptionDelete: categoryOptionDeleteReducer,
  // TODO product
  productList: productListReducer,
  productImport: productImportReducer,
  productCreate: productCreateReducer,
  productUpdate: productUpdateReducer,
  productChangeStatus: productChangeStatusReducer,
  productDelete: productDeleteReducer,
  productDeleteImage: productDeleteImageReducer,
  // TODO voucher
  voucherList: voucherListReducer,
  voucherImport: voucherImportReducer,
  voucherCreate: voucherCreateReducer,
  voucherUpdate: voucherUpdateReducer,
  voucherChangeStatus: voucherChangeStatusReducer,
  voucherDelete: voucherDeleteReducer,
  // TODO order
  orderCreate: orderCreateReducer
})

const persistedReducer = persistReducer(persistConfig, reducer)

function makeStore(initialState = exampleInitialState) {
  return createStore(
    persistedReducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk))
  )
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? makeStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}