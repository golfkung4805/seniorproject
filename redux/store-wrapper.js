// import localStorage  from'localStorage'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { createWrapper } from 'next-redux-wrapper'

import { cartReducers, modalProductDetailReducers } from './reducers/cartReducers'

import { 
  localSocketReducer,
  localModalReducer,  
  localSelectManageReducer,
  localUploadFileReducer
} from './reducers/localReducers'

import { 
  customerSignInReducer,
  customerListReducer, 
  customerOrderListReducer,
  customerImportReducer,
  customerCreateReducer, 
  customerUpdateReducer, 
  customerChangeStatusReducer,
  customerDeleteReducer 
} from './reducers/customerReducers'

import { 
  employeeListReducer,
  employeeSignInReducer, 
  employeeImportReducer,
  employeeCreateReducer, 
  employeeUpdateReducer, 
  employeeChangeStatusReducer,
  employeeDeleteReducer 
} from './reducers/employeeReducers'

import { 
  tableListReducer, 
  tableImportReducer,
  tableCreateReducer, 
  tableUpdateReducer, 
  tableChangeStatusReducer,
  tableDeleteReducer 
} from './reducers/tableReducers'

import { 
  paymentMethodListReducer,
  paymentMethodChangeStatusReducer,
} from './reducers/paymentMethodReducers'

import { 
  categoryListReducer, 
  categoryImportReducer,
  categoryCreateReducer, 
  categoryUpdateReducer, 
  categoryChangeStatusReducer,
  categoryDeleteReducer,
  categoryIngredientDeleteReducer,
  categoryOptionCreateReducer,
  categoryOptionUpdateReducer,
  categoryOptionDeleteReducer
} from './reducers/categoryReducers'

import { 
  productListReducer, 
  productImportReducer,
  productCreateReducer, 
  productUpdateReducer, 
  productChangeStatusReducer,
  productDeleteReducer ,
  productDeleteImageReducer
} from './reducers/productReducers'

import { 
  voucherListReducer, 
  voucherImportReducer,
  voucherCreateReducer, 
  voucherUpdateReducer, 
  voucherChangeStatusReducer,
  voucherDeleteReducer 
} from './reducers/voucherReducers'

import { orderCreateReducer } from './reducers/orderReducers'

const composeEnhancer = typeof window != 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const initialState = {
  cart: {
    cartItems: [],
  },
}

const reducer = combineReducers({
  // TODO Cart Frontend 
  cart: cartReducers,
  modalProductDetail: modalProductDetailReducers,
  // TODO local
  localSocket: localSocketReducer,
  localModal: localModalReducer,
  localSelectManage: localSelectManageReducer,
  localUploadFile: localUploadFileReducer,
  // TODO customer
  customerList: customerListReducer,
  customerOrderList: customerOrderListReducer,
  customerSignIn: customerSignInReducer,
  customerImport: customerImportReducer,
  customerCreate: customerCreateReducer,
  customerUpdate: customerUpdateReducer,
  customerChangeStatus: customerChangeStatusReducer,
  customerDelete: customerDeleteReducer,
  // TODO employee
  employeeSignIn: employeeSignInReducer,
  employeeList: employeeListReducer,
  employeeImport: employeeImportReducer,
  employeeCreate: employeeCreateReducer,
  employeeUpdate: employeeUpdateReducer,
  employeeChangeStatus: employeeChangeStatusReducer,
  employeeDelete: employeeDeleteReducer,
  // TODO table
  tableList: tableListReducer,
  tableImport: tableImportReducer,
  tableCreate: tableCreateReducer,
  tableUpdate: tableUpdateReducer,
  tableChangeStatus: tableChangeStatusReducer,
  tableDelete: tableDeleteReducer,
  // TODO payment method
  paymentMethodList: paymentMethodListReducer,
  paymentMethodChangeStatus: paymentMethodChangeStatusReducer,
  // TODO category
  categoryList: categoryListReducer,
  categoryImport: categoryImportReducer,
  categoryCreate: categoryCreateReducer,
  categoryUpdate: categoryUpdateReducer,
  categoryChangeStatus: categoryChangeStatusReducer,
  categoryDelete: categoryDeleteReducer,
  categoryIngredientDelete: categoryIngredientDeleteReducer,
  categoryOptionCreate: categoryOptionCreateReducer,
  categoryOptionUpdate: categoryOptionUpdateReducer,
  categoryOptionDelete: categoryOptionDeleteReducer,
  // TODO product
  productList: productListReducer,
  productImport: productImportReducer,
  productCreate: productCreateReducer,
  productUpdate: productUpdateReducer,
  productChangeStatus: productChangeStatusReducer,
  productDelete: productDeleteReducer,
  productDeleteImage: productDeleteImageReducer,
  // TODO voucher
  voucherList: voucherListReducer,
  voucherImport: voucherImportReducer,
  voucherCreate: voucherCreateReducer,
  voucherUpdate: voucherUpdateReducer,
  voucherChangeStatus: voucherChangeStatusReducer,
  voucherDelete: voucherDeleteReducer,
  // TODO order
  orderCreate: orderCreateReducer
})

const store = () => createStore(
  reducer, 
  initialState, 
  composeEnhancer(applyMiddleware(thunk))
)

export const wrapper = createWrapper(store)