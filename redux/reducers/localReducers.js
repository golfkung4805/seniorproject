import { 
  LOCAL_SOCKET,

  LOCAL_MODAL_CREATE,
  LOCAL_MODAL_UPDATE,
  LOCAL_MODAL_UPLOAD_FILE,
  LOCAL_MODAL_CLOSE,

  LOCAL_SELECT_MANAGE,

  LOCAL_UPLOAD_FILE,
  LOCAL_UPLOAD_DRAG_FILE,
  LOCAL_UPLOAD_RESET
} from '../constants/localConstants'

import update from 'immutability-helper';

const localSocketReducer = (state = {}, action) => {
  switch (action.type) {
    case LOCAL_SOCKET:
      return { isSocket: action.payload }
    default:
      return state
  }
}

const localModalReducer = (state = {}, action) => {
  switch (action.type) {
    case LOCAL_MODAL_CREATE:
      return { isUpload: false, isUpdate: false, isVisible: true }
    case LOCAL_MODAL_UPDATE:
      return { isUpload: false, isUpdate: true, isVisible: true }
    case LOCAL_MODAL_UPLOAD_FILE:
      return { isUpload: true, isUpdate: false, isVisible: true }
    case LOCAL_MODAL_CLOSE:
      return { isVisible: false }
    default:
      return state
  }
}

const localSelectManageReducer = (state = {}, action) => {
  switch (action.type) {
    case LOCAL_SELECT_MANAGE:
      return { isSelectManage: action.payload }
    default:
      return state
  }
}

const localUploadFileReducer = (state = { fileList: [] }, action) => {
  switch (action.type) {
    // case LOCAL_UPLOAD_INSERT:
    //   const newFile = action.payload
    //   const existFile = state.fileList.find((x) => x.name === newFile.name)
    //   if(existFile){
    //     return { fileList: state.fileList.map((x)=> x.name === existFile.name? newFile: x) }
    //   }else{
    //     return { fileList: [...state.fileList, newFile] }
    //   }
    // case LOCAL_UPLOAD_REMOVE:
    //   return { fileList: state.fileList.filter((x)=> x.uid !== action.payload.uid)  }
    case LOCAL_UPLOAD_FILE:
      return { fileList: action.payload }
    case LOCAL_UPLOAD_DRAG_FILE:
      return { fileList: update(state.fileList, {
        $splice: [
          [action.payload.dragIndex, 1], [action.payload.hoverIndex, 0, action.payload.dragRow],
        ],
      }) }
    case LOCAL_UPLOAD_RESET:
      return { fileList: [] }
    default:
      return state
  }
}

export {
  localSocketReducer,
  localModalReducer,
  localSelectManageReducer,
  localUploadFileReducer
}