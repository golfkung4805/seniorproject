import {
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_LIST_FAIL,

  ORDER_CREATE_REQUEST,
  ORDER_CREATE_SUCCESS,
  ORDER_CREATE_FAIL,
  ORDER_CREATE_RESET
} from '../../redux/constants/orderConstants'

const orderCreateReducer = (state = { loading: false }, action) => {
  switch (action.type) {
    case ORDER_CREATE_REQUEST:
      return { loading: true }
    case ORDER_CREATE_SUCCESS:
      return { loading: false, success: true, order: action.payload }
    case ORDER_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case ORDER_CREATE_RESET:
      return { loading: false }
    default:
      return state
  }
}

export {
  orderCreateReducer
}