import { 
  CUSTOMER_ANONYMOUS,
  CUSTOMER_UPDATE_PROFILE_SUCCESS,
  CUSTOMER_SIGNIN_REQUEST,
  CUSTOMER_SIGNIN_SUCCESS,
  CUSTOMER_SIGNIN_FAIL,
  CUSTOMER_SIGNIN_RESET,
  CUSTOMER_SIGNOUT,
  
  CUSTOMER_LIST_REQUEST,
  CUSTOMER_LIST_SUCCESS,
  CUSTOMER_LIST_FAIL,

  CUSTOMER_LIST_ORDER_REQUEST,
  CUSTOMER_LIST_ORDER_SUCCESS,
  CUSTOMER_LIST_ORDER_FAIL,

  CUSTOMER_IMPORT_REQUEST,
  CUSTOMER_IMPORT_SUCCESS,
  CUSTOMER_IMPORT_FAIL,
  CUSTOMER_IMPORT_RESET,

  CUSTOMER_CREATE_REQUEST,
  CUSTOMER_CREATE_SUCCESS,
  CUSTOMER_CREATE_FAIL,
  CUSTOMER_CREATE_RESET,
  
  CUSTOMER_UPDATE_REQUEST,
  CUSTOMER_UPDATE_SUCCESS,
  CUSTOMER_UPDATE_FAIL,
  CUSTOMER_UPDATE_RESET,

  CUSTOMER_CHANGE_REQUEST,
  CUSTOMER_CHANGE_SUCCESS,
  CUSTOMER_CHANGE_FAIL,
  CUSTOMER_CHANGE_RESET,
  
  CUSTOMER_DELETE_REQUEST,
  CUSTOMER_DELETE_SUCCESS,
  CUSTOMER_DELETE_FAIL,
  CUSTOMER_DELETE_RESET
} from '../constants/customerConstants'

const customerSignInReducer = (state = { cusInfo: {}}, action) => {
  switch (action.type) {
    case CUSTOMER_ANONYMOUS:
      return { cusInfo: {...action.payload, isLogin: false}, loading: false, }
    case CUSTOMER_SIGNIN_REQUEST:
      return { ...state, loading: true }
    case CUSTOMER_SIGNIN_SUCCESS:
      return { ...state, loading: false, cusInfo: {...action.payload, isLogin: true}, success: true }
    case CUSTOMER_UPDATE_PROFILE_SUCCESS:
      return { ...state, cusInfo: {...action.payload, isLogin: true} }
    case CUSTOMER_SIGNIN_FAIL:
      return { ...state, loading: false, error: action.payload, success: false }
    case CUSTOMER_SIGNIN_RESET:
      return { cusInfo: {...state.cusInfo} }
    case CUSTOMER_SIGNOUT:
      return {  }
    default:
      return state
  }
}

const customerListReducer = (state = { data: [] },action) => {
  switch (action.type) {
    case CUSTOMER_LIST_REQUEST:
      return { ...state, loading: true }
    case CUSTOMER_LIST_SUCCESS:
      return { 
        data: action.payload.data, 
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total, 
        }, 
        loading: false 
    }
    case CUSTOMER_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const customerOrderListReducer = (state = { data: [] },action) => {
  switch (action.type) {
    case CUSTOMER_LIST_ORDER_REQUEST:
      return { ...state, loading: true }
    case CUSTOMER_LIST_ORDER_SUCCESS:
      return { 
        data: action.payload.data, 
        loading: false 
    }
    case CUSTOMER_LIST_ORDER_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const customerImportReducer = (state = {}, action) => {
  switch (action.type) {
    case CUSTOMER_IMPORT_REQUEST:
      return { loading: true }
    case CUSTOMER_IMPORT_SUCCESS:
      return { loading: false, success: true, customer: action.payload }
    case CUSTOMER_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CUSTOMER_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const customerCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case CUSTOMER_CREATE_REQUEST:
      return { loading: true }
    case CUSTOMER_CREATE_SUCCESS:
      return { loading: false, success: true, customer: action.payload }
    case CUSTOMER_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CUSTOMER_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const customerUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case CUSTOMER_UPDATE_REQUEST:
      return { loading: true }
    case CUSTOMER_UPDATE_SUCCESS:
      return { loading: false, success: true, customer: action.payload }
    case CUSTOMER_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CUSTOMER_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const customerChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case CUSTOMER_CHANGE_REQUEST:
      return { loading: true, success: false }
    case CUSTOMER_CHANGE_SUCCESS:
      return { loading: false, success: true, customer: action.payload }
    case CUSTOMER_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CUSTOMER_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const customerDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case CUSTOMER_DELETE_REQUEST:
      return { loading: true }
    case CUSTOMER_DELETE_SUCCESS:
      return { loading: false, success: true, customer: action.payload }
    case CUSTOMER_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CUSTOMER_DELETE_RESET:
      return { }
    default:
      return state
  }
}

export {
  customerSignInReducer,
  customerListReducer,
  customerOrderListReducer,
  customerImportReducer,
  customerCreateReducer,
  customerUpdateReducer,
  customerChangeStatusReducer,
  customerDeleteReducer
}