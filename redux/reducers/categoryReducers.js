import { 
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAIL,

  CATEGORY_IMPORT_REQUEST,
  CATEGORY_IMPORT_SUCCESS,
  CATEGORY_IMPORT_FAIL,
  CATEGORY_IMPORT_RESET,

  CATEGORY_CREATE_REQUEST,
  CATEGORY_CREATE_SUCCESS,
  CATEGORY_CREATE_FAIL,
  CATEGORY_CREATE_RESET,
  
  CATEGORY_UPDATE_REQUEST,
  CATEGORY_UPDATE_SUCCESS,
  CATEGORY_UPDATE_FAIL,
  CATEGORY_UPDATE_RESET,

  CATEGORY_CHANGE_REQUEST,
  CATEGORY_CHANGE_SUCCESS,
  CATEGORY_CHANGE_FAIL,
  CATEGORY_CHANGE_RESET,
  
  CATEGORY_DELETE_REQUEST,
  CATEGORY_DELETE_SUCCESS,
  CATEGORY_DELETE_FAIL,
  CATEGORY_DELETE_RESET,
  
  INGREDIENT_DELETE_REQUEST,
  INGREDIENT_DELETE_SUCCESS,
  INGREDIENT_DELETE_FAIL,
  INGREDIENT_DELETE_RESET,

  OPTION_CREATE_REQUEST,
  OPTION_CREATE_SUCCESS,
  OPTION_CREATE_FAIL,
  OPTION_CREATE_RESET,

  OPTION_UPDATE_REQUEST,
  OPTION_UPDATE_SUCCESS,
  OPTION_UPDATE_FAIL,
  OPTION_UPDATE_RESET,
  
  OPTION_DELETE_REQUEST,
  OPTION_DELETE_SUCCESS,
  OPTION_DELETE_FAIL,
  OPTION_DELETE_RESET,
} from '../constants/categoryConstants'

const categoryListReducer = (state = { data: [] }, action) => {
  switch (action.type) {
    case CATEGORY_LIST_REQUEST:
      return { ...state, loading: true }
    case CATEGORY_LIST_SUCCESS:
      return { 
        data: action.payload.data, 
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total, 
        }, 
        loading: false 
    }
    case CATEGORY_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const categoryImportReducer = (state = {}, action) => {
  switch (action.type) {
    case CATEGORY_IMPORT_REQUEST:
      return { loading: true }
    case CATEGORY_IMPORT_SUCCESS:
      return { loading: false, success: true, category: action.payload }
    case CATEGORY_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CATEGORY_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const categoryCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case CATEGORY_CREATE_REQUEST:
      return { loading: true }
    case CATEGORY_CREATE_SUCCESS:
      return { loading: false, success: true, category: action.payload }
    case CATEGORY_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CATEGORY_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const categoryUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case CATEGORY_UPDATE_REQUEST:
      return { loading: true }
    case CATEGORY_UPDATE_SUCCESS:
      return { loading: false, success: true, category: action.payload }
    case CATEGORY_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CATEGORY_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const categoryChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case CATEGORY_CHANGE_REQUEST:
      return { loading: true, success: false }
    case CATEGORY_CHANGE_SUCCESS:
      return { loading: false, success: true, category: action.payload }
    case CATEGORY_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CATEGORY_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const categoryDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case CATEGORY_DELETE_REQUEST:
      return { loading: true }
    case CATEGORY_DELETE_SUCCESS:
      return { loading: false, success: true, category: action.payload }
    case CATEGORY_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case CATEGORY_DELETE_RESET:
      return { }
    default:
      return state
  }
}

const categoryIngredientDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case INGREDIENT_DELETE_REQUEST:
      return { loading: true }
    case INGREDIENT_DELETE_SUCCESS:
      return { loading: false, success: true, ingredient: action.payload }
    case INGREDIENT_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case INGREDIENT_DELETE_RESET:
      return { }
    default:
      return state
  }
}

const categoryOptionCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case OPTION_CREATE_REQUEST:
      return { loading: true }
    case OPTION_CREATE_SUCCESS:
      return { loading: false, success: true, option: action.payload }
    case OPTION_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case OPTION_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const categoryOptionUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case OPTION_UPDATE_REQUEST:
      return { loading: true }
    case OPTION_UPDATE_SUCCESS:
      return { loading: false, success: true, option: action.payload }
    case OPTION_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case OPTION_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const categoryOptionDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case OPTION_DELETE_REQUEST:
      return { loading: true }
    case OPTION_DELETE_SUCCESS:
      return { loading: false, success: true, option: action.payload }
    case OPTION_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case OPTION_DELETE_RESET:
      return { }
    default:
      return state
  }
}

export {
  categoryListReducer,
  categoryImportReducer,
  categoryCreateReducer,
  categoryUpdateReducer,
  categoryChangeStatusReducer,
  categoryDeleteReducer,
  categoryIngredientDeleteReducer,
  categoryOptionCreateReducer,
  categoryOptionUpdateReducer,
  categoryOptionDeleteReducer
}