import {
  VOUCHER_LIST_REQUEST,
  VOUCHER_LIST_SUCCESS,
  VOUCHER_LIST_FAIL,

  VOUCHER_IMPORT_REQUEST,
  VOUCHER_IMPORT_SUCCESS,
  VOUCHER_IMPORT_FAIL,
  VOUCHER_IMPORT_RESET,
  
  VOUCHER_CREATE_REQUEST,
  VOUCHER_CREATE_SUCCESS,
  VOUCHER_CREATE_FAIL,
  VOUCHER_CREATE_RESET,

  VOUCHER_UPDATE_REQUEST,
  VOUCHER_UPDATE_SUCCESS,
  VOUCHER_UPDATE_FAIL,
  VOUCHER_UPDATE_RESET,

  VOUCHER_CHANGE_REQUEST,
  VOUCHER_CHANGE_SUCCESS,
  VOUCHER_CHANGE_FAIL,
  VOUCHER_CHANGE_RESET,

  VOUCHER_DELETE_REQUEST,
  VOUCHER_DELETE_SUCCESS,
  VOUCHER_DELETE_FAIL,
  VOUCHER_DELETE_RESET
} from '../constants/voucherConstants'


const voucherListReducer = (state = { data: [], pagination: { current: 1, pageSize: 10, total: 10 } }, action) => {
  switch (action.type) {
    case VOUCHER_LIST_REQUEST:
      return { ...state, loading: true }
    case VOUCHER_LIST_SUCCESS:
      return { 
        data: action.payload.data,
        pagination: {
          current: action.payload.current || 0,
          pageSize: action.payload.pageSize || 0,
          total: action.payload.total || 0
        },
        loading: false
      }
    case VOUCHER_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const voucherImportReducer = (state = {}, action) => {
  switch (action.type) {
    case VOUCHER_IMPORT_REQUEST:
      return { loading: true }
    case VOUCHER_IMPORT_SUCCESS:
      return { loading: false, success: true, voucher: action.payload }
    case VOUCHER_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case VOUCHER_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const voucherCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case VOUCHER_CREATE_REQUEST:
      return { loading: true }
    case VOUCHER_CREATE_SUCCESS:
      return { loading: false, success: true, voucher: action.payload }
    case VOUCHER_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case VOUCHER_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const voucherUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case VOUCHER_UPDATE_REQUEST:
      return { loading: true }
    case VOUCHER_UPDATE_SUCCESS:
      return { loading: false, success: true, voucher: action.payload }
    case VOUCHER_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case VOUCHER_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const voucherChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case VOUCHER_CHANGE_REQUEST:
      return { loading: true, success: false }
    case VOUCHER_CHANGE_SUCCESS:
      return { loading: false, success: true, voucher: action.payload }
    case VOUCHER_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case VOUCHER_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const voucherDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case VOUCHER_DELETE_REQUEST:
      return { loading: true }
    case VOUCHER_DELETE_SUCCESS:
      return { loading: false, success: true, voucher: action.payload }
    case VOUCHER_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case VOUCHER_DELETE_RESET:
      return { }
    default:
      return state
  }
}

export {
  voucherListReducer,
  voucherImportReducer,
  voucherCreateReducer,
  voucherUpdateReducer,
  voucherChangeStatusReducer,
  voucherDeleteReducer
}