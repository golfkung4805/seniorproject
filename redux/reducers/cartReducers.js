import {
  CART_ADD_ITEM,
  CART_MINUS_ITEM,
  CART_PLUS_ITEM,
  CART_REMOVE_ITEM,
  CART_CANCEL_REMOVE_ITEM,
  CART_ADD_FAIL,
  CART_EMPTY,

  CART_PRODUCT_DETAIL,
  CART_PRODUCT_DETAIL_FAIL,
  CART_PRODUCT_DETAIL_RESET
} from '../constants/cartConstants'

const cartReducers = (state = { cartItems: [] }, action) => {
  switch (action.type) {
    case CART_ADD_ITEM:
      const item = action.payload
      const existItem = state.cartItems.find((x) => x.PDID === item.PDID)
      if(existItem){
        return {
          ...state,
          error: '',
          cartItems: state.cartItems.map((x)=> x.PDID === existItem.PDID? item: x)
        }
      }else{
        return { ...state, error: '', cartItems: [...state.cartItems, item] }
      }
    case CART_MINUS_ITEM:
      const existItemMinus = state.cartItems.find((x) => x.PDID === action.payload)
      if(existItemMinus){
        if(existItemMinus.orderQty>1){
          existItemMinus.orderQty-=1
        }else{
          existItemMinus.checkRemove = true
        }
        return {
          ...state,
          error: '',
          cartItems: state.cartItems.map((x)=> x.PDID === existItemMinus.PDID? existItemMinus: x)
        }
      }else{
        return state
      }
    case CART_PLUS_ITEM:
      const existItemPlus = state.cartItems.find((x) => x.PDID === action.payload)
      if(existItemPlus){
        if(existItemPlus.orderQty<20){
          existItemPlus.orderQty+=1
        }else{
          existItemPlus.orderQty = 20
        }
        return {
          ...state,
          error: '',
          cartItems: state.cartItems.map((x)=> x.PDID === existItemPlus.PDID? existItemPlus: x)
        }
      }else{
        return state
      }
    case CART_REMOVE_ITEM:
      return { ...state, error: '', cartItems: state.cartItems.filter((x)=> x.PDID !== action.payload)}
    case CART_CANCEL_REMOVE_ITEM:
      const existItemCancel = state.cartItems.find((x) => x.PDID === action.payload)
      if(existItemCancel){
        existItemCancel.checkRemove = false
        return { ...state, error: '', cartItems: state.cartItems.map((x)=> x.PDID === existItemCancel.PDID? existItemCancel: x) }
      }else{
        return state
      }
    case CART_ADD_FAIL:
      return { ...state, error: action.payload }
    case CART_EMPTY:
      return { error: '', cartItems: [] }
    default:
      return state
  }
}

const modalProductDetailReducers = (state = { productDetail: {} }, action) => {
  switch (action.type) {
    case CART_PRODUCT_DETAIL:
      return { ...state, productDetail: action.payload }
    case CART_PRODUCT_DETAIL_RESET:
      return { ...state, productDetail: {} }
    case CART_PRODUCT_DETAIL_FAIL:
      return { ...state, productDetail: {}, error: action.payload }
    default:
      return state
  }
}

export {
  cartReducers,
  modalProductDetailReducers
}