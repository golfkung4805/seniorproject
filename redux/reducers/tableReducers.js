import { 
  TABLE_LIST_REQUEST,
  TABLE_LIST_SUCCESS,
  TABLE_LIST_FAIL,

  TABLE_IMPORT_REQUEST,
  TABLE_IMPORT_SUCCESS,
  TABLE_IMPORT_FAIL,
  TABLE_IMPORT_RESET,

  TABLE_CREATE_REQUEST,
  TABLE_CREATE_SUCCESS,
  TABLE_CREATE_FAIL,
  TABLE_CREATE_RESET,
  
  TABLE_UPDATE_REQUEST,
  TABLE_UPDATE_SUCCESS,
  TABLE_UPDATE_FAIL,
  TABLE_UPDATE_RESET,

  TABLE_CHANGE_REQUEST,
  TABLE_CHANGE_SUCCESS,
  TABLE_CHANGE_FAIL,
  TABLE_CHANGE_RESET,
  
  TABLE_DELETE_REQUEST,
  TABLE_DELETE_SUCCESS,
  TABLE_DELETE_FAIL,
  TABLE_DELETE_RESET
} from '../constants/tableConstants'

const tableListReducer = (state = { data: [] }, action) => {
  switch (action.type) {
    case TABLE_LIST_REQUEST:
      return { ...state, loading: true }
    case TABLE_LIST_SUCCESS:
      return { 
        data: action.payload.data, 
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total, 
        }, 
        loading: false 
    }
    case TABLE_LIST_FAIL:
      return { loading: false, data:[], error: action.payload }
    default:
      return state
  }
}

const tableImportReducer = (state = {}, action) => {
  switch (action.type) {
    case TABLE_IMPORT_REQUEST:
      return { loading: true }
    case TABLE_IMPORT_SUCCESS:
      return { loading: false, success: true, table: action.payload }
    case TABLE_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TABLE_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const tableCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case TABLE_CREATE_REQUEST:
      return { loading: true }
    case TABLE_CREATE_SUCCESS:
      return { loading: false, success: true, table: action.payload }
    case TABLE_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TABLE_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const tableUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case TABLE_UPDATE_REQUEST:
      return { loading: true }
    case TABLE_UPDATE_SUCCESS:
      return { loading: false, success: true, table: action.payload }
    case TABLE_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TABLE_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const tableChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case TABLE_CHANGE_REQUEST:
      return { loading: true, success: false }
    case TABLE_CHANGE_SUCCESS:
      return { loading: false, success: true, table: action.payload }
    case TABLE_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TABLE_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const tableDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case TABLE_DELETE_REQUEST:
      return { loading: true }
    case TABLE_DELETE_SUCCESS:
      return { loading: false, success: true, table: action.payload }
    case TABLE_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case TABLE_DELETE_RESET:
      return { }
    default:
      return state
  }
}

export {
  tableListReducer,
  tableImportReducer,
  tableCreateReducer,
  tableUpdateReducer,
  tableChangeStatusReducer,
  tableDeleteReducer
}