import { 
  PAYMENT_METHOD_LIST_REQUEST,
  PAYMENT_METHOD_LIST_SUCCESS,
  PAYMENT_METHOD_LIST_FAIL,

  PAYMENT_METHOD_CHANGE_REQUEST,
  PAYMENT_METHOD_CHANGE_SUCCESS,
  PAYMENT_METHOD_CHANGE_FAIL,
  PAYMENT_METHOD_CHANGE_RESET,
} from '../constants/paymentMethodConstants'

const paymentMethodListReducer = (state = { data: [] }, action) => {
  switch (action.type) {
    case PAYMENT_METHOD_LIST_REQUEST:
      return { ...state, loading: true }
    case PAYMENT_METHOD_LIST_SUCCESS:
      return { 
        data: action.payload.data, 
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total, 
        }, 
        loading: false 
    }
    case PAYMENT_METHOD_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const paymentMethodChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case PAYMENT_METHOD_CHANGE_REQUEST:
      return { loading: true, success: false }
    case PAYMENT_METHOD_CHANGE_SUCCESS:
      return { loading: false, success: true, paymentMethod: action.payload }
    case PAYMENT_METHOD_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PAYMENT_METHOD_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

export {
  paymentMethodListReducer,
  paymentMethodChangeStatusReducer,
}