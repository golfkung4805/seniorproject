import { 
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAIL,

  PRODUCT_IMPORT_REQUEST,
  PRODUCT_IMPORT_SUCCESS,
  PRODUCT_IMPORT_FAIL,
  PRODUCT_IMPORT_RESET,

  PRODUCT_CREATE_REQUEST,
  PRODUCT_CREATE_SUCCESS,
  PRODUCT_CREATE_FAIL,
  PRODUCT_CREATE_RESET,
  
  PRODUCT_UPDATE_REQUEST,
  PRODUCT_UPDATE_SUCCESS,
  PRODUCT_UPDATE_FAIL,
  PRODUCT_UPDATE_RESET,

  PRODUCT_CHANGE_REQUEST,
  PRODUCT_CHANGE_SUCCESS,
  PRODUCT_CHANGE_FAIL,
  PRODUCT_CHANGE_RESET,
  
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAIL,
  PRODUCT_DELETE_RESET,

  PRODUCT_DELETE_IMAGE_REQUEST,
  PRODUCT_DELETE_IMAGE_SUCCESS,
  PRODUCT_DELETE_IMAGE_FAIL,
  PRODUCT_DELETE_IMAGE_RESET
} from '../constants/productConstants'

const productListReducer = (state = { data: [] },action) => {
  switch (action.type) {
    case PRODUCT_LIST_REQUEST:
      return { ...state, loading: true }
    case PRODUCT_LIST_SUCCESS:
      return { 
        data: action.payload.data, 
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total, 
        }, 
        loading: false 
    }
    case PRODUCT_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const productImportReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_IMPORT_REQUEST:
      return { loading: true }
    case PRODUCT_IMPORT_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const productCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_CREATE_REQUEST:
      return { loading: true }
    case PRODUCT_CREATE_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const productUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_UPDATE_REQUEST:
      return { loading: true }
    case PRODUCT_UPDATE_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const productChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_CHANGE_REQUEST:
      return { loading: true, success: false }
    case PRODUCT_CHANGE_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const productDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_DELETE_REQUEST:
      return { loading: true }
    case PRODUCT_DELETE_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_DELETE_RESET:
      return { }
    default:
      return state
  }
}

const productDeleteImageReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_DELETE_IMAGE_REQUEST:
      return { loading: true }
    case PRODUCT_DELETE_IMAGE_SUCCESS:
      return { loading: false, success: true, product: action.payload }
    case PRODUCT_DELETE_IMAGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case PRODUCT_DELETE_IMAGE_RESET:
      return { }
    default:
      return state
  }
}

export {
  productListReducer,
  productImportReducer,
  productCreateReducer,
  productUpdateReducer,
  productChangeStatusReducer,
  productDeleteReducer,
  productDeleteImageReducer
}