import {
  EMPLOYEE_SIGNIN_REQUEST,
  EMPLOYEE_SIGNIN_SUCCESS,
  EMPLOYEE_SIGNIN_FAIL,
  EMPLOYEE_SIGNIN_RESET,
  EMPLOYEE_SIGNOUT,

  EMPLOYEE_LIST_REQUEST,
  EMPLOYEE_LIST_SUCCESS,
  EMPLOYEE_LIST_FAIL,

  EMPLOYEE_IMPORT_REQUEST,
  EMPLOYEE_IMPORT_SUCCESS,
  EMPLOYEE_IMPORT_FAIL,
  EMPLOYEE_IMPORT_RESET,
  
  EMPLOYEE_CREATE_REQUEST,
  EMPLOYEE_CREATE_SUCCESS,
  EMPLOYEE_CREATE_FAIL,
  EMPLOYEE_CREATE_RESET,

  EMPLOYEE_UPDATE_REQUEST,
  EMPLOYEE_UPDATE_SUCCESS,
  EMPLOYEE_UPDATE_FAIL,
  EMPLOYEE_UPDATE_RESET,

  EMPLOYEE_CHANGE_REQUEST,
  EMPLOYEE_CHANGE_SUCCESS,
  EMPLOYEE_CHANGE_FAIL,
  EMPLOYEE_CHANGE_RESET,

  EMPLOYEE_DELETE_REQUEST,
  EMPLOYEE_DELETE_SUCCESS,
  EMPLOYEE_DELETE_FAIL,
  EMPLOYEE_DELETE_RESET
} from '../constants/employeeConstants'

const employeeSignInReducer = (state = { empInfo: { } }, action) => {
  switch (action.type) {
    case EMPLOYEE_SIGNIN_REQUEST:
      return { ...state, loading: true, error: '' }
    case EMPLOYEE_SIGNIN_SUCCESS:
      return { ...state, loading: false, empInfo: {...action.payload, isLogin: true}, success: true }
    case EMPLOYEE_SIGNIN_FAIL:
      return { ...state, loading: false, empInfo: { },  error: action.payload, success: false }
    case EMPLOYEE_SIGNIN_RESET:
      return { empInfo: {...state.empInfo} }
    case EMPLOYEE_SIGNOUT:
      return { empInfo: { } }
    default:
      return state
  }
}

const employeeListReducer = (state = { data: [] }, action) => {
  switch (action.type) {
    case EMPLOYEE_LIST_REQUEST:
      return { ...state, loading: true }
    case EMPLOYEE_LIST_SUCCESS:
      return { 
        data: action.payload.data,
        pagination: {
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total
        },
        loading: false
      }
    case EMPLOYEE_LIST_FAIL:
      return { loading: false, data: [], error: action.payload }
    default:
      return state
  }
}

const employeeImportReducer = (state = {}, action) => {
  switch (action.type) {
    case EMPLOYEE_IMPORT_REQUEST:
      return { loading: true }
    case EMPLOYEE_IMPORT_SUCCESS:
      return { loading: false, success: true, employee: action.payload }
    case EMPLOYEE_IMPORT_FAIL:
      return { loading: false, success: false, error: action.payload }
    case EMPLOYEE_IMPORT_RESET:
      return { }
    default:
      return state
  }
}

const employeeCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case EMPLOYEE_CREATE_REQUEST:
      return { loading: true }
    case EMPLOYEE_CREATE_SUCCESS:
      return { loading: false, success: true, employee: action.payload }
    case EMPLOYEE_CREATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case EMPLOYEE_CREATE_RESET:
      return { }
    default:
      return state
  }
}

const employeeUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case EMPLOYEE_UPDATE_REQUEST:
      return { loading: true }
    case EMPLOYEE_UPDATE_SUCCESS:
      return { loading: false, success: true, employee: action.payload }
    case EMPLOYEE_UPDATE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case EMPLOYEE_UPDATE_RESET:
      return { }
    default:
      return state
  }
}

const employeeChangeStatusReducer = (state = {}, action) => {
  switch (action.type) {
    case EMPLOYEE_CHANGE_REQUEST:
      return { loading: true, success: false }
    case EMPLOYEE_CHANGE_SUCCESS:
      return { loading: false, success: true, employee: action.payload }
    case EMPLOYEE_CHANGE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case EMPLOYEE_CHANGE_RESET:
      return { }
    default:
      return state
  }
}

const employeeDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case EMPLOYEE_DELETE_REQUEST:
      return { loading: true }
    case EMPLOYEE_DELETE_SUCCESS:
      return { loading: false, success: true, employee: action.payload }
    case EMPLOYEE_DELETE_FAIL:
      return { loading: false, success: false, error: action.payload }
    case EMPLOYEE_DELETE_RESET:
      return { }
    default:
      return state
  }
}

export {
  employeeSignInReducer,
  employeeListReducer,
  employeeImportReducer,
  employeeCreateReducer,
  employeeUpdateReducer,
  employeeChangeStatusReducer,
  employeeDeleteReducer
}