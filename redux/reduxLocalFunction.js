const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };

const getError = (error) => {
  try {
    let getError = ''  
    if(error.response.data.errors){
      getError = error.response.data.errors[0].msg
    }else{
      getError = error.response.data.msg
    }
    return {msg: getError}
  } catch (error) {
    return {msg: error.message}
  }
}

export {
  config,
  getError
}