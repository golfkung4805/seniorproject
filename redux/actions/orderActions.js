import axios from 'axios'
import _ from 'lodash'

import {
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_LIST_FAIL,

  ORDER_CREATE_REQUEST,
  ORDER_CREATE_SUCCESS,
  ORDER_CREATE_FAIL
} from '../../redux/constants/orderConstants'

import { getError } from '../reduxLocalFunction'

const createOrders = (order) => async (dispatch, getState) => {
  // use PDID-> get price
  // use optionPrice -> empty
  // use objectOptions->get priceOption push to optionPrice
  // use orderQty
  dispatch({type: ORDER_CREATE_REQUEST})
  try {
    const { customerSignIn: { cusInfo } } = getState()
    let ODID = (!_.isEmpty(order.lastOrder))? order.lastOrder.ODID: null
    if(_.isEmpty(order.lastOrder)){
      const resCreateOrder = await axios.post(`${process.env.API_URL}/v1/order/`, [{
        CusID: cusInfo.CusID,
        TableID: order.TableID
      }])
      ODID = resCreateOrder.data.ODID
      if(resCreateOrder.data.status === 401){
        dispatch({type: ORDER_CREATE_FAIL, payload: resCreateOrder.data.msg })
        return
      }
    }
    const { data } = await axios.post(`${process.env.API_URL}/v1/order/${ODID}`, [{
      cartItems: order.cartItems
    }])
    dispatch({type: ORDER_CREATE_SUCCESS, payload: data})
  } catch (error) {
    console.log(getError(error))
    dispatch({type: ORDER_CREATE_FAIL, payload: getError(error)})
  }
}

export {
  createOrders
}