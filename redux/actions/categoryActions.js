import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'

import { 
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAIL,

  CATEGORY_IMPORT_REQUEST,
  CATEGORY_IMPORT_SUCCESS,
  CATEGORY_IMPORT_FAIL,

  CATEGORY_CREATE_REQUEST,
  CATEGORY_CREATE_SUCCESS,
  CATEGORY_CREATE_FAIL,

  CATEGORY_UPDATE_REQUEST,
  CATEGORY_UPDATE_SUCCESS,
  CATEGORY_UPDATE_FAIL,

  CATEGORY_CHANGE_REQUEST,
  CATEGORY_CHANGE_SUCCESS,
  CATEGORY_CHANGE_FAIL,

  CATEGORY_DELETE_REQUEST,
  CATEGORY_DELETE_SUCCESS,
  CATEGORY_DELETE_FAIL,

  INGREDIENT_DELETE_REQUEST,
  INGREDIENT_DELETE_SUCCESS,
  INGREDIENT_DELETE_FAIL,

  OPTION_CREATE_REQUEST,
  OPTION_CREATE_SUCCESS,
  OPTION_CREATE_FAIL,

  OPTION_UPDATE_REQUEST,
  OPTION_UPDATE_SUCCESS,
  OPTION_UPDATE_FAIL,

  OPTION_DELETE_REQUEST,
  OPTION_DELETE_SUCCESS,
  OPTION_DELETE_FAIL
} from '../constants/categoryConstants'

import { getError } from '../reduxLocalFunction'

const listCategory = (
    params = { pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }}
) => async (dispatch) =>{
  dispatch({ type: CATEGORY_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/category`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: CATEGORY_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CATEGORY_LIST_FAIL, payload: getError(error) })
  }
}

const importCategory = (CategoryData = {}) => async (dispatch) => {
  dispatch({ type: CATEGORY_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/category/import`, CategoryData)
    dispatch({ type: CATEGORY_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CATEGORY_IMPORT_FAIL, payload: getError(error) })
  }
}

const createCategory = (CategoryData = {}) => async (dispatch) => {
  dispatch({ type: CATEGORY_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/category`, [{
      CName: CategoryData.CName, 
      CIcon: CategoryData.CIcon, 
      Ingredient: CategoryData.categoryIngredient
    }])
    dispatch({ type: CATEGORY_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CATEGORY_CREATE_FAIL, payload: getError(error) })
  }
}

const updateCategory = (CategoryData = {}) => async (dispatch) => {
  dispatch({ type: CATEGORY_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/category/${CategoryData.CID}`, [{
      CName: CategoryData.CName, 
      CIcon: CategoryData.CIcon, 
      Ingredient: CategoryData.categoryIngredient
    }])
    dispatch({ type: CATEGORY_UPDATE_SUCCESS, payload: data })
    
  } catch (error) {
    dispatch({ type: CATEGORY_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusCategory = (CategoryID = "") => async (dispatch) => {
  dispatch({ type: CATEGORY_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/category/${CategoryID}/Status`)
    dispatch({ type: CATEGORY_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CATEGORY_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteCategory = (CategoryID = "") => async (dispatch) => {
  dispatch({ type: CATEGORY_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/category/${CategoryID}`)
    dispatch({ type: CATEGORY_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CATEGORY_DELETE_FAIL, payload: getError(error) })
  }
}

const deleteCategoryIngredient = (CategoryID = "") => async (dispatch) => {
  dispatch({ type: INGREDIENT_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/category/ingredient/${CategoryID}`)
    dispatch({ type: INGREDIENT_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: INGREDIENT_DELETE_FAIL, payload: getError(error) })
  }
}

const createCategoryOption = (CategoryData = {}, CID = "") => async (dispatch) => {
  dispatch({ type: OPTION_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/category/option/`, [{
      CID,
      CIID: CategoryData.CIID,
      Option: CategoryData.categoryIngredientsOption
    }])
    dispatch({ type: OPTION_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: OPTION_CREATE_FAIL, payload: getError(error) })
  }
}

const updateCategoryOption = (CategoryData = {}, CID = "") => async (dispatch) => {
  dispatch({ type: OPTION_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/category/option/${CategoryData.CIID}`, [{
      CID,
      Option: CategoryData.categoryIngredientsOption
    }])
    dispatch({ type: OPTION_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: OPTION_UPDATE_FAIL, payload: getError(error) })
  }
}

const deleteCategoryOption = (CategoryID = "") => async (dispatch) => {
  dispatch({ type: OPTION_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/category/option/${CategoryID}`)
    dispatch({ type: OPTION_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: OPTION_DELETE_FAIL, payload: getError(error) })
  }
}

export {
  listCategory,
  importCategory,
  createCategory,
  updateCategory,
  changeStatusCategory,
  deleteCategory,
  deleteCategoryIngredient,
  createCategoryOption,
  updateCategoryOption,
  deleteCategoryOption
}