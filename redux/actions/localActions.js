import axios from 'axios'

import { 
  LOCAL_SOCKET,
  LOCAL_MODAL_CREATE,
  LOCAL_MODAL_UPDATE,
  LOCAL_MODAL_UPLOAD_FILE,
  LOCAL_MODAL_CLOSE,

  LOCAL_SELECT_MANAGE,

  LOCAL_UPLOAD_FILE,
  LOCAL_UPLOAD_SHOW_UPDATE_FILE,
  LOCAL_UPLOAD_DRAG_FILE
} from '../constants/localConstants'

const socketLocal = (socket = {}) => async (dispatch) => {
  dispatch({ type: LOCAL_SOCKET, payload: socket})
}

const selectManageLocal = (select = "manage") => async (dispatch) => {
  dispatch({ type: LOCAL_SELECT_MANAGE, payload: select})
}

const modalOpenLocal = (isUpload = false, isEditing = false) => async (dispatch) => {
  if(isUpload){
    dispatch({ type: LOCAL_MODAL_UPLOAD_FILE })
  }else{
    if(isEditing){
      dispatch({ type: LOCAL_MODAL_UPDATE })
    }else{
      dispatch({ type: LOCAL_MODAL_CREATE })
    }
  }
}

const modalCloseLocal = () => async (dispatch) => {
  dispatch({ type: LOCAL_MODAL_CLOSE })
}

const uploadFileLocal = (fileList = []) => async (dispatch) => {
  dispatch({ type: LOCAL_UPLOAD_FILE, payload: fileList })
}

const uploadShowUpdateFileLocal = (PDID = "") => async (dispatch) => {
  try {
    const {data} = await axios.get(`${process.env.API_URL}/v1/product/ImageList/${PDID}`)
    const fileList = await data.map((value)=>{
      return {
        uid: value._ID,
        name: value.PDImage,
        status: 'done',
        url: `${process.env.API_URL}/v1/product/image/${value.PDImage}`,
        response: {"status": "success"},
        // response: '{"status": "success"}'
      }
    })
    dispatch({ type: LOCAL_UPLOAD_FILE, payload: fileList })
  } catch (error) {
    
  }
}

const uploadDragableFileLocal = (moveRow={}) => async (dispatch) => {
  dispatch({ type: LOCAL_UPLOAD_DRAG_FILE, payload: moveRow })
}

// const uploadFileInsertLocal = (fileList = {}) => async (dispatch) => {
//   dispatch({ type: LOCAL_UPLOAD_INSERT, payload: fileList })
// }

// const uploadFileRemoveLocal = (fileList = {}) => async (dispatch) => {
//   dispatch({ type: LOCAL_UPLOAD_REMOVE, payload: fileList })
// }

export {
  socketLocal,
  selectManageLocal,
  modalOpenLocal,
  modalCloseLocal,
  uploadFileLocal,
  uploadShowUpdateFileLocal,
  uploadDragableFileLocal
}