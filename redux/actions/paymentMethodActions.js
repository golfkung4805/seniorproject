import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'

import { 
  PAYMENT_METHOD_LIST_REQUEST,
  PAYMENT_METHOD_LIST_SUCCESS,
  PAYMENT_METHOD_LIST_FAIL,

  PAYMENT_METHOD_CHANGE_REQUEST,
  PAYMENT_METHOD_CHANGE_SUCCESS,
  PAYMENT_METHOD_CHANGE_FAIL,
} from '../constants/paymentMethodConstants'

import { getError } from '../reduxLocalFunction'

const listPaymentMethod = (
    params = { pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }}
) => async (dispatch) =>{
  dispatch({ type: PAYMENT_METHOD_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/paymentmethod`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: PAYMENT_METHOD_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PAYMENT_METHOD_LIST_FAIL, payload: getError(error) })
  }
}

const changeStatusPaymentMethod = (PayID = "") => async (dispatch) => {
  dispatch({ type: PAYMENT_METHOD_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/paymentmethod/${PayID}/Status`)
    dispatch({ type: PAYMENT_METHOD_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PAYMENT_METHOD_CHANGE_FAIL, payload: getError(error) })
  }
}

export {
  listPaymentMethod,
  changeStatusPaymentMethod,
}