import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'

import { 
  TABLE_LIST_REQUEST,
  TABLE_LIST_SUCCESS,
  TABLE_LIST_FAIL,

  TABLE_IMPORT_REQUEST,
  TABLE_IMPORT_SUCCESS,
  TABLE_IMPORT_FAIL,

  TABLE_CREATE_REQUEST,
  TABLE_CREATE_SUCCESS,
  TABLE_CREATE_FAIL,

  TABLE_UPDATE_REQUEST,
  TABLE_UPDATE_SUCCESS,
  TABLE_UPDATE_FAIL,

  TABLE_CHANGE_REQUEST,
  TABLE_CHANGE_SUCCESS,
  TABLE_CHANGE_FAIL,

  TABLE_DELETE_REQUEST,
  TABLE_DELETE_SUCCESS,
  TABLE_DELETE_FAIL
} from '../constants/tableConstants'

import { getError } from '../reduxLocalFunction'

const listTable = (
    params = { pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }}
) => async (dispatch) =>{
  dispatch({ type: TABLE_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/table`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: TABLE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_LIST_FAIL, payload: getError(error) })
  }
}

const importTable = (TableData = {}) => async (dispatch) => {
  dispatch({ type: TABLE_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/table/import`, TableData)
    dispatch({ type: TABLE_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_IMPORT_FAIL, payload: getError(error) })
  }
}

const createTable = (TableData = {}) => async (dispatch) => {
  dispatch({ type: TABLE_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/table`, [{
      TName: TableData.TableName
    }])
    dispatch({ type: TABLE_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_CREATE_FAIL, payload: getError(error) })
  }
}

const updateTable = (TableData = {}) => async (dispatch) => {
  dispatch({ type: TABLE_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/table/${TableData.TableID}`, [{
      TName: TableData.TableName
    }])
    dispatch({ type: TABLE_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusTable = (TableID = "") => async (dispatch) => {
  dispatch({ type: TABLE_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/table/${TableID}/Status`)
    dispatch({ type: TABLE_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteTable = (TableID = "") => async (dispatch) => {
  dispatch({ type: TABLE_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/table/${TableID}`)
    dispatch({ type: TABLE_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: TABLE_DELETE_FAIL, payload: getError(error) })
  }
}

export {
  listTable,
  importTable,
  createTable,
  updateTable,
  changeStatusTable,
  deleteTable
}