import axios from "axios";
import qs from 'qs'
import _ from 'lodash'

import {
  EMPLOYEE_SIGNIN_REQUEST,
  EMPLOYEE_SIGNIN_SUCCESS,
  EMPLOYEE_SIGNIN_FAIL,
  EMPLOYEE_SIGNOUT,

  EMPLOYEE_LIST_REQUEST,
  EMPLOYEE_LIST_SUCCESS,
  EMPLOYEE_LIST_FAIL,

  EMPLOYEE_IMPORT_REQUEST,
  EMPLOYEE_IMPORT_SUCCESS,
  EMPLOYEE_IMPORT_FAIL,
  
  EMPLOYEE_CREATE_REQUEST,
  EMPLOYEE_CREATE_SUCCESS,
  EMPLOYEE_CREATE_FAIL,

  EMPLOYEE_UPDATE_REQUEST,
  EMPLOYEE_UPDATE_SUCCESS,
  EMPLOYEE_UPDATE_FAIL,

  EMPLOYEE_CHANGE_REQUEST,
  EMPLOYEE_CHANGE_SUCCESS,
  EMPLOYEE_CHANGE_FAIL,

  EMPLOYEE_DELETE_REQUEST,
  EMPLOYEE_DELETE_SUCCESS,
  EMPLOYEE_DELETE_FAIL
} from '../constants/employeeConstants'

import { getError } from '../reduxLocalFunction'

const signin = (EmpData = {}) => async (dispatch) => {
  dispatch({ type: EMPLOYEE_SIGNIN_REQUEST })
  try {
    const { data } = await axios.post(`${process.env.API_URL}/v1/employee/signin`, [{
      Username: EmpData.Username,
      Passwd: EmpData.Password
    }])
    dispatch({ type: EMPLOYEE_SIGNIN_SUCCESS, payload: data })
    // localStorage.setItem('empInfo', JSON.stringify(data))
  } catch (error) {
    dispatch({ type: EMPLOYEE_SIGNIN_FAIL, payload: getError(error) })
  }
}

const signout = () => async (dispatch) => {
  // localStorage.removeItem('empInfo')
  dispatch({ type:EMPLOYEE_SIGNOUT })
}

const listEmployee = (
  params = { pagination: {
    current: 1,
    pageSize: 10,
    total: 10,
  }}
) => async (dispatch) =>{
  dispatch({ type: EMPLOYEE_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/employee`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: EMPLOYEE_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_LIST_FAIL, payload: getError(error) })
  }
}

const importEmployee = (EmpData = {}) => async (dispatch) => {
  dispatch({ type: EMPLOYEE_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/employee/import`, EmpData)
    dispatch({ type: EMPLOYEE_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_IMPORT_FAIL, payload: getError(error) })
  }
}

const createEmployee = (EmpData = {}) => async (dispatch) => {
  dispatch({ type: EMPLOYEE_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/employee`, [{
      Username: EmpData.EmpUsername, 
      Passwd: EmpData.EmpPasswd, 
      Gender: EmpData.EmpGender, 
      Firstname: EmpData.EmpFirstname, 
      Lastname: EmpData.EmpLastname, 
      Nickname: EmpData.EmpNickname,
      Tel: EmpData.EmpTel, 
      Email: EmpData.EmpEmail,
      Role: EmpData.EmpRole
    }])
    dispatch({ type: EMPLOYEE_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_CREATE_FAIL, payload: getError(error) })
  }
}

const updateEmployee = (EmpData = {}) => async (dispatch) => {
  dispatch({ type: EMPLOYEE_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/employee/${EmpData.EmpID}`, [{
      Username: EmpData.EmpUsername, 
      Passwd: _.isEmpty(EmpData.EmpPasswd)? "-": EmpData.EmpPasswd, 
      Gender: EmpData.EmpGender, 
      Firstname: EmpData.EmpFirstname, 
      Lastname: EmpData.EmpLastname, 
      Nickname: EmpData.EmpNickname,
      Tel: EmpData.EmpTel, 
      Email: EmpData.EmpEmail,
      Role: EmpData.EmpRole || '-'
    }])
    dispatch({ type: EMPLOYEE_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusEmployee = (EmpID = "") => async (dispatch) => {
  dispatch({ type: EMPLOYEE_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/employee/${EmpID}/Status`)
    dispatch({ type: EMPLOYEE_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteEmployee = (EmpID = "") => async (dispatch) => {
  dispatch({ type: EMPLOYEE_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/employee/${EmpID}`)
    dispatch({ type: EMPLOYEE_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: EMPLOYEE_DELETE_FAIL, payload: getError(error) })
  }
}

export {
  signin,
  signout,

  listEmployee,
  importEmployee,
  createEmployee,
  updateEmployee,
  changeStatusEmployee,
  deleteEmployee
}