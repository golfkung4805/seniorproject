import axios from "axios";
import qs from 'qs'
import _ from 'lodash'

import {
  VOUCHER_LIST_REQUEST,
  VOUCHER_LIST_SUCCESS,
  VOUCHER_LIST_FAIL,

  VOUCHER_IMPORT_REQUEST,
  VOUCHER_IMPORT_SUCCESS,
  VOUCHER_IMPORT_FAIL,
  
  VOUCHER_CREATE_REQUEST,
  VOUCHER_CREATE_SUCCESS,
  VOUCHER_CREATE_FAIL,

  VOUCHER_UPDATE_REQUEST,
  VOUCHER_UPDATE_SUCCESS,
  VOUCHER_UPDATE_FAIL,

  VOUCHER_CHANGE_REQUEST,
  VOUCHER_CHANGE_SUCCESS,
  VOUCHER_CHANGE_FAIL,

  VOUCHER_DELETE_REQUEST,
  VOUCHER_DELETE_SUCCESS,
  VOUCHER_DELETE_FAIL
} from '../constants/voucherConstants'

import { getError } from '../reduxLocalFunction'

const listVoucher = (
  params = { pagination: {
    current: 1,
    pageSize: 10,
    total: 10,
  }}
) => async (dispatch) =>{
  dispatch({ type: VOUCHER_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/voucher`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: VOUCHER_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_LIST_FAIL, payload: getError(error) })
  }
}

const importVoucher = (VoucherData = {}) => async (dispatch) => {
  dispatch({ type: VOUCHER_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/voucher/import`, VoucherData)
    dispatch({ type: VOUCHER_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_IMPORT_FAIL, payload: getError(error) })
  }
}

const createVoucher = (VoucherData = {}) => async (dispatch) => {
  dispatch({ type: VOUCHER_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/voucher`, [{
      VoucCode: VoucherData.VoucCode, 
      VoucName: VoucherData.VoucName, 
      VoucUsePoint: VoucherData.VoucUsePoint, 
      VoucDiscount: VoucherData.VoucDiscount, 
      VoucType: VoucherData.VoucType, 
      VoucCondition: VoucherData.VoucCondition,
      VoucStart: VoucherData.VoucRangeDate[0], 
      VoucEnd: VoucherData.VoucRangeDate[1],
      VoucQuota: VoucherData.VoucQuota
    }])
    dispatch({ type: VOUCHER_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_CREATE_FAIL, payload: getError(error) })
  }
}

const updateVoucher = (VoucherData = {}) => async (dispatch) => {
  dispatch({ type: VOUCHER_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/voucher/${VoucherData.VoucID}`, [{
      VoucCode: VoucherData.VoucCode, 
      VoucName: VoucherData.VoucName, 
      VoucUsePoint: VoucherData.VoucUsePoint, 
      VoucDiscount: VoucherData.VoucDiscount, 
      VoucType: VoucherData.VoucType, 
      VoucCondition: VoucherData.VoucCondition,
      VoucStart: VoucherData.VoucRangeDate[0].toDate(), 
      VoucEnd: VoucherData.VoucRangeDate[1].toDate(),
      VoucQuota: VoucherData.VoucQuota
    }])
    dispatch({ type: VOUCHER_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusVoucher = (VoucID = "") => async (dispatch) => {
  dispatch({ type: VOUCHER_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/voucher/${VoucID}/Status`)
    dispatch({ type: VOUCHER_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteVoucher = (VoucID = "") => async (dispatch) => {
  dispatch({ type: VOUCHER_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/voucher/${VoucID}`)
    dispatch({ type: VOUCHER_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: VOUCHER_DELETE_FAIL, payload: getError(error) })
  }
}

export {
  listVoucher,
  importVoucher,
  createVoucher,
  updateVoucher,
  changeStatusVoucher,
  deleteVoucher
}