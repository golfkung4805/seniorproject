import _ from 'lodash'
import axios from 'axios'

import {
  CART_ADD_ITEM,
  CART_MINUS_ITEM,
  CART_PLUS_ITEM,
  CART_REMOVE_ITEM,
  CART_CANCEL_REMOVE_ITEM,
  CART_ADD_FAIL,

  CART_PRODUCT_DETAIL,
  CART_PRODUCT_DETAIL_FAIL,
} from '../constants/cartConstants'

import { getError } from '../reduxLocalFunction'

const addToCart = (product) => (dispatch) => {
  try {
    dispatch({
      type: CART_ADD_ITEM,
      payload: {
        PDID: product.PDID,
        PDName: product.PDName,
        PDPrice: product.PDPrice,
        PDImage: product.PDImage,
        orderQty: product.qty,
        orderDesc: product.description,
        optionPrice: product.optionPrice,
        optionShow: product.optionShow,
        objectOptions: product.objectOptions,
        checkRemove: false
      },
    });
  } catch (error) {
    console.log(error)
    dispatch({ type: CART_ADD_FAIL, payload: getError(error) })
  }
}

const minusToCart = (productID) => async (dispatch) => {
  try {
    dispatch({type: CART_MINUS_ITEM, payload: productID});
  } catch (error) {
    dispatch({ type: CART_ADD_FAIL, payload: getError(error) })
  }
}

const plusToCart = (productID) => async (dispatch) => {
  try {
    dispatch({type: CART_PLUS_ITEM, payload: productID});
  } catch (error) {
    dispatch({ type: CART_ADD_FAIL, payload: getError(error) })
  }
}

const removeFromCart = (productID) => async (dispatch) => {
  try {
    dispatch({ type: CART_REMOVE_ITEM, payload: productID })
  } catch (error) {
    dispatch({ type: CART_ADD_FAIL, payload: getError(error) })
  }
}

const cancelRemoveFromCart = (productID) => async (dispatch) => {
  try {
    dispatch({ type: CART_CANCEL_REMOVE_ITEM, payload: productID })
  } catch (error) {
    dispatch({ type: CART_ADD_FAIL, payload: getError(error) })
  }
}

const showProductDetail = (productID) => async(dispatch) => {
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/product/${productID}`)
    dispatch({type: CART_PRODUCT_DETAIL, payload: data[0]});
  } catch (error) {
    dispatch({ type: CART_PRODUCT_DETAIL_FAIL, payload: getError(error) })
  }
}

export {
  addToCart,
  minusToCart,
  plusToCart,
  removeFromCart,
  cancelRemoveFromCart,
  
  showProductDetail
}