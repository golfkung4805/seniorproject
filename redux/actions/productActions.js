import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'

import { 
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAIL,

  PRODUCT_IMPORT_REQUEST,
  PRODUCT_IMPORT_SUCCESS,
  PRODUCT_IMPORT_FAIL,

  PRODUCT_CREATE_REQUEST,
  PRODUCT_CREATE_SUCCESS,
  PRODUCT_CREATE_FAIL,

  PRODUCT_UPDATE_REQUEST,
  PRODUCT_UPDATE_SUCCESS,
  PRODUCT_UPDATE_FAIL,

  PRODUCT_CHANGE_REQUEST,
  PRODUCT_CHANGE_SUCCESS,
  PRODUCT_CHANGE_FAIL,

  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAIL,

  PRODUCT_DELETE_IMAGE_REQUEST,
  PRODUCT_DELETE_IMAGE_SUCCESS,
  PRODUCT_DELETE_IMAGE_FAIL
} from '../constants/productConstants'

import { config, getError } from '../reduxLocalFunction'

const listProduct = (
    params = { pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }}
) => async (dispatch) =>{
  dispatch({ type: PRODUCT_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/product`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: PRODUCT_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_LIST_FAIL, payload: getError(error) })
  }
}

const importProduct = (ProductData = []) => async (dispatch) => {
  dispatch({ type: PRODUCT_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/product/import`, ProductData)
    dispatch({ type: PRODUCT_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_IMPORT_FAIL, payload: getError(error) })
  }
}

const createProduct = (formData = []) => async (dispatch) => {
  dispatch({ type: PRODUCT_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/product`, formData, config)
    dispatch({ type: PRODUCT_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_CREATE_FAIL, payload: getError(error) })
  }
}

const updateProduct = (formData = [], PDID = "") => async (dispatch) => {
  dispatch({ type: PRODUCT_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/product/${PDID}`, formData, config)
    dispatch({ type: PRODUCT_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusProduct = (PDID = "") => async (dispatch) => {
  dispatch({ type: PRODUCT_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/product/${PDID}/Status`)
    dispatch({ type: PRODUCT_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteProduct = (PDID = "") => async (dispatch) => {
  dispatch({ type: PRODUCT_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/product/${PDID}`)
    dispatch({ type: PRODUCT_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_DELETE_FAIL, payload: getError(error) })
  }
}

const deleteImageProduct = (_ID = "") => async (dispatch) => {
  dispatch({ type: PRODUCT_DELETE_IMAGE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/product/image/${_ID}`)
    dispatch({ type: PRODUCT_DELETE_IMAGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: PRODUCT_DELETE_IMAGE_FAIL, payload: getError(error) })
  }
}

export {
  listProduct,
  importProduct,
  createProduct,
  updateProduct,
  changeStatusProduct,
  deleteProduct,
  deleteImageProduct
}