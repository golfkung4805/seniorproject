import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'

import { 
  CUSTOMER_SIGNIN_REQUEST,
  CUSTOMER_SIGNIN_SUCCESS,
  CUSTOMER_SIGNIN_FAIL,
  
  CUSTOMER_LIST_REQUEST,
  CUSTOMER_LIST_SUCCESS,
  CUSTOMER_LIST_FAIL,

  CUSTOMER_LIST_ORDER_REQUEST,
  CUSTOMER_LIST_ORDER_SUCCESS,
  CUSTOMER_LIST_ORDER_FAIL,

  CUSTOMER_IMPORT_REQUEST,
  CUSTOMER_IMPORT_SUCCESS,
  CUSTOMER_IMPORT_FAIL,

  CUSTOMER_CREATE_REQUEST,
  CUSTOMER_CREATE_SUCCESS,
  CUSTOMER_CREATE_FAIL,

  CUSTOMER_UPDATE_REQUEST,
  CUSTOMER_UPDATE_SUCCESS,
  CUSTOMER_UPDATE_FAIL,

  CUSTOMER_CHANGE_REQUEST,
  CUSTOMER_CHANGE_SUCCESS,
  CUSTOMER_CHANGE_FAIL,

  CUSTOMER_DELETE_REQUEST,
  CUSTOMER_DELETE_SUCCESS,
  CUSTOMER_DELETE_FAIL
} from '../constants/customerConstants'

import { getError } from '../reduxLocalFunction'

const signIn = (CusData = {}) => async (dispatch) => {
  dispatch({ type: CUSTOMER_SIGNIN_REQUEST })
  try {
    const { data } = await axios.post(`${process.env.API_URL}/v1/customer/signin`, {
      Username: CusData.username,
      Passwd: CusData.password
    })
    dispatch({ type: CUSTOMER_SIGNIN_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_SIGNIN_FAIL, payload: getError(error) })
  }
}

const signOut = () => async (dispatch) => {
  dispatch({ type:CUSTOMER_SIGNOUT })
}

const listCustomer = (
    params = { pagination: {
      current: 1,
      pageSize: 10,
      total: 10,
    }}
) => async (dispatch) =>{
  dispatch({ type: CUSTOMER_LIST_REQUEST})
  try {
    const { data } = await axios.get(`${process.env.API_URL}/v1/customer`, { params, paramsSerializer: params => qs.stringify(params) })
    dispatch({ type: CUSTOMER_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_LIST_FAIL, payload: getError(error) })
  }
}

const listOrderCustomer = (CusID = "") => async (dispatch) =>{
dispatch({ type: CUSTOMER_LIST_ORDER_REQUEST})
try {
  const { data } = await axios.get(`${process.env.API_URL}/v1/customer/${CusID}/order`)
  dispatch({ type: CUSTOMER_LIST_ORDER_SUCCESS, payload: data })
} catch (error) {
  dispatch({ type: CUSTOMER_LIST_ORDER_FAIL, payload: getError(error) })
}
}

const importCustomer = (CusData = {}) => async (dispatch) => {
  dispatch({ type: CUSTOMER_IMPORT_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/customer/import`, CusData)
    dispatch({ type: CUSTOMER_IMPORT_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_IMPORT_FAIL, payload: getError(error) })
  }
}

const createCustomer = (CusData = {}) => async (dispatch) => {
  dispatch({ type: CUSTOMER_CREATE_REQUEST})
  try {
    const {data} = await axios.post(`${process.env.API_URL}/v1/customer`, [{
      Username: CusData.CusUsername, 
      Passwd: CusData.CusPasswd, 
      Gender: CusData.CusGender, 
      Firstname: CusData.CusFirstname, 
      Lastname: CusData.CusLastname, 
      Nickname: CusData.CusNickname,
      Tel: CusData.CusTel, 
      Email: CusData.CusEmail
    }])
    dispatch({ type: CUSTOMER_CREATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_CREATE_FAIL, payload: getError(error) })
  }
}

const updateCustomer = (CusData = {}) => async (dispatch) => {
  dispatch({ type: CUSTOMER_UPDATE_REQUEST})
  try {
    const {data} = await axios.put(`${process.env.API_URL}/v1/customer/${CusData.CusID}`, [{
      Username: CusData.CusUsername, 
      Passwd: _.isEmpty(CusData.CusPasswd)? "-": CusData.CusPasswd, 
      Gender: CusData.CusGender, 
      Firstname: CusData.CusFirstname, 
      Lastname: CusData.CusLastname, 
      Nickname: CusData.CusNickname,
      Tel: CusData.CusTel, 
      Email: CusData.CusEmail
    }])
    dispatch({ type: CUSTOMER_UPDATE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_UPDATE_FAIL, payload: getError(error) })
  }
}

const changeStatusCustomer = (CusID = "") => async (dispatch) => {
  dispatch({ type: CUSTOMER_CHANGE_REQUEST})
  try {
    const { data } = await axios.put(`${process.env.API_URL}/v1/customer/${CusID}/Status`)
    dispatch({ type: CUSTOMER_CHANGE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_CHANGE_FAIL, payload: getError(error) })
  }
}

const deleteCustomer = (CusID = "") => async (dispatch) => {
  dispatch({ type: CUSTOMER_DELETE_REQUEST})
  try {
    const { data } = await axios.delete(`${process.env.API_URL}/v1/customer/${CusID}`)
    dispatch({ type: CUSTOMER_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: CUSTOMER_DELETE_FAIL, payload: getError(error) })
  }
}

export {
  signIn,
  signOut,

  listCustomer,
  listOrderCustomer,
  importCustomer,
  createCustomer,
  updateCustomer,
  changeStatusCustomer,
  deleteCustomer
}