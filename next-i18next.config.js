module.exports = {
  i18n: {
    defaultNS: 'common',
    defaultLocale: 'th',
    locales: ['th', 'en'],
    localeDetection: false,
  },
};